const functions = require('firebase-functions');
const admin = require('firebase-admin');
const crypto = require('crypto');
const moment = require('moment');
var mailgun = require("mailgun-js");
var api_key = 'key-afd4e783b2fabeb21ace2cbe8838aed6';
var DOMAIN = 'www.teckdk.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});
admin.initializeApp(functions.config().firebase);

//push notification customer
exports.sendPushNotificationsCustomer = functions.database.ref('/payments/{shopId}/{paymentID}').onWrite((change, context) => {
	const post = change.after.val();
	console.log('data was gotten');
	if(post.notificationSent == 'YES'){
		console.log('notification was prevented');
		return;
	}
	console.log('on write captures ' + context.params.shopId);

	post.notificationSent = 'YES';
	const uniqueID = post.customerUUID;
	console.log(uniqueID);

	const payload = {
      notification: {
        title: 'Booking Status!',
        body: 'Your booking was successfully completed.',
        badge: '1',
        sound: 'default'
      }
    };
    return admin.database().ref('fcmtoken/'+uniqueID).once('value').then(allToken => {
    	console.log('call was made');
    	if (allToken.val()){
    		console.log('call was made ' + allToken.val());
			const token = allToken.val();
			return admin.messaging().sendToDevice(token.key, payload).then(response => {
				console.log('notification sucessfully sent');
				return change.after.ref.set(post);

			});
    	}
    });
});

//push notification barberShop
exports.sendPushNotificationsBarberShop = functions.database.ref('/payments/{shopId}/{paymentID}').onWrite((change, context) => {
	const post = change.after.val();
	console.log('data was gotten');
	if(post.notificationSent == 'YES'){
		console.log('notification was prevented');
		return;
	}
	console.log('on write captures ' + context.params.shopId);

	post.notificationSent = 'YES';
	const customerUniqueID = post.customerUUID; 
	const uniqueID = post.barberShopUUIDD;
	console.log(uniqueID);

    return admin.database().ref('users/' + customerUniqueID).once('value').then(dataToken => {
    	console.log('customer data was gotten');
    	if(dataToken.val()) {
    		console.log('call made sucessfully');
    		const userMainName = dataToken.val();

			const payload = {
		      notification: {
		        title: 'New Booking!',
		        body: 'Your were just booked by ' + userMainName.firstName,
		        badge: '1',
		        sound: 'default'
		      }
		    };


		    return admin.database().ref('fcmtoken/'+ uniqueID).once('value').then(allToken => {
		    	console.log('call was made');
		    	if (allToken.val()){
		    		console.log('call was made ' + allToken.val());
					const token = allToken.val();
					return admin.messaging().sendToDevice(token.key, payload).then(response => {
						console.log('notification sucessfully sent');
						return change.after.ref.set(post);

					});
		    	}
		    });
    	}
    });
});

//send push notification customer booking completed
exports.sendPushNotificationsBarberShopBookingCompleted = functions.database.ref('/bookingCompleted/{bookingId}').onWrite((change, context) => {
	const post = change.after.val();
	console.log('data was gotten booking completed');
	if(post.notificationSent == 'YES'){
		console.log('notification was prevented booking completed');
		return;
	}
	console.log('on write captures ' + context.params.bookingId);

	post.notificationSent = 'YES';
	const uniqueID = post.customerID;
	console.log(uniqueID);

	const payload = {
      notification: {
        title: 'Booking Completed!',
        body: 'Were you satisfied by the service rendered? Please give a feedback',
        badge: '1',
        sound: 'default'
      }
    };
    return admin.database().ref('fcmtoken/'+uniqueID).once('value').then(allToken => {
    	console.log('call was made booking completed');
    	if (allToken.val()){
    		console.log('call was made booking completed' + allToken.val());
			const token = allToken.val();
			return admin.messaging().sendToDevice(token.key, payload).then(response => {
				console.log('notification sucessfully sent booking completed');
				return change.after.ref.set(post);

			});
    	}
    });
});



// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//handle authorize http request
exports.quickPayStayShapPaymentHandle = functions.https.onRequest((req, res) => {
	if (req.method === 'POST') {
		res.send(200).end();
		console.log('post method was caught');
		const specialPayID = res.req.body.id;
		const specialDateCreated = res.req.body.created_at;
		const barberID = res.req.body.variables.barberID;
		const barberShopID = res.req.body.variables.barberShopID;
		const bookingID = res.req.body.variables.bookingUUID;
		const customerID = res.req.body.variables.customerUUID;
		const serviceID = res.req.body.basket[0].item_no;
		const formattedDate = res.req.body.created_at;
		const userTimeStamp = res.req.body.variables.dateCreated;
		const userCalendar = res.req.body.variables.calendar;
		const userTimezone = res.req.body.variables.timezone;
		const userLocale = res.req.body.variables.local;
		const totalPrice = res.req.body.basket[0].item_price;
		const checkSumValue = res.req.headers['quickpay-checksum-sha256'];
		const bodyData = res.req.body;
		const operationsArray = res.req.body.operations;
		var methodOfpaymentData = res.req.body.facilitator;

		if (specialPayID == null || specialDateCreated == null || barberID == null || barberShopID == null || bookingID == null || serviceID == null || formattedDate == null || userTimeStamp == null || userCalendar == null || userTimezone == null || userLocale == null || totalPrice == null || customerID == null || checkSumValue == null || bodyData == null || operationsArray == null){
			console.log('error - null values detected');
		} else {
			console.log('data sucessfully came through');

			if (methodOfpaymentData == null) {
				methodOfpaymentData = 'card';
			}

		  	var bodyString = JSON.stringify(bodyData);
		  	var calculateChecksum = sign(bodyString, '1d1ae8b451e70bee8813ea1925baa181d9c94d3764c0ee296621f56086173f69');

		  	if (checkSumValue == calculateChecksum) {
		  			console.log('check eval worked well');
				 	var x;
				 	for (x in operationsArray) {
				 		if (operationsArray[x].type == "authorize" && operationsArray[x].pending == false && operationsArray[x].qp_status_msg == "Approved" && operationsArray[x].aq_status_msg == "Approved") {
				 			console.log('Data was posted well');
					 		var operationType = operationsArray[x].type;
					 		var operationqpstatus = operationsArray[x].qp_status_msg;
					 		var operationapstatus = operationsArray[x].aq_status_msg;
					 		var operationDateCreated = operationsArray[x].created_at;
					 		const dataHoldForPost = {paymentID: specialPayID.toString(), 
					 			barberUUIDD: barberID, 
					 			barberShopUUIDD: barberShopID, 
					 			serviceUUIDD: serviceID, 
					 			bookingUUIDD: bookingID, 
					 			customerUUID: customerID, 
					 			methodOfPayment: methodOfpaymentData, 
					 			formattedDateString: formattedDate, 
					 			dateCreated: specialDateCreated, 
					 			appDateCreated: userTimeStamp, 
					 			appTimezone: userTimezone, 
					 			appCalendar: userCalendar, 
					 			appLocale: userLocale, 
					 			priceTotal: totalPrice.toString(), 
					 			payment_qp_status_msg: operationqpstatus, 
					 			payment_aq_status_msg: operationapstatus, 
					 			paymentTypeOperation: operationType, 
					 			paymentTypeCaptureOperation: 'not available', 
					 			captureAppDate: 'not available', 
					 			captureAppTimeZone: 'not available', 
					 			captureAppCalendar: 'not available', 
					 			captureAppLocal: 'not available', 
					 			paymentCaptureDate: 'not available', 
					 			capture_qp_status_msg: 'not available', 
					 			capture_aq_status_msg: 'not available', 
					 			paymentTypeCancelOperation: 'not available', 
					 			cancelAppDate: 'not available', 
					 			cancelAppTimeZone: 'not available', 
					 			cancelAppCalendar: 'not available', 
					 			cancelAppLocal: 'not available', 
					 			paymentCancelDate: 'not available', 
					 			cancel_qp_status_msg: 'not available', 
					 			cancel_aq_status_msg: 'not available', 
					 			paymentTypeRefundOperation: 'not available', 
					 			refundAppDate: 'not available', 
					 			refundAppTimeZone: 'not available', 
					 			refundAppCalendar: 'not available', 
					 			refundAppLocal: 'not available', 
					 			paymentRefundDate: 'not available', 
					 			refund_qp_status_msg: 'not available', 
					 			refund_aq_status_msg: 'not available',
					 			amountRefunded: 'not available', 
					 			notificationSent: 'NO'};

					 		return admin.database().ref().child('payments').child(barberShopID).child(specialPayID).set(dataHoldForPost);

				 		}
				 	}
		  	} else {
		  		console.log('validation failed payment');
		  	}
		}


	}
 res.send("Hello from Firebase Joseph! ");
});

//edit booking as payment has been made to ensure completion
exports.editBookingsNodeToShowCompletionAfterPaymentMade = functions.database.ref('/payments/{shopId}/{paymentID}').onCreate((snapshot, context) => {
	const post = snapshot.val();
	console.log('data was gotten');
	console.log('on create shop details for completion ' + context.params.shopId);

	const uniqueID = post.barberShopUUIDD;
	const bookingIDD = post.bookingUUIDD;
	console.log(uniqueID);
	const valueHolder = 'YES'
	const bookingStat = 'booked'
	const decision = {isCompleted: valueHolder, bookingStatus: bookingStat};
	console.log('data was gotten now about write data');

    return admin.database().ref().child('bookings').child(uniqueID).child(bookingIDD).update(decision);
});





//handle capture http request
exports.quickPayStayShapCaptureHandle = functions.https.onRequest((req, res) => {
	if (req.method === 'POST') {
		res.send(200).end();
		console.log('post method for capture was caught');
		const captureSpecialPayID = res.req.body.id;
		const captureBarberShopID = res.req.body.variables.barberShopID;
		const captureCheckSumValue = res.req.headers['quickpay-checksum-sha256'];
		const captureBodyData = res.req.body;
		const captureOperationsArray = res.req.body.operations;

		if (captureSpecialPayID == null || captureCheckSumValue == null || captureBodyData == null || captureOperationsArray == null){
			console.log('error - null values detected capture');
		} else {
			console.log('data sucessfully came through capture');
		  	var bodyString = JSON.stringify(captureBodyData);
		  	var calculateChecksum = sign(bodyString, '1d1ae8b451e70bee8813ea1925baa181d9c94d3764c0ee296621f56086173f69');
		  	if (captureCheckSumValue == calculateChecksum) {
		  			console.log('check eval worked well capture');
				 	var x;
				 	for (x in captureOperationsArray) {
				 		if (captureOperationsArray[x].type == "capture" && captureOperationsArray[x].pending == false && captureOperationsArray[x].qp_status_msg == "Approved" && captureOperationsArray[x].aq_status_msg == "Approved") {
				 			console.log('Data was posted well');
					 		var captureOperationType = captureOperationsArray[x].type;
					 		var captureOperationqpstatus = captureOperationsArray[x].qp_status_msg;
					 		var captureOperationapstatus = captureOperationsArray[x].aq_status_msg;
					 		var captureOperationDateCreated = captureOperationsArray[x].created_at;
					 		const dataHoldCaptureForPost = {paymentTypeCaptureOperation: captureOperationType, paymentCaptureDate: captureOperationDateCreated, capture_qp_status_msg: captureOperationqpstatus, capture_aq_status_msg: captureOperationapstatus};
					 		return admin.database().ref().child('payments').child(captureBarberShopID).child(captureSpecialPayID).update(dataHoldCaptureForPost);
				 		}
				 	}
		  	} else {
		  		console.log('validation failed capture');
		  	}
		}


	}
 res.send("Hello from Firebase capture Joseph! ");
});


//handle cancel transaction
exports.quickPayStayShapCancelHandle = functions.https.onRequest((req, res) => {
	if (req.method === 'POST') {
		res.send(200).end();
		console.log('post method for cancel was caught');
		const cancelSpecialPayID = res.req.body.id;
		const cancelBarberShopID = res.req.body.variables.barberShopID;
		const cancelCheckSumValue = res.req.headers['quickpay-checksum-sha256'];
		const cancelBodyData = res.req.body;
		const cancelOperationsArray = res.req.body.operations;

		if (cancelSpecialPayID == null || cancelCheckSumValue == null || cancelBodyData == null || cancelOperationsArray == null){
			console.log('error - null values detected cancel');
		} else {
			console.log('data sucessfully came through cancel');
		  	var bodyStringCancel = JSON.stringify(cancelBodyData);
		  	var calculateChecksumCancel = sign(bodyStringCancel, '1d1ae8b451e70bee8813ea1925baa181d9c94d3764c0ee296621f56086173f69');
		  	if (cancelCheckSumValue == calculateChecksumCancel) {
		  			console.log('check eval worked well cancel');
				 	var cac;
				 	for (cac in cancelOperationsArray) {
				 		if (cancelOperationsArray[cac].type == "cancel" && cancelOperationsArray[cac].pending == false && cancelOperationsArray[cac].qp_status_msg == "Approved" && cancelOperationsArray[cac].aq_status_msg == "Approved") {
				 			console.log('Data was posted well');
					 		var cancelOperationType = cancelOperationsArray[cac].type;
					 		var cancelOperationqpstatus = cancelOperationsArray[cac].qp_status_msg;
					 		var cancelOperationapstatus = cancelOperationsArray[cac].aq_status_msg;
					 		var cancelOperationDateCreated = cancelOperationsArray[cac].created_at;
					 		const dataHoldCancelForPost = {paymentTypeCancelOperation: cancelOperationType, paymentCancelDate: cancelOperationDateCreated, cancel_qp_status_msg: cancelOperationqpstatus, cancel_aq_status_msg: cancelOperationapstatus};
					 		return admin.database().ref().child('payments').child(cancelBarberShopID).child(cancelSpecialPayID).update(dataHoldCancelForPost);
				 		}
				 	}
		  	} else {
		  		console.log('validation failed cancel');
		  	}
		}


	}
 res.send("Hello from Firebase cancel Joseph! ");
});

//handle refund operation
exports.quickPayStayShapRefundHandle = functions.https.onRequest((req, res) => {
	if (req.method === 'POST') {
		res.send(200).end();
		console.log('post method for refund was caught');
		const refundSpecialPayID = res.req.body.id;
		const refundBarberShopID = res.req.body.variables.barberShopID;
		const refundCheckSumValue = res.req.headers['quickpay-checksum-sha256'];
		const refundBodyData = res.req.body;
		const refundOperationsArray = res.req.body.operations;

		if (refundSpecialPayID == null || refundCheckSumValue == null || refundBodyData == null || refundOperationsArray == null){
			console.log('error - null values detected refund');
		} else {
			console.log('data sucessfully came through refund');
		  	var bodyStringRefund = JSON.stringify(refundBodyData);
		  	var calculateChecksumRefund = sign(bodyStringRefund, '1d1ae8b451e70bee8813ea1925baa181d9c94d3764c0ee296621f56086173f69');
		  	if (refundCheckSumValue == calculateChecksumRefund) {
		  			console.log('check eval worked well refund');
				 	var refu;
				 	for (refu in refundOperationsArray) {
				 		if (refundOperationsArray[refu].type == "refund" && refundOperationsArray[refu].pending == false && refundOperationsArray[refu].qp_status_msg == "Approved" && refundOperationsArray[refu].aq_status_msg == "Approved") {
				 			console.log('Data was posted well');
					 		var refundOperationType = refundOperationsArray[refu].type;
					 		var refundOperationqpstatus = refundOperationsArray[refu].qp_status_msg;
					 		var refundOperationapstatus = refundOperationsArray[refu].aq_status_msg;
					 		var refundOperationDateCreated = refundOperationsArray[refu].created_at;
					 		var refundOperationAmount = refundOperationsArray[refu].amount;
					 		var correctRefundAmount = parseInt(refundOperationAmount);
					 		var correctAmountToPost = correctRefundAmount * 0.01;
					 		const dataHoldRefundForPost = {paymentTypeRefundOperation: refundOperationType, paymentRefundDate: refundOperationDateCreated, refund_qp_status_msg: refundOperationqpstatus, refund_aq_status_msg: refundOperationapstatus, amountRefunded: correctAmountToPost.toString()};
					 		return admin.database().ref().child('payments').child(refundBarberShopID).child(refundSpecialPayID).update(dataHoldRefundForPost);
				 		}
				 	}
		  	} else {
		  		console.log('validation failed refund');
		  	}
		}


	}
 res.send("Hello from Firebase cancel Joseph! ");
});

//handle send email for payout sent
exports.sendEmailForPayOutTransaction = functions.database.ref('/payouts/{shopId}/{payoutID}').onWrite((change, context) => {
	const post = change.after.val();
	console.log('data was gotten');
	if(post.emailSent == 'YES'){
		console.log('notification was prevented');
		return;
	}
	console.log('email sent to ' + context.params.shopId);

	post.emailSent = 'YES';
	const uniqueID = post.barberShopID;
	const regNumber = post.regNR;
	const kontorNumber = post.kontorNr
	const amount = post.amountToPayOut;
	const dateCreated = post.datePayoutCreated;
	console.log(uniqueID);
	
	return admin.database().ref('users/' + uniqueID).once('value').then(dataToken => {
		if(dataToken.val()) {
    		console.log('user data gotten');
    		const userMainName = dataToken.val();

			var data = {
			  from: 'Stay Sharp App <postmaster@www.teckdk.com>',
			  to: 'info@teckdk.com',
			  subject: 'Payout details for customer',
			  html: '<p><strong>Company name</strong>: </p>'+ userMainName.companyName + '<p><strong>Company ID</strong>: </p>' + uniqueID + '<p><strong>Reg number</strong>: </p>' + regNumber + '<p><strong>Kontor number</strong>: </p>' + kontorNumber + '<p><strong>Amount</strong>: </p>' + amount + '<p><strong>Date created</strong>: </p>' + dateCreated
			};

			mailgun.messages().send(data, function (error, body) {
				if(error){
					console.log('there was an error');
					console.log(error);
				} else {
					console.log("message sent sucessfully");
					return change.after.ref.set(post);
				}
			});


    	}

	})


});

//send email for BarberStaff password
exports.sendEmailForNewBarberRegistered = functions.database.ref('/users/{Id}').onWrite((change, context) => {
	const post = change.after.val();
	console.log('data was gotten');
	if(post.emailSent == 'YES'){
		console.log('notification was prevented');
		return;
	}
	console.log('email sent to ' + context.params.Id);
	const userRole = post.role;

	if (userRole == 'barberStaff') {
		post.emailSent = 'YES';
		const userEmail = post.email
		const userLastName = post.lastName
		var data = {
		  from: 'Stay Sharp App <postmaster@www.teckdk.com>',
		  to: userEmail,
		  subject: 'Password for authentication',
		  html: '<p>Hi </p>' + userLastName + '<p>You have been registered as a Barber. Your password is listed below</p>' + '<p><strong>Password:</strong> theCuttingAppPassKey123</p>' + '<p>Thanks</p>' + '<p>Stay Sharp Team</p>'
		};

		mailgun.messages().send(data, function (error, body) {
			if(error){
				console.log('there was an error');
				console.log(error);
			} else {
				console.log("message sent sucessfully");
				return change.after.ref.set(post);
			}
		});
	} else {
		return;
	}


});

//delete wrong data
exports.deleteBookingWithInvalidLocal = functions.database.ref('/bookings/{shopId}/{bookingID}').onCreate((snapshot, context) => {
	console.log('data created');
	const localArrayHolder = [ "af",
							 "af_NA",
							 "af_ZA",
							 "agq",
							 "agq_CM",
							 "ak",
							 "ak_GH",
							 "sq",
							 "sq_AL",
							 "sq_XK",
							 "sq_MK",
							 "am",
							 "am_ET",
							 "ar",
							 "ar_DZ",
							 "ar_BH",
							 "ar_TD",
							 "ar_KM",
							 "ar_DJ",
							 "ar_EG",
							 "ar_ER",
							 "ar_IQ",
							 "ar_IL",
							 "ar_JO",
							 "ar_KW",
							 "ar_LB",
							 "ar_LY",
							 "ar_MR",
							 "ar_MA",
							 "ar_OM",
							 "ar_PS",
							 "ar_QA",
							 "ar_SA",
							 "ar_SO",
							 "ar_SS",
							 "ar_SD",
							 "ar_SY",
							 "ar_TN",
							 "ar_AE",
							 "ar_EH",
							 "ar_001",
							 "ar_YE",
							 "hy",
							 "hy_AM",
							 "as",
							 "as_IN",
							 "asa",
							 "asa_TZ",
							 "az_Latn",
							 "az_Latn_AZ",
							 "az_Cyrl",
							 "az_Cyrl_AZ",
							 "ksf",
							 "ksf_CM",
							 "bm_Latn",
							 "bm_Latn_ML",
							 "bas",
							 "bas_CM",
							 "eu",
							 "eu_ES",
							 "be",
							 "be_BY",
							 "bem",
							 "bem_ZM",
							 "bez",
							 "bez_TZ",
							 "bn",
							 "bn_BD",
							 "bn_IN",
							 "brx",
							 "brx_IN",
							 "bs_Latn",
							 "bs_Latn_BA",
							 "bs_Cyrl",
							 "bs_Cyrl_BA",
							 "br",
							 "br_FR",
							 "bg",
							 "bg_BG",
							 "my",
							 "my_MM",
							 "ca",
							 "ca_AD",
							 "ca_FR",
							 "ca_IT",
							 "ca_ES",
							 "tzm_Latn",
							 "tzm_Latn_MA",
							 "ckb",
							 "ckb_IR",
							 "ckb_IQ",
							 "chr",
							 "chr_US",
							 "cgg",
							 "cgg_UG",
							 "zh",
							 "zh_Hans_CN",
							 "zh_Hant_HK",
							 "zh_Hant_MO",
							 "zh_Hans",
							 "zh_Hans_HK",
							 "zh_Hans_MO",
							 "zh_Hans_SG",
							 "zh_Hant_TW",
							 "zh_Hant",
							 "ksh",
							 "ksh_DE",
							 "kw",
							 "kw_GB",
							 "hr",
							 "hr_BA",
							 "hr_HR",
							 "cs",
							 "cs_CZ",
							 "da",
							 "da_DK",
							 "da_GL",
							 "dua",
							 "dua_CM",
							 "nl",
							 "nl_AW",
							 "nl_BE",
							 "nl_BQ",
							 "nl_CW",
							 "nl_NL",
							 "nl_SX",
							 "nl_SR",
							 "dz",
							 "dz_BT",
							 "ebu",
							 "ebu_KE",
							 "en",
							 "en_AL",
							 "en_AS",
							 "en_AD",
							 "en_AI",
							 "en_AG",
							 "en_AU",
							 "en_AT",
							 "en_BS",
							 "en_BB",
							 "en_BE",
							 "en_BZ",
							 "en_BM",
							 "en_BA",
							 "en_BW",
							 "en_IO",
							 "en_VG",
							 "en_CM",
							 "en_CA",
							 "en_KY",
							 "en_CX",
							 "en_CC",
							 "en_CK",
							 "en_HR",
							 "en_CY",
							 "en_CZ",
							 "en_DK",
							 "en_DG",
							 "en_DM",
							 "en_ER",
							 "en_EE",
							 "en_150",
							 "en_FK",
							 "en_FJ",
							 "en_FI",
							 "en_FR",
							 "en_GM",
							 "en_DE",
							 "en_GH",
							 "en_GI",
							 "en_GR",
							 "en_GD",
							 "en_GU",
							 "en_GG",
							 "en_GY",
							 "en_HK",
							 "en_HU",
							 "en_IS",
							 "en_IN",
							 "en_IE",
							 "en_IM",
							 "en_IL",
							 "en_IT",
							 "en_JM",
							 "en_JE",
							 "en_KE",
							 "en_KI",
							 "en_LV",
							 "en_LS",
							 "en_LR",
							 "en_LT",
							 "en_LU",
							 "en_MO",
							 "en_MG",
							 "en_MW",
							 "en_MY",
							 "en_MT",
							 "en_MH",
							 "en_MU",
							 "en_FM",
							 "en_ME",
							 "en_MS",
							 "en_NA",
							 "en_NR",
							 "en_NL",
							 "en_NZ",
							 "en_NG",
							 "en_NU",
							 "en_NF",
							 "en_MP",
							 "en_NO",
							 "en_PK",
							 "en_PW",
							 "en_PG",
							 "en_PH",
							 "en_PN",
							 "en_PL",
							 "en_PT",
							 "en_PR",
							 "en_RO",
							 "en_RU",
							 "en_RW",
							 "en_WS",
							 "en_SC",
							 "en_SL",
							 "en_SG",
							 "en_SX",
							 "en_SK",
							 "en_SI",
							 "en_SB",
							 "en_ZA",
							 "en_SS",
							 "en_ES",
							 "en_SH",
							 "en_KN",
							 "en_LC",
							 "en_VC",
							 "en_SD",
							 "en_SZ",
							 "en_SE",
							 "en_CH",
							 "en_TZ",
							 "en_TK",
							 "en_TO",
							 "en_TT",
							 "en_TR",
							 "en_TC",
							 "en_TV",
							 "en_UM",
							 "en_VI",
							 "en_UG",
							 "en_GB",
							 "en_US",
							 "en_US_POSIX",
							 "en_VU",
							 "en_001",
							 "en_ZM",
							 "en_ZW",
							 "eo",
							 "et",
							 "et_EE",
							 "ee",
							 "ee_GH",
							 "ee_TG",
							 "ewo",
							 "ewo_CM",
							 "fo",
							 "fo_FO",
							 "fil",
							 "fil_PH",
							 "fi",
							 "fi_FI",
							 "fr",
							 "fr_DZ",
							 "fr_BE",
							 "fr_BJ",
							 "fr_BF",
							 "fr_BI",
							 "fr_CM",
							 "fr_CA",
							 "fr_CF",
							 "fr_TD",
							 "fr_KM",
							 "fr_CG",
							 "fr_CD",
							 "fr_CI",
							 "fr_DJ",
							 "fr_GQ",
							 "fr_FR",
							 "fr_GF",
							 "fr_PF",
							 "fr_GA",
							 "fr_GP",
							 "fr_GN",
							 "fr_HT",
							 "fr_LU",
							 "fr_MG",
							 "fr_ML",
							 "fr_MQ",
							 "fr_MR",
							 "fr_MU",
							 "fr_YT",
							 "fr_MC",
							 "fr_MA",
							 "fr_NC",
							 "fr_NE",
							 "fr_RE",
							 "fr_RW",
							 "fr_SN",
							 "fr_SC",
							 "fr_BL",
							 "fr_MF",
							 "fr_PM",
							 "fr_CH",
							 "fr_SY",
							 "fr_TG",
							 "fr_TN",
							 "fr_VU",
							 "fr_WF",
							 "fur",
							 "fur_IT",
							 "ff",
							 "ff_CM",
							 "ff_GN",
							 "ff_MR",
							 "ff_SN",
							 "gl",
							 "gl_ES",
							 "lg",
							 "lg_UG",
							 "ka",
							 "ka_GE",
							 "de",
							 "de_AT",
							 "de_BE",
							 "de_DE",
							 "de_LI",
							 "de_LU",
							 "de_CH",
							 "el",
							 "el_CY",
							 "el_GR",
							 "gu",
							 "gu_IN",
							 "guz",
							 "guz_KE",
							 "ha_Latn",
							 "ha_Latn_GH",
							 "ha_Latn_NE",
							 "ha_Latn_NG",
							 "haw",
							 "haw_US",
							 "he",
							 "he_IL",
							 "hi",
							 "hi_IN",
							 "hu",
							 "hu_HU",
							 "is",
							 "is_IS",
							 "ig",
							 "ig_NG",
							 "smn",
							 "smn_FI",
							 "id",
							 "id_ID",
							 "iu",
							 "iu_Cans",
							 "iu_Cans_CA",
							 "ga",
							 "ga_IE",
							 "it",
							 "it_IT",
							 "it_SM",
							 "it_CH",
							 "ja",
							 "ja_JP",
							 "dyo",
							 "dyo_SN",
							 "kea",
							 "kea_CV",
							 "kab",
							 "kab_DZ",
							 "kkj",
							 "kkj_CM",
							 "kl",
							 "kl_GL",
							 "kln",
							 "kln_KE",
							 "kam",
							 "kam_KE",
							 "kn",
							 "kn_IN",
							 "ks",
							 "ks_Arab",
							 "ks_Arab_IN",
							 "kk_Cyrl",
							 "kk_Cyrl_KZ",
							 "km",
							 "km_KH",
							 "ki",
							 "ki_KE",
							 "rw",
							 "rw_RW",
							 "kok",
							 "kok_IN",
							 "ko",
							 "ko_KP",
							 "ko_KR",
							 "khq",
							 "khq_ML",
							 "ses",
							 "ses_ML",
							 "nmg",
							 "nmg_CM",
							 "ky_Cyrl",
							 "ky_Cyrl_KG",
							 "lkt",
							 "lkt_US",
							 "lag",
							 "lag_TZ",
							 "lo",
							 "lo_LA",
							 "lv",
							 "lv_LV",
							 "ln",
							 "ln_AO",
							 "ln_CF",
							 "ln_CG",
							 "ln_CD",
							 "lt",
							 "lt_LT",
							 "dsb",
							 "dsb_DE",
							 "lu",
							 "lu_CD",
							 "luo",
							 "luo_KE",
							 "lb",
							 "lb_LU",
							 "luy",
							 "luy_KE",
							 "mk",
							 "mk_MK",
							 "jmc",
							 "jmc_TZ",
							 "mgh",
							 "mgh_MZ",
							 "kde",
							 "kde_TZ",
							 "mg",
							 "mg_MG",
							 "ms_Latn",
							 "ms_Arab",
							 "ms_Arab_BN",
							 "ms_Arab_MY",
							 "ms_Latn_BN",
							 "ms_Latn_MY",
							 "ms_Latn_SG",
							 "ml",
							 "ml_IN",
							 "mt",
							 "mt_MT",
							 "gv",
							 "gv_IM",
							 "mr",
							 "mr_IN",
							 "mas",
							 "mas_KE",
							 "mas_TZ",
							 "mer",
							 "mer_KE",
							 "mgo",
							 "mgo_CM",
							 "mn_Cyrl",
							 "mn_Cyrl_MN",
							 "mfe",
							 "mfe_MU",
							 "mua",
							 "mua_CM",
							 "naq",
							 "naq_NA",
							 "ne",
							 "ne_IN",
							 "ne_NP",
							 "nnh",
							 "nnh_CM",
							 "jgo",
							 "jgo_CM",
							 "nd",
							 "nd_ZW",
							 "se",
							 "se_FI",
							 "se_NO",
							 "se_SE",
							 "nb",
							 "nb_NO",
							 "nb_SJ",
							 "nn",
							 "nn_NO",
							 "nus",
							 "nus_SD",
							 "nyn",
							 "nyn_UG",
							 "or",
							 "or_IN",
							 "om",
							 "om_ET",
							 "om_KE",
							 "os",
							 "os_GE",
							 "os_RU",
							 "ps",
							 "ps_AF",
							 "fa",
							 "fa_AF",
							 "fa_IR",
							 "pl",
							 "pl_PL",
							 "pt",
							 "pt_AO",
							 "pt_BR",
							 "pt_CV",
							 "pt_GW",
							 "pt_MO",
							 "pt_MZ",
							 "pt_PT",
							 "pt_ST",
							 "pt_TL",
							 "pa_Guru",
							 "pa_Arab",
							 "pa_Arab_PK",
							 "pa_Guru_IN",
							 "qu",
							 "qu_BO",
							 "qu_EC",
							 "qu_PE",
							 "ro",
							 "ro_MD",
							 "ro_RO",
							 "rm",
							 "rm_CH",
							 "rof",
							 "rof_TZ",
							 "rn",
							 "rn_BI",
							 "ru",
							 "ru_BY",
							 "ru_KZ",
							 "ru_KG",
							 "ru_MD",
							 "ru_RU",
							 "ru_UA",
							 "rwk",
							 "rwk_TZ",
							 "sah",
							 "sah_RU",
							 "saq",
							 "saq_KE",
							 "sg",
							 "sg_CF",
							 "sbp",
							 "sbp_TZ",
							 "gd",
							 "gd_GB",
							 "seh",
							 "seh_MZ",
							 "sr_Cyrl",
							 "sr_Cyrl_BA",
							 "sr_Cyrl_XK",
							 "sr_Latn",
							 "sr_Latn_BA",
							 "sr_Latn_XK",
							 "sr_Latn_ME",
							 "sr_Latn_RS",
							 "sr_Cyrl_ME",
							 "sr_Cyrl_RS",
							 "ksb",
							 "ksb_TZ",
							 "sn",
							 "sn_ZW",
							 "ii",
							 "ii_CN",
							 "si",
							 "si_LK",
							 "sk",
							 "sk_SK",
							 "sl",
							 "sl_SI",
							 "xog",
							 "xog_UG",
							 "so",
							 "so_DJ",
							 "so_ET",
							 "so_KE",
							 "so_SO",
							 "es",
							 "es_AR",
							 "es_BO",
							 "es_IC",
							 "es_EA",
							 "es_CL",
							 "es_CO",
							 "es_CR",
							 "es_CU",
							 "es_DO",
							 "es_EC",
							 "es_SV",
							 "es_GQ",
							 "es_GT",
							 "es_HN",
							 "es_419",
							 "es_MX",
							 "es_NI",
							 "es_PA",
							 "es_PY",
							 "es_PE",
							 "es_PH",
							 "es_PR",
							 "es_ES",
							 "es_US",
							 "es_UY",
							 "es_VE",
							 "zgh",
							 "zgh_MA",
							 "sw",
							 "sw_CD",
							 "sw_KE",
							 "sw_TZ",
							 "sw_UG",
							 "sv",
							 "sv_AX",
							 "sv_FI",
							 "sv_SE",
							 "gsw",
							 "gsw_FR",
							 "gsw_LI",
							 "gsw_CH",
							 "shi_Latn",
							 "shi_Latn_MA",
							 "shi_Tfng",
							 "shi_Tfng_MA",
							 "dav",
							 "dav_KE",
							 "tg_Cyrl",
							 "tg_Cyrl_TJ",
							 "ta",
							 "ta_IN",
							 "ta_MY",
							 "ta_SG",
							 "ta_LK",
							 "twq",
							 "twq_NE",
							 "te",
							 "te_IN",
							 "teo",
							 "teo_KE",
							 "teo_UG",
							 "th",
							 "th_TH",
							 "bo",
							 "bo_CN",
							 "bo_IN",
							 "ti",
							 "ti_ER",
							 "ti_ET",
							 "to",
							 "to_TO",
							 "tr",
							 "tr_CY",
							 "tr_TR",
							 "tk_Latn",
							 "tk_Latn_TM",
							 "uk",
							 "uk_UA",
							 "hsb",
							 "hsb_DE",
							 "ur",
							 "ur_IN",
							 "ur_PK",
							 "ug",
							 "ug_Arab",
							 "ug_Arab_CN",
							 "uz_Cyrl",
							 "uz_Arab",
							 "uz_Arab_AF",
							 "uz_Latn",
							 "uz_Latn_UZ",
							 "uz_Cyrl_UZ",
							 "vai_Vaii",
							 "vai_Latn",
							 "vai_Latn_LR",
							 "vai_Vaii_LR",
							 "vi",
							 "vi_VN",
							 "vun",
							 "vun_TZ",
							 "wae",
							 "wae_CH",
							 "cy",
							 "cy_GB",
							 "fy",
							 "fy_NL",
							 "yav",
							 "yav_CM",
							 "yi",
							 "yi_001",
							 "yo",
							 "yo_BJ",
							 "yo_NG",
							 "dje",
							 "dje_NE",
							 "zu",
							 "zu_ZA"];

	const post = snapshot.val();
	console.log('data was gottem');
	const bookingLocal = post.local;
	const shopIDentifier = context.params.shopId;
	const bookingIdentifier = context.params.bookingID;

  	if (localArrayHolder.includes(bookingLocal)) {
	 	console.log("It is here");
	 	return
	 } else {
	 	console.log("Not here");
	 	return admin.database().ref('bookings/'+ shopIDentifier + '/'+bookingIdentifier).remove();
	 }

});









function sign(params, api_key) {
  var flattened_params = flatten_params(params);
  var values = Object.keys(flattened_params).sort().map(key => flattened_params[key]).join(' ');
  return crypto.createHmac('sha256', api_key).update(values).digest('hex');
}

function flatten_params(params, result, path) {
  result = result || {};
  path = path || [];
  if (params instanceof Object) {
    var k;
    for (k in params) {
      flatten_params(params[k], result, [...path, k]);
    }
  } else {
    result[path.map(key => '[' + key + ']').join()] = params;
  }
  return result;
}




