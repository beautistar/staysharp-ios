//
//  VerifyDateAssociation.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 26/08/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

class VerifyDateAssociation {
    
    static func checkDateData(timeZone: String, calendar: String, locale: String, dateData: String)-> Bool{
        
        let bookingRegionCreated = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: calendar, LocName: locale)
        
        if let _ = dateData.toDate(style: .extended, region: bookingRegionCreated) {
            return true
        } else {
            return false
        }
    }
    
}
