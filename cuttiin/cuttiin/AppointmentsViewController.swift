//
//  AppointmentsViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate
import UserNotifications
import FBSDKLoginKit

class AppointmentsViewController: UIViewController, UITabBarControllerDelegate {
    var bookingsPaid = [Bookings]()
    var customerPaid = [Customer]()
    var barberBooked = [Barber]()
    var barberNames = [String]()
    var customerNames = [String]()
    var bookingPaidFormattedTime = [String]()
    var customerProfileImagesURL = [String]()
    var appointments = [Appointments]()
    var appointmnetsNewData = [Appointments]()
    var firstCall = true
    let mySpecialNotificationKey = "com.teckdkapps.specialNotificationKey"
    
    var profileImageUrlArray = [String]()
    var clientNameArray = [String]()
    var bookingStartTimeArray = [String]()
    var bookingEndTimeArray = [String]()
    var bookedBarberNameArray = [String]()
    var bookedServicePriceArray =  [String]()
    var bookingUniqueUUIDD = [String]()
    var notPaidButBookedListPaymentID = [String]()
    
    var bookingTimeDataTzone = [String]()
    var bookingTimeDataClandar = [String]()
    var bookingTimeDataLocal = [String]()
    var bookedBarberIdentification = [String]()
    
    let headerContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let headerTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("naviagtionTitleTextCalendarView", comment: "TODAYS BOOKINGS")
        ht.font = UIFont(name: "Arch-LightCond", size: 30)
        ht.textColor = UIColor.white
        ht.textAlignment = .center
        return ht
    }()
    
    let headerSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceVertical = true
        cv.backgroundColor = UIColor.clear
        cv.register(customAppointmentViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        view.addSubview(headerContanerView)
        view.addSubview(headerSeperatorView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContraints()
        getPaidBookings()
        firstCall = true
        self.tabBarController?.delegate = self
        postUserSpecialToken()
        NotificationCenter.default.addObserver(self, selector: #selector(updateTabBarBadgeNumbersBarbers), name: NSNotification.Name(rawValue: mySpecialNotificationKey), object: nil)
    }
    
    
    func preventWrongLocal(){
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        switch userCalender {
        case "gregorian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
            
        case "buddhist":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "chinese":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "coptic":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "ethiopicAmeteMihret":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "hebrew":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "iso8601":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "indian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamic":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamicCivil":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "japanese":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                    print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "persian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                    print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "republicOfChina":
                if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                    print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamicTabular":
                if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                    print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamicUmmAlQura":
                if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                    print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        default:
            print("Sky High")
                if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone)  {
                    print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        }
    }
    
    func handleWrongLocalAndDate(){
        print("get kicked out")
        FBSDKLoginManager().logOut()
        do{
            try Auth.auth().signOut()
            self.tabBarController?.selectedIndex = 1
            let welcomeviewcontroller = WelcomeViewController()
            self.present(welcomeviewcontroller, animated: true, completion: nil)
            welcomeviewcontroller.handleWrongLocalAndDate()
            
        }catch let logoutError {
            print(logoutError)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 1 {
            //do your stuff
            tabBarController.tabBar.items?[1].badgeValue = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
            
        }
    }
    
    @objc func updateTabBarBadgeNumbersBarbers(){
        tabBarController?.tabBar.items?[1].badgeValue = "\(UIApplication.shared.applicationIconBadgeNumber)"
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func postUserSpecialToken(){
        if let uid = Auth.auth().currentUser?.uid, let fcmtokenkey = Messaging.messaging().fcmToken {
            let firebaseRefFCMToken = Database.database().reference()
            firebaseRefFCMToken.child("fcmtoken").child(uid).child("key").setValue(fcmtokenkey)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if !firstCall {
//            self.getPaidBookings()
//        }
//        self.firstCall = false
        if Auth.auth().currentUser?.uid == nil {
            UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
            let welcomeviewcontroller = WelcomeViewController()
            present(welcomeviewcontroller, animated: true, completion: nil)
        }
    }
    
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        self.getPaidBookings()
        sender.endRefreshing()
    }
    
    func getPaidBookings(){
        DispatchQueue.global(qos: .background).async {
            if let userID = Auth.auth().currentUser?.uid  {
                let firebaseRef = Database.database().reference()
                firebaseRef.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshooottt) in
                    if let dictionary = snapshooottt.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionary) == true {
                        
                        let BarberShopCOnfirm = HandleDataRequest.handleUserBarberShop(firebaseData: dictionary)
                        let barberConfirm = HandleDataRequest.handleUserBarberNode(firebaseData: dictionary)
                        
                        if BarberShopCOnfirm {
                            let userObject = BarberShop()
                            userObject.setValuesForKeys(dictionary)
                            
                            if let userRole = userObject.role, let uniqueUUID = userObject.uniqueID, userRole == "barberShop", let currencyCheck = userObject.currencyCode {
                                UserDefaults.standard.set(uniqueUUID, forKey: "theOriginalBarberShopUUID")
                                self.getBarberShopBookings(uniqIDD: uniqueUUID, currencyCode: currencyCheck)
                            }
                        } else if barberConfirm {
                            let userBarberObject = Barber()
                            userBarberObject.setValuesForKeys(dictionary)
                            
                            if let barberShopIDDX = userBarberObject.barberShopID, let userRole = userBarberObject.role, userRole == "barberStaff" {
                                UserDefaults.standard.set(barberShopIDDX, forKey: "theOriginalBarberShopUUID")
                                self.getRighBarberShopID(givenID: barberShopIDDX, role: userRole, barberShopIDX: barberShopIDDX)
                            }
                        }
                    }
                })
                
            }
        }
    }
    
    func getRighBarberShopID(givenID: String, role: String, barberShopIDX: String){
        DispatchQueue.global(qos: .background).async {
            let fBase = Database.database().reference()
            fBase.child("users").child(barberShopIDX).observeSingleEvent(of: .value, with: { (snpapshoot) in
                if let dictionary = snpapshoot.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionary) {
                    let specUser = BarberShop()
                    specUser.setValuesForKeys(dictionary)
                    if let currencyCodeUN = specUser.currencyCode {
                        self.getBookingsForBarbersSpecific(uniqIDDDD: barberShopIDX, barberSHOPIDENT: givenID, currencyCodes: currencyCodeUN)
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getBarberShopBookings(uniqIDD: String, currencyCode: String){
        DispatchQueue.global(qos: .background).async {
            let firebaseReferree = Database.database().reference()
            firebaseReferree.child("bookings").child(uniqIDD).observeSingleEvent(of: .value, with: { (snapshoootootot) in
                self.bookingsPaid.removeAll()
                self.profileImageUrlArray.removeAll()
                self.clientNameArray.removeAll()
                self.bookingStartTimeArray.removeAll()
                self.bookingEndTimeArray.removeAll()
                self.bookedBarberNameArray.removeAll()
                self.bookedServicePriceArray.removeAll()
                self.bookingUniqueUUIDD.removeAll()
                
                //self.appointments.removeAll()
                self.appointmnetsNewData.removeAll()
                
                if let dictionaryMain = snapshoootootot.value as? [String: AnyObject] {
                    let numberOfBookings = dictionaryMain.count
                    var numberOfBookingsEquator = 0
                    for dictionary in dictionaryMain {
                        numberOfBookingsEquator = numberOfBookingsEquator + 1
                        
                        if let dicto = dictionary.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dicto) == true {
                            
                            let bookingForShop = Bookings()
                            bookingForShop.setValuesForKeys(dicto)
                            
                            let currentDateNow = DateInRegion()
                            if let isCancelled = bookingForShop.bookingCancel {
                                if isCancelled == "NO" {
                                    if let bookingPaymentIDD = bookingForShop.paymentID, let customerID =  bookingForShop.customerID, let bookedBarber = bookingForShop.bookedBarberID, let bookingStartString = bookingForShop.bookingStartTime, let bookTimeZone = bookingForShop.timezone, let bookCalendar = bookingForShop.calendar, let bookLocal = bookingForShop.local {
                                        let bookRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bookTimeZone, calenName: bookCalendar, LocName: bookLocal)
                                        
                                        let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookTimeZone, calendar: bookCalendar, locale: bookLocal)
                                        let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookTimeZone, calendar: bookCalendar, locale: bookLocal, dateData: bookingStartString)
                                        
                                        if verifyBookingData == true && verifyBookingStartDate == true {
                                            if let bookingStartDate = bookingStartString.toDate(style: .extended, region: bookRegion) {
                                                if bookingStartDate.isInside(date: currentDateNow, granularity: .day) {
                                                    
                                                    
                                                    if uniqIDD != "" && bookingPaymentIDD != "" && bookingPaymentIDD != "nil" {
                                                        let firebaseReferreexxxx = Database.database().reference()
                                                        print("nowo")
                                                        firebaseReferreexxxx.child("payments").child(uniqIDD).child(bookingPaymentIDD).observeSingleEvent(of: .value, with: { (snapshooototo) in
                                                            if let dictionayRefund = snapshooototo.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund) == true {
                                                                let paymentHold = Payments()
                                                                paymentHold.setValuesForKeys(dictionayRefund)
                                                                print("omoge")
                                                                if let payConfirm = paymentHold.payment_aq_status_msg, let payCapture = paymentHold.capture_aq_status_msg, let amountPaid = paymentHold.priceTotal {
                                                                    if payConfirm == "Approved" && payCapture == "not available" {
                                                                        
                                                                        self.bookingsPaid.append(bookingForShop)
                                                                        if let bookingEndString = bookingForShop.bookingEndTime, let bookUniqueIDD = bookingForShop.bookingID, let tzxe = bookingForShop.timezone, let clandx = bookingForShop.calendar, let clocalx = bookingForShop.local {
                                                                            print("red dragon", customerID)
                                                                            let firebaseReferrexxxezero = Database.database().reference()
                                                                            firebaseReferrexxxezero.child("users").child(customerID).observeSingleEvent(of: .value, with: { (snaapppshottssxoxo) in
                                                                                if let dictionaryxoxol = snaapppshottssxoxo.value as? [String: AnyObject], HandleDataRequest.handleUserCustomerNode(firebaseData: dictionaryxoxol) == true {
                                                                                    
                                                                                    print("Buy disiel", Array(dictionaryxoxol.keys))
                                                                                    let paidBookingCustomer = Customer()
                                                                                    paidBookingCustomer.setValuesForKeys(dictionaryxoxol)
                                                                                    self.customerPaid.append(paidBookingCustomer)
                                                                                    if let cusName = paidBookingCustomer.firstName, let cusImage = paidBookingCustomer.profileImageUrl {
                                                                                        
                                                                                        
                                                                                        let firebaseReferrexxxebb = Database.database().reference()
                                                                                        firebaseReferrexxxebb.child("users").child(bookedBarber).observeSingleEvent(of: .value, with: { (snaapppshottsslora) in
                                                                                            if let dictionaryfaves = snaapppshottsslora.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryfaves) == true {
                                                                                                let bookedbarb = Barber()
                                                                                                bookedbarb.setValuesForKeys(dictionaryfaves)
                                                                                                self.barberBooked.append(bookedbarb)
                                                                                                if let barbName = bookedbarb.firstName {
                                                                                                    
                                                                                                    self.arrayDefaultCombine(imageURL: cusImage, clName: cusName,bookStartTSS: bookingStartString, bookEndTSS: bookingEndString,bookedBarberNm: barbName, bookedServicePr: amountPaid, bookingUniqueIDDD: bookUniqueIDD, bookTzone: tzxe, bookClandar: clandx, bookLocal: clocalx, bookedBarberUUID: bookedBarber, currencyCodeBoking: currencyCode)
                                                                                                }
                                                                                            }
                                                                                        }, withCancel: nil)
                                                                                        
                                                                                    }
                                                                                }
                                                                            }, withCancel: nil)
                                                                            
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }//from here
                                                        })
                                                    }
                                                }
                                            }
                                        }
                                        //from here
                                    } else {
                                        print("No data came thru")
                                    }
                                }
                                
                            }
                        }
                    }
                    
                    if numberOfBookingsEquator == numberOfBookings {
                        if !dictionaryMain.isEmpty {
                            print("red head")
                            self.perform(#selector(self.searchAndDeleteOldArrayItems), with: self, afterDelay: 3)
                        }
                    }
                    
                }
            }, withCancel: nil)
        }
    }
    
    func getBookingsForBarbersSpecific(uniqIDDDD: String, barberSHOPIDENT: String, currencyCodes: String){
        DispatchQueue.global(qos: .background).async {
            let firebaseReferree = Database.database().reference()
            firebaseReferree.child("bookings").child(uniqIDDDD).observeSingleEvent(of: .value, with: { (snapshoootootot) in
                self.bookingsPaid.removeAll()
                self.profileImageUrlArray.removeAll()
                self.clientNameArray.removeAll()
                self.bookingStartTimeArray.removeAll()
                self.bookingEndTimeArray.removeAll()
                self.bookedBarberNameArray.removeAll()
                self.bookedServicePriceArray.removeAll()
                self.bookingUniqueUUIDD.removeAll()
                
                //self.appointments.removeAll()
                self.appointmnetsNewData.removeAll()
                
                if let dictionaryMain = snapshoootootot.value as? [String: AnyObject] {
                    let numberOfBookings = dictionaryMain.count
                    var numberOfBookingsEquator = 0
                    for dictionary in dictionaryMain {
                        numberOfBookingsEquator = numberOfBookingsEquator + 1
                        
                        if let dicto = dictionary.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dicto) {
                            let bookingForShop = Bookings()
                            bookingForShop.setValuesForKeys(dicto)
                            
                            let currentDateNow = DateInRegion()
                            if let isCancelled = bookingForShop.bookingCancel {
                                if isCancelled == "NO" {
                                    if let bookingPaymentIDD = bookingForShop.paymentID, let customerID =  bookingForShop.customerID, let bookedBarber = bookingForShop.bookedBarberID, let bookingStartString = bookingForShop.bookingStartTime, let bookTimeZone = bookingForShop.timezone, let bookCalendar = bookingForShop.calendar, let bookLocal = bookingForShop.local {
                                        let bookRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bookTimeZone, calenName: bookCalendar, LocName: bookLocal)
                                        
                                        let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookTimeZone, calendar: bookCalendar, locale: bookLocal)
                                        let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookTimeZone, calendar: bookCalendar, locale: bookLocal, dateData: bookingStartString)
                                        
                                        if verifyBookingData == true && verifyBookingStartDate == true {
                                            if let bookingStartDate = bookingStartString.toDate(style: .extended, region: bookRegion) {
                                                if bookingStartDate.isInside(date: currentDateNow, granularity: .day) && bookedBarber == barberSHOPIDENT {
                                                    
                                                    if uniqIDDDD != "" && bookingPaymentIDD != "" && bookingPaymentIDD != "nil" {
                                                        
                                                        let firebaseReferreexxxx = Database.database().reference()
                                                        firebaseReferreexxxx.child("payments").child(uniqIDDDD).child(bookingPaymentIDD).observeSingleEvent(of: .value, with: { (snapshooototo) in
                                                            if let dictionayRefund = snapshooototo.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund) == true {
                                                                let paymentHold = Payments()
                                                                paymentHold.setValuesForKeys(dictionayRefund)
                                                                
                                                                if let payConfirm = paymentHold.payment_aq_status_msg, let payCapture = paymentHold.capture_aq_status_msg, let amountPiad = paymentHold.priceTotal  {
                                                                    if payConfirm == "Approved" && payCapture == "not available" {
                                                                        
                                                                        self.bookingsPaid.append(bookingForShop)
                                                                        if let bookingEndString = bookingForShop.bookingEndTime, let bookUniqueIDD = bookingForShop.bookingID, let tzxe = bookingForShop.timezone, let clandx = bookingForShop.calendar, let clocalx = bookingForShop.local {
                                                                            
                                                                            
                                                                            let firebaseReferrexxxezero = Database.database().reference()
                                                                            firebaseReferrexxxezero.child("users").child(customerID).observeSingleEvent(of: .value, with: { (snaapppshottssxoxo) in
                                                                                if let dictionaryxoxol = snaapppshottssxoxo.value as? [String: AnyObject], HandleDataRequest.handleUserCustomerNode(firebaseData: dictionaryxoxol) == true {
                                                                                    let paidBookingCustomer = Customer()
                                                                                    paidBookingCustomer.setValuesForKeys(dictionaryxoxol)
                                                                                    self.customerPaid.append(paidBookingCustomer)
                                                                                    if let cusName = paidBookingCustomer.firstName, let cusImage = paidBookingCustomer.profileImageUrl {
                                                                                        
                                                                                        
                                                                                        let firebaseReferrexxxebb = Database.database().reference()
                                                                                        firebaseReferrexxxebb.child("users").child(bookedBarber).observeSingleEvent(of: .value, with: { (snaapppshottsslora) in
                                                                                            if let dictionaryfaves = snaapppshottsslora.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryfaves) == true {
                                                                                                let bookedbarb = Barber()
                                                                                                bookedbarb.setValuesForKeys(dictionaryfaves)
                                                                                                self.barberBooked.append(bookedbarb)
                                                                                                if let barbName = bookedbarb.firstName {
                                                                                                    self.arrayDefaultCombine(imageURL: cusImage, clName: cusName,bookStartTSS: bookingStartString, bookEndTSS: bookingEndString,bookedBarberNm: barbName, bookedServicePr: amountPiad, bookingUniqueIDDD: bookUniqueIDD, bookTzone: tzxe, bookClandar: clandx, bookLocal: clocalx, bookedBarberUUID: bookedBarber, currencyCodeBoking: currencyCodes)
                                                                                                }
                                                                                            }
                                                                                        }, withCancel: nil)
                                                                                        
                                                                                    }
                                                                                }
                                                                            }, withCancel: nil)
                                                                            
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }//from here
                                                        })
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        print("No data came thru")
                                    }
                                }
                            }
                        }
                    }
                    
                    //if
                    if numberOfBookingsEquator == numberOfBookings {
                        if !dictionaryMain.isEmpty {
                            print("red head")
                            self.perform(#selector(self.searchAndDeleteOldArrayItems), with: self, afterDelay: 3)
                        }
                    }
                    
                }
            }, withCancel: nil)
        }
    }
    
    func arrayDefaultCombine(imageURL: String , clName: String , bookStartTSS: String, bookEndTSS: String , bookedBarberNm: String , bookedServicePr: String , bookingUniqueIDDD: String , bookTzone: String , bookClandar: String , bookLocal: String , bookedBarberUUID: String, currencyCodeBoking: String ){
        
        let claxandNow = DateByUserDeviceInitializer.calenderNow
        let tznxNow = DateByUserDeviceInitializer.tzone
        let loclxnNow = DateByUserDeviceInitializer.localCode
        
        let verufyCurrentDateDetails = VerifyDateDetails.checkDateData(timeZone: tznxNow, calendar: claxandNow, locale: loclxnNow)
        
        if verufyCurrentDateDetails == true {
            let currentRegionOfDeviceNow = DateByUserDeviceInitializer.getRegion(TZoneName: tznxNow, calenName: claxandNow, LocName: loclxnNow)
            let bookingUniqueRegionSP = DateByUserDeviceInitializer.getRegion(TZoneName: bookTzone, calenName: bookClandar, LocName: bookLocal)
            
            let appoint = Appointments()
            appoint.bookedBarberName = bookedBarberNm
            appoint.profileImageUrl = imageURL
            appoint.bookedServicePrice = bookedServicePr + currencyCodeBoking
            appoint.bookingUniqueID = bookingUniqueIDDD
            appoint.bookedBarberUUID = bookedBarberUUID
            appoint.clientName = clName
            
            
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string: imageURL) {
                    do {
                        let data = try Data(contentsOf: url)
                        
                        if let downloadedImage = UIImage(data: data) {
                            appoint.profileImage = downloadedImage
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    } catch {
                        appoint.profileImage = UIImage()
                    }
                }
            }
            
            if let bookst = bookStartTSS.toDate(style: .extended, region: bookingUniqueRegionSP){
                appoint.bookingStartTime = bookst.convertTo(region: currentRegionOfDeviceNow)
                let startTimeIndex = bookst.convertTo(region: currentRegionOfDeviceNow)
                
                
                appoint.bookingStartTimeString = NSLocalizedString("startAtTextCollectionView", comment: "Starts at: ") + startTimeIndex.toString(DateToStringStyles.time(.short))
            } else {
                appoint.bookingStartTimeString = ""
            }
            
            if let bookend = bookEndTSS.toDate(style: .extended, region: bookingUniqueRegionSP){
                appoint.bookingEndTime = bookend.convertTo(region: currentRegionOfDeviceNow)
                let endTimeIndex = bookend.convertTo(region: currentRegionOfDeviceNow)
                appoint.bookingEndTimeString = NSLocalizedString("endsAtTextCollectionView", comment: "Ends at: ") + endTimeIndex.toString(DateToStringStyles.time(.short))
            } else {
                appoint.bookingEndTimeString = ""
            }
            
            if let uuiidBook = appoint.bookingUniqueID {
                if let firstNegative = self.appointments.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    if let indexValue = self.appointments.index(of: firstNegative) {
                        self.appointments[indexValue] = appoint
                    }
                    print("runaway gorila")
                } else {
                    self.appointments.append(appoint)
                }
                
                if let _ = self.appointmnetsNewData.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    print("runaway gorila new moves")
                } else {
                    self.appointmnetsNewData.append(appoint)
                }
                
                
                self.appointments.sort(by: { (appstx, appsty) -> Bool in
                    return appstx.bookingStartTime < appsty.bookingStartTime
                })
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    @objc func searchAndDeleteOldArrayItems(){
        for singleApp in self.appointments {
            if let uuiidBook = singleApp.bookingUniqueID {
                
                if let _ = self.appointmnetsNewData.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    print("good data")
                } else {
                    self.appointments = self.appointments.filter { $0.bookingUniqueID != uuiidBook }
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func setupViewContraints(){
        headerContanerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        headerContanerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerContanerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerContanerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        headerContanerView.addSubview(headerTitle)
        
        headerTitle.topAnchor.constraint(equalTo: headerContanerView.topAnchor).isActive = true
        headerTitle.centerXAnchor.constraint(equalTo: headerContanerView.centerXAnchor).isActive = true
        headerTitle.widthAnchor.constraint(equalTo: headerContanerView.widthAnchor).isActive = true
        headerTitle.heightAnchor.constraint(equalTo: headerContanerView.heightAnchor).isActive = true
        
        headerSeperatorView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerSeperatorView.topAnchor.constraint(equalTo: headerContanerView.bottomAnchor).isActive = true
        headerSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerSeperatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        collectView.topAnchor.constraint(equalTo: headerSeperatorView.bottomAnchor).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
        }
    }

}


class customAppointmentViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    let clientNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let openingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let closingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    //new buttons start here
    
    let buttonContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var acceptBookingButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("acceptBookingAppointmentView", comment: "ACCEPT"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 24)
        st.tag = 0
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        //st.addTarget(self, action: #selector(handleServed), for: .touchUpInside)
        return st
    }()
    
    lazy var declineBookingButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("declineBookingAppointmentView", comment: "DECLINE"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 24)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        //st.addTarget(self, action: #selector(handleServed), for: .touchUpInside)
        return st
    }()
    
    //new buttons ends here
    
    let bookedBarberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(clientNamePlaceHolder)
        addSubview(openingTimePlaceHolder)
        addSubview(closingTimePlaceHolder)
        addSubview(bookedBarberNamePlaceHolder)
        addSubview(servicePricePlaceHolder)
        addSubview(seperatorView)
        
        
        backgroundColor = UIColor(r: 23, g: 69, b: 90)
        addContraintsWithFormat(format: "H:|-16-[v0(90)]|", views: thumbnailImageView)
        addContraintsWithFormat(format: "H:|-116-[v0][v1(50)]-10-|", views: clientNamePlaceHolder, servicePricePlaceHolder)
        addContraintsWithFormat(format: "H:|-116-[v0]-60-|", views: openingTimePlaceHolder)
        addContraintsWithFormat(format: "H:|-116-[v0]-60-|", views: closingTimePlaceHolder)
        addContraintsWithFormat(format: "H:|-116-[v0]-60-|", views: bookedBarberNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-10-[v0(20)][v1(20)][v2(20)][v3(20)]-10-|", views: clientNamePlaceHolder, openingTimePlaceHolder,closingTimePlaceHolder, bookedBarberNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-10-[v0(20)]|", views: servicePricePlaceHolder)
        addContraintsWithFormat(format: "V:|-10-[v0]-10-[v1(5)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    override func prepareForReuse() {
        self.bookedBarberNamePlaceHolder.text = ""
        self.clientNamePlaceHolder.text = ""
        self.servicePricePlaceHolder.text = ""
        self.thumbnailImageView.image = UIImage()
        self.openingTimePlaceHolder.text = ""
        self.closingTimePlaceHolder.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
