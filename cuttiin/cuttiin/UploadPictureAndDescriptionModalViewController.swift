//
//  UploadPictureAndDescriptionModalViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/6/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

class UploadPictureAndDescriptionModalViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    var numberofcustomer: NumberOfCustomersViewController?
    let picker = UIImagePickerController()
    
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_photo_smallest")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let shortDescriptionHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("shortDescriptionLableUploadPicture", comment: "Short Description(optional)")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        lnhp.isHidden = false
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let shortDescriptionTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        em.textColor = UIColor(r: 118, g: 187, b: 220)
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.textAlignment = .justified
        em.isEditable = true
        em.isSelectable = true
        em.dataDetectorTypes = UIDataDetectorTypes.link
        em.autocorrectionType = UITextAutocorrectionType.yes
        em.spellCheckingType = UITextSpellCheckingType.yes
        em.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        em.layer.borderWidth = 2
        em.layer.masksToBounds = true
        em.layer.cornerRadius = 5
        return em
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("writeShortDescriptionUploadPictureView", comment: "Write a short description!")
        ht.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
        ht.textColor = UIColor(r: 118, g: 187, b: 220)
        ht.textAlignment = .center
        return ht
    }()
    
    let descriptionBody: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionBodyTextUploadPictureView", comment: "Here you can write a small description and upload a photo of your choice, to give your barber an insight on your prefered service")
        ht.numberOfLines = 0
        ht.font = UIFont(name: "HelveticaNeue", size: 13)
        ht.textColor = UIColor(r: 118, g: 187, b: 220)
        ht.textAlignment = .center
        return ht
    }()
    
    lazy var saveThePicButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        fbutton.setTitle(NSLocalizedString("saveButtonUploadPictureView", comment: "SAVE"), for: .normal)
        fbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.layer.borderWidth = 2
        fbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        fbutton.addTarget(self, action: #selector(handleSavePic), for: .touchUpInside)
        return fbutton
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 118, g: 187, b: 220)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()
    
    lazy var closeThisViewButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.setTitle(NSLocalizedString("closeButtonTitleUploadPictureView", comment: "CLOSE"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 17)
        fbutton.addTarget(self, action: #selector(handleDismissView), for: .touchUpInside)
        return fbutton
    }()
    
    override func viewDidLoad() {
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        picker.delegate = self
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(scrollView)
        shortDescriptionTextField.delegate = self
        setupViewContraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func handleDismissView(){
        dismiss(animated: true, completion: nil)
    }
    
    func setupViewContraints(){
        
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 550)
        
        scrollView.addSubview(closeThisViewButton)
        scrollView.addSubview(selectImageView)
        scrollView.addSubview(shortDescriptionHiddenPlaceHolder)
        scrollView.addSubview(shortDescriptionTextField)
        
        scrollView.addSubview(descriptionTitle)
        scrollView.addSubview(descriptionBody)
        scrollView.addSubview(saveThePicButton)
        scrollView.addSubview(errorMessagePlaceHolder)
        
        
        closeThisViewButton.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 25).isActive = true
        closeThisViewButton.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        closeThisViewButton.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.25).isActive = true
        closeThisViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        selectImageView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 40).isActive = true
        selectImageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        selectImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        shortDescriptionHiddenPlaceHolder.topAnchor.constraint(equalTo: selectImageView.bottomAnchor, constant: 20).isActive = true
        shortDescriptionHiddenPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        shortDescriptionHiddenPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        shortDescriptionHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shortDescriptionTextField.topAnchor.constraint(equalTo: shortDescriptionHiddenPlaceHolder.bottomAnchor).isActive = true
        shortDescriptionTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        shortDescriptionTextField.heightAnchor.constraint(equalToConstant: 100).isActive = true
        shortDescriptionTextField.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        
        descriptionTitle.topAnchor.constraint(equalTo: shortDescriptionTextField.bottomAnchor, constant: 10).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        descriptionBody.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        descriptionBody.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionBody.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionBody.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        saveThePicButton.topAnchor.constraint(equalTo: descriptionBody.bottomAnchor, constant: 10).isActive = true
        saveThePicButton.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        saveThePicButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        saveThePicButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: saveThePicButton.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        
    }
    
    @objc func handleSavePic(){
        if self.selectImageView.image == UIImage(named: "add_photo_smallest") {
            self.errorMessagePlaceHolder.text = NSLocalizedString("uploadAIcErrorMessageUploadPictureView", comment: "Please upload a picture")
        }else {
            numberofcustomer?.pictureAndDescriptionUploadContainerView.isHidden = false
            numberofcustomer?.pictureSelectImageView.image = self.selectImageView.image
            numberofcustomer?.shortDescriptionTextField.text = self.shortDescriptionTextField.text
            numberofcustomer?.navigationItem.rightBarButtonItem?.isEnabled = true
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.text == NSLocalizedString("shortDescriptionTextNumberOfCustomer", comment: "Short Description") {textView.text = ""}
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = NSLocalizedString("errorMessageNoCameraChoosePhotoView", comment: "Sorry, this device has no camera")
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: "Take photo", style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: "Choose photo", style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            selectImageView.contentMode = .scaleAspectFit
            selectImageView.image = selectedImage
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleNextSkipAction(){
        let openingHoursview = OpeningHoursViewController()
        self.selectImageView.image = UIImage(named: "add_picture")
        let navController = UINavigationController(rootViewController: openingHoursview)
        present(navController, animated: true, completion: nil)
    }
}


