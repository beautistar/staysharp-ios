//
//  BookingCompleted.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class BookingCompleted: NSObject {
    var barberShopID: String?
    var bookingID: String?
    var barberID: String?
    var customerID: String?
    var serviceCompletedBarber: String?
    var serviceCompletedCustomer: String?
    var reasonForNotCompleted: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
    var notificationSent: String?
}
