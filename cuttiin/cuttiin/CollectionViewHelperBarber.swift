//
//  CollectionViewHelperBarber.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/19/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Firebase


extension AppointmentsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.appointments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customAppointmentViewCell
        
        //empty cell data
        //real data
        
        cell.openingTimePlaceHolder.text = ""
        if let opentime = self.appointments[safe: indexPath.row]?.bookingStartTimeString {
            cell.openingTimePlaceHolder.text = opentime
        } else {
            cell.openingTimePlaceHolder.text = ""
        }


        cell.closingTimePlaceHolder.text = ""
        if let closetime = self.appointments[safe: indexPath.row]?.bookingEndTimeString {
            cell.closingTimePlaceHolder.text = closetime
        } else {
            cell.closingTimePlaceHolder.text = ""
        }


        cell.bookedBarberNamePlaceHolder.text = ""
        if let barberName = self.appointments[safe: indexPath.row]?.bookedBarberName {
            cell.bookedBarberNamePlaceHolder.text = barberName
        } else {
            cell.bookedBarberNamePlaceHolder.text = ""
        }


        cell.servicePricePlaceHolder.text = ""
        if let servicepricee = self.appointments[safe: indexPath.row]?.bookedServicePrice {
            cell.servicePricePlaceHolder.text = servicepricee
        } else {
            cell.servicePricePlaceHolder.text = ""
        }


        cell.thumbnailImageView.image = UIImage()
        if let profileimagess = self.appointments[safe: indexPath.row]?.profileImage {
            cell.thumbnailImageView.image = profileimagess
        } else {
            cell.thumbnailImageView.image = UIImage()
        }


        cell.clientNamePlaceHolder.text = ""
        if let clientnamess = self.appointments[safe: indexPath.row]?.clientName {
            cell.clientNamePlaceHolder.text = clientnamess
        } else {
            cell.clientNamePlaceHolder.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("zombie")
        if let uuidAvail = UserDefaults.standard.object(forKey: "theOriginalBarberShopUUID") as? String{
            if let bookIDDD = self.appointments[safe: indexPath.row]?.bookingUniqueID {
                let orderDetail = OrderDetailViewController()
                orderDetail.specificAppintment = self.appointments[indexPath.row]
                orderDetail.barberShopID = uuidAvail
                orderDetail.bookingUniqueIDDD = bookIDDD
                orderDetail.bookedBarberUUIDAX = self.appointments[indexPath.row].bookedBarberUUID
                orderDetail.appointmentsviewhold = self
                orderDetail.indexPathSelected = indexPath.row
                navigationController?.pushViewController(orderDetail, animated: true)
            }
        }
    }
}

extension OrderDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceBooked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customOrderDetailCollectionViewCell
        
        cell.serviceNamePlaceHolder.text = ""
        if let serveTit = self.serviceBooked[safe: indexPath.row]?.serviceTitle {
            cell.serviceNamePlaceHolder.text = serveTit
        } else {
            cell.serviceNamePlaceHolder.text = ""
        }
        
        cell.serviceTimePlaceHolder.text = ""
        if let serTit = self.serviceBooked[safe: indexPath.row]?.estimatedTime {
            cell.serviceTimePlaceHolder.text = serTit + "min"
        } else {
            cell.serviceTimePlaceHolder.text = ""
        }
        
        cell.servicePricePlaceHolder.text = ""
        if let serCos = self.serviceBooked[safe: indexPath.row]?.serviceCost, let curr = self.serviceBooked[safe: indexPath.row]?.localCurrency {
            cell.servicePricePlaceHolder.text = serCos + curr
        } else {
            cell.servicePricePlaceHolder.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension BookingHistoryBarberShopDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceBooked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdXBA", for: indexPath) as! customOrderDetailCollectionViewCell
        
        cell.serviceNamePlaceHolder.text = ""
        if let serveTit = self.serviceBooked[safe: indexPath.row]?.serviceTitle {
            cell.serviceNamePlaceHolder.text = serveTit
        } else {
            cell.serviceNamePlaceHolder.text = ""
        }
        
        cell.serviceTimePlaceHolder.text = ""
        if let serTit = self.serviceBooked[safe: indexPath.row]?.estimatedTime {
            cell.serviceTimePlaceHolder.text = serTit + "min"
        } else {
            cell.serviceTimePlaceHolder.text = ""
        }
        
        cell.servicePricePlaceHolder.text = ""
        if let serCos = self.serviceBooked[safe: indexPath.row]?.serviceCost, let curr = self.serviceBooked[safe: indexPath.row]?.localCurrency {
            cell.servicePricePlaceHolder.text = serCos + curr
        } else {
            cell.servicePricePlaceHolder.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}



extension BookingsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            return self.appointmentsCustomer.count
        default:
            return self.appointmentsCustomerIsCompleted.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
            
            cell.bookedBarberShopNamePlaceHolder.text = ""
            if let bshopName = self.appointmentsCustomer[safe: indexPath.row]?.bookedBarberShopName {
                cell.bookedBarberShopNamePlaceHolder.text = bshopName
            } else {
                cell.bookedBarberShopNamePlaceHolder.text = ""
            }
            
            cell.bookingStartTimePlaceHolder.text = ""
            if let sttime = self.appointmentsCustomer[safe: indexPath.row]?.bookingStartTimeString {
                cell.bookingStartTimePlaceHolder.text = sttime
            } else {
                cell.bookingStartTimePlaceHolder.text = ""
            }
            
            cell.thumbnailImageView.image = UIImage()
            if let servimg = self.appointmentsCustomer[safe: indexPath.row]?.serviceImage {
                cell.thumbnailImageView.image = servimg
            } else {
                cell.thumbnailImageView.image = UIImage()
            }
            
            cell.totalTimePlaceHolder.text = ""
            if let btotal = self.appointmentsCustomer[safe: indexPath.row]?.bookingTotalTime {
                cell.totalTimePlaceHolder.text = btotal
            } else {
                cell.totalTimePlaceHolder.text = ""
            }
            
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdIsCompleted", for: indexPath) as! customUpcomingAndCompletedCollectionViewCellIsCompleted
            
            cell.bookedBarberShopNamePlaceHolderIsCompleted.text = ""
            if let shopnamexlx = self.appointmentsCustomerIsCompleted[safe: indexPath.row]?.bookedBarberShopName {
                cell.bookedBarberShopNamePlaceHolderIsCompleted.text = shopnamexlx
            } else {
                cell.bookedBarberShopNamePlaceHolderIsCompleted.text = ""
            }
            
            cell.bookingStartTimePlaceHolderIsCompleted.text = ""
            if let bbstttime = self.appointmentsCustomerIsCompleted[safe: indexPath.row]?.bookingStartTimeString {
                cell.bookingStartTimePlaceHolderIsCompleted.text = bbstttime
            } else {
                cell.bookingStartTimePlaceHolderIsCompleted.text = ""
            }
            
            cell.thumbnailImageViewIsCompleted.image = UIImage()
            if let seimg = self.appointmentsCustomerIsCompleted[safe: indexPath.row]?.serviceImage {
                cell.thumbnailImageViewIsCompleted.image = seimg
            } else {
                cell.thumbnailImageViewIsCompleted.image = UIImage()
            }
            
            cell.totalTimePlaceHolderIsCompleted.text = ""
            if let btota = self.appointmentsCustomerIsCompleted[safe: indexPath.row]?.bookingTotalTime {
                cell.totalTimePlaceHolderIsCompleted.text = btota
            } else {
                cell.totalTimePlaceHolderIsCompleted.text = ""
            }
            
            tabBarController?.tabBar.items?[0].badgeValue = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            if let selectDataHold = self.appointmentsCustomer[safe: indexPath.row]?.bookingUniqueID {
                let bookOrderDetail = OrderDetailCustomerViewController()
                bookOrderDetail.orderdetailcustomer = self.appointmentsCustomer[indexPath.row]
                bookOrderDetail.bookingviewcontroler = self
                bookOrderDetail.bookingIDD = selectDataHold
                bookOrderDetail.indexPathSelected = indexPath.row
                let navController = UINavigationController(rootViewController: bookOrderDetail)
                present(navController, animated: true, completion: nil)
            }
        case 1:
            if let selectDataHoldComp = self.appointmentsCustomerIsCompleted[safe: indexPath.row]?.bookingUniqueID {
                let ratingView = RatingAssemblerViewController()
                ratingView.customerCompletedBooking = self.appointmentsCustomerIsCompleted[indexPath.row]
                ratingView.bookingviewcontroler = self
                ratingView.selectedBookingIDDAX = selectDataHoldComp
                ratingView.indexPathSelected = indexPath.row
                let navController = UINavigationController(rootViewController: ratingView)
                present(navController, animated: true, completion: nil)
            }
        default:
            print("sky high")
        }
    }
}








extension BookingHistoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            return self.appointmentsCustomer.count
        default:
            return self.appointmentsCustomerCancelled.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdXEB", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
            
            if let bshopName = self.appointmentsCustomer[safe: indexPath.row]?.bookedBarberShopName {
                cell.bookedBarberShopNamePlaceHolder.text = bshopName
            } else {
                cell.bookedBarberShopNamePlaceHolder.text = nil
            }
            
            if let sttime = self.appointmentsCustomer[safe: indexPath.row]?.bookingStartTimeString {
                cell.bookingStartTimePlaceHolder.text = sttime
            } else {
                cell.bookingStartTimePlaceHolder.text = nil
            }
            
            if let servimg = self.appointmentsCustomer[safe: indexPath.row]?.serviceImage {
                cell.thumbnailImageView.image = servimg
            } else {
                cell.thumbnailImageView.image = nil
            }
            
            if let btotal = self.appointmentsCustomer[safe: indexPath.row]?.bookingTotalTime {
                cell.totalTimePlaceHolder.text = btotal
            } else {
                cell.totalTimePlaceHolder.text = nil
            }
            
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIDXOX", for: indexPath) as! customUpcomingAndCompletedCancelledCollectionViewCell
            
            if let bshopName = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookedBarberShopName {
                cell.bookedBarberShopNamePlaceHolder.text = bshopName
            } else {
                cell.bookedBarberShopNamePlaceHolder.text = nil
            }
            
            if let sttime = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookingStartTimeString {
                cell.bookingStartTimePlaceHolder.text = sttime
            } else {
                cell.bookingStartTimePlaceHolder.text = nil
            }
            
            if let servimg = self.appointmentsCustomerCancelled[safe: indexPath.row]?.serviceImage {
                cell.thumbnailImageView.image = servimg
            } else {
                cell.thumbnailImageView.image = nil
            }
            
            if let btotal = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookingTotalTime {
                cell.totalTimePlaceHolder.text = btotal
            } else {
                cell.totalTimePlaceHolder.text = nil
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            if let selectDataHold = self.appointmentsCustomer[safe: indexPath.row]?.bookingUniqueID {
                let bookOrderDetail = BookingDetailHistoryViewController()
                bookOrderDetail.orderdetailcustomer = self.appointmentsCustomer[indexPath.row]
                bookOrderDetail.bookingviewcontroler = self
                bookOrderDetail.bookingIDD = selectDataHold
                bookOrderDetail.indexPathSelected = indexPath.row
                let navController = UINavigationController(rootViewController: bookOrderDetail)
                present(navController, animated: true, completion: nil)
            }
        default:
            if let selectDataHold = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookingUniqueID {
                let bookOrderDetail = BookingDetailHistoryViewController()
                bookOrderDetail.orderdetailcustomer = self.appointmentsCustomerCancelled[indexPath.row]
                bookOrderDetail.bookingviewcontroler = self
                bookOrderDetail.bookingIDD = selectDataHold
                bookOrderDetail.indexPathSelected = indexPath.row
                let navController = UINavigationController(rootViewController: bookOrderDetail)
                present(navController, animated: true, completion: nil)
            }
            
        }
    }
}



extension BookingHistoryBarberShopViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            return self.appointmentsCustomer.count
        default:
            return self.appointmentsCustomerCancelled.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdBA", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
            
            if let bshopName = self.appointmentsCustomer[safe: indexPath.row]?.bookedBarberShopName {
                cell.bookedBarberShopNamePlaceHolder.text = bshopName
            } else {
                cell.bookedBarberShopNamePlaceHolder.text = nil
            }
            
            if let sttime = self.appointmentsCustomer[safe: indexPath.row]?.bookingStartTimeString {
                cell.bookingStartTimePlaceHolder.text = sttime
            } else {
                cell.bookingStartTimePlaceHolder.text = nil
            }
            
            if let servimg = self.appointmentsCustomer[safe: indexPath.row]?.serviceImage {
                cell.thumbnailImageView.image = servimg
            } else {
                cell.thumbnailImageView.image = nil
            }
            
            if let btotal = self.appointmentsCustomer[safe: indexPath.row]?.bookingTotalTime {
                cell.totalTimePlaceHolder.text = btotal
            } else {
                cell.totalTimePlaceHolder.text = nil
            }
            
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIDXOXBA", for: indexPath) as! customUpcomingAndCompletedCancelledCollectionViewCell
            
            if let bshopName = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookedBarberShopName {
                cell.bookedBarberShopNamePlaceHolder.text = bshopName
            } else {
                cell.bookedBarberShopNamePlaceHolder.text = nil
            }
            
            if let sttime = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookingStartTimeString {
                cell.bookingStartTimePlaceHolder.text = sttime
            } else {
                cell.bookingStartTimePlaceHolder.text = nil
            }
            
            if let servimg = self.appointmentsCustomerCancelled[safe: indexPath.row]?.serviceImage {
                cell.thumbnailImageView.image = servimg
            } else {
                cell.thumbnailImageView.image = nil
            }
            
            if let btotal = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookingTotalTime {
                cell.totalTimePlaceHolder.text = btotal
            } else {
                cell.totalTimePlaceHolder.text = nil
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            if let selectDataHold = self.appointmentsCustomer[safe: indexPath.row]?.bookingUniqueID, let barberShopIDSS = self.appointmentsCustomer[safe: indexPath.row]?.barberShopUniqueID {
                let bookOrderDetail = BookingHistoryBarberShopDetailViewController()
                bookOrderDetail.specificAppintment = self.appointmentsCustomer[indexPath.row]
                bookOrderDetail.appointmentsviewhold = self
                bookOrderDetail.bookingUniqueIDDD = selectDataHold
                bookOrderDetail.barberShopID = barberShopIDSS
                bookOrderDetail.indexPathSelected = indexPath.row
                let navController = UINavigationController(rootViewController: bookOrderDetail)
                present(navController, animated: true, completion: nil)
            }
        default:
            if let selectDataHold = self.appointmentsCustomerCancelled[safe: indexPath.row]?.bookingUniqueID, let barberShopIDSS = self.appointmentsCustomer[safe: indexPath.row]?.barberShopUniqueID {
                let bookOrderDetail = BookingHistoryBarberShopDetailViewController()
                bookOrderDetail.specificAppintment = self.appointmentsCustomerCancelled[indexPath.row]
                bookOrderDetail.appointmentsviewhold = self
                bookOrderDetail.bookingUniqueIDDD = selectDataHold
                bookOrderDetail.barberShopID = barberShopIDSS
                bookOrderDetail.indexPathSelected = indexPath.row
                let navController = UINavigationController(rootViewController: bookOrderDetail)
                present(navController, animated: true, completion: nil)
            }
            
        }
    }
}


extension UpcomingBarberShopBookingViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.appointmentsCustomer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdBAUP", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
        
        if let bshopName = self.appointmentsCustomer[safe: indexPath.row]?.bookedBarberShopName {
            cell.bookedBarberShopNamePlaceHolder.text = bshopName
        } else {
            cell.bookedBarberShopNamePlaceHolder.text = nil
        }
        
        if let sttime = self.appointmentsCustomer[safe: indexPath.row]?.bookingStartTimeString {
            cell.bookingStartTimePlaceHolder.text = sttime
        } else {
            cell.bookingStartTimePlaceHolder.text = nil
        }
        
        if let servimg = self.appointmentsCustomer[safe: indexPath.row]?.serviceImage {
            cell.thumbnailImageView.image = servimg
        } else {
            cell.thumbnailImageView.image = nil
        }
        
        if let btotal = self.appointmentsCustomer[safe: indexPath.row]?.bookingTotalTime {
            cell.totalTimePlaceHolder.text = btotal
        } else {
            cell.totalTimePlaceHolder.text = nil
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectDataHold = self.appointmentsCustomer[safe: indexPath.row]?.bookingUniqueID, let barberShopIDSS = self.appointmentsCustomer[safe: indexPath.row]?.barberShopUniqueID {
            let bookOrderDetail = BookingHistoryBarberShopDetailViewController()
            bookOrderDetail.specificAppintment = self.appointmentsCustomer[indexPath.row]
            bookOrderDetail.bookingUniqueIDDD = selectDataHold
            bookOrderDetail.barberShopID = barberShopIDSS
            bookOrderDetail.indexPathSelected = indexPath.row
            let navController = UINavigationController(rootViewController: bookOrderDetail)
            present(navController, animated: true, completion: nil)
        }
    }
    
}


extension OrderDetailCustomerViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceBooked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customOrderDetailCustomerCollectionViewCell
        cell.serviceNamePlaceHolder.text = self.serviceBooked[indexPath.row].serviceTitle
        
        
        cell.servicePricePlaceHolder.text = ""
        cell.serviceTimePlaceHolder.text = ""
        
        if let priceOS = self.serviceBooked[indexPath.row].serviceCost, let timeOS = self.serviceBooked[indexPath.row].estimatedTime, let curr = self.serviceBooked[indexPath.row].localCurrency {
            cell.servicePricePlaceHolder.text = priceOS + curr
            cell.serviceTimePlaceHolder.text = timeOS + "min"
        } else {
            cell.servicePricePlaceHolder.text = ""
            cell.serviceTimePlaceHolder.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension BookingDetailHistoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceBooked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customOrderDetailCustomerCollectionViewCell
        cell.serviceNamePlaceHolder.text = self.serviceBooked[indexPath.row].serviceTitle
        
        cell.servicePricePlaceHolder.text = ""
        cell.serviceTimePlaceHolder.text = ""
        
        if let priceOS = self.serviceBooked[indexPath.row].serviceCost, let timeOS = self.serviceBooked[indexPath.row].estimatedTime, let curr = self.serviceBooked[indexPath.row].localCurrency {
            cell.servicePricePlaceHolder.text = priceOS + curr
            cell.serviceTimePlaceHolder.text = timeOS + "min"
        } else {
            cell.servicePricePlaceHolder.text = ""
            cell.serviceTimePlaceHolder.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension CalendarViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.upperCollectionView {
            return self.customYearHolder.count
        } else {
            return self.customddatesholder.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.upperCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdUP", for: indexPath) as! customCalendarViewControllerCollectionViewCell
            if let yearIN = self.customYearHolder[safe: indexPath.row]?.yearStringValue {
               cell.openStartTimeHolder.text = yearIN
            } else {
                cell.openStartTimeHolder.text = nil
            }
            
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdDW", for: indexPath) as! customCalendarViewControllerLowerCollectionViewCell
            cell.monthOfTheYear.text = self.customddatesholder[indexPath.row].monthName
            if let amount = self.customddatesholder[safe: indexPath.row]?.amount, let currCode = self.currencyCode {
                
                cell.totalEarningsForTheMonth.text = String(describing: amount) + currCode
            } else {
                cell.totalEarningsForTheMonth.text = nil
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.upperCollectionView {
            return CGSize(width: 100, height: upperCollectView.frame.height)
        }else {
            return CGSize(width: lowerCollectView.frame.width, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.upperCollectionView {
            return 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.upperCollectionView {
            if let selectedCellUpper = self.customYearHolder[safe: indexPath.row], let yearInt = selectedCellUpper.yearIntValue {
                print(yearInt, "Timbala")
                self.handleUpperCollectionSelction(yearChoice: yearInt)
            }
        } else {
            if let selectedCellLower = self.customddatesholder[safe: indexPath.row]?.monthName {
                self.handleLowerCollectionViewSelection(monthSelected: selectedCellLower)
                
            }
        }
    }
}

extension ShoppingListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bookedServiceArry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customShoppingListCollectionViewCell
        cell.serviceNamePlaceHolder.text = self.bookedServiceArry[indexPath.row].serviceTitle
        cell.servicePricePlaceHolder.text = self.bookedServiceArry[indexPath.row].serviceCost
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customProfileSettingsCollectionViewCell
        cell.settingButtonTitlePlaceHolder.text = self.settingButtons[indexPath.row].settingsTitle
        cell.thumbnailImageView.image = self.settingButtons[indexPath.row].settingsIcon
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let profilesettingsedit = ProfileSettingsEditorViewController()
        navigationController?.pushViewController(profilesettingsedit, animated: true)
    }
}

extension ProfileSettingsEditorViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customProfileSettingsEditorCollectionViewCell
        cell.settingButtonTitlePlaceHolder.setTitle(self.settingButtons[indexPath.row].settingsTitle, for: .normal)
        cell.settingButtonTitlePlaceHolder.tag = indexPath.row
        cell.settingButtonTitlePlaceHolder.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.tag = indexPath.row
        cell.thumbnailImageView.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.setImage(self.settingButtons[indexPath.row].settingsIcon, for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension BarberProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIDBPRO", for: indexPath) as! customBarberShopProfileSettingsCollectionViewCell
        cell.settingButtonTitlePlaceHolder.text = self.settingButtons[indexPath.row].settingsTitle
        cell.thumbnailImageView.image = self.settingButtons[indexPath.row].settingsIcon
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let profilesettingsedit = BarberShopProfileSettingsViewController()
        navigationController?.pushViewController(profilesettingsedit, animated: true)
    }
}

extension BarberShopProfileSettingsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customBarbershopProfileSettingsEditorCollectionViewCell
        cell.settingButtonTitlePlaceHolder.setTitle(self.settingButtons[indexPath.row].settingsTitle, for: .normal)
        cell.settingButtonTitlePlaceHolder.tag = indexPath.row
        cell.settingButtonTitlePlaceHolder.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.tag = indexPath.row
        cell.thumbnailImageView.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.setImage(self.settingButtons[indexPath.row].settingsIcon, for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension BarberShopBarbersAndServicesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            return self.serviceOne.count
        }else {
            return self.barberlistone.count
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdd", for: indexPath) as! customBarbersBarberShopProfileCollectionViewCell
            cell.barberShopServiceNamePlaceHolder.text = self.serviceOne[indexPath.row].serviceTitle
            cell.barberShopServiceDescriptionPlaceHolder.text = self.serviceOne[indexPath.row].shortDescription
            cell.servicePricePlaceHolder.text = self.serviceOne[indexPath.row].serviceCost! + "-"
            cell.serviceTimeTakenPlaceHolder.text = self.serviceOne[indexPath.row].estimatedTime! + "min"
            cell.thumbnailImageView.image = UIImage()
            cell.barberShopCoverImageView.layer.borderColor = UIColor.clear.cgColor
            cell.barberShopServiceLogoImageView.image = self.serviceOne[indexPath.row].serviceImage
            
            return cell
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdd2", for: indexPath) as! customBarbersBarberShopProfileCollectionViewBarberSelectedCell
            cell.barberShopBarberLogoImageView.image = self.barberlistone[indexPath.row].profileImage
            cell.barberShopBarberNamePlaceHolder.text = self.barberlistone[indexPath.row].lastName
            cell.thumbnailImageView.image = UIImage()
            cell.barberShopBarberCoverImageView.layer.borderColor = UIColor.clear.cgColor
            if let vla = self.barberlistone[indexPath.row].ratingValue{
                cell.starView.configure(self.attribute, current: vla, max: 5)
            }
            
            if let stringTime = self.openingTimeString {
                cell.barberTimeToBeAvailablePlaceHolder.text = stringTime
            } else {
                cell.barberTimeToBeAvailablePlaceHolder.text = "closed today"
            }
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            guard let selSerID =  self.serviceOne[safe: indexPath.row]?.serviceID else {
                return
            }
            
            if self.selectedServiceOne.contains(selSerID) {
                print("No Show")
            }else {
                self.selectedServiceOne.append(selSerID)
            }
            
            switch self.selectedLadiesGentsKidsButton {
            case "Ladies":
                if self.ladiesSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.ladiesSelectedList.append(selSerID)
                }
            case "Gents":
                if self.gentsSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.gentsSelectedList.append(selSerID)
                }
            case "Kids":
                if self.kidsSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.kidsSelectedList.append(selSerID)
                }
            default:
                print("Sky high")
            }
            
            //print(self.ladiesSelectedList, "a")
            //print(self.gentsSelectedList, "b")
            //print(self.kidsSelectedList, "c")
            //print(self.selectedServiceOne, "Hell razar")
            
            self.handleMoveToTheNextView()
            
        } else {
            
            self.selectedBarberOne.removeAll()
            self.availableSelectedList.removeAll()
            self.topRatedSelectedList.removeAll()
            guard let selSerID =  self.barberlistone[safe: indexPath.row]?.uniqueID else {
                return
            }
            
            if self.selectedBarberOne.contains(selSerID) {
                print("No Show")
            }else {
                self.selectedBarberOne.append(selSerID)
            }
            
            if self.selectedBarberOne.count > 0 {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
            
            switch self.selectedAvailableTopRated {
            case "Available":
                if self.availableSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.availableSelectedList.append(selSerID)
                }
            case "Top Rated":
                if self.topRatedSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.topRatedSelectedList.append(selSerID)
                }
            default:
                print("Sky high")
            }
            
            //print(self.availableSelectedList, "c")
            //print(self.topRatedSelectedList, "d")
            //print(self.selectedBarberOne, "e")
            
            self.handleBarberMoveToNextView(barberUniqueID: selSerID)
            
        }
    }
    
    
}

extension PayOutViewControllerViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.payCollectionViewData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIDPayoout", for: indexPath) as! customPayoutCollectionViewCellBarbers
        
        cell.payoutKey.text = self.payCollectionViewData[indexPath.row].dataKey
        cell.payoutValue.text = self.payCollectionViewData[indexPath.row].dataValue
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension MonthlyPaymentViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.barberEarningsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdMOnthlyPayment", for: indexPath) as! customMonthlyPaymentCollectionViewCell
        
        cell.barberNamePlaceHolder.text = ""
        if let barberName = self.barberEarningsArray[safe: indexPath.row]?.barberStaffName {
            cell.barberNamePlaceHolder.text = barberName
        }else {
            cell.barberNamePlaceHolder.text = ""
        }
        
        cell.thumbnailImageView.image = UIImage()
        if let barberImg = self.barberEarningsArray[safe: indexPath.row]?.profileImage {
            cell.thumbnailImageView.image = barberImg
        } else {
            cell.thumbnailImageView.image = UIImage()
        }
        
        cell.totalAmountPlaceHolder.text = ""
        if let numTotal = self.barberEarningsArray[safe: indexPath.row]?.totalBarberEarning, let currCode = self.currentUserCurency {
            cell.totalAmountPlaceHolder.text = String(describing: numTotal) + currCode
        } else {
            cell.totalAmountPlaceHolder.text = ""
        }
        
        cell.totalNumberOfClientsPlaceHolder.text = ""
        if let numClients = self.barberEarningsArray[safe: indexPath.row]?.totalBarberNumbers {
            cell.totalNumberOfClientsPlaceHolder.text = String(describing: numClients)
        } else {
            cell.totalNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.creditCardAmountPlaceHolder.text = ""
        if let amCard = self.barberEarningsArray[safe: indexPath.row]?.cardPaymentAmount, let currCode = self.currentUserCurency {
            cell.creditCardAmountPlaceHolder.text = String(describing: amCard) + currCode
            
            self.creditCardAmount = Int(amCard)
            
        } else {
            cell.creditCardAmountPlaceHolder.text = ""
        }
        
        cell.creditCardNumberOfClientsPlaceHolder.text = ""
        if let numClients = self.barberEarningsArray[indexPath.row].cardPaymentNumber {
            cell.creditCardNumberOfClientsPlaceHolder.text = String(describing: numClients)
        } else {
            cell.creditCardNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.mobilePayAmountPlaceHolder.text = ""
        if let numMobile = self.barberEarningsArray[safe: indexPath.row]?.mobilePaymentAmount, let currCode = self.currentUserCurency  {
            print("Heror worship never ends", numMobile)
            cell.mobilePayAmountPlaceHolder.text = String(describing: numMobile) + currCode
            
            self.mobilePayAmount = Int(numMobile)
            
        } else {
            cell.mobilePayAmountPlaceHolder.text = ""
        }
        
        cell.mobilePayNumberOfClientsPlaceHolder.text = ""
        if let numClients = self.barberEarningsArray[indexPath.row].mobilePaymentNumber {
            cell.mobilePayNumberOfClientsPlaceHolder.text = String(describing: numClients)
        } else {
            cell.mobilePayNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.cashPaymentAmountPlaceHolder.text = ""
        if let numCash = self.barberEarningsArray[safe: indexPath.row]?.cashPaymentAmount , let currCode = self.currentUserCurency  {
            print("Heror worship begins", numCash)
            cell.cashPaymentAmountPlaceHolder.text = String(describing: numCash) + currCode
        } else {
            cell.cashPaymentAmountPlaceHolder.text = ""
        }
        
        cell.cashPaymentNumberOfClientsPlaceHolder.text = ""
        if let numCashClients = self.barberEarningsArray[safe: indexPath.row]?.cashPaymentNumber {
            cell.cashPaymentNumberOfClientsPlaceHolder.text = String(describing: numCashClients)
        } else {
            cell.cashPaymentNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.refundAmountPlaceHolder.text = ""
        if let numRefund = self.barberEarningsArray[safe: indexPath.row]?.totalRefundsAmount, let currCode = self.currentUserCurency  {
            cell.refundAmountPlaceHolder.text = String(describing: numRefund) + currCode
        } else {
            cell.refundAmountPlaceHolder.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 300) //260
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension AddServiceViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.barberShopBarbersAvailable.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customSelectBarbersCollectionViewCellBarbers
        cell.servicesAvailable.text = self.barberShopBarbersAvailable[indexPath.row].lastName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let selSerID =  barberShopBarbersAvailable[indexPath.row].uniqueID {
            self.selectedbarberShopBarbers.append(selSerID)
            
            if let selectedCell = collectionView.cellForItem(at: indexPath) as? customSelectBarbersCollectionViewCellBarbers {
                selectedCell.thumbnailImageView.image = UIImage(named: "check_box_active")
                print(self.selectedbarberShopBarbers.count)
                self.textFieldDidChange()
                
                if !self.notRegisteringANewBarberShop {
                    if self.selectedbarberShopBarbers.count > 0 {
                        self.textFieldDidChange()
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let selSer =  barberShopBarbersAvailable[indexPath.row].uniqueID  {
            if self.selectedbarberShopBarbers.count > 0{
                selectedbarberShopBarbers = selectedbarberShopBarbers.filter { $0 != selSer }
                
                if let selectedCell = collectionView.cellForItem(at: indexPath) as? customSelectBarbersCollectionViewCellBarbers  {
                    selectedCell.thumbnailImageView.image = UIImage(named: "check_box_inactive")
                    print(self.selectedbarberShopBarbers.count)
                    self.textFieldDidChange()
                    
                    if !self.notRegisteringANewBarberShop {
                        if self.selectedbarberShopBarbers.count > 0 {
                            self.textFieldDidChange()
                        }
                    }
                }
            }else {
                print("out of range")
            }
        }
    }
}


