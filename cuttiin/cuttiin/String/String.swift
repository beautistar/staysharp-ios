//
//  String.swift
//  Sarahah
//
//  Created by SevenStar Infotech on 18/08/17.
//  Copyright © 2017 SevenStar Infotech. All rights reserved.
//

import Foundation


extension String {
    
    func removeNull()->String {
        if(self.characters.count==0){
            return ""
        }
        else if(self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)=="<null>"||self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)=="(null)") {
            return ""
        }
        else {
            return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        }
    }
}
