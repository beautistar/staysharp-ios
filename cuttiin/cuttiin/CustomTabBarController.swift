//
//  CustomTabBarController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    var dataHold: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedIndex = 1
        
        let bookingController = BookingsViewController()
        let firstNavigationController = UINavigationController(rootViewController: bookingController)
        firstNavigationController.title = NSLocalizedString("bookingTabButtonCustomTabBarView", comment: "Bookings")
        firstNavigationController.tabBarItem.image = UIImage(named: "bookings_passive")
        
        let barberShopsController = BarberShopsViewController()
        let secondNavigationController = UINavigationController(rootViewController: barberShopsController)
        secondNavigationController.title = NSLocalizedString("barberShopTabbuttonCustomTabBarView", comment: "Shops")
        secondNavigationController.tabBarItem.image = UIImage(named: "barbers_appointments_passive")
        
        let profileController = ProfileViewController()
        let fourthNavigationController = UINavigationController(rootViewController: profileController)
        fourthNavigationController.title = NSLocalizedString("profileTabbuttonCustomTabBarView", comment: "Profile")
        fourthNavigationController.tabBarItem.image = UIImage(named: "profile_passive")
        
        viewControllers = [firstNavigationController, secondNavigationController, fourthNavigationController]
        
        tabBar.isTranslucent = false
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: 1000, height: 0.5)
        topBorder.backgroundColor = UIColor(r: 11, g: 49, b: 68).cgColor
        
        tabBar.layer.addSublayer(topBorder)
        tabBar.clipsToBounds = true
        tabBar.tintColor = UIColor(r: 118, g: 187, b: 220)
        tabBar.unselectedItemTintColor = UIColor.white
        tabBar.barTintColor = UIColor(r: 11, g: 49, b: 68)
    }
    
    override func viewWillLayoutSubviews() {
        var newTabBarFrame = tabBar.frame
        
        let newTabBarHeight: CGFloat = 60
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight
        
        tabBar.frame = newTabBarFrame
    }

}
