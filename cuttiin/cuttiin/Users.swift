//
//  User.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/14/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Users: NSObject {
    var firstName: String?
    var lastName: String?
    var email: String?
    var mobileNumber: String?
    var uniqueID: String?
    var role: String?
    var rating: String?
    var ratingValue: CGFloat?
    var currencyCode: String?
    var profileImageName: String?
    var profileImageUrl: String?
    var profileImage: UIImage?
    var barberShopID: String?
    var companyName: String?
    var companyLogoImageName: String?
    var companyLogoImageUrl: String?
    var companyCoverPhotoUrl: String?
    var companyCoverPhotoName: String?
    var cvrNumber: String?
    var accountNumerRegNr: String?
    var accountNumerKontoNr: String?
    var companyFormattedAddress: String?
    var companyAddressLongitude: String?
    var companyAddressLatitude: String?
    var dateAccountCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
    var cardID: String?
    var cardDateCreated: String?
    var cardDateCalendar: String?
    var cardDateTimeZone: String?
    var cardDateLocale: String?
    var cardHasBeenDeleted: String?
    var authentication: String?
    var isDeleted: String?
    var emailSent: String?
    
    func getUserAccountRole() -> String{
        guard let newRole = self.role else {
            return "nil"
        }
        
        return newRole
    }
    
}


