//
//  VerifyLocales.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 14/09/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

import SwiftDate

class VerifyLocales {
    
    static func checkForUnavailableLocale() -> Bool {
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        switch userCalender {
        case "gregorian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
            
        case "buddhist":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "chinese":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "coptic":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "ethiopicAmeteMihret":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "hebrew":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "iso8601":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "indian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "islamic":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "islamicCivil":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "japanese":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "persian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "republicOfChina":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "islamicTabular":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        case "islamicUmmAlQura":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        default:
            print("Sky High")
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                return true
            } else {
                return false
            }
        }
    }
    
}
