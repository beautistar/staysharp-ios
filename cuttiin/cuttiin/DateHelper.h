//
//  DateHelper.h
//  cuttiin
//
//  Created by Sevenstar Infotech on 01/06/18.
//  Copyright © 2018 teckdk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject


+(NSMutableArray *)checkTimeBetweenTwoTimes:(int)openHour openMinute:(int)openMinute closeHour:(int)closeHour closeMinute:(int)closeMinute selectedDate:(NSDate *)selectedDate;


+(BOOL)checkTimeBetweenTwoTime:(int)openHour openMinute:(int)openMinute closeHour:(int)closeHour closeMinute:(int)closeMinute selectedDate:(NSDate *)selectedDate;



@end
