//
//  DateHelper.m
//  cuttiin
//
//  Created by Sevenstar Infotech on 01/06/18.
//  Copyright © 2018 teckdk. All rights reserved.
//

#import "DateHelper.h"

@implementation DateHelper


+(NSMutableArray *)checkTimeBetweenTwoTimes:(int)openHour openMinute:(int)openMinute closeHour:(int)closeHour closeMinute:(int)closeMinute selectedDate:(NSDate *)selectedDate {
    
    
    NSDateComponents *openingTime = [[NSDateComponents alloc] init];
    openingTime.hour = openHour;
    openingTime.minute = openMinute;
    
    NSDateComponents *closingTime = [[NSDateComponents alloc] init];
    closingTime.hour = closeHour;
    closingTime.minute = closeMinute;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"hh:mm"];
    
    NSString *nowTimeString = [formatter stringFromDate:selectedDate];
    
    NSDate *now = [formatter dateFromString:nowTimeString];
    
    NSDateComponents *currentTime = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute/*|NSCalendarUnitSecond*/ fromDate:now];
    
    NSMutableArray *times = [@[openingTime, closingTime/*, currentTime*/] mutableCopy];
    [times sortUsingComparator:^NSComparisonResult(NSDateComponents *t1, NSDateComponents *t2) {
        if (t1.hour > t2.hour) {
            return NSOrderedDescending;
        }
        
        if (t1.hour < t2.hour) {
            return NSOrderedAscending;
        }
        // hour is the same
        if (t1.minute > t2.minute) {
            return NSOrderedDescending;
        }
        
        if (t1.minute < t2.minute) {
            return NSOrderedAscending;
        }
        // hour and minute are the same
        if (t1.second > t2.second) {
            return NSOrderedDescending;
        }
        
        if (t1.second < t2.second) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];
    
    return times;
}


+(BOOL)checkTimeBetweenTwoTime:(int)openHour openMinute:(int)openMinute closeHour:(int)closeHour closeMinute:(int)closeMinute selectedDate:(NSDate *)selectedDate {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate: selectedDate];
    NSInteger currentHour = [components hour];
    NSInteger currentMinutes = [components minute];
    
    if (((currentHour >= openHour) && (currentHour <= closeHour)) && ((currentMinutes >= openMinute) && (currentMinutes<=closeMinute))) {
        
        return YES;
    }
    else {
        if (((currentHour > openHour) && (currentHour < closeHour)) || ((currentHour == openHour) && (currentMinutes >= openMinute)) || ((currentHour == closeHour) && (currentMinutes<=closeMinute))) {

            return YES;
        }
        else {
            return NO;
        }
    }
}

@end
