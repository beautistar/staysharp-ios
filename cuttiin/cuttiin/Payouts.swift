//
//  Payouts.swift
//  cuttiin
//
//  Created by Umoru Joseph on 10/12/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Payouts: NSObject {
    var payoutID: String?
    var barberShopID: String?
    var amountToPayOut: String?
    var regNR: String?
    var kontorNr: String?
    var datePayoutCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
    var emailSent: String?
}
