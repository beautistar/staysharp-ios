//
//  LoginViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/14/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import Navajo_Swift
import FBSDKLoginKit

class LoginViewController: UIViewController {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var passwordValid = false
    let loginButton = FBSDKLoginButton()
    var welcomeviewholder: WelcomeViewController?
    
    let inputsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.keyboardType = .emailAddress
        em.autocapitalizationType = .none
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    let passwordHiddenPlaceHolder: UILabel = {
        let php = UILabel()
        php.translatesAutoresizingMaskIntoConstraints = false
        php.text = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
        php.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        php.textColor = UIColor(r: 129, g: 129, b: 129)
        php.isHidden = true
        php.adjustsFontSizeToFitWidth = true
        php.minimumScaleFactor = 0.1
        php.baselineAdjustment = .alignCenters
        php.textAlignment = .left
        return php
    }()
    
    let passwordTextField: UITextField = {
        let ps = UITextField()
        ps.translatesAutoresizingMaskIntoConstraints = false
        ps.textColor = UIColor.black
        ps.placeholder = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
        ps.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ps.isSecureTextEntry = true
        ps.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingDidBegin)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingChanged)
        return ps
    }()
    
    lazy var showPassordTextFieldButton: UIButton = {
        let sptfb = UIButton()
        sptfb.translatesAutoresizingMaskIntoConstraints = false
        sptfb.isUserInteractionEnabled = false
        sptfb.setTitle(NSLocalizedString("showButtonTitleLoginView", comment: "Show"), for: .normal)
        sptfb.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        sptfb.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 9)
        sptfb.backgroundColor = UIColor.white
        sptfb.addTarget(self, action: #selector(handleShowPasswordText), for: .touchUpInside)
        return sptfb
    }()
    
    let passwordSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return view
    }()
    
    lazy var signInButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("logInButtonTitleWelcomeView", comment: "LOG IN"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor.black.cgColor
        st.addTarget(self, action: #selector(handleSignInIntoAccount), for: .touchUpInside)
        st.isEnabled = false
        return st
    }()
    
    lazy var forgotPasswordButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("forgotPasswordButtonTitleLoginView", comment: "Forgot your password? Reset"), for: .normal)
        st.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 13.0)
        st.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        st.addTarget(self, action: #selector(handleForgotPassword), for: .touchUpInside)
        return st
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        emhp.textColor = UIColor(r: 129, g: 129, b: 129)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("loginTextNavigationTitle", comment: "Login")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(inputsContainerView)
        view.addSubview(signInButton)
        view.addSubview(forgotPasswordButton)
        view.addSubview(errorMessagePlaceHolder)
        setupInputContainerView()
        setupButtons()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleBackAction(){
        self.hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    //new entry controls
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.emailTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.emailTextField.text = ""
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordTextField.text = ""
        self.passwordSeperatorView.backgroundColor = UIColor.black
    }
    
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleShowPasswordText(){
        passwordTextField.isSecureTextEntry = false
    }
    
    @objc func handleSignInIntoAccount(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            return
        }
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, errorr) in
            if let error = errorr {
                print(error.localizedDescription)
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let errCode = AuthErrorCode(rawValue: error._code){
                    switch errCode {
                    case .networkError:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                    case .invalidCredential:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("invalidCredentialsErrorCodeLoginView", comment: "login credentials supplied is invalid")
                    case .invalidEmail:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAddressMalformedErrorCodeLoginView", comment: "email address is malformed")
                    case .operationNotAllowed:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care")
                    case .emailAlreadyInUse:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAlreadyInUseErrorCodeLoginView", comment: "email already in use by an existing account")
                    case .userDisabled:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care")
                    case .wrongPassword:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("wrongPasswordErrorCodeLoginView", comment: "Attempting to sign in with wrong password")
                    case .userNotFound:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("accountNotFoundEasilyErrorCodeLoginView", comment: "Account disabled please contact Customer care - mising link")
                    default:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                    
                    }
                }
                return
            }
            
            UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.hideKeyboard()
            self.handleDismissViewProperly()
        })
    }
    
    func handleDismissViewProperly(){
        dismiss(animated: false, completion: nil)
    }
    
    @objc func handleForgotPassword(){
        let daytimeChoice = ChangePasswordViewController()
        navigationController?.pushViewController(daytimeChoice, animated: true)
    }
    
    @objc func textFieldDidChange(){
        if emailTextField.text == "" || passwordTextField.text == "" {
            //Disable button
            self.showPassordTextFieldButton.isUserInteractionEnabled = true
            self.passwordTextField.isSecureTextEntry = true
            signInButton.isEnabled = false
            signInButton.setTitleColor(UIColor.black, for: .normal)
            signInButton.layer.borderColor = UIColor.black.cgColor
            signInButton.backgroundColor = UIColor.clear
            
        } else {
            //Enable button
            if emailValid == true {
                signInButton.isEnabled = true
                signInButton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
                signInButton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
                signInButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                self.showPassordTextFieldButton.isUserInteractionEnabled = true
                
            }
        }
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.emailHiddenPlaceHolder.isHidden = false
        self.passwordHiddenPlaceHolder.isHidden = true
        
    }
    
    @objc func passwordtextFieldDidChange(){
        self.passwordSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = false
    }
    
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            signInButton.isEnabled = true
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        }else{
            signInButton.isEnabled = false
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailNotFormattedProperlyPlaceholderLoginView", comment: "Email is not properly formatted")
        }
    }
    
    func setupInputContainerView(){
        inputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        inputsContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        
        inputsContainerView.addSubview(emailHiddenPlaceHolder)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeperatorView)
        inputsContainerView.addSubview(passwordHiddenPlaceHolder)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(showPassordTextFieldButton)
        inputsContainerView.addSubview(passwordSeperatorView)
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        passwordHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        passwordHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        passwordHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        passwordHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: passwordHiddenPlaceHolder.bottomAnchor).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: inputsContainerView.rightAnchor, constant: -20).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        showPassordTextFieldButton.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        showPassordTextFieldButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        showPassordTextFieldButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        passwordSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordSeperatorView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        passwordSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        passwordSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
    }
    
    func setupButtons(){
        signInButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 10).isActive = true
        signInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signInButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        signInButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        forgotPasswordButton.topAnchor.constraint(equalTo: signInButton.bottomAnchor, constant: 10).isActive = true
        forgotPasswordButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        forgotPasswordButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12)
        forgotPasswordButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: forgotPasswordButton.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
    }

}
