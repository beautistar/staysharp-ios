//
//  BarberProfileViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate
import FBSDKLoginKit
import MGStarRatingView

class BarberProfileViewController: UIViewController, StarRatingDelegate {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var settingButtons = [Settings]()
    var calledOnceHolder = true
    var userRoleGotten: String?
    var barberRatingCounterHold = [Ratings]()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showEditProfileView))
        tcview.addGestureRecognizer(tapGesture)
        return tcview
    }()
    
    let barberShopCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "babershop0")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    let lightDarkShadeBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.clear
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let userNameAndTextInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    let userNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let userNameDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let numberOfReviewsPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let editProfilePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = "View and Edit Profile"
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 9)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customBarberShopProfileSettingsCollectionViewCell.self, forCellWithReuseIdentifier: "cellIDBPRO")
        return cv
    }()
    
    let buildVersionDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("navigationTitleTextCustomerProfileView", comment: "Profile")
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.delegate = self
        view.addSubview(upperInputsContainerView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjects()
        settingViewAlign()
        handleShowUserData()
        calledOnceHolder = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !calledOnceHolder {
            self.handleShowUserData()
        }
        calledOnceHolder = false
        
        if let userIDDXX = Auth.auth().currentUser?.uid {
            //Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            self.buildVersionDescriptionPlaceHolder.text = NSLocalizedString("userSpecialIDProfileView", comment: "Unique ID: ") + userIDDXX
        }
        
        if Auth.auth().currentUser?.uid == nil {
            self.tabBarController?.selectedIndex = 1
            let welcomeviewcontroller = WelcomeViewController()
            self.present(welcomeviewcontroller, animated: true, completion: nil)
        }
    }
    
    func handleShowUserData(){
        DispatchQueue.global(qos: .background).async {
            if let userUID = Auth.auth().currentUser?.uid {
                let firebaseUserReference = Database.database().reference()
                firebaseUserReference.child("users").child(userUID).observeSingleEvent(of: .value, with: { (snapshotUserData) in
                    if let dictionaryUserData = snapshotUserData.value as? [String: AnyObject] {
                        
                        
                        let BarberShopCOnfirm = HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryUserData)
                        let barberConfirm = HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryUserData)
                        
                        
                        if BarberShopCOnfirm {
                            let userDataNeeded = BarberShop()
                            userDataNeeded.setValuesForKeys(dictionaryUserData)
                            
                            if let shopRating = userDataNeeded.rating, let userRole = userDataNeeded.role, let rateInt = Int(shopRating), userRole == "barberShop", let companyName = userDataNeeded.companyName, let address = userDataNeeded.companyFormattedAddress, let companyLogo = userDataNeeded.companyLogoImageUrl {
                                
                                
                                self.userRoleGotten = "barberShop"
                                let rateFloat = CGFloat(integerLiteral: rateInt)
                                self.starView.configure(self.attribute, current: rateFloat, max: 5)
                                DispatchQueue.main.async {
                                    self.userNamePlaceHolder.text = companyName
                                    self.userNameDescriptionPlaceHolder.text = address
                                    self.selectImageView.loadImagesUsingCacheWithUrlString(urlString: companyLogo)
                                }
                            }
                            
                        } else if barberConfirm {
                            let userBarberDataNeeded = Barber()
                            userBarberDataNeeded.setValuesForKeys(dictionaryUserData)
                            
                            if  userBarberDataNeeded.role == "barberStaff", let barberShopID = userBarberDataNeeded.barberShopID, let imageURL = userBarberDataNeeded.profileImageUrl, let firstName = userBarberDataNeeded.firstName, let lastName = userBarberDataNeeded.lastName {
                                self.userRoleGotten = "barberStaff"
                                
                                DispatchQueue.main.async {
                                    self.countNumberOfReviewsForBarbers(shopID: barberShopID)
                                    self.selectImageView.loadImagesUsingCacheWithUrlString(urlString: imageURL)
                                    self.userNamePlaceHolder.text = firstName + " " + lastName
                                }
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func countNumberOfReviews(shopID: String){
        DispatchQueue.global(qos: .background).async {
            let firebaseRatingCounter = Database.database().reference()
            firebaseRatingCounter.child("rating").child(shopID).observeSingleEvent(of: .value, with: { (snapshotNumberOfRating) in
                if let dictionaryNumberOfRating = snapshotNumberOfRating.value as? [String: AnyObject] {
                    let numberOFRating = dictionaryNumberOfRating.count
                    DispatchQueue.main.async {
                        self.numberOfReviewsPlaceHolder.text = "\(numberOFRating) reviews"
                    }
                } else {
                    DispatchQueue.main.async {
                        self.numberOfReviewsPlaceHolder.text = "0 reviews"
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func countNumberOfReviewsForBarbers(shopID: String){
        DispatchQueue.global(qos: .background).async {
            
            if let barberUniqueID = Auth.auth().currentUser?.uid {
                self.barberRatingCounterHold.removeAll()
                let firebaseRatingCounter = Database.database().reference()
                firebaseRatingCounter.child("rating").child(shopID).observeSingleEvent(of: .value, with: { (snapshotNumberOfRating) in
                    if let dictionaryNumberOfRating = snapshotNumberOfRating.value as? [String: AnyObject] {
                        
                        for ratingSingleValue in dictionaryNumberOfRating {
                            if let ratingObjectSingle = ratingSingleValue.value as? [String: AnyObject], HandleDataRequest.handleRatingsNode(firebaseData: ratingObjectSingle){
                                let ratingObjectHolder = Ratings()
                                ratingObjectHolder.setValuesForKeys(ratingObjectSingle)
                                
                                if let _ = ratingObjectHolder.barberRatingVale, let uniqueValue = ratingObjectHolder.barberID {
                                    if barberUniqueID == uniqueValue {
                                        self.barberRatingCounterHold.append(ratingObjectHolder)
                                        let numberOFRating = self.barberRatingCounterHold.count
                                        DispatchQueue.main.async {
                                            self.numberOfReviewsPlaceHolder.text = "\(numberOFRating) reviews"
                                        }
                                    }
                                } else {
                                    let numberOFRating = self.barberRatingCounterHold.count
                                    DispatchQueue.main.async {
                                        self.numberOfReviewsPlaceHolder.text = "\(numberOFRating) reviews"
                                    }
                                }
                                
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.numberOfReviewsPlaceHolder.text = "0 reviews"
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    
    @objc func showEditProfileView(){
        if let gotRole = self.userRoleGotten {
            let baberprofileEdit = BarberProfileEditorViewController()
            baberprofileEdit.userRoleFromProfile = gotRole
            self.navigationController?.pushViewController(baberprofileEdit, animated: true)
        } else {
            print("Manya")
        }
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
        print(value)
    }
    
    func setupViewObjects(){
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 140).isActive = true
        
        upperInputsContainerView.addSubview(barberShopCoverImageView)
        
        barberShopCoverImageView.topAnchor.constraint(equalTo: upperInputsContainerView.topAnchor).isActive = true
        barberShopCoverImageView.centerXAnchor.constraint(equalTo: upperInputsContainerView.centerXAnchor).isActive = true
        barberShopCoverImageView.widthAnchor.constraint(equalTo: upperInputsContainerView.widthAnchor).isActive = true
        barberShopCoverImageView.heightAnchor.constraint(equalTo: upperInputsContainerView.heightAnchor, multiplier: 1).isActive = true
        
        barberShopCoverImageView.addSubview(lightDarkShadeBackgroundView)
        
        
        lightDarkShadeBackgroundView.topAnchor.constraint(equalTo: barberShopCoverImageView.topAnchor).isActive = true
        lightDarkShadeBackgroundView.centerXAnchor.constraint(equalTo: barberShopCoverImageView.centerXAnchor).isActive = true
        lightDarkShadeBackgroundView.widthAnchor.constraint(equalTo: barberShopCoverImageView.widthAnchor).isActive = true
        lightDarkShadeBackgroundView.heightAnchor.constraint(equalTo: barberShopCoverImageView.heightAnchor, multiplier: 1).isActive = true
        
        lightDarkShadeBackgroundView.addSubview(selectImageView)
        lightDarkShadeBackgroundView.addSubview(starView)
        lightDarkShadeBackgroundView.addSubview(numberOfReviewsPlaceHolder)
        lightDarkShadeBackgroundView.addSubview(userNamePlaceHolder)
        lightDarkShadeBackgroundView.addSubview(userNameDescriptionPlaceHolder)
        lightDarkShadeBackgroundView.addSubview(editProfilePlaceHolder)
        lightDarkShadeBackgroundView.addSubview(buildVersionDescriptionPlaceHolder)
        
        selectImageView.topAnchor.constraint(equalTo: lightDarkShadeBackgroundView.topAnchor, constant: 5).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: lightDarkShadeBackgroundView.leftAnchor, constant: 24).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: lightDarkShadeBackgroundView.widthAnchor, multiplier: 0.25, constant: 5).isActive = true
        selectImageView.bottomAnchor.constraint(equalTo: lightDarkShadeBackgroundView.bottomAnchor, constant: -10).isActive = true
        
        userNamePlaceHolder.topAnchor.constraint(equalTo: lightDarkShadeBackgroundView.topAnchor, constant: 5).isActive = true
        userNamePlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        userNamePlaceHolder.rightAnchor.constraint(equalTo: lightDarkShadeBackgroundView.rightAnchor).isActive = true
        userNamePlaceHolder.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        userNameDescriptionPlaceHolder.topAnchor.constraint(equalTo: userNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        userNameDescriptionPlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        userNameDescriptionPlaceHolder.rightAnchor.constraint(equalTo: lightDarkShadeBackgroundView.rightAnchor, constant: -10).isActive = true
        userNameDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        starView.topAnchor.constraint(equalTo: userNameDescriptionPlaceHolder.bottomAnchor, constant: 10).isActive = true
        starView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        starView.rightAnchor.constraint(equalTo: lightDarkShadeBackgroundView.rightAnchor).isActive = true
        starView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        numberOfReviewsPlaceHolder.topAnchor.constraint(equalTo: starView.bottomAnchor, constant: 10).isActive = true
        numberOfReviewsPlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        numberOfReviewsPlaceHolder.rightAnchor.constraint(equalTo: lightDarkShadeBackgroundView.rightAnchor, constant: -120).isActive = true
        numberOfReviewsPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        editProfilePlaceHolder.topAnchor.constraint(equalTo: starView.bottomAnchor, constant: 10).isActive = true
        editProfilePlaceHolder.leftAnchor.constraint(equalTo: numberOfReviewsPlaceHolder.rightAnchor, constant: 10).isActive = true
        editProfilePlaceHolder.rightAnchor.constraint(equalTo: lightDarkShadeBackgroundView.rightAnchor, constant: -5).isActive = true
        editProfilePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        
        buildVersionDescriptionPlaceHolder.topAnchor.constraint(equalTo: numberOfReviewsPlaceHolder.bottomAnchor, constant: 5).isActive = true
        buildVersionDescriptionPlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        buildVersionDescriptionPlaceHolder.rightAnchor.constraint(equalTo: lightDarkShadeBackgroundView.rightAnchor).isActive = true
        buildVersionDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 13).isActive = true
        
        
        collectView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    func settingViewAlign(){
        let value = Settings()
        value.settingsTitle = NSLocalizedString("settingButtonCollectionViewProfileView", comment: "Settings")
        value.settingsIcon = UIImage(named: "settings")
        self.settingButtons.append(value)
    }

}

class customBarberShopProfileSettingsCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let settingButtonTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        fnhp.text = "one hand"
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(settingButtonTitlePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|-16-[v0(150)][v1(50)]-16-|", views: settingButtonTitlePlaceHolder, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: settingButtonTitlePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
