//
//  PayOutCollectionDataHolder.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/30/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class PayOutCollectionDataHolder: NSObject {
    var dataKey: String?
    var dataValue: String?
}
