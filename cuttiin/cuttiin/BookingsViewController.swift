//
//  BookingsViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import SwiftDate

class BookingsViewController: UIViewController {
    var customerMadeBookings = [Bookings]()
    var customerMadeBookingsIsCompleted = [Bookings]()
    var barberShopBookNames = [String]()
    var indexValueMain = 1
    
    var bookingSelectedStartTime = [String]()
    var bookingSelectedCalendar = [String]()
    var bookingSelectedTimeZone = [String]()
    var bookingSelectedLocal = [String]()
    var bookingSelectedTotalTime = [String]()
    var bookingSelectedServiceImageUrl = [String]()
    var bookingSelectedBarberShopName = [String]()
    var bookingSelectedBookingUniqueID = [String]()
    var bookingSelectedBarberShopUniqueID = [String]()
    var appointmentsCustomer = [AppointmentsCustomer]()
    var appointmentsCustomerNewData = [AppointmentsCustomer]()
    var arrayLengthHolder = [Int]()
    
    lazy var upcomingAndCompletedSegmentedControl: UISegmentedControl = {
        let sssegmentcontrol = UISegmentedControl(items: [NSLocalizedString("upcomingTitleSegmentChoosePhotoView", comment: "UPCOMING"), NSLocalizedString("completedTitleSegmentChoosePhotoView", comment: "COMPLETED")])
        sssegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        sssegmentcontrol.tintColor = UIColor.white
        sssegmentcontrol.selectedSegmentIndex = 0
        sssegmentcontrol.addTarget(self, action: #selector(handleSegmentedControlSelection), for: .valueChanged)
        return sssegmentcontrol
    }()
    
    let badgeLabelBooking: UILabel = {
        let bdge = UILabel()
        bdge.translatesAutoresizingMaskIntoConstraints = false
        //bdge.text = "15"
        bdge.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        bdge.textColor = UIColor.white
        bdge.adjustsFontSizeToFitWidth = true
        bdge.minimumScaleFactor = 0.1
        bdge.baselineAdjustment = .alignCenters
        bdge.textAlignment = .center
        bdge.layer.cornerRadius = 12.5
        bdge.layer.masksToBounds = true
        //bdge.backgroundColor = UIColor.red
        return bdge
    }()
    
    let badgeLabelUpcoming: UILabel = {
        let bdge = UILabel()
        bdge.translatesAutoresizingMaskIntoConstraints = false
        //bdge.text = "19"
        bdge.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        bdge.textColor = UIColor.white
        bdge.adjustsFontSizeToFitWidth = true
        bdge.minimumScaleFactor = 0.1
        bdge.baselineAdjustment = .alignCenters
        bdge.textAlignment = .center
        bdge.layer.cornerRadius = 12.5
        bdge.layer.masksToBounds = true
        //bdge.backgroundColor = UIColor.red
        return bdge
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.alwaysBounceVertical = true
        cv.register(customUpcomingAndCompletedCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        cv.register(customUpcomingAndCompletedCollectionViewCellIsCompleted.self, forCellWithReuseIdentifier: "cellIdIsCompleted")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()
    
    var dictionaryCustomerBookingCount = 0
    var totalCount = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Bookings"
        UserDefaults.standard.set(false, forKey: "theBookingProcessIsComplete")
        self.tabBarController?.selectedIndex = 1
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        view.addSubview(upcomingAndCompletedSegmentedControl)
        view.addSubview(badgeLabelBooking)
        view.bringSubview(toFront: badgeLabelBooking)
        view.addSubview(badgeLabelUpcoming)
        view.bringSubview(toFront: badgeLabelUpcoming)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        getBookingDataAsAppointments()
        getBookingDataAsAppointmentsIsCompleted()
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        print("ro money call")
        getBookingDataAsAppointments()
        getBookingDataAsAppointmentsIsCompleted()
        sender.endRefreshing()
    }
    
    func getBookingDataAsAppointments(){
        
        DispatchQueue.global(qos: .background).async {
            if let customerUUID = Auth.auth().currentUser?.uid {
                let firebaseReferenceCustomerBookingHold = Database.database().reference()
                
                
                
                firebaseReferenceCustomerBookingHold.child("customer-bookings").child(customerUUID).observeSingleEvent(of: .value, with: { (snapshootDataCustomerBooking) in
                    
                    self.bookingSelectedStartTime.removeAll()
                    self.bookingSelectedCalendar.removeAll()
                    self.bookingSelectedTimeZone.removeAll()
                    self.bookingSelectedLocal.removeAll()
                    self.bookingSelectedTotalTime.removeAll()
                    self.bookingSelectedServiceImageUrl.removeAll()
                    self.bookingSelectedBarberShopName.removeAll()
                    self.bookingSelectedBookingUniqueID.removeAll()
                    self.bookingSelectedBarberShopUniqueID.removeAll()
                    
                    
                    //self.appointmentsCustomer.removeAll()
                    self.appointmentsCustomerNewData.removeAll()
                    
                    if let dictionaryCustomerBooking = snapshootDataCustomerBooking.value as? [String: AnyObject] {
                        let numberOfBookings = dictionaryCustomerBooking.count
                        var numberOfBookingsEquator = 0
                        
                        for custom in dictionaryCustomerBooking {
                            numberOfBookingsEquator = numberOfBookingsEquator + 1
                            if let customSingle = custom.value as? [String: AnyObject] {
                                
                                
                                let customerBookingConfirm = HandleDataRequest.handleCustomerBookingsNode(firebaseData: customSingle)
                                
                                if customerBookingConfirm {
                                    let customerbooking = CustomerBookings()
                                    customerbooking.setValuesForKeys(customSingle)
                                    
                                    if let bookIDD = customerbooking.bookingID, let barberShop = customerbooking.barberShopID {
                                        let firbaseDatabaseReference =  Database.database().reference()
                                        firbaseDatabaseReference.child("bookings").child(barberShop).child(bookIDD).observeSingleEvent(of: .value, with: { (snapshotBookingDataView) in
                                            
                                            
                                            if let dictionaryBooking = snapshotBookingDataView.value as? [String: AnyObject] {
                                                
                                                let bookingCOnfirm = HandleDataRequest.handleBookingsNode(firebaseData: dictionaryBooking)
                                                
                                                if bookingCOnfirm {
                                                    let bookingTemHolder = Bookings()
                                                    bookingTemHolder.setValuesForKeys(dictionaryBooking)
                                                    
                                                    //make data processing
                                                    if let customerIDXOX = bookingTemHolder.customerID, let userIDXOX = Auth.auth().currentUser?.uid, let payedBook = bookingTemHolder.paymentID, let barberIDX = bookingTemHolder.bookedBarberID, let bookStartString =  bookingTemHolder.bookingStartTime, let bookClandar = bookingTemHolder.calendar, let bookTizne = bookingTemHolder.timezone, let bookLocal = bookingTemHolder.local, let bookServiceUUID = bookingTemHolder.bookedServiceID, let bookkUniqueIDX = bookingTemHolder.bookingID, let bookTotalTmx =  bookingTemHolder.ConfirmedTotalTime, let shopID = bookingTemHolder.bookedBarberShopID, let isCancelled = bookingTemHolder.bookingCancel {
                                                        
                                                        let verifyDateData = VerifyDateDetails.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal)
                                                        let verifyDateStartTime = VerifyDateAssociation.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal, dateData: bookStartString)
                                                        
                                                        if verifyDateData == true && verifyDateStartTime == true {
                                                            if customerIDXOX == userIDXOX && payedBook != "nil" && isCancelled == "NO" {
                                                                
                                                                let firbaseDatabaseReferencePaymentInstantChecker =  Database.database().reference()
                                                                
                                                                if shopID != "" && payedBook != "nil" && payedBook != "" {
                                                                    firbaseDatabaseReferencePaymentInstantChecker.child("payments").child(shopID).child(payedBook).observeSingleEvent(of: .value, with: { (snapshotPayXXccCC) in
                                                                        
                                                                        if let dictionayRefund = snapshotPayXXccCC.value as? [String: AnyObject] {
                                                                            
                                                                            let paymentCinfirm = HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund)
                                                                            
                                                                            if paymentCinfirm {
                                                                                let paymentHold = Payments()
                                                                                
                                                                                paymentHold.setValuesForKeys(dictionayRefund)
                                                                                print("teckno baby")
                                                                                if let payConfirm = paymentHold.payment_aq_status_msg, let payCapture = paymentHold.capture_aq_status_msg {
                                                                                    if payConfirm == "Approved" && payCapture == "not available" {
                                                                                        
                                                                                        let firbaseDatabaseReferenceBarberDetailsBarberName =  Database.database().reference()
                                                                                        firbaseDatabaseReferenceBarberDetailsBarberName.child("users").child(barberIDX).observeSingleEvent(of: .value, with: { (snapshotUserDataHold) in
                                                                                            if let dictionaryDataHoldBarberName = snapshotUserDataHold.value as? [String: AnyObject] {
                                                                                                
                                                                                                let barberConfirm = HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryDataHoldBarberName)
                                                                                                
                                                                                                if barberConfirm {
                                                                                                    let userDataHold = Barber()
                                                                                                    userDataHold.setValuesForKeys(dictionaryDataHoldBarberName)
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    if let barberShopName = userDataHold.barberShopID {
                                                                                                        let firbaseDatabaseReferenceBarberShopOwnerDetails =  Database.database().reference()
                                                                                                        firbaseDatabaseReferenceBarberShopOwnerDetails.child("users").child(barberShopName).observeSingleEvent(of: .value, with: { (snapshotUserShopName) in
                                                                                                            if let dictionaryShopUserDataHold = snapshotUserShopName.value as? [String: AnyObject] {
                                                                                                                
                                                                                                                let barberShopConfirm = HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryShopUserDataHold)
                                                                                                                
                                                                                                                if barberShopConfirm {
                                                                                                                    let shopOwnerHold = BarberShop()
                                                                                                                    shopOwnerHold.setValuesForKeys(dictionaryShopUserDataHold)
                                                                                                                    
                                                                                                                    if let bookedBarberShopNameHold = shopOwnerHold.companyName {
                                                                                                                        let firbaseDatabaseServiceImageReferencServieDetails =  Database.database().reference()
                                                                                                                        
                                                                                                                        
                                                                                                                        firbaseDatabaseServiceImageReferencServieDetails.child("service").child(barberShopName).child(bookServiceUUID).observeSingleEvent(of: .value, with: { (snapshoototService) in
                                                                                                                            
                                                                                                                            self.totalCount = self.totalCount + 1
                                                                                                                            
                                                                                                                            if self.totalCount >= self.dictionaryCustomerBookingCount {
                                                                                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                                                                                                                                })
                                                                                                                            }
                                                                                                                            else {
                                                                                                                                self.perform(#selector(self.hideLoader), with: self, afterDelay: 10.0)
                                                                                                                            }
                                                                                                                            
                                                                                                                            if let dictionaryServiceDetails = snapshoototService.value as? [String: AnyObject] {
                                                                                                                                
                                                                                                                                let serviceCOnfirm = HandleDataRequest.handleServiceNode(firebaseData: dictionaryServiceDetails)
                                                                                                                                
                                                                                                                                if serviceCOnfirm {
                                                                                                                                    let serviceDatHold = Service()
                                                                                                                                    serviceDatHold.setValuesForKeys(dictionaryServiceDetails)
                                                                                                                                    if let serviceImageURL = serviceDatHold.serviceImageUrl {
                                                                                                                                        self.arrayDefaultCombineCustomerBooking(serviceImageURL: serviceImageURL, bookedBarberShopName: bookedBarberShopNameHold, bookingUniqueIDDD: bookkUniqueIDX, bookingTotalTime: bookTotalTmx, bookStartTSS: bookStartString, bookTzone: bookTizne, bookClandar: bookClandar, bookLocal: bookLocal, shopUniqueID: barberShopName)
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                self.totalCount = self.totalCount + 1
                                                                                                                            }
                                                                                                                        }, withCancel: nil)
                                                                                                                    } else {
                                                                                                                        self.totalCount = self.totalCount + 1
                                                                                                                    }
                                                                                                                }
                                                                                                                
                                                                                                                
                                                                                                            } else {
                                                                                                                self.totalCount = self.totalCount + 1
                                                                                                            }
                                                                                                        }, withCancel: nil)
                                                                                                    } else {
                                                                                                        self.totalCount = self.totalCount + 1
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                self.totalCount = self.totalCount + 1
                                                                                            }
                                                                                        }, withCancel: nil)
                                                                                    } else {
                                                                                        self.totalCount = self.totalCount + 1
                                                                                    }
                                                                                }
                                                                            }
                                                                            
                                                                        }//from here
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                        })
                                        
                                    }
                                }
                            }
                        }
                        if numberOfBookingsEquator == numberOfBookings {
                            if !dictionaryCustomerBooking.isEmpty {
                                print("red head")
                                self.perform(#selector(self.searchAndDeleteOldArrayItems), with: self, afterDelay: 3)
                            }
                        }
                    }  else {
                    }
                })
                
            }
        }
    }
    
    @objc func hideLoader() {
    }
    
    
    func arrayDefaultCombineCustomerBooking(serviceImageURL: String , bookedBarberShopName: String , bookingUniqueIDDD: String , bookingTotalTime: String , bookStartTSS: String , bookTzone: String , bookClandar: String , bookLocal: String ,shopUniqueID: String ){
        let claxandNow = DateByUserDeviceInitializer.calenderNow
        let tznxNow = DateByUserDeviceInitializer.tzone
        let loclxnNow = DateByUserDeviceInitializer.localCode
        
        let VerifyCurrentDateDetails = VerifyDateDetails.checkDateData(timeZone: tznxNow, calendar: claxandNow, locale: loclxnNow)
        
        if VerifyCurrentDateDetails == true {
            let currentRegionOfDeviceNow = DateByUserDeviceInitializer.getRegion(TZoneName: tznxNow, calenName: claxandNow, LocName: loclxnNow)
            let bookingUniqueRegionSP = DateByUserDeviceInitializer.getRegion(TZoneName: bookTzone, calenName: bookClandar, LocName: bookLocal)
            
            let appoint = AppointmentsCustomer()
            appoint.serviceImage = UIImage()
            appoint.serviceImageUrl = serviceImageURL
            appoint.bookedBarberShopName = bookedBarberShopName
            appoint.bookingTotalTime = bookingTotalTime + "min"
            appoint.bookingUniqueID = bookingUniqueIDDD
            appoint.barberShopUniqueID = shopUniqueID
            
            if let bookst = bookStartTSS.toDate(style: .extended, region: bookingUniqueRegionSP){
                let dateData = bookst.convertTo(region: currentRegionOfDeviceNow)
                appoint.bookingStartTimeString = dateData.toString(DateToStringStyles.dateTime(.short))
                appoint.bookingStartTime = dateData
            } else {
                appoint.bookingStartTimeString = ""
            }
            
            if let url = URL(string: serviceImageURL) {
                do {
                    let data = try Data(contentsOf: url)
                    
                    if let downloadedImage = UIImage(data: data) {
                        appoint.serviceImage = downloadedImage
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                } catch {
                    appoint.serviceImage = UIImage()
                }
            }
            
            
            if let uuiidBook = appoint.bookingUniqueID {
                if let firstNegative = self.appointmentsCustomer.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    if let indexValue = self.appointmentsCustomer.index(of: firstNegative) {
                        self.appointmentsCustomer[indexValue] = appoint
                    }
                    print("runaway man")
                } else {
                    self.appointmentsCustomer.append(appoint)
                }
                
                if let _ = self.appointmentsCustomerNewData.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    print("runaway man second")
                } else {
                    self.appointmentsCustomerNewData.append(appoint)
                }
            }
            
            
            self.appointmentsCustomer.sort(by: { (appstx, appsty) -> Bool in
                return appstx.bookingStartTime > appsty.bookingStartTime
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    @objc func searchAndDeleteOldArrayItems(){
        for singleApp in self.appointmentsCustomer {
            if let uuiidBook = singleApp.bookingUniqueID {
                
                if let _ = self.appointmentsCustomerNewData.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    print("good data")
                } else {
                    self.appointmentsCustomer = self.appointmentsCustomer.filter { $0.bookingUniqueID != uuiidBook }
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    @objc func handleSegmentedControlSelection(sender: UISegmentedControl){
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    //Data for completed custom collection view cell
    
    var bookingSelectedStartTimeIsCompleted = [String]()
    var bookingSelectedCalendarIsCompleted = [String]()
    var bookingSelectedTimeZoneIsCompleted = [String]()
    var bookingSelectedLocalIsCompleted = [String]()
    var bookingSelectedTotalTimeIsCompleted = [String]()
    var bookingSelectedServiceImageUrlIsCompleted = [String]()
    var bookingSelectedBarberShopNameIsCompleted = [String]()
    var bookingSelectedBookingUniqueIDIsCompleted = [String]()
    var appointmentsCustomerIsCompleted = [AppointmentsCustomer]()
    var appointmentsCustomerIsCompletedNewData = [AppointmentsCustomer]()
    var listOfCompletedBookings = [BookingCompleted]()
    var arrayLoppingInitial = 0
    
    //new proper of way of doing things
    func getBookingDataAsAppointmentsIsCompleted(){
        print("function called")
        DispatchQueue.global(qos: .background).async {
            if let customerUUID = Auth.auth().currentUser?.uid {
                let firebaseReferenceCustomerBookingHold = Database.database().reference()
                firebaseReferenceCustomerBookingHold.child("customer-bookings").child(customerUUID).observeSingleEvent(of: .value, with: { (snapshootDataCustomerBooking) in
                    print("customer id: ", customerUUID)
                    self.bookingSelectedStartTimeIsCompleted.removeAll()
                    self.bookingSelectedCalendarIsCompleted.removeAll()
                    self.bookingSelectedTimeZoneIsCompleted.removeAll()
                    self.bookingSelectedLocalIsCompleted.removeAll()
                    self.bookingSelectedTotalTimeIsCompleted.removeAll()
                    self.bookingSelectedServiceImageUrlIsCompleted.removeAll()
                    self.bookingSelectedBarberShopNameIsCompleted.removeAll()
                    self.bookingSelectedBookingUniqueIDIsCompleted.removeAll()
                    
                    //self.appointmentsCustomerIsCompleted.removeAll()
                    self.appointmentsCustomerIsCompletedNewData.removeAll()
                    
                    self.listOfCompletedBookings.removeAll()
                    
                    if let dictionaryCustomerBooking = snapshootDataCustomerBooking.value as? [String: AnyObject] {
                        
                        
                        let numberOfBookingsIsCompleted = dictionaryCustomerBooking.count
                        var numberOfBookingsEquatorisCompleted = 0
                        
                        for custom in dictionaryCustomerBooking {
                            numberOfBookingsEquatorisCompleted = numberOfBookingsEquatorisCompleted + 1
                            if let customeSingleDataHold = custom.value as? [String: AnyObject] {
                                
                                let customerBookingConfirm = HandleDataRequest.handleCustomerBookingsNode(firebaseData: customeSingleDataHold)
                                if customerBookingConfirm {
                                    print("customer booking conf")
                                    let customerbooking = CustomerBookings()
                                    customerbooking.setValuesForKeys(customeSingleDataHold)
                                    
                                    if let bookIDD = customerbooking.bookingID, let barberShop = customerbooking.barberShopID {
                                        let firbaseDatabaseReference =  Database.database().reference()
                                        firbaseDatabaseReference.child("bookings").child(barberShop).child(bookIDD).observeSingleEvent(of: .value, with: { (snapshotBookingDataView) in
                                            if let dictionaryBooking = snapshotBookingDataView.value as? [String: AnyObject] {
                                                
                                                let bookingConfirm = HandleDataRequest.handleBookingsNode(firebaseData: dictionaryBooking)
                                                
                                                if bookingConfirm {
                                                    let bookingTemHolder = Bookings()
                                                    bookingTemHolder.setValuesForKeys(dictionaryBooking)
                                                    print("data was good")
                                                    //make data processing
                                                    if let customerIDXOX = bookingTemHolder.customerID, let userIDXOX = Auth.auth().currentUser?.uid, let payedBook = bookingTemHolder.paymentID, let barberIDX = bookingTemHolder.bookedBarberID, let bookStartString =  bookingTemHolder.bookingStartTime, let bookClandar = bookingTemHolder.calendar, let bookTizne = bookingTemHolder.timezone, let bookLocal = bookingTemHolder.local, let bookServiceUUID = bookingTemHolder.bookedServiceID, let bookkUniqueIDX = bookingTemHolder.bookingID, let bookTotalTmx =  bookingTemHolder.ConfirmedTotalTime, let shopID = bookingTemHolder.bookedBarberShopID {
                                                        
                                                        
                                                        let verifyDateDetails = VerifyDateDetails.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal)
                                                        let verifyDate = VerifyDateAssociation.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal, dateData: bookStartString)
                                                        
                                                        if verifyDateDetails == true && verifyDate == true {
                                                            print("date data was good")
                                                            if customerIDXOX == userIDXOX && payedBook != "nil" {
                                                                let firbaseDatabaseReferencePaymentInstantChecker =  Database.database().reference()
                                                                
                                                                if shopID != "" && payedBook != "" && payedBook != "nil" {
                                                                    firbaseDatabaseReferencePaymentInstantChecker.child("payments").child(shopID).child(payedBook).observeSingleEvent(of: .value, with: { (snapshotPayXXccCC) in
                                                                        if let dictionayRefund = snapshotPayXXccCC.value as? [String: AnyObject] {
                                                                            
                                                                            let paymentConfirm = HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund)
                                                                            
                                                                            if paymentConfirm {
                                                                                print("payment data was good")
                                                                                let paymentHold = Payments()
                                                                                paymentHold.setValuesForKeys(dictionayRefund)
                                                                                
                                                                                
                                                                                
                                                                                if let payConfirm = paymentHold.payment_aq_status_msg, let captureConfirm = paymentHold.capture_aq_status_msg {
                                                                                    if payConfirm == "Approved" && captureConfirm == "Approved" {
                                                                                        print("payment was made")
                                                                                        //completion checker start
                                                                                        let firbaseDatabaseReferenceCompletionChecker =  Database.database().reference()
                                                                                        firbaseDatabaseReferenceCompletionChecker.child("bookingCompleted").child(bookkUniqueIDX).observeSingleEvent(of: .value, with: { (snapshotototCompletionHolderNew) in
                                                                                            if let dictionCompletion = snapshotototCompletionHolderNew.value as? [String: AnyObject] {
                                                                                                
                                                                                                let bookingCompletedConfirm = HandleDataRequest.handleBookingCompletedNode(firebaseData: dictionCompletion)
                                                                                                
                                                                                                if bookingCompletedConfirm {
                                                                                                    let completionObject = BookingCompleted()
                                                                                                    completionObject.setValuesForKeys(dictionCompletion)
                                                                                                    
                                                                                                    self.listOfCompletedBookings.append(completionObject)
                                                                                                    
                                                                                                    if let hasNotBeenComp = completionObject.serviceCompletedCustomer {
                                                                                                        if hasNotBeenComp == "NO" {
                                                                                                            
                                                                                                            let firbaseDatabaseReferenceBarberDetailsBarberName =  Database.database().reference()
                                                                                                            firbaseDatabaseReferenceBarberDetailsBarberName.child("users").child(barberIDX).observeSingleEvent(of: .value, with: { (snapshotUserDataHold) in
                                                                                                                if let dictionaryDataHoldBarberName = snapshotUserDataHold.value as? [String: AnyObject] {
                                                                                                                    
                                                                                                                    let barberConfirm = HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryDataHoldBarberName)
                                                                                                                    
                                                                                                                    if barberConfirm {
                                                                                                                        let userDataHold = Barber()
                                                                                                                        userDataHold.setValuesForKeys(dictionaryDataHoldBarberName)
                                                                                                                        
                                                                                                                        
                                                                                                                        if let barberShopName = userDataHold.barberShopID {
                                                                                                                            
                                                                                                                            let firbaseDatabaseReferenceBarberShopOwnerDetails =  Database.database().reference()
                                                                                                                            firbaseDatabaseReferenceBarberShopOwnerDetails.child("users").child(barberShopName).observeSingleEvent(of: .value, with: { (snapshotUserShopName) in
                                                                                                                                if let dictionaryShopUserDataHold = snapshotUserShopName.value as? [String: AnyObject] {
                                                                                                                                    
                                                                                                                                    let barbershopConfirm = HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryShopUserDataHold)
                                                                                                                                    
                                                                                                                                    if barbershopConfirm {
                                                                                                                                        let shopOwnerHold = BarberShop()
                                                                                                                                        shopOwnerHold.setValuesForKeys(dictionaryShopUserDataHold)
                                                                                                                                        if let bookedBarberShopNameHold = shopOwnerHold.companyName {
                                                                                                                                            
                                                                                                                                            
                                                                                                                                            let firbaseDatabaseServiceImageReferencServieDetails =  Database.database().reference()
                                                                                                                                            firbaseDatabaseServiceImageReferencServieDetails.child("service").child(barberShopName).child(bookServiceUUID).observeSingleEvent(of: .value, with: { (snapshoototService) in
                                                                                                                                                if let dictionaryServiceDetails = snapshoototService.value as? [String: AnyObject] {
                                                                                                                                                    
                                                                                                                                                    let serviceConfirm = HandleDataRequest.handleServiceNode(firebaseData: dictionaryServiceDetails)
                                                                                                                                                    
                                                                                                                                                    if serviceConfirm {
                                                                                                                                                        
                                                                                                                                                        let serviceDatHold = Service()
                                                                                                                                                        serviceDatHold.setValuesForKeys(dictionaryServiceDetails)
                                                                                                                                                        if let serviceImageURL = serviceDatHold.serviceImageUrl {
                                                                                                                                                            
                                                                                                                                                            self.arrayDefaultCombineCustomerBookingIsCompleted(serviceImageURL: serviceImageURL, bookedBarberShopName: bookedBarberShopNameHold, bookingUniqueIDDD: bookkUniqueIDX, bookingTotalTime: bookTotalTmx, bookStartTSS: bookStartString, bookTzone: bookTizne, bookClandar: bookClandar, bookLocal: bookLocal)
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    
                                                                                                                                                }
                                                                                                                                            }, withCancel: nil)
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }, withCancel: nil)
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }, withCancel: nil)
                                                                                                        }
                                                                                                        
                                                                                                    }
                                                                                                }
                                                                                                
                                                                                                
                                                                                            }
                                                                                        }, withCancel: nil)
                                                                                    }
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                        }, withCancel: nil)
                                        
                                    }
                                }
                                
                                
                            }//from here
                            
                        }
                        
                        if numberOfBookingsEquatorisCompleted == numberOfBookingsIsCompleted {
                            if !dictionaryCustomerBooking.isEmpty {
                                print("red head smoke")
                                self.perform(#selector(self.searchAndDeleteOldArrayItemsIsCompleted), with: self, afterDelay: 3)
                            }
                        }
                    }
                }, withCancel: nil)
                
            }
        }
    }
    
    func arrayDefaultCombineCustomerBookingIsCompleted(serviceImageURL: String , bookedBarberShopName: String , bookingUniqueIDDD: String , bookingTotalTime: String , bookStartTSS: String , bookTzone: String , bookClandar: String , bookLocal: String ){
        let claxandNow = DateByUserDeviceInitializer.calenderNow
        let tznxNow = DateByUserDeviceInitializer.tzone
        let loclxnNow = DateByUserDeviceInitializer.localCode
        
        let VerifyCurrentDateDetails = VerifyDateDetails.checkDateData(timeZone: tznxNow, calendar: claxandNow, locale: loclxnNow)
        
        if VerifyCurrentDateDetails == true {
            let currentRegionOfDeviceNow = DateByUserDeviceInitializer.getRegion(TZoneName: tznxNow, calenName: claxandNow, LocName: loclxnNow)
            let bookingUniqueRegionSP = DateByUserDeviceInitializer.getRegion(TZoneName: bookTzone, calenName: bookClandar, LocName: bookLocal)
            
            let appointIsCompleted = AppointmentsCustomer()
            appointIsCompleted.serviceImageUrl = serviceImageURL
            appointIsCompleted.bookedBarberShopName = bookedBarberShopName
            appointIsCompleted.bookingTotalTime = bookingTotalTime + "min"
            appointIsCompleted.bookingUniqueID = bookingUniqueIDDD
            
            if let url = URL(string: serviceImageURL) {
                do {
                    let data = try Data(contentsOf: url)
                    
                    if let downloadedImage = UIImage(data: data) {
                        appointIsCompleted.serviceImage = downloadedImage
                    }
                } catch {
                    appointIsCompleted.serviceImage = UIImage()
                }
            }
            
            
            if let bookst = bookStartTSS.toDate(style: .extended, region: bookingUniqueRegionSP) {
                let dateData = bookst.convertTo(region: currentRegionOfDeviceNow)
                appointIsCompleted.bookingStartTimeString = dateData.toString(DateToStringStyles.dateTime(.short))
                appointIsCompleted.bookingStartTime = dateData
            } else {
                appointIsCompleted.bookingStartTimeString = ""
            }
            
            if let uuiidBook = appointIsCompleted.bookingUniqueID {
                if let firstNegative = self.appointmentsCustomerIsCompleted.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    if let indexValue = self.appointmentsCustomerIsCompleted.index(of: firstNegative) {
                        print("sucessfully updated")
                        self.appointmentsCustomerIsCompleted[indexValue] = appointIsCompleted
                    }
                    print("no new data")
                } else {
                    self.appointmentsCustomerIsCompleted.append(appointIsCompleted)
                }
                
                //appointmentsCustomerIsCompletedNewData
                if let _ = self.appointmentsCustomerIsCompletedNewData.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    print("no new data second way too")
                } else {
                    self.appointmentsCustomerIsCompletedNewData.append(appointIsCompleted)
                }
            }
            
            self.appointmentsCustomerIsCompleted.sort(by: { (appstx, appsty) -> Bool in
                return appstx.bookingStartTime > appsty.bookingStartTime
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    @objc func searchAndDeleteOldArrayItemsIsCompleted(){
        for singleApp in self.appointmentsCustomerIsCompleted {
            if let uuiidBook = singleApp.bookingUniqueID {
                
                if let _ = self.appointmentsCustomerIsCompletedNewData.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    print("good data new day")
                } else {
                    self.appointmentsCustomerIsCompleted = self.appointmentsCustomerIsCompleted.filter { $0.bookingUniqueID != uuiidBook }
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    
    
    func setupViewObjectContriants(){
        upcomingAndCompletedSegmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        upcomingAndCompletedSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upcomingAndCompletedSegmentedControl.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        upcomingAndCompletedSegmentedControl.heightAnchor.constraint(equalToConstant: 29).isActive = true
        
        badgeLabelBooking.topAnchor.constraint(equalTo: view.topAnchor, constant: 7).isActive = true
        badgeLabelBooking.rightAnchor.constraint(equalTo: view.centerXAnchor, constant: -5).isActive = true
        badgeLabelBooking.widthAnchor.constraint(equalToConstant: 25).isActive = true
        badgeLabelBooking.bottomAnchor.constraint(equalTo: upcomingAndCompletedSegmentedControl.bottomAnchor, constant: -2).isActive = true
        
        
        badgeLabelUpcoming.topAnchor.constraint(equalTo: view.topAnchor, constant: 7).isActive = true
        badgeLabelUpcoming.rightAnchor.constraint(equalTo: upcomingAndCompletedSegmentedControl.rightAnchor, constant: -5).isActive = true
        badgeLabelUpcoming.widthAnchor.constraint(equalToConstant: 25).isActive = true
        badgeLabelUpcoming.bottomAnchor.constraint(equalTo: upcomingAndCompletedSegmentedControl.bottomAnchor, constant: -2).isActive = true
        
        collectView.topAnchor.constraint(equalTo: upcomingAndCompletedSegmentedControl.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
            collectionView.sendSubview(toBack: refresherController)
        }
    }
}


class customUpcomingAndCompletedCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFill
        return tniv
    }()
    
    let bookedBarberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookingStartTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(bookingStartTimePlaceHolder)
        addSubview(bookedBarberShopNamePlaceHolder)
        addSubview(totalTimePlaceHolder)
        addSubview(seperatorView)
        
        backgroundColor = UIColor(r: 23, g: 69, b: 90)
        addContraintsWithFormat(format: "H:|-16-[v0(90)]|", views: thumbnailImageView)
        addContraintsWithFormat(format: "H:|-116-[v0]-60-|", views: bookingStartTimePlaceHolder)
        addContraintsWithFormat(format: "H:|-116-[v0][v1(50)]-10-|", views: bookedBarberShopNamePlaceHolder, totalTimePlaceHolder)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-25-|", views: bookingStartTimePlaceHolder, bookedBarberShopNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-10-|", views: bookingStartTimePlaceHolder, totalTimePlaceHolder)
        addContraintsWithFormat(format: "V:|-10-[v0]-10-[v1(5)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    
    override func prepareForReuse() {
        self.thumbnailImageView.image = UIImage()
        self.bookingStartTimePlaceHolder.text = ""
        self.bookedBarberShopNamePlaceHolder.text = ""
        self.totalTimePlaceHolder.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//Data for second colection view cell

class customUpcomingAndCompletedCollectionViewCellIsCompleted: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageViewIsCompleted: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFill
        return tniv
    }()
    
    let bookedBarberShopNamePlaceHolderIsCompleted: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookingStartTimePlaceHolderIsCompleted: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolderIsCompleted: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10) 
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorViewIsCompleted: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageViewIsCompleted)
        addSubview(bookingStartTimePlaceHolderIsCompleted)
        addSubview(bookedBarberShopNamePlaceHolderIsCompleted)
        addSubview(totalTimePlaceHolderIsCompleted)
        addSubview(seperatorViewIsCompleted)
        
        backgroundColor = UIColor(r: 23, g: 69, b: 90)
        addContraintsWithFormat(format: "H:|-16-[v0(90)]|", views: thumbnailImageViewIsCompleted)
        addContraintsWithFormat(format: "H:|-116-[v0]-60-|", views: bookingStartTimePlaceHolderIsCompleted)
        addContraintsWithFormat(format: "H:|-116-[v0][v1(50)]-10-|", views: bookedBarberShopNamePlaceHolderIsCompleted, totalTimePlaceHolderIsCompleted)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-25-|", views: bookingStartTimePlaceHolderIsCompleted, bookedBarberShopNamePlaceHolderIsCompleted)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-10-|", views: bookingStartTimePlaceHolderIsCompleted, totalTimePlaceHolderIsCompleted)
        addContraintsWithFormat(format: "V:|-10-[v0]-10-[v1(5)]|", views: thumbnailImageViewIsCompleted,seperatorViewIsCompleted)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorViewIsCompleted)
        
    }
    
    override func prepareForReuse() {
        self.thumbnailImageViewIsCompleted.image = UIImage()
        self.bookingStartTimePlaceHolderIsCompleted.text = ""
        self.bookedBarberShopNamePlaceHolderIsCompleted.text = ""
        self.totalTimePlaceHolderIsCompleted.text = ""
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
