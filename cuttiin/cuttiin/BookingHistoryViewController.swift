//
//  BookingHistoryViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 11/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate

class BookingHistoryViewController: UIViewController {
    var customerMadeBookings = [Bookings]()
    var customerMadeBookingsIsCompleted = [Bookings]()
    var barberShopBookNames = [String]()
    var indexValueMain = 1
    
    var bookingSelectedStartTime = [String]()
    var bookingSelectedCalendar = [String]()
    var bookingSelectedTimeZone = [String]()
    var bookingSelectedLocal = [String]()
    var bookingSelectedTotalTime = [String]()
    var bookingSelectedServiceImageUrl = [String]()
    var bookingSelectedBarberShopName = [String]()
    var bookingSelectedBookingUniqueID = [String]()
    var bookingSelectedBarberShopUniqueID = [String]()
    var appointmentsCustomer = [AppointmentsCustomer]()
    
    //cancelled Booking data holders
    
    var bookingSelectedStartTimeCancelled = [String]()
    var bookingSelectedCalendarCancelled = [String]()
    var bookingSelectedTimeZoneCancelled = [String]()
    var bookingSelectedLocalCancelled = [String]()
    var bookingSelectedTotalTimeCancelled = [String]()
    var bookingSelectedServiceImageUrlCancelled = [String]()
    var bookingSelectedBarberShopNameCancelled = [String]()
    var bookingSelectedBookingUniqueIDCancelled = [String]()
    var bookingSelectedBarberShopUniqueIDCancelled = [String]()
    var appointmentsCustomerCancelled = [AppointmentsCustomer]()
    
    var arrayLengthHolder = [Int]()
    
    lazy var upcomingAndCompletedSegmentedControl: UISegmentedControl = {
        let sssegmentcontrol = UISegmentedControl(items: [NSLocalizedString("bookingHistorySegementBookingHisoryView", comment: "BOOKING HISTORY"), NSLocalizedString("bookingCancelledSegementBookingHisoryView", comment: "BOOKING CANCELLED")])
        sssegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        sssegmentcontrol.tintColor = UIColor.white
        sssegmentcontrol.selectedSegmentIndex = 0
        sssegmentcontrol.addTarget(self, action: #selector(handleSegmentedControlSelection), for: .valueChanged)
        return sssegmentcontrol
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customUpcomingAndCompletedCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdXEB")
        cv.register(customUpcomingAndCompletedCancelledCollectionViewCell.self, forCellWithReuseIdentifier: "cellIDXOX")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.title = NSLocalizedString("historyButtonProfileSettingsEditorView", comment: "History")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(upcomingAndCompletedSegmentedControl)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        getBookingDataAsAppointments()
        getCancelledBookings()
    }
    
    @objc func handleBackAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        print("ro money call")
        getBookingDataAsAppointments()
        getCancelledBookings()
        sender.endRefreshing()
    }
    
    @objc func getBookingDataAsAppointments(){
        
        DispatchQueue.global(qos: .background).async {
            
            if let customerUUID = Auth.auth().currentUser?.uid {
                
                let firebaseReferenceCustomerBookingHold = Database.database().reference()
                firebaseReferenceCustomerBookingHold.child("customer-bookings").child(customerUUID).observeSingleEvent(of: .value, with: { (snapshootDataCustomerBooking) in
                    
                    self.bookingSelectedStartTime.removeAll()
                    self.bookingSelectedCalendar.removeAll()
                    self.bookingSelectedTimeZone.removeAll()
                    self.bookingSelectedLocal.removeAll()
                    self.bookingSelectedTotalTime.removeAll()
                    self.bookingSelectedServiceImageUrl.removeAll()
                    self.bookingSelectedBarberShopName.removeAll()
                    self.bookingSelectedBookingUniqueID.removeAll()
                    //self.appointmentsCustomer.removeAll()
                    self.bookingSelectedBarberShopUniqueID.removeAll()
                    
                    if let dictionaryCustomerBooking = snapshootDataCustomerBooking.value as? [String: AnyObject] {
                        for custom in dictionaryCustomerBooking {
                            if let customSingle = custom.value as? [String: AnyObject], HandleDataRequest.handleCustomerBookingsNode(firebaseData: customSingle) == true {
                                let customerbooking = CustomerBookings()
                                customerbooking.setValuesForKeys(customSingle)
                                
                                if let bookIDD = customerbooking.bookingID, let barberShop = customerbooking.barberShopID {
                                    let firbaseDatabaseReference =  Database.database().reference()
                                    firbaseDatabaseReference.child("bookings").child(barberShop).child(bookIDD).observeSingleEvent(of: .value, with: { (snapshotBookingDataView) in
                                        if let dictionaryBooking = snapshotBookingDataView.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryBooking) == true {
                                            
                                            let bookingTemHolder = Bookings()
                                            bookingTemHolder.setValuesForKeys(dictionaryBooking)
                                            //make data processing
                                            if let customerIDXOX = bookingTemHolder.customerID, let userIDXOX = Auth.auth().currentUser?.uid, let payedBook = bookingTemHolder.paymentID, let barberIDX = bookingTemHolder.bookedBarberID, let bookStartString =  bookingTemHolder.bookingStartTime, let bookClandar = bookingTemHolder.calendar, let bookTizne = bookingTemHolder.timezone, let bookLocal = bookingTemHolder.local, let bookServiceUUID = bookingTemHolder.bookedServiceID, let bookkUniqueIDX = bookingTemHolder.bookingID, let bookTotalTmx =  bookingTemHolder.ConfirmedTotalTime, let shopID = bookingTemHolder.bookedBarberShopID, let checkComplete = bookingTemHolder.isCompleted {
                                                
                                                let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal)
                                                let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal, dateData: bookStartString)
                                                
                                                
                                                
                                                if customerIDXOX == userIDXOX && payedBook != "nil" && payedBook != "" && checkComplete == "YES" && verifyBookingData == true && verifyBookingStartDate == true {
                                                    
                                                    let firbaseDatabaseReferencePaymentInstantChecker =  Database.database().reference()
                                                    firbaseDatabaseReferencePaymentInstantChecker.child("payments").child(shopID).child(payedBook).observeSingleEvent(of: .value, with: { (snapshotPayXXccCC) in
                                                        
                                                        if let dictionayRefund = snapshotPayXXccCC.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund) == true {
                                                            let paymentHold = Payments()
                                                            paymentHold.setValuesForKeys(dictionayRefund)
                                                            
                                                            if let payConfirm = paymentHold.payment_aq_status_msg, let payCapture = paymentHold.capture_aq_status_msg {
                                                                if payConfirm == "Approved" && payCapture == "Approved" {
                                                                    let firbaseDatabaseReferenceBarberDetailsBarberName =  Database.database().reference()
                                                                    firbaseDatabaseReferenceBarberDetailsBarberName.child("users").child(barberIDX).observeSingleEvent(of: .value, with: { (snapshotUserDataHold) in
                                                                        if let dictionaryDataHoldBarberName = snapshotUserDataHold.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryDataHoldBarberName) == true {
                                                                            let userDataHold = Barber()
                                                                            userDataHold.setValuesForKeys(dictionaryDataHoldBarberName)
                                                                            
                                                                            if let barberShopName = userDataHold.barberShopID {
                                                                                let firbaseDatabaseReferenceBarberShopOwnerDetails =  Database.database().reference()
                                                                                firbaseDatabaseReferenceBarberShopOwnerDetails.child("users").child(barberShopName).observeSingleEvent(of: .value, with: { (snapshotUserShopName) in
                                                                                    if let dictionaryShopUserDataHold = snapshotUserShopName.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryShopUserDataHold) == true {
                                                                                        let shopOwnerHold = BarberShop()
                                                                                        shopOwnerHold.setValuesForKeys(dictionaryShopUserDataHold)
                                                                                        if let bookedBarberShopNameHold = shopOwnerHold.companyName {
                                                                                            let firbaseDatabaseServiceImageReferencServieDetails =  Database.database().reference()
                                                                                            firbaseDatabaseServiceImageReferencServieDetails.child("service").child(barberShopName).child(bookServiceUUID).observeSingleEvent(of: .value, with: { (snapshoototService) in
                                                                                                if let dictionaryServiceDetails = snapshoototService.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionaryServiceDetails) == true {
                                                                                                    let serviceDatHold = Service()
                                                                                                    serviceDatHold.setValuesForKeys(dictionaryServiceDetails)
                                                                                                    if let serviceImageURL = serviceDatHold.serviceImageUrl {
                                                                                                        self.arrayDefaultCombineCustomerBooking(serviceImageURL: serviceImageURL, bookedBarberShopName: bookedBarberShopNameHold, bookingUniqueIDDD: bookkUniqueIDX, bookingTotalTime: bookTotalTmx, bookStartTSS: bookStartString, bookTzone: bookTizne, bookClandar: bookClandar, bookLocal: bookLocal, shopUniqueID: barberShopName)
                                                                                                    }
                                                                                                }
                                                                                            }, withCancel: nil)
                                                                                        }
                                                                                    }
                                                                                }, withCancel: nil)
                                                                            }
                                                                        }
                                                                    }, withCancel: nil)
                                                                    
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                            
                                        }
                                    })
                                    
                                }
                            }
                        }
                    }
                })
                
            }
        }
    }
    
    func arrayDefaultCombineCustomerBooking(serviceImageURL: String , bookedBarberShopName: String , bookingUniqueIDDD: String , bookingTotalTime: String , bookStartTSS: String , bookTzone: String , bookClandar: String , bookLocal: String ,shopUniqueID: String ){
        
        let claxandNow = DateByUserDeviceInitializer.calenderNow
        let tznxNow = DateByUserDeviceInitializer.tzone
        let loclxnNow = DateByUserDeviceInitializer.localCode
        
        let verufyCurrentDateDetails = VerifyDateDetails.checkDateData(timeZone: tznxNow, calendar: claxandNow, locale: loclxnNow)
        
        if verufyCurrentDateDetails == true {
            let currentRegionOfDeviceNow = DateByUserDeviceInitializer.getRegion(TZoneName: tznxNow, calenName: claxandNow, LocName: loclxnNow)
            let bookingUniqueRegionSP = DateByUserDeviceInitializer.getRegion(TZoneName: bookTzone, calenName: bookClandar, LocName: bookLocal)
            
            let appoint = AppointmentsCustomer()
            appoint.serviceImageUrl = serviceImageURL
            appoint.bookedBarberShopName = bookedBarberShopName
            appoint.bookingTotalTime = bookingTotalTime + "min"
            appoint.bookingUniqueID = bookingUniqueIDDD
            appoint.barberShopUniqueID = shopUniqueID
            
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string: serviceImageURL) {
                    do {
                        let data = try Data(contentsOf: url)
                        
                        if let downloadedImage = UIImage(data: data) {
                            appoint.serviceImage = downloadedImage
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    } catch {
                        appoint.serviceImage = UIImage()
                    }
                }
            }
            
            if let bookst = bookStartTSS.toDate(style: .extended, region: bookingUniqueRegionSP){
                let dateData = bookst.convertTo(region: currentRegionOfDeviceNow)
                appoint.bookingStartTimeString = dateData.toString(DateToStringStyles.dateTime(.short))
                appoint.bookingStartTime = dateData
            } else {
                appoint.bookingStartTimeString = ""
            }
            
            
            if let uuiidBook = appoint.bookingUniqueID {
                if let firstNegative = self.appointmentsCustomer.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    if let indexValue = self.appointmentsCustomer.index(of: firstNegative) {
                        self.appointmentsCustomer[indexValue] = appoint
                    }
                } else {
                    self.appointmentsCustomer.append(appoint)
                }
            }
            
            self.appointmentsCustomer.sort(by: { (appstx, appsty) -> Bool in
                return appstx.bookingStartTime > appsty.bookingStartTime
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func getCancelledBookings(){
        DispatchQueue.global(qos: .background).async {
            if let customerUUID = Auth.auth().currentUser?.uid {
                let firebaseReferenceCustomerBookingHold = Database.database().reference()
                firebaseReferenceCustomerBookingHold.child("customer-bookings").child(customerUUID).observeSingleEvent(of: .value, with: { (snapshootDataCustomerBooking) in
                    
                    self.bookingSelectedStartTimeCancelled.removeAll()
                    self.bookingSelectedCalendarCancelled.removeAll()
                    self.bookingSelectedTimeZoneCancelled.removeAll()
                    self.bookingSelectedLocalCancelled.removeAll()
                    self.bookingSelectedTotalTimeCancelled.removeAll()
                    self.bookingSelectedServiceImageUrlCancelled.removeAll()
                    self.bookingSelectedBarberShopNameCancelled.removeAll()
                    self.bookingSelectedBookingUniqueIDCancelled.removeAll()
                    //self.appointmentsCustomerCancelled.removeAll()
                    self.bookingSelectedBarberShopUniqueIDCancelled.removeAll()
                    
                    if let dictionaryCustomerBooking = snapshootDataCustomerBooking.value as? [String: AnyObject] {
                        for custom in dictionaryCustomerBooking {
                            if let customSingle = custom.value as? [String: AnyObject], HandleDataRequest.handleCustomerBookingsNode(firebaseData: customSingle) == true {
                                let customerbooking = CustomerBookings()
                                customerbooking.setValuesForKeys(customSingle)
                                
                                if let bookIDD = customerbooking.bookingID, let barberShop = customerbooking.barberShopID {
                                    let firbaseDatabaseReference =  Database.database().reference()
                                    firbaseDatabaseReference.child("bookings").child(barberShop).child(bookIDD).observeSingleEvent(of: .value, with: { (snapshotBookingDataView) in
                                        if let dictionaryBooking = snapshotBookingDataView.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryBooking) == true {
                                            let bookingTemHolder = Bookings()
                                            bookingTemHolder.setValuesForKeys(dictionaryBooking)
                                            //make data processing
                                            if let customerIDXOX = bookingTemHolder.customerID, let userIDXOX = Auth.auth().currentUser?.uid, let payedBook = bookingTemHolder.paymentID, let barberIDX = bookingTemHolder.bookedBarberID, let bookStartString =  bookingTemHolder.bookingStartTime, let bookClandar = bookingTemHolder.calendar, let bookTizne = bookingTemHolder.timezone, let bookLocal = bookingTemHolder.local, let bookServiceUUID = bookingTemHolder.bookedServiceID, let bookkUniqueIDX = bookingTemHolder.bookingID, let bookTotalTmx =  bookingTemHolder.ConfirmedTotalTime, let shopID = bookingTemHolder.bookedBarberShopID, let checkComplete = bookingTemHolder.isCompleted, let isCancelled = bookingTemHolder.bookingCancel {
                                                
                                                let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal)
                                                let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal, dateData: bookStartString)
                                                
                                                if customerIDXOX == userIDXOX && payedBook != "nil" && payedBook != "" && checkComplete == "YES" && isCancelled == "YES" && verifyBookingData == true && verifyBookingStartDate == true {
                                                    
                                                    let firbaseDatabaseReferencePaymentInstantChecker =  Database.database().reference()
                                                    firbaseDatabaseReferencePaymentInstantChecker.child("payments").child(shopID).child(payedBook).observeSingleEvent(of: .value, with: { (snapshotPayXXccCC) in
                                                        
                                                        if let dictionayRefund = snapshotPayXXccCC.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund) == true {
                                                            let paymentHold = Payments()
                                                            paymentHold.setValuesForKeys(dictionayRefund)
                                                            
                                                            if let payConfirm = paymentHold.payment_aq_status_msg {
                                                                if payConfirm == "Approved" {
                                                                    
                                                                    let firbaseDatabaseReferenceBarberDetailsBarberName =  Database.database().reference()
                                                                    firbaseDatabaseReferenceBarberDetailsBarberName.child("users").child(barberIDX).observeSingleEvent(of: .value, with: { (snapshotUserDataHold) in
                                                                        if let dictionaryDataHoldBarberName = snapshotUserDataHold.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryDataHoldBarberName) {
                                                                            let userDataHold = Barber()
                                                                            userDataHold.setValuesForKeys(dictionaryDataHoldBarberName)
                                                                            
                                                                            if let barberShopName = userDataHold.barberShopID {
                                                                                let firbaseDatabaseReferenceBarberShopOwnerDetails =  Database.database().reference()
                                                                                firbaseDatabaseReferenceBarberShopOwnerDetails.child("users").child(barberShopName).observeSingleEvent(of: .value, with: { (snapshotUserShopName) in
                                                                                    if let dictionaryShopUserDataHold = snapshotUserShopName.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryShopUserDataHold) == true {
                                                                                        let shopOwnerHold = BarberShop()
                                                                                        shopOwnerHold.setValuesForKeys(dictionaryShopUserDataHold)
                                                                                        if let bookedBarberShopNameHold = shopOwnerHold.companyName {
                                                                                            let firbaseDatabaseServiceImageReferencServieDetails =  Database.database().reference()
                                                                                            firbaseDatabaseServiceImageReferencServieDetails.child("service").child(barberShopName).child(bookServiceUUID).observeSingleEvent(of: .value, with: { (snapshoototService) in
                                                                                                if let dictionaryServiceDetails = snapshoototService.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionaryServiceDetails) == true {
                                                                                                    let serviceDatHold = Service()
                                                                                                    serviceDatHold.setValuesForKeys(dictionaryServiceDetails)
                                                                                                    if let serviceImageURL = serviceDatHold.serviceImageUrl {
                                                                                                        self.arrayDefaultCombineCustomerBookingCancelled(serviceImageURL: serviceImageURL, bookedBarberShopName: bookedBarberShopNameHold, bookingUniqueIDDD: bookkUniqueIDX, bookingTotalTime: bookTotalTmx, bookStartTSS: bookStartString, bookTzone: bookTizne, bookClandar: bookClandar, bookLocal: bookLocal, shopUniqueID: barberShopName)
                                                                                                    }
                                                                                                }
                                                                                            }, withCancel: nil)
                                                                                        }
                                                                                    }
                                                                                }, withCancel: nil)
                                                                            }
                                                                        }
                                                                    }, withCancel: nil)
                                                                    
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                            
                                        }
                                    })
                                    
                                }
                            }
                        }
                    }
                })
                
            }
        }
    }
    
    func arrayDefaultCombineCustomerBookingCancelled(serviceImageURL: String , bookedBarberShopName: String , bookingUniqueIDDD: String , bookingTotalTime: String , bookStartTSS: String , bookTzone: String , bookClandar: String , bookLocal: String ,shopUniqueID: String ){
        
        let claxandNow = DateByUserDeviceInitializer.calenderNow
        let tznxNow = DateByUserDeviceInitializer.tzone
        let loclxnNow = DateByUserDeviceInitializer.localCode
        
        let verufyCurrentDateDetails = VerifyDateDetails.checkDateData(timeZone: tznxNow, calendar: claxandNow, locale: loclxnNow)
        
        if verufyCurrentDateDetails == true {
            let currentRegionOfDeviceNow = DateByUserDeviceInitializer.getRegion(TZoneName: tznxNow, calenName: claxandNow, LocName: loclxnNow)
            let bookingUniqueRegionSP = DateByUserDeviceInitializer.getRegion(TZoneName: bookTzone, calenName: bookClandar, LocName: bookLocal)
            
            let appoint = AppointmentsCustomer()
            appoint.serviceImageUrl = serviceImageURL
            appoint.bookedBarberShopName = bookedBarberShopName
            appoint.bookingTotalTime = bookingTotalTime + "min"
            appoint.bookingUniqueID = bookingUniqueIDDD
            appoint.barberShopUniqueID = shopUniqueID
            
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string: serviceImageURL) {
                    do {
                        let data = try Data(contentsOf: url)
                        
                        if let downloadedImage = UIImage(data: data) {
                            appoint.serviceImage = downloadedImage
                        }
                    } catch {
                        appoint.serviceImage = UIImage()
                    }
                }
            }
            
            if let bookst = bookStartTSS.toDate(style: .extended, region: bookingUniqueRegionSP){
                let dateData = bookst.convertTo(region: currentRegionOfDeviceNow)
                appoint.bookingStartTimeString = dateData.toString(DateToStringStyles.dateTime(.short))
                appoint.bookingStartTime = dateData
            } else {
                appoint.bookingStartTimeString = ""
            }
            
            if let uuiidBook = appoint.bookingUniqueID {
                if let firstNegative = self.appointmentsCustomerCancelled.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    if let indexValue = self.appointmentsCustomerCancelled.index(of: firstNegative) {
                        self.appointmentsCustomerCancelled[indexValue] = appoint
                    }
                    print("runing gaga")
                } else {
                    self.appointmentsCustomerCancelled.append(appoint)
                }
            }
            
            self.appointmentsCustomerCancelled.sort(by: { (appstx, appsty) -> Bool in
                return appstx.bookingStartTime > appsty.bookingStartTime
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    
    func setupViewObjectContriants(){
        
        upcomingAndCompletedSegmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        upcomingAndCompletedSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upcomingAndCompletedSegmentedControl.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        upcomingAndCompletedSegmentedControl.heightAnchor.constraint(equalToConstant: 29).isActive = true
        
        collectView.topAnchor.constraint(equalTo: upcomingAndCompletedSegmentedControl.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
            collectionView.sendSubview(toBack: refresherController)
        }
    }
    
    @objc func handleSegmentedControlSelection(){
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

}

class customUpcomingAndCompletedCancelledCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFill
        return tniv
    }()
    
    let bookedBarberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookingStartTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(bookingStartTimePlaceHolder)
        addSubview(bookedBarberShopNamePlaceHolder)
        addSubview(totalTimePlaceHolder)
        addSubview(seperatorView)
        
        backgroundColor = UIColor(r: 23, g: 69, b: 90)
        addContraintsWithFormat(format: "H:|-16-[v0(90)]|", views: thumbnailImageView)
        addContraintsWithFormat(format: "H:|-116-[v0]-60-|", views: bookingStartTimePlaceHolder)
        addContraintsWithFormat(format: "H:|-116-[v0][v1(50)]-10-|", views: bookedBarberShopNamePlaceHolder, totalTimePlaceHolder)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-25-|", views: bookingStartTimePlaceHolder, bookedBarberShopNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-10-|", views: bookingStartTimePlaceHolder, totalTimePlaceHolder)
        addContraintsWithFormat(format: "V:|-10-[v0]-10-[v1(5)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    
    override func prepareForReuse() {
        self.thumbnailImageView.image = UIImage()
        self.bookingStartTimePlaceHolder.text = ""
        self.bookedBarberShopNamePlaceHolder.text = ""
        self.totalTimePlaceHolder.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


