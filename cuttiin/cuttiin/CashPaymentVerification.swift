//
//  CashPaymentVerification.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 05/01/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

@objcMembers class CashPaymentVerification: NSObject {
    var barberShopID: String?
    var isCashPaymentAllowed: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
}
