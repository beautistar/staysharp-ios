//
//  NearByBarberShopViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/29/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import Firebase
import CoreLocation

class NearByBarberShopViewController: UIViewController, UISearchBarDelegate, CLLocationManagerDelegate {
    var locationManager = CLLocationManager()
    var userCurrentLocation: CLLocation?
    let firebaseRef = Database.database().reference()
    var barbermarkers = [BarberMarker]()
    var oldListbarbermarkers = [BarberMarker]()
    var distancesFormUser = [Double]()
    var flag = false
    let attribute = StarRatingAttribute(type: .rate,point: 10,spacing: 5, emptyColor: .black, fillColor: .white)
    
    lazy var nearSearchBar: UISearchBar = {
        let nearSearch = UISearchBar()
        nearSearch.showsCancelButton = true
        nearSearch.showsBookmarkButton = false
        nearSearch.searchBarStyle = UISearchBarStyle.prominent
        nearSearch.placeholder = NSLocalizedString("searchBarTextBarberShopsView", comment: "Search")
        nearSearch.tintColor = UIColor.black
        nearSearch.showsSearchResultsButton = false
        return nearSearch
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customNearByTopRatedCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        nearSearchBar.delegate = self
        nearSearchBar.sizeToFit()
        self.navigationItem.titleView = nearSearchBar
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
        nearSearchBar.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        nearSearchBar.resignFirstResponder()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        if let localo = location {
            if !flag {
                self.locationManager.stopUpdatingLocation()
                self.userCurrentLocation = localo
                self.getAllMarkers()
                flag = true
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
     
        switch status {
            case .restricted:
                
                print("Location access was restricted.")
                self.locationManager.requestAlwaysAuthorization()
            
            case .denied:
                print("User denied access to location.")
                self.locationManager.requestAlwaysAuthorization()
            
            case .notDetermined:
                print("Location access notDetermined.")
                self.locationManager.requestAlwaysAuthorization()
            
            case .authorizedAlways: fallthrough
            
            case .authorizedWhenInUse:
                print("Location status is OK.")
        }
    }
    
    func getAllMarkers(){
        DispatchQueue.global(qos: .background).async {
            self.barbermarkers.removeAll()
            let firebaseReferenceVerificationCheck = Database.database().reference()
            firebaseReferenceVerificationCheck.child("barberShopVerification").observeSingleEvent(of: .value, with: { (snapshototoAllData) in
                if let dictionaryVer = snapshototoAllData.value as? [String: AnyObject] {
                    for dataHold in dictionaryVer {
                        if let verificDataHold = dataHold.value as? [String: AnyObject] {
                            let barbershopVerificationVeri = HandleDataRequest.handleBarberShopVerificationNode(firebaseData: verificDataHold)
                            
                            if barbershopVerificationVeri {
                                let shopCheckHold = BarberShopVerification()
                                shopCheckHold.setValuesForKeys(verificDataHold)
                                
                                
                                if let verificationChoice = shopCheckHold.isBarberShopVerified, let shopIDD = shopCheckHold.barberShopID {
                                    if verificationChoice == "YES" {
                                        let firebaseReferenceBarberHop = Database.database().reference()
                                        firebaseReferenceBarberHop.child("users").child(shopIDD).observeSingleEvent(of: .value, with: { (snapshotototData) in
                                            if let userDataDictionary = snapshotototData.value as? [String: AnyObject] {
                                                
                                                let barberShopVerification = HandleDataRequest.handleUserBarberShop(firebaseData: userDataDictionary)
                                                
                                                if barberShopVerification {
                                                    let userSingleData = BarberShop()
                                                    userSingleData.setValuesForKeys(userDataDictionary)
                                                    
                                                    if let lon = userSingleData.companyAddressLongitude, let lat = userSingleData.companyAddressLatitude, let formAddress = userSingleData.companyFormattedAddress, let imageURL = userSingleData.companyLogoImageUrl, let url = URL(string: imageURL), let cmpName = userSingleData.companyName, let timezoneHold = userSingleData.timezone {
                                                        let markerB = BarberMarker()
                                                        
                                                        
                                                        let latitude = (lat as NSString).doubleValue
                                                        let longitude = (lon as NSString).doubleValue
                                                        let locValue: CLLocation = CLLocation(latitude: latitude, longitude: longitude)
                                                        markerB.companyID = shopIDD
                                                        markerB.companyName = cmpName
                                                        markerB.companyLogoImageUrl = imageURL
                                                        markerB.companyFormattedAddress = formAddress
                                                        markerB.companyAddressLongitude = lon
                                                        markerB.companyAddressLatitude = lat
                                                        markerB.timezone = timezoneHold
                                                        
                                                        do {
                                                            let data = try Data(contentsOf: url)
                                                            
                                                            if let downloadedImage = UIImage(data: data) {
                                                                markerB.companyLogoImage = downloadedImage
                                                                DispatchQueue.main.async {
                                                                    self.collectionView.reloadData()
                                                                }
                                                            }
                                                        } catch {
                                                            print("profile picture not available")
                                                        }
                                                        
                                                        if let userLoc = self.userCurrentLocation {
                                                            let metersLoc = userLoc.distance(from: locValue)
                                                            let roundUPVal = metersLoc.roundTo(places: 1)
                                                            markerB.distanceFromCurrentUser = roundUPVal
                                                        }
                                                        
                                                        
                                                        if let ratingValue = userSingleData.rating, let ratingInt = Int(ratingValue) {
                                                            markerB.ratingValue = CGFloat(integerLiteral: ratingInt)
                                                        } else {
                                                            markerB.ratingValue = CGFloat(integerLiteral: 0)
                                                        }
                                                        
                                                        if let _ = self.barbermarkers.first(where: { $0.companyID ==  shopIDD }) {
                                                            print("runaway man")
                                                        } else {
                                                            print("kimonos")
                                                            self.barbermarkers.append(markerB)
                                                            self.oldListbarbermarkers.append(markerB)
                                                            print(self.barbermarkers.count, self.oldListbarbermarkers.count, "zamanaka")
                                                            
                                                            self.barbermarkers.sort(by: { (message1, message2) -> Bool in
                                                                return (message1.distanceFromCurrentUser)! < (message2.distanceFromCurrentUser)!
                                                            })
                                                            
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                        })
                                        
                                    }
                                    
                                }
                            }
                            
                        } //here
                    }
                }
            })
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let imageObject = self.barbermarkers.filter{ (($0.companyName?.contains(searchText))! || ($0.companyName?.lowercased().contains(searchText))! || ($0.companyFormattedAddress?.contains(searchText))! || ($0.companyFormattedAddress?.lowercased().contains(searchText))!)}
        
        if imageObject.count > 0{
            self.barbermarkers = imageObject
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }else {
            self.oldlistMarkerSort()
            self.barbermarkers.removeAll()
            self.barbermarkers = self.oldListbarbermarkers
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func oldlistMarkerSort(){
        if self.oldListbarbermarkers.count > 0 {
            self.oldListbarbermarkers.sort(by: { (message1, message2) -> Bool in
                return (message1.distanceFromCurrentUser)! < (message2.distanceFromCurrentUser)!
            })
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        nearSearchBar.text = ""
        nearSearchBar.resignFirstResponder()
        self.oldlistMarkerSort()
        self.barbermarkers.removeAll()
        self.barbermarkers = self.oldListbarbermarkers
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let imageObject = self.barbermarkers.filter{ (($0.companyName?.contains(searchBar.text!))! || ($0.companyName?.lowercased().contains((searchBar.text)!))! || ($0.companyFormattedAddress?.contains(searchBar.text!))! || ($0.companyFormattedAddress?.lowercased().contains((searchBar.text)!))!)}
        
        if imageObject.count > 0{
            self.barbermarkers = imageObject
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }else {
            self.oldlistMarkerSort()
            self.barbermarkers.removeAll()
            self.barbermarkers = self.oldListbarbermarkers
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func setupViewObjectContriants(){
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -5).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
}

class customNearByTopRatedCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 10,spacing: 5, emptyColor: .black, fillColor: .white)
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.black
        return tniv
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "barbershop1")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = UIColor.clear
        return imageView
    }()
    
    let textBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    let barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.green
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let barberShopDistancePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.backgroundColor = UIColor.clear
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    func setupViews(){
        
        //starView.configure(attribute, current: 3, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.backgroundColor = UIColor.green
        addSubview(barberShopCoverImageView)
        addSubview(textBackgroundView)
        addSubview(seperatorView)
        addSubview(barberShopLogoImageView)
        addSubview(starView)
        addSubview(barberShopNamePlaceHolder)
        addSubview(barberShopAddressPlaceHolder)
        addSubview(barberShopDistancePlaceHolder)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|[v0]|", views: barberShopCoverImageView)
        addContraintsWithFormat(format: "V:|[v0][v1(2)]|", views: barberShopCoverImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: textBackgroundView)
        addContraintsWithFormat(format: "V:|-60-[v0]-2-|", views: textBackgroundView)
        addContraintsWithFormat(format: "H:|-16-[v0(60)]|", views: barberShopLogoImageView)
        addContraintsWithFormat(format: "V:|-20-[v0(60)][v1(20)]-2-|", views: barberShopLogoImageView,starView)
        addContraintsWithFormat(format: "H:|-8-[v0(100)]|", views: starView)
        addContraintsWithFormat(format: "H:|-100-[v0]-10-|", views: barberShopNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-60-[v0][v1]-2-|", views: barberShopNamePlaceHolder, barberShopAddressPlaceHolder)
        addContraintsWithFormat(format: "H:|-100-[v0]-50-|", views: barberShopAddressPlaceHolder)
        addContraintsWithFormat(format: "H:|-325-[v0(50)]-5-|", views: barberShopDistancePlaceHolder)
        addContraintsWithFormat(format: "V:|-60-[v0(50)]-2-|", views: barberShopDistancePlaceHolder)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
