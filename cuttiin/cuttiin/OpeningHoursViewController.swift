//
//  OpeningHoursViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/19/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate

class OpeningHoursViewController: UIViewController {
    var daysOfTheWeekBeforePresenting = ["Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    var notRegisteringANewBarberShop = true
    var currentDayWorkHour = [WorkHourButtons]()
    
    let InputContainerView: UIView = {
        let icview = UIView()
        icview.translatesAutoresizingMaskIntoConstraints = false
        return icview
    }()
    
    lazy var mondayHourButton: UIButton = {
        let mbutton = UIButton()
        mbutton.translatesAutoresizingMaskIntoConstraints = false
        mbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        mbutton.setTitle(NSLocalizedString("mondayButtonOpeningHoursView", comment: "MON, CLOSED"), for: .normal)
        mbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        mbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        mbutton.layer.masksToBounds = true
        mbutton.layer.borderWidth = 2
        mbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        mbutton.layer.cornerRadius = 5
        mbutton.addTarget(self, action: #selector(handleHourSelected), for: .touchUpInside)
        return mbutton
    }()
    
    lazy var tuesdayHourButton: UIButton = {
        let tbutton = UIButton()
        tbutton.translatesAutoresizingMaskIntoConstraints = false
        tbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        tbutton.setTitle(NSLocalizedString("tuesdayButtonOpeningHoursView", comment: "TUE, CLOSED"), for: .normal)
        tbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        tbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        tbutton.layer.masksToBounds = true
        tbutton.layer.borderWidth = 2
        tbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        tbutton.layer.cornerRadius = 5
        tbutton.addTarget(self, action: #selector(handleHourSelected), for: .touchUpInside)
        return tbutton
    }()
    
    lazy var wednesdayHourButton: UIButton = {
        let wbutton = UIButton()
        wbutton.translatesAutoresizingMaskIntoConstraints = false
        wbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        wbutton.setTitle(NSLocalizedString("wednesdayButtonOpeningHoursView", comment: "WED, CLOSED"), for: .normal)
        wbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        wbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        wbutton.layer.masksToBounds = true
        wbutton.layer.borderWidth = 2
        wbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        wbutton.layer.cornerRadius = 5
        wbutton.addTarget(self, action: #selector(handleHourSelected), for: .touchUpInside)
        return wbutton
    }()
    
    lazy var thursdayHourButton: UIButton = {
        let thbutton = UIButton()
        thbutton.translatesAutoresizingMaskIntoConstraints = false
        thbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        thbutton.setTitle(NSLocalizedString("thursdayButtonOpeningHoursView", comment: "THU, CLOSED"), for: .normal)
        thbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        thbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        thbutton.layer.masksToBounds = true
        thbutton.layer.borderWidth = 2
        thbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        thbutton.layer.cornerRadius = 5
        thbutton.addTarget(self, action: #selector(handleHourSelected), for: .touchUpInside)
        return thbutton
    }()
    
    lazy var fridayHourButton: UIButton = {
        let frbutton = UIButton()
        frbutton.translatesAutoresizingMaskIntoConstraints = false
        frbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        frbutton.setTitle(NSLocalizedString("fridayButtonOpeningHoursView", comment: "FRI, CLOSED"), for: .normal)
        frbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        frbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        frbutton.layer.masksToBounds = true
        frbutton.layer.borderWidth = 2
        frbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        frbutton.layer.cornerRadius = 5
        frbutton.addTarget(self, action: #selector(handleHourSelected), for: .touchUpInside)
        return frbutton
    }()
    
    lazy var saturdayHourButton: UIButton = {
        let sabutton = UIButton()
        sabutton.translatesAutoresizingMaskIntoConstraints = false
        sabutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        sabutton.setTitle(NSLocalizedString("saturdayButtonOpeningHoursView", comment: "SAT, CLOSED"), for: .normal)
        sabutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        sabutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        sabutton.layer.masksToBounds = true
        sabutton.layer.borderWidth = 2
        sabutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        sabutton.layer.cornerRadius = 5
        sabutton.addTarget(self, action: #selector(handleHourSelected), for: .touchUpInside)
        return sabutton
    }()
    
    lazy var sundayHourButton: UIButton = {
        let sunbutton = UIButton()
        sunbutton.translatesAutoresizingMaskIntoConstraints = false
        sunbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        sunbutton.setTitle(NSLocalizedString("sundayButtonOpeningHoursView", comment: "SUN, CLOSED"), for: .normal)
        sunbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        sunbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        sunbutton.layer.masksToBounds = true
        sunbutton.layer.borderWidth = 2
        sunbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        sunbutton.layer.cornerRadius = 5
        sunbutton.addTarget(self, action: #selector(handleHourSelected), for: .touchUpInside)
        return sunbutton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        view.addSubview(InputContainerView)
        setupViewConstraints()
        getWorksForDaysOfTheWeek()
        if !notRegisteringANewBarberShop {
            navigationItem.title = NSLocalizedString("navigationTitleTextFirstOpeningHourView", comment: "Opening and Closing Times")
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
        } else {
            navigationItem.title = NSLocalizedString("navigationTitleTextSecondOpeningHours", comment: "Opening Hours: Step 3 of 5")
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(handleDoneAction))
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    @objc func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Auth.auth().currentUser?.uid == nil {
            dismiss(animated: false, completion: nil)
            return
        }else {
            self.workHoursToBeDeleted()
            if let netAvail = UserDefaults.standard.object(forKey: "theOpeningAndClosingTimeWasSet") as? [String], let openTime = UserDefaults.standard.object(forKey: "theOpeningTimeWasSet") as? String, let closingTime = UserDefaults.standard.object(forKey: "theClosingTimeWasSet") as? String{
                
                for daysCho in netAvail {
                    print(daysCho)
                    switch daysCho {
                    case "Monday":
                        self.mondayHourButton.setTitle(NSLocalizedString("mondayOpenTextButtonOpeningTimeView", comment: "MON, OPEN: ") + openTime + NSLocalizedString("closedOpenTextButtonOpeningTimeView", comment: " - CLOSE: ") + closingTime, for: .normal)
                        UserDefaults.standard.set("", forKey: "theOpeningAndClosingTimeWasSet")
                        UserDefaults.standard.removeObject(forKey: "theOpeningAndClosingTimeWasSet")
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    case "Tuesday":
                        self.tuesdayHourButton.setTitle(NSLocalizedString("tuesdayOpenTextButtonOpeningTimeView", comment: "TUE, OPEN: ") + openTime + NSLocalizedString("closedOpenTextButtonOpeningTimeView", comment: " - CLOSE: ") + closingTime, for: .normal)
                        UserDefaults.standard.set("", forKey: "theOpeningAndClosingTimeWasSet")
                        UserDefaults.standard.removeObject(forKey: "theOpeningAndClosingTimeWasSet")
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    case "Wednesday":
                        self.wednesdayHourButton.setTitle(NSLocalizedString("wednesdayOpenTextButtonOpeningTimeView", comment: "WED, OPEN: ") + openTime + NSLocalizedString("closedOpenTextButtonOpeningTimeView", comment: " - CLOSE: ") + closingTime, for: .normal)
                        UserDefaults.standard.set("", forKey: "theOpeningAndClosingTimeWasSet")
                        UserDefaults.standard.removeObject(forKey: "theOpeningAndClosingTimeWasSet")
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    case "Thursday":
                        self.thursdayHourButton.setTitle(NSLocalizedString("thurdayOpenTextButtonOpeningTimeView", comment: "THUR, OPEN: ") + openTime + NSLocalizedString("closedOpenTextButtonOpeningTimeView", comment: " - CLOSE: ") + closingTime, for: .normal)
                        UserDefaults.standard.set("", forKey: "theOpeningAndClosingTimeWasSet")
                        UserDefaults.standard.removeObject(forKey: "theOpeningAndClosingTimeWasSet")
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    case "Friday":
                        self.fridayHourButton.setTitle(NSLocalizedString("fridayOpenTextButtonOpeningTimeView", comment: "FRI, OPEN: ") + openTime + NSLocalizedString("closedOpenTextButtonOpeningTimeView", comment: " - CLOSE: ") + closingTime, for: .normal)
                        UserDefaults.standard.set("", forKey: "theOpeningAndClosingTimeWasSet")
                        UserDefaults.standard.removeObject(forKey: "theOpeningAndClosingTimeWasSet")
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    case "Saturday":
                        self.saturdayHourButton.setTitle(NSLocalizedString("saturdayOpenTextButtonOpeningTimeView", comment: "SAT, OPEN: ") + openTime + NSLocalizedString("closedOpenTextButtonOpeningTimeView", comment: " - CLOSE: ") + closingTime, for: .normal)
                        UserDefaults.standard.set("", forKey: "theOpeningAndClosingTimeWasSet")
                        UserDefaults.standard.removeObject(forKey: "theOpeningAndClosingTimeWasSet")
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    case "Sunday":
                        self.sundayHourButton.setTitle(NSLocalizedString("sundayOpenTextButtonOpeningTimeView", comment: "SUN, OPEN: ") + openTime + NSLocalizedString("closedOpenTextButtonOpeningTimeView", comment: " - CLOSE: ") + closingTime, for: .normal)
                        UserDefaults.standard.set("", forKey: "theOpeningAndClosingTimeWasSet")
                        UserDefaults.standard.removeObject(forKey: "theOpeningAndClosingTimeWasSet")
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    default:
                        print("Sky high")
                    }
                }
                
            }else {
                print("It did not work")
            }
        }
    }
    
    
    func workHoursToBeDeleted(){
        if let netAvail = UserDefaults.standard.object(forKey: "workHoursDeleted") as? String{
            switch netAvail {
            case "Monday":
                self.mondayHourButton.setTitle(NSLocalizedString("mondayButtonOpeningHoursView", comment: "MON, CLOSED"), for: .normal)
                UserDefaults.standard.set("", forKey: "workHoursDeleted")
                UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
            case "Tuesday":
                self.tuesdayHourButton.setTitle(NSLocalizedString("tuesdayButtonOpeningHoursView", comment: "TUE, CLOSED"), for: .normal)
                UserDefaults.standard.set("", forKey: "workHoursDeleted")
                UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
            case "Wednesday":
                self.wednesdayHourButton.setTitle(NSLocalizedString("wednesdayButtonOpeningHoursView", comment: "WED, CLOSED"), for: .normal)
                UserDefaults.standard.set("", forKey: "workHoursDeleted")
                UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
            case "Thursday":
                self.thursdayHourButton.setTitle(NSLocalizedString("thursdayButtonOpeningHoursView", comment: "THU, CLOSED"), for: .normal)
                UserDefaults.standard.set("", forKey: "workHoursDeleted")
                UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
            case "Friday":
                self.fridayHourButton.setTitle(NSLocalizedString("fridayButtonOpeningHoursView", comment: "FRI, CLOSED"), for: .normal)
                UserDefaults.standard.set("", forKey: "workHoursDeleted")
                UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
            case "Saturday":
                self.saturdayHourButton.setTitle(NSLocalizedString("saturdayButtonOpeningHoursView", comment: "SAT, CLOSED"), for: .normal)
                UserDefaults.standard.set("", forKey: "workHoursDeleted")
                UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
            case "Sunday":
                self.sundayHourButton.setTitle(NSLocalizedString("sundayButtonOpeningHoursView", comment: "SUN, CLOSED"), for: .normal)
                UserDefaults.standard.set("", forKey: "workHoursDeleted")
                UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
            default:
                print("Sky high")
            }
        }
    }
    
    func getWorksForDaysOfTheWeek(){
        let daysOfTheWeekToBeUsed = ["Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        
        for day in daysOfTheWeekToBeUsed{
            self.getOpeningAndCLosingTimeForChoosenDate(daySelect: day)
        }
    }
    
    func getOpeningAndCLosingTimeForChoosenDate(daySelect: String){
        let currentDateGotten = DateInRegion()
        if let uid = Auth.auth().currentUser?.uid {
            let firebaseRefere = Database.database().reference()
            firebaseRefere.child("workhours").child(uid).child(daySelect).observeSingleEvent(of: .value, with: { (snapshooottss) in
                if let dictionary = snapshooottss.value as? [String: AnyObject], HandleDataRequest.handleWorkhoursNode(firebaseData: dictionary) == true {
                    let workhour = Workhours()
                    workhour.setValuesForKeys(dictionary)
                    if let openTimeHour = workhour.openingTimeHour, let openTimeMinute = workhour.openingTimeMinute, let openTimeSecond = workhour.openingTimeSecond, let closingTimeHour = workhour.closingTimeHour, let closingTimeMinute = workhour.closingTimeMinute, let closingTimeSecond = workhour.closingTimeSecond {
                        
                        guard let openHour = Int(openTimeHour), let openMinute = Int(openTimeMinute), let openSecond = Int(openTimeSecond), let closeHour = Int(closingTimeHour), let closeMinute = Int(closingTimeMinute), let closeSecond = Int(closingTimeSecond) else {
                            print("conversionof open close time to int failed")
                            return
                        }
                        
                        if let startTimeDate = currentDateGotten.dateBySet(hour: openHour, min: openMinute, secs: openSecond), let closeTimeDate = currentDateGotten.dateBySet(hour: closeHour, min: closeMinute, secs: closeSecond) {
                            let workhourbuttonHold = WorkHourButtons()
                            workhourbuttonHold.dayOfTheWeek = daySelect
                            workhourbuttonHold.openingClosingTimeString = "\(startTimeDate.toString(DateToStringStyles.time(.short))) - \(closeTimeDate.toString(DateToStringStyles.time(.short)))"
                            self.setupButtonValuesForWorkHour(daySchedule: workhourbuttonHold)
                        }
                    }
                    
                    
                }else {
                    print("Day work hours not gotten")
                }
            }, withCancel: { (erorororor) in
                print(erorororor)
            })
        }
    }
    
    func setupButtonValuesForWorkHour(daySchedule: WorkHourButtons){
        if let day = daySchedule.dayOfTheWeek, let timeHold = daySchedule.openingClosingTimeString {
            switch day {
            case "Monday":
                self.mondayHourButton.setTitle(NSLocalizedString("mondayUpdateTextOpeningTimeView", comment: "MON, ") + timeHold, for: .normal)
            case "Tuesday":
                self.tuesdayHourButton.setTitle(NSLocalizedString("tuesdayUpdateTextOpeningTimeView", comment: "TUE, ") + timeHold, for: .normal)
            case "Wednesday":
                self.wednesdayHourButton.setTitle(NSLocalizedString("wednesdayUpdateTextOpeningTimeView", comment: "WED, ") + timeHold, for: .normal)
            case "Thursday":
                self.thursdayHourButton.setTitle(NSLocalizedString("thursdayUpdateTextOpeningTimeView", comment: "THUR, ") + timeHold, for: .normal)
            case "Friday":
                self.fridayHourButton.setTitle(NSLocalizedString("fridayUpdateTextOpeningTimeView", comment: "FRI, ") + timeHold, for: .normal)
            case "Saturday":
                self.saturdayHourButton.setTitle(NSLocalizedString("saturdayUpdateTextOpeningTimeView", comment: "SAT, ") + timeHold, for: .normal)
            case "Sunday":
                self.sundayHourButton.setTitle(NSLocalizedString("sundayUpdateTextOpeningTimeView", comment: "SUN, ") + timeHold, for: .normal)
            default:
                print("Sky high")
            }
        }
    }
    
    func setupViewConstraints(){
        InputContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        InputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        InputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        InputContainerView.heightAnchor.constraint(equalToConstant: 400).isActive = true
        
        InputContainerView.addSubview(mondayHourButton)
        InputContainerView.addSubview(tuesdayHourButton)
        InputContainerView.addSubview(wednesdayHourButton)
        InputContainerView.addSubview(thursdayHourButton)
        InputContainerView.addSubview(fridayHourButton)
        InputContainerView.addSubview(saturdayHourButton)
        InputContainerView.addSubview(sundayHourButton)
        
        mondayHourButton.topAnchor.constraint(equalTo: InputContainerView.topAnchor).isActive = true
        mondayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor).isActive = true
        mondayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        mondayHourButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        tuesdayHourButton.topAnchor.constraint(equalTo: mondayHourButton.bottomAnchor, constant: 10).isActive = true
        tuesdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor).isActive = true
        tuesdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        tuesdayHourButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        wednesdayHourButton.topAnchor.constraint(equalTo: tuesdayHourButton.bottomAnchor, constant: 10).isActive = true
        wednesdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor).isActive = true
        wednesdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        wednesdayHourButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        thursdayHourButton.topAnchor.constraint(equalTo: wednesdayHourButton.bottomAnchor, constant: 10).isActive = true
        thursdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor).isActive = true
        thursdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        thursdayHourButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        fridayHourButton.topAnchor.constraint(equalTo: thursdayHourButton.bottomAnchor, constant: 10).isActive = true
        fridayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor).isActive = true
        fridayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        fridayHourButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        saturdayHourButton.topAnchor.constraint(equalTo: fridayHourButton.bottomAnchor, constant: 10).isActive = true
        saturdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor).isActive = true
        saturdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        saturdayHourButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        sundayHourButton.topAnchor.constraint(equalTo: saturdayHourButton.bottomAnchor, constant: 10).isActive = true
        sundayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor).isActive = true
        sundayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        sundayHourButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
    }
    
    @objc func handleHourSelected(sender: Any){
        
        guard let pressedAct = (sender as? UIButton) else {
            return
        }
        
        switch pressedAct {
        case self.mondayHourButton:
            self.openingAndClosingTimeView(specificDay: "Monday")
        case self.tuesdayHourButton:
            self.openingAndClosingTimeView(specificDay: "Tuesday")
        case self.wednesdayHourButton:
            self.openingAndClosingTimeView(specificDay: "Wednesday")
        case self.thursdayHourButton:
            self.openingAndClosingTimeView(specificDay: "Thursday")
        case self.fridayHourButton:
            self.openingAndClosingTimeView(specificDay: "Friday")
        case self.saturdayHourButton:
            self.openingAndClosingTimeView(specificDay: "Saturday")
        case self.sundayHourButton:
            self.openingAndClosingTimeView(specificDay: "Sunday")
        default:
            print("Another button")
        }
        
    }
    
    func openingAndClosingTimeView(specificDay: String){
        let daytimeChoice = DayTimeChoiceViewController()
        
        
        switch specificDay {
        case "Monday":
            daytimeChoice.navigationItem.title = NSLocalizedString("mondayNavigationTitleOpeningTimeView", comment: "Monday")
        case "Tuesday":
            daytimeChoice.navigationItem.title = NSLocalizedString("tuesdayNavigationTitleOpeningTimeView", comment: "Tuesday")
        case "Wednesday":
            daytimeChoice.navigationItem.title = NSLocalizedString("wednesdayNavigationTitleOpeningTimeView", comment: "Wednesday")
        case "Thursday":
            daytimeChoice.navigationItem.title = NSLocalizedString("thursdayNavigationTitleOpeningTimeView", comment: "Thursday")
        case "Friday":
            daytimeChoice.navigationItem.title = NSLocalizedString("fridayNavigationTitleOpeningTimeView", comment: "Friday")
        case "Saturday":
            daytimeChoice.navigationItem.title = NSLocalizedString("saturdayNavigationTitleOpeningTimeView", comment: "Saturday")
        case "Sunday":
            daytimeChoice.navigationItem.title = NSLocalizedString("sundayNavigationTitleOpeningTimeView", comment: "Sunday")
        default:
            print("Sky high")
        }
        
        daytimeChoice.specificDayData = specificDay
        self.daysOfTheWeekBeforePresenting = daysOfTheWeekBeforePresenting.filter { $0 != specificDay }
        daytimeChoice.daysOfTheWeek = self.daysOfTheWeekBeforePresenting
        navigationController?.pushViewController(daytimeChoice, animated: true)
        self.daysOfTheWeekBeforePresenting = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    }
    
    @objc func handleDoneAction(sender: Any){
        UserDefaults.standard.set("hello", forKey: "theOpeningAndClosingTimeWasSet")
        UserDefaults.standard.set("hello", forKey: "workHoursDeleted")
        let addServiceView = AddServiceViewController()
        let navController = UINavigationController(rootViewController: addServiceView)
        present(navController, animated: true, completion: nil)
        
    }

}
