//
//  ZoomImageViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/29/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import ZoomImageView

class ZoomImageViewController: UIViewController {
    var tappedImage: UIImage?
    
    lazy var closeThisViewButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.setTitle(NSLocalizedString("closeButtonTitleUploadPictureView", comment: "CLOSE"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 17)
        fbutton.addTarget(self, action: #selector(handleDismissView), for: .touchUpInside)
        return fbutton
    }()
    
    let serviceDescriptionThumbnailImageView: ZoomImageView = {
        let tniv = ZoomImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(serviceDescriptionThumbnailImageView)
        view.addSubview(closeThisViewButton)
        view.bringSubview(toFront: closeThisViewButton)
        setupViewContraints()
        serviceDescriptionThumbnailImageView.image = tappedImage
    }
    
    func setupViewContraints(){
        serviceDescriptionThumbnailImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        serviceDescriptionThumbnailImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        serviceDescriptionThumbnailImageView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -5).isActive = true
        serviceDescriptionThumbnailImageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        closeThisViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 25).isActive = true
        closeThisViewButton.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        closeThisViewButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        closeThisViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
    }
    
    @objc func handleDismissView(){
        dismiss(animated: true, completion: nil)
    }
}
