//
//  CustomDateAmountHolder.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/6/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

@objcMembers class CustomDateAmountHolder: NSObject {
    var monthName: String?
    var datePaid: DateInRegion?
    var amount: Double?
    var bookingUUID: String?
}
