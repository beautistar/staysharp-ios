//
//  Payments.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Payments: NSObject {
    var appDateCreated: String?
    var appTimezone: String?
    var appCalendar: String?
    var appLocale: String?
    var barberShopUUIDD: String?
    var barberUUIDD: String?
    var bookingUUIDD: String?
    var customerUUID: String?
    var formattedDateString: String?
    var paymentID: String?
    var dateCreated: String?
    var serviceUUIDD: String?
    var priceTotal: String?
    var paymentTypeOperation: String?
    var payment_qp_status_msg: String?
    var payment_aq_status_msg: String?
    
    var captureAppDate: String?
    var captureAppTimeZone: String?
    var captureAppCalendar: String?
    var captureAppLocal: String?
    var paymentTypeCaptureOperation: String?
    var paymentCaptureDate: String?
    var capture_qp_status_msg: String?
    var capture_aq_status_msg: String?
    
    var cancelAppDate: String?
    var cancelAppTimeZone: String?
    var cancelAppCalendar: String?
    var cancelAppLocal: String?
    var paymentTypeCancelOperation: String?
    var paymentCancelDate: String?
    var cancel_qp_status_msg: String?
    var cancel_aq_status_msg: String?
    
    var refundAppDate: String?
    var refundAppTimeZone: String?
    var refundAppCalendar: String?
    var refundAppLocal: String?
    var paymentTypeRefundOperation: String?
    var paymentRefundDate: String?
    var refund_qp_status_msg: String?
    var refund_aq_status_msg: String?
    var amountRefunded: String?
    
    var notificationSent: String?
    var methodOfPayment: String?
}
































