//
//  AddServiceViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/22/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate

class AddServiceViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var notRegisteringANewBarberShop = true
    var noteditingService = true
    var barberShopID: String?
    var serviceID: String?
    var notEditingNotRegistering = true
    var barberShopBarbersAvailable = [Barber]()
    var barberShopBarbersAvailableStringID = [String]()
    var selectedbarberShopBarbers = [String]()
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    let picker = UIImagePickerController()
    
    let imageAndServiceContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_photo_smallest")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let serviceTitleHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("serviceTitleTextAddServiceView", comment: "Service Title")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceTitleTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("serviceTitleTextAddServiceView", comment: "Service Title")
        em.addTarget(self, action: #selector(serviceTitletextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(serviceTitletextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let serviceTitleSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let estimatedTimeHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("estimatedTimeTextAddServiceView", comment: "Estimated time")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let estimatedTimeTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        em.placeholder = NSLocalizedString("estimatedMinTimeAddServiceView", comment: "Min")
        em.keyboardType = .numberPad
        em.textAlignment = .center
        em.layer.borderWidth = 2
        em.layer.borderColor = UIColor.black.cgColor
        em.layer.cornerRadius = 5
        em.layer.masksToBounds = true
        em.addTarget(self, action: #selector(estimatedTimetextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(estimatedTimetextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let serviceCostHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("serviceCostTextAddServiceView", comment: "Cost")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceCostTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        em.textAlignment = .center
        em.placeholder = "-"
        em.keyboardType = .decimalPad
        em.layer.borderWidth = 2
        em.layer.borderColor = UIColor.black.cgColor
        em.layer.cornerRadius = 5
        em.layer.masksToBounds = true
        em.addTarget(self, action: #selector(serviceCosttextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(serviceCosttextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let serviceContainerView: UIView = {
        let scview = UIView()
        scview.translatesAutoresizingMaskIntoConstraints = false
        return scview
    }()
    
    lazy var styleSelectionSegmentedControl: UISegmentedControl = {
        let sssegmentcontrol = UISegmentedControl(items: [NSLocalizedString("ladiesCartegoryTabBarberShopProfileView", comment: "LADIES"), NSLocalizedString("gentsCartegoryTabBarberShopProfileView", comment: "GENTS"), NSLocalizedString("kidsCartegoryTabBarberShopProfileView", comment: "KIDS")])
        sssegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        sssegmentcontrol.tintColor = UIColor(r: 23, g: 69, b: 90)
        sssegmentcontrol.selectedSegmentIndex = 1
        sssegmentcontrol.addTarget(self, action: #selector(handleSegmentedControlSelection), for: .valueChanged)
        return sssegmentcontrol
    }()
    
    let shortDescriptionHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("shortDescriptionTextNumberOfCustomer", comment: "Short Description")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let shortDescriptionTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.lightGray
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.text = NSLocalizedString("shortDescriptionTextNumberOfCustomer", comment: "Short Description")
        em.textAlignment = .justified
        em.isEditable = true
        em.isSelectable = true
        em.dataDetectorTypes = UIDataDetectorTypes.link
        em.autocorrectionType = UITextAutocorrectionType.yes
        em.spellCheckingType = UITextSpellCheckingType.yes
        em.layer.borderColor = UIColor.black.cgColor
        em.layer.borderWidth = 2
        em.layer.masksToBounds = true
        em.layer.cornerRadius = 5
        return em
    }()
    
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.white
        cv.allowsMultipleSelection = true
        cv.register(customSelectBarbersCollectionViewCellBarbers.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionTextAddServiceView", comment: "This will add the service in the Gents section only")
        ht.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.numberOfLines = 0
        ht.textColor = UIColor(r: 23, g: 69, b: 90)
        ht.textAlignment = .center
        return ht
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        picker.delegate = self
        if !noteditingService {
            let deleteButton = UIBarButtonItem(title: NSLocalizedString("deleteButtonNavigationButtonAddServiceView", comment: "Delete"), style: .done, target: self, action: #selector(handleDeleteActionSelection))
            deleteButton.tintColor = UIColor.red
            let nextButton = UIBarButtonItem(title: NSLocalizedString("doneButtonPaymentView", comment: "Done"), style: .done, target: self, action: #selector(postUpdatedServiceDetails))
            navigationItem.rightBarButtonItems = [nextButton, deleteButton]
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
        } else {
            navigationItem.title = NSLocalizedString("addServiceHeaderTextForRegistrationNewUsersView", comment: "Add a Service: Step 4 of 5")
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("nextButtonNumberOfCustomer", comment: "Next"), style: .done, target: self, action: #selector(handleUserSegementSelection))
        }
        
        navigationItem.rightBarButtonItems?[safe: 0]?.isEnabled = false
        view.addSubview(scrollView)
        estimatedTimeTextField.delegate = self
        serviceCostTextField.delegate = self
        shortDescriptionTextField.delegate = self
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectConstriants()
        if !notRegisteringANewBarberShop {
            navigationItem.title = NSLocalizedString("notRegisteringHeaderTextHolderLabelAddServiceView", comment: "Add a Service")
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
            self.getThisServiceAssociatedBarbers()
        }
        handleEditService()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Auth.auth().currentUser?.uid == nil {
            dismiss(animated: false, completion: nil)
        }
        
    }
    
    func handleEditService(){
        if !noteditingService {
            if let shopID = self.barberShopID, let serveID = self.serviceID {
                let firebaseReferenceEditService = Database.database().reference()
                firebaseReferenceEditService.child("service").child(shopID).child(serveID).observeSingleEvent(of: .value, with: { (snapshotEditService) in
                    if let dictionaryEditService = snapshotEditService.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionaryEditService) == true {
                        let editService = Service()
                        editService.setValuesForKeys(dictionaryEditService)
                        if let serveTitle = editService.serviceTitle, let servicePic = editService.serviceImageUrl, let serviceDescroption = editService.shortDescription, let estimatedTime = editService.estimatedTime, let estimatedPrice = editService.serviceCost, let categoryService = editService.category {
                            self.serviceTitleTextField.text = serveTitle
                            self.selectImageView.loadImagesUsingCacheWithUrlString(urlString: servicePic)
                            self.estimatedTimeTextField.text = estimatedTime
                            self.serviceCostTextField.text = estimatedPrice
                            self.shortDescriptionTextField.text = serviceDescroption
                            
                            switch categoryService {
                            case "Ladies":
                                self.styleSelectionSegmentedControl.selectedSegmentIndex = 0
                            case "Gents":
                                self.styleSelectionSegmentedControl.selectedSegmentIndex = 1
                            case "Kids":
                                self.styleSelectionSegmentedControl.selectedSegmentIndex = 2
                            default:
                                print("Sky High")
                            }
                        }
                    }
                })
            }
        }
    }
    
    @objc func postUpdatedServiceDetails(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        if let shopID = self.barberShopID, let serveID = self.serviceID, let serviceTitle = serviceTitleTextField.text, let estimatedTime = self.estimatedTimeTextField.text, let serviceCost = self.serviceCostTextField.text, let shortDescription = self.shortDescriptionTextField.text {
            
            
            let imageName = NSUUID().uuidString
            let storageRef = Storage.storage().reference().child("serviceImage").child("\(imageName).jpeg")
            
            if let profileImage = selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
                storageRef.putData(uploadData, metadata: nil, completion: { (metaData, errorrr) in
                    if let error = errorrr {
                        self.errorMessagePlaceHolder.text = error.localizedDescription
                        print(error.localizedDescription)
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    storageRef.downloadURL(completion: { (urlllll, errorororororoor) in
                        guard let downloadURL = urlllll?.absoluteString else {
                            // Uh-oh, an error occurred!
                            return
                        }
                        
                        let firebaseReferenceUpdateService = Database.database().reference()
                        firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("serviceTitle").setValue(serviceTitle)
                        firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("shortDescription").setValue(shortDescription)
                        firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("estimatedTime").setValue(estimatedTime)
                        firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("serviceCost").setValue(serviceCost)
                        firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("serviceImageName").setValue("\(imageName).jpeg")
                        firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("serviceImageUrl").setValue(downloadURL)
                        if let crCode = Locale.current.currencyCode {
                            firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("localCurrency").setValue(crCode)
                        }
                        
                        let selectionMade = self.styleSelectionSegmentedControl.selectedSegmentIndex
                        switch selectionMade {
                        case 0:
                            firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("category").setValue("Ladies")
                        case 1:
                            firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("category").setValue("Gents")
                        case 2:
                            firebaseReferenceUpdateService.child("service").child(shopID).child(serveID).child("category").setValue("Kids")
                        default:
                            print("Sky High")
                        }
                        
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.dismiss(animated: true, completion: nil)
                        
                    })
                
                })
            }
            
        }
    }
    
    @objc func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setupViewObjectConstriants(){
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 450)
        
        
        scrollView.addSubview(imageAndServiceContainerView)
        scrollView.addSubview(styleSelectionSegmentedControl)
        scrollView.addSubview(shortDescriptionHiddenPlaceHolder)
        scrollView.addSubview(shortDescriptionTextField)
        scrollView.addSubview(collectView)
        scrollView.addSubview(descriptionTitle)
        scrollView.addSubview(errorMessagePlaceHolder)
        
        imageAndServiceContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 5).isActive = true
        imageAndServiceContainerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        imageAndServiceContainerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        imageAndServiceContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        imageAndServiceContainerView.addSubview(selectImageView)
        imageAndServiceContainerView.addSubview(serviceContainerView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndServiceContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: imageAndServiceContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: imageAndServiceContainerView.widthAnchor, multiplier: 0.25).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndServiceContainerView.heightAnchor).isActive = true
        
        serviceContainerView.topAnchor.constraint(equalTo: imageAndServiceContainerView.topAnchor).isActive = true
        serviceContainerView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        serviceContainerView.rightAnchor.constraint(equalTo: imageAndServiceContainerView.rightAnchor).isActive = true
        serviceContainerView.heightAnchor.constraint(equalTo: imageAndServiceContainerView.heightAnchor).isActive = true
        
        serviceContainerView.addSubview(serviceTitleHiddenPlaceHolder)
        serviceContainerView.addSubview(serviceTitleTextField)
        serviceContainerView.addSubview(serviceTitleSeperatorView)
        serviceContainerView.addSubview(estimatedTimeHiddenPlaceHolder)
        serviceContainerView.addSubview(estimatedTimeTextField)
        serviceContainerView.addSubview(serviceCostHiddenPlaceHolder)
        serviceContainerView.addSubview(serviceCostTextField)
        
        serviceTitleHiddenPlaceHolder.topAnchor.constraint(equalTo: serviceContainerView.topAnchor).isActive = true
        serviceTitleHiddenPlaceHolder.centerXAnchor.constraint(equalTo: serviceContainerView.centerXAnchor).isActive = true
        serviceTitleHiddenPlaceHolder.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor).isActive = true
        serviceTitleHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        serviceTitleTextField.topAnchor.constraint(equalTo: serviceTitleHiddenPlaceHolder.bottomAnchor).isActive = true
        serviceTitleTextField.centerXAnchor.constraint(equalTo: serviceContainerView.centerXAnchor).isActive = true
        serviceTitleTextField.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor).isActive = true
        serviceTitleTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        serviceTitleSeperatorView.leftAnchor.constraint(equalTo: serviceContainerView.leftAnchor).isActive = true
        serviceTitleSeperatorView.topAnchor.constraint(equalTo: serviceTitleTextField.bottomAnchor).isActive = true
        serviceTitleSeperatorView.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor).isActive = true
        serviceTitleSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        estimatedTimeTextField.bottomAnchor.constraint(equalTo: serviceContainerView.bottomAnchor).isActive = true
        estimatedTimeTextField.leftAnchor.constraint(equalTo: serviceContainerView.leftAnchor).isActive = true
        estimatedTimeTextField.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor, multiplier: 0.5).isActive = true
        estimatedTimeTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        serviceCostTextField.bottomAnchor.constraint(equalTo: serviceContainerView.bottomAnchor).isActive = true
        serviceCostTextField.leftAnchor.constraint(equalTo: estimatedTimeTextField.rightAnchor, constant: 20).isActive = true
        serviceCostTextField.rightAnchor.constraint(equalTo: serviceContainerView.rightAnchor).isActive = true
        serviceCostTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        estimatedTimeHiddenPlaceHolder.bottomAnchor.constraint(equalTo: estimatedTimeTextField.topAnchor).isActive = true
        estimatedTimeHiddenPlaceHolder.leftAnchor.constraint(equalTo: serviceContainerView.leftAnchor).isActive = true
        estimatedTimeHiddenPlaceHolder.widthAnchor.constraint(equalTo: estimatedTimeTextField.widthAnchor).isActive = true
        estimatedTimeHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        serviceCostHiddenPlaceHolder.bottomAnchor.constraint(equalTo: serviceCostTextField.topAnchor).isActive = true
        serviceCostHiddenPlaceHolder.rightAnchor.constraint(equalTo: serviceContainerView.rightAnchor).isActive = true
        serviceCostHiddenPlaceHolder.widthAnchor.constraint(equalTo: serviceCostTextField.widthAnchor).isActive = true
        serviceCostHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        styleSelectionSegmentedControl.topAnchor.constraint(equalTo: imageAndServiceContainerView.bottomAnchor, constant: 10).isActive = true
        styleSelectionSegmentedControl.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        styleSelectionSegmentedControl.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        styleSelectionSegmentedControl.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shortDescriptionHiddenPlaceHolder.topAnchor.constraint(equalTo: styleSelectionSegmentedControl.bottomAnchor, constant: 10).isActive = true
        shortDescriptionHiddenPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        shortDescriptionHiddenPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        shortDescriptionHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shortDescriptionTextField.topAnchor.constraint(equalTo: shortDescriptionHiddenPlaceHolder.bottomAnchor).isActive = true
        shortDescriptionTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        shortDescriptionTextField.heightAnchor.constraint(equalToConstant: 100).isActive = true
        shortDescriptionTextField.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        
        descriptionTitle.topAnchor.constraint(equalTo: shortDescriptionTextField.bottomAnchor, constant: 10).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        collectView.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 5).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    @objc func textFieldDidChange(){
        if serviceTitleTextField.text == "" || shortDescriptionTextField.text == NSLocalizedString("shortDescriptionTextNumberOfCustomer", comment: "Short Description") || shortDescriptionTextField.text == "" || estimatedTimeTextField.text == "" || serviceCostTextField.text == "" {
            //Disable button
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            if !noteditingService {
                self.navigationItem.rightBarButtonItems?[safe: 0]?.isEnabled = false
            }
            
            
        } else {
            //Enable button
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            if !noteditingService {
                self.navigationItem.rightBarButtonItems?[safe: 0]?.isEnabled = true
            } else {
                if !self.notRegisteringANewBarberShop {
                    print("ziko manan bay")
                    if self.selectedbarberShopBarbers.count > 0 {
                        print("ziko")
                        self.navigationItem.rightBarButtonItems?[safe: 0]?.isEnabled = true
                    } else {
                        self.navigationItem.rightBarButtonItems?[safe: 0]?.isEnabled = false
                    }
                }
            }
        }
    }
    
    @objc func serviceTitletextFieldDidChange(){
        self.serviceTitleSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.estimatedTimeTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceCostTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceTitleHiddenPlaceHolder.isHidden = false
        self.shortDescriptionHiddenPlaceHolder.isHidden = true
        self.shortDescriptionTextField.layer.borderColor = UIColor.black.cgColor
    }
    
    @objc func estimatedTimetextFieldDidChange(){
        self.estimatedTimeTextField.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
        self.serviceCostTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceTitleSeperatorView.backgroundColor = UIColor.black
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = true
        self.shortDescriptionTextField.layer.borderColor = UIColor.black.cgColor
    }
    
    @objc func serviceCosttextFieldDidChange(){
        self.serviceCostTextField.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
        self.estimatedTimeTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceTitleSeperatorView.backgroundColor = UIColor.black
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = true
        self.shortDescriptionTextField.layer.borderColor = UIColor.black.cgColor
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = UIColor.black
        if textView.text == NSLocalizedString("shortDescriptionTextNumberOfCustomer", comment: "Short Description") {textView.text = ""}
        self.serviceTitleSeperatorView.backgroundColor = UIColor.black
        self.estimatedTimeTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceCostTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = false
        textView.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.serviceTitleSeperatorView.backgroundColor = UIColor.black
        self.estimatedTimeTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceCostTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = false
        textView.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
        self.textFieldDidChange()
    }
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("takePhotoAlertViewProfileEdit", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotAlertViewProfileEdit", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            selectImageView.contentMode = .scaleAspectFit
            selectImageView.image = selectedImage
            self.textFieldDidChange()
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = "Sorry, this device has no camera"
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @objc func handleUserSegementSelection(){
        let segmentSelect = self.styleSelectionSegmentedControl.selectedSegmentIndex
        
        switch segmentSelect {
        case 0:
            self.handleAddingServices(selection: "Ladies")
        case 1:
            self.handleAddingServices(selection: "Gents")
        case 2:
            self.handleAddingServices(selection: "Kids")
        default:
            print("Sky High")
        }
        
    }
    
    func getThisServiceAssociatedBarbers(){
        if let uid = Auth.auth().currentUser?.uid {
            let firebaseBarberShopBarbers = Database.database().reference()
            firebaseBarberShopBarbers.child("barberShop-Barbers").child(uid).observeSingleEvent(of: .value, with: { (snapshotBarberShopBarbers) in
                if let dictionaryBarberShopBarber = snapshotBarberShopBarbers.value as? [String: AnyObject] {
                    for shopBarbersHolder in dictionaryBarberShopBarber {
                        if let barberShopBarbersSingle = shopBarbersHolder.value as? [String: AnyObject], HandleDataRequest.handleBarberShopBarbersNode(firebaseData: barberShopBarbersSingle) == true {
                            let barberIDHolder = BarberShopBarbers()
                            barberIDHolder.setValuesForKeys(barberShopBarbersSingle)
                            
                            if let barberIDSingle = barberIDHolder.barberStaffID {
                                let firebaseReferenceBarber = Database.database().reference()
                                firebaseReferenceBarber.child("users").child(barberIDSingle).observeSingleEvent(of: .value, with: { (snapshotBarberDataHold) in
                                    if let dictionaryBarberUser = snapshotBarberDataHold.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryBarberUser) == true {
                                        let userDataSingleHold = Barber()
                                        userDataSingleHold.setValuesForKeys(dictionaryBarberUser)
                                        if let barberUniqueID = userDataSingleHold.uniqueID {
                                            self.barberShopBarbersAvailableStringID.append(barberUniqueID)
                                            self.barberShopBarbersAvailable.append(userDataSingleHold)
                                        }
                                        DispatchQueue.main.async {
                                            self.collectionView.reloadData()
                                        }
                                    }
                                }, withCancel: nil)
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func handleAddingServices(selection: String){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let newDate = DateInRegion()
        let strDate = newDate.toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        
        let newUserLocal = "en"
        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
        let newStrDate = newUserDate.toString(.extended)
        
        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
        
        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
        
        
        guard let uid = Auth.auth().currentUser?.uid, let serviceTitle = serviceTitleTextField.text, let estimatedTime = self.estimatedTimeTextField.text, let serviceCost = self.serviceCostTextField.text, let shortDescription = self.shortDescriptionTextField.text else {
            print("An errror occured")
            return
        }
        print("Data gotten")
        let imageName = NSUUID().uuidString
        let objectName = NSUUID().uuidString
        let ref = Database.database().reference().child("service").child(uid).child(objectName)
        let storageRef = Storage.storage().reference().child("serviceImage").child("\(imageName).jpeg")
        
        if let profileImage = selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1), let crCode = Locale.current.currencyCode {
            print("picture gotten form image view")
            storageRef.putData(uploadData, metadata: nil, completion: { (metaData, errorrr) in
                if let error = errorrr {
                    self.errorMessagePlaceHolder.text = error.localizedDescription
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                print("picture uploaded")
                
                storageRef.downloadURL(completion: { (urlllll, errorrrrrr) in
                    guard let downloadURL = urlllll?.absoluteString else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    
                    let value = ["serviceID": objectName,"serviceTitle": serviceTitle, "estimatedTime": estimatedTime, "serviceCost": serviceCost,"serviceCostLocal":userLocal, "shortDescription": shortDescription, "category": selection, "serviceImageName" : "\(imageName).jpeg", "serviceImageUrl": downloadURL, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe, "localCurrency": crCode, "isDeleted":"NO"]
                    
                    ref.updateChildValues(value, withCompletionBlock: { (errorrrrrr, ref) in
                        if let error = errorrrrrr {
                            print(error.localizedDescription)
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        
                        print("service created")
                        
                        if self.selectedbarberShopBarbers.count > 0 {
                            print("service barber and barber services")
                            for slBarber in self.selectedbarberShopBarbers {
                                let objKey = NSUUID().uuidString
                                let databaseref = Database.database().reference()
                                let serviceBarberRefrence = databaseref.child("serviceBarbers").child(objectName).child(objKey)
                                let values = ["barberID": slBarber, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                                serviceBarberRefrence.updateChildValues(values, withCompletionBlock: { (errrorrrrr, refffz) in
                                    if let error = errrorrrrr {
                                        print(error.localizedDescription)
                                        self.activityIndicator.stopAnimating()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        return
                                    }
                                    let barberServiceRefrence = databaseref.child("barberServices").child(slBarber).child(objKey)
                                    let value2 = ["serviceID":objectName, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                                    barberServiceRefrence.updateChildValues(value2, withCompletionBlock: { (errrorrri, refffss) in
                                        if let error = errrorrri {
                                            print(error.localizedDescription)
                                            self.activityIndicator.stopAnimating()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                            return
                                        }
                                        print("servce and barbers sccessfully created")
                                    })
                                })
                                
                            }
                        }
                        
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.handleMoveToTheNextView()
                        
                    })
                })
                
                
            })
        } else {
            print("brazzen road")
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        
    }
    
    @objc func handleSegmentedControlSelection(){
        let segmentSelect = self.styleSelectionSegmentedControl.selectedSegmentIndex
        
        switch segmentSelect {
        case 0:
            self.descriptionTitle.text = NSLocalizedString("descriptionTextAddServiceViewLadies", comment: "This will add the service in the Ladies section only")
        case 1:
            self.descriptionTitle.text = NSLocalizedString("descriptionTextAddServiceViewGents", comment: "This will add the service in the Gents section only")
        case 2:
            self.descriptionTitle.text = NSLocalizedString("descriptionTextAddServiceViewKids", comment: "This will add the service in the Kids section only")
        default:
            print("Sky High")
        }
    }
    
    func handleMoveToTheNextView(){
        if !notRegisteringANewBarberShop {
            
            if !notEditingNotRegistering {
                self.dismiss(animated: true, completion: nil)
            } else {
                let addBarberView = AddBarberViewController()
                addBarberView.notRegisteringANewBarberShop = false
                addBarberView.addserviceviewcontrl = self
                let navController = UINavigationController(rootViewController: addBarberView)
                present(navController, animated: true, completion: nil)
            }
        }else {
            let addBarberView = AddBarberViewController()
            let navController = UINavigationController(rootViewController: addBarberView)
            present(navController, animated: true, completion: nil)
        }
        
    }
    
    @objc func handleDeleteActionSelection(){
        
        let alertController = UIAlertController(title: NSLocalizedString("deleteHeaderAlertViewAddServiceView", comment: "Delete this Service!"), message: NSLocalizedString("deleteServiceMessageAlertViewAddService", comment: "Are you sure you want to delele this service, as it cannot be undone"), preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("yesButtonAlertOrderDetail", comment: "Yes"), style: .default) { action in
            self.deleteSelectedService()
        }
        alertController.addAction(takePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func deleteSelectedService(){
        if let shopID = self.barberShopID, let serveID = self.serviceID {
            let firebaseDeleteService = Database.database().reference()
            firebaseDeleteService.child("service").child(shopID).child(serveID).child("isDeleted").setValue("YES")
            self.dismiss(animated: true, completion: nil)
        }
    }

}


class customSelectBarbersCollectionViewCellBarbers: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.white
        tniv.contentMode = .scaleAspectFit
        tniv.image = UIImage(named: "check_box_inactive")
        return tniv
    }()
    
    let servicesAvailable: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.text = "Hello"
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.black
        return sv
    }()
    
    func setupViews(){
        addSubview(servicesAvailable)
        addSubview(thumbnailImageView)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.white
        addContraintsWithFormat(format: "H:|-16-[v0]-16-[v1(20)]-16-|", views: servicesAvailable, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: servicesAvailable,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
