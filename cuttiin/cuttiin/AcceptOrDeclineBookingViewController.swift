//
//  AcceptOrDeclineBookingViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate
import SwiftyJSON

class AcceptOrDeclineBookingViewController: UIViewController {
    var buttonTag: Int?
    var baeberShoopUUID: String?
    var bookingID: String?
    var barberID: String?
    var paymentIDDSA: String?
    var paymentAmountString: String?
    var orderDetail = OrderDetailViewController()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    lazy var dataObjectsContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let statusPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 25)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("suceesPlaceHolderTextAcceptDeclineView", comment: "Success!")
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let statusDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("statusDescriptionTextAcceptDeclineView", comment: "You can find your served clients in the calender section")
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let checkImageDescriptionThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.image = UIImage(named: "checkmark_big")
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    lazy var appointmentsButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("appointmentAcceptButtonAceptDeclineView", comment: "APPOINTMENTS"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.addTarget(self, action: #selector(handleShowAppointView), for: .touchUpInside)
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(dataObjectsContanerView)
        //view.addSubview(statusPlaceHolder)
        //view.addSubview(statusDescriptionPlaceHolder)
        //view.addSubview(checkImageDescriptionThumbnailImageView)
        //view.addSubview(appointmentsButton)
        setViewContriants()
        actionSelectionForBookingCompletion()
    }
    
    
    func actionSelectionForBookingCompletion(){
        
        DispatchQueue.global(qos: .background).async {
            
            if let shopID = self.baeberShoopUUID, let bookID = self.bookingID,let barberIDASAA = self.barberID, let customerIDXER = UserDefaults.standard.object(forKey: "dataForCustomerIdentifierOnOrderDetailAccpetDecline") as? String, let payID = self.paymentIDDSA , let payAmString = self.paymentAmountString , let paymentAmountInt = Int(payAmString) {
                let currentTimeDate = DateInRegion().toString(.extended)
                let userTimezone = DateByUserDeviceInitializer.tzone
                let userCalender = DateByUserDeviceInitializer.calenderNow
                let userLocal = DateByUserDeviceInitializer.localCode
                
                
                let newUserLocal = "en"
                let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                let newStrDate = newUserDate.toString(.extended)
                
                let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                
                let correctDateToUse = (localeTimezoneVerify) ? currentTimeDate : newStrDate
                let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                
                let valueNot = ["barberShopID":shopID, "bookingID":bookID,"barberID": barberIDASAA, "customerID": customerIDXER, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe, "serviceCompletedBarber": "NO", "serviceCompletedCustomer": "NO", "reasonForNotCompleted":"not available", "notificationSent":"NO"]
                
                let value = ["barberShopID":shopID, "bookingID":bookID,"barberID": barberIDASAA, "customerID": customerIDXER, "dateCreated": correctDateToUse,  "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe, "serviceCompletedBarber": "YES", "serviceCompletedCustomer":"NO", "reasonForNotCompleted":"not available", "notificationSent":"NO"]
                
                if let cuurentTag = self.buttonTag {
                    switch cuurentTag {
                    case 0:
                        print("didint come")
                        
                        DispatchQueue.main.async {
                            self.handleCaptureTransaction(amountInteger: paymentAmountInt, paymentIDDAX: payID, shopIDSAX: shopID)
                        }
                        self.postBookingCompletion(bookIDD: bookID, dataValue: valueNot)
                    case 1:
                        print("served")
                        
                        DispatchQueue.main.async {
                            self.handleCaptureTransaction(amountInteger: paymentAmountInt, paymentIDDAX: payID, shopIDSAX: shopID)
                            let fireBaseRefeee = Database.database().reference()
                            fireBaseRefeee.child("bookings").child(shopID).child(bookID).child("bookingStatus").setValue("completed")
                            
                        }
                        self.postBookingCompletion(bookIDD: bookID, dataValue: value)
                    default:
                        print("Sky high")
                    }
                }
            }
        }
    }
    
    func postBookingCompletion(bookIDD: String, dataValue: [String : String]){
        DispatchQueue.global(qos: .background).async {
            let fireBaseRefeee = Database.database().reference()
            let userReference = fireBaseRefeee.child("bookingCompleted").child(bookIDD)
            userReference.updateChildValues(dataValue) { (errorrrrrrede, refereTT) in
                if let error = errorrrrrrede {
                    print(error.localizedDescription)
                    return
                }
            }
        }
    }
    
    
    func handleCaptureTransaction(amountInteger: Int, paymentIDDAX: String, shopIDSAX: String){
        
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.white
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        DispatchQueue.global(qos: .background).async {
            let firebaseReferenceComfirmMethodOfPay = Database.database().reference()
            firebaseReferenceComfirmMethodOfPay.child("payments").child(shopIDSAX).child(paymentIDDAX).observeSingleEvent(of: .value, with: { (snapshootMethodConfirm) in
                if let dictionaryMethodConfirm = snapshootMethodConfirm.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionaryMethodConfirm) == true {
                    let paymentMethodConfirmSingleData = Payments()
                    paymentMethodConfirmSingleData.setValuesForKeys(dictionaryMethodConfirm)
                    
                    if let singleDataConfirm = paymentMethodConfirmSingleData.methodOfPayment, let checkQuickPay = paymentMethodConfirmSingleData.capture_qp_status_msg, let checkClearHaus = paymentMethodConfirmSingleData.capture_aq_status_msg {
                        
                        if checkQuickPay == "not available" && checkClearHaus == "not available" {
                            let newDate = DateInRegion()
                            let strDate = newDate.toString(.extended)
                            let formdate = newDate.toString(.iso(.withDashSeparatorInDate))
                            let userTimezone = DateByUserDeviceInitializer.tzone
                            let userCalender = DateByUserDeviceInitializer.calenderNow
                            let userLocal = DateByUserDeviceInitializer.localCode
                            
                            
                            let newUserLocal = "en"
                            let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                            let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                            let newStrDate = newUserDate.toString(.extended)
                            
                            let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                            
                            let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
                            let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                            
                            
                            if singleDataConfirm == "cash" {
                                let firebaseReferenceCaptureCashPayment = Database.database().reference()
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppDate").setValue(correctDateToUse)
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppTimeZone").setValue(userTimezone)
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppCalendar").setValue(userCalender)
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppLocal").setValue(correctLocaleToUSe)
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("paymentTypeCaptureOperation").setValue("capture")
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("paymentCaptureDate").setValue(formdate)
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("capture_qp_status_msg").setValue("Approved")
                                firebaseReferenceCaptureCashPayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("capture_aq_status_msg").setValue("Approved")
                                DispatchQueue.main.async {
                                    self.activityIndicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                }
                                
                            } else {
                                
                                let headers = [
                                    "accept-version": "v10",
                                    "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                                    "content-type": "application/json",
                                    "cache-control": "no-cache",
                                    "postman-token": "6646ec47-0ae8-260f-be5f-bf8dd2e42a1e",
                                    "QuickPay-Callback-Url": "https://us-central1-cuttiin-f6186.cloudfunctions.net/quickPayStayShapCaptureHandle"
                                ]
                                
                                let correctAmounMultiplr = amountInteger * 100
                                
                                let parameters: JSON = ["amount" : correctAmounMultiplr]
                                
                                if let data = try? parameters.rawData() {
                                    
                                    let request = NSMutableURLRequest(url: NSURL(string: "https://api.quickpay.net/payments/\(paymentIDDAX)/capture?synchronized=")! as URL,
                                                                      cachePolicy: .useProtocolCachePolicy,
                                                                      timeoutInterval: 10.0)
                                    request.httpMethod = "POST"
                                    request.allHTTPHeaderFields = headers
                                    request.httpBody = data
                                    
                                    let session = URLSession.shared
                                    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, errorererererer) -> Void in
                                        if let error = errorererererer {
                                            print(error.localizedDescription)
                                            DispatchQueue.main.async {
                                                self.activityIndicator.stopAnimating()
                                                UIApplication.shared.endIgnoringInteractionEvents()
                                            }
                                        } else {
                                            let httpResponse = response as? HTTPURLResponse
                                            if let stCode = httpResponse?.statusCode {
                                                print(stCode)
                                                let currentTimeDate = DateInRegion().toString(.extended)
                                                let userTimezone = DateByUserDeviceInitializer.tzone
                                                let userCalender = DateByUserDeviceInitializer.calenderNow
                                                let userLocal = DateByUserDeviceInitializer.localCode
                                                
                                                
                                                let newUserLocal = "en"
                                                let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                                                let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                                                let newStrDate = newUserDate.toString(.extended)
                                                
                                                let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                                                
                                                let correctDateToUse = (localeTimezoneVerify) ? currentTimeDate : newStrDate
                                                let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                                                
                                                let firebaseUpdatePayment = Database.database().reference()
                                                firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppDate").setValue(correctDateToUse)
                                                firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppTimeZone").setValue(userTimezone)
                                                firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppCalendar").setValue(userCalender)
                                                firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppLocal").setValue(correctLocaleToUSe)
                                                DispatchQueue.main.async {
                                                    self.activityIndicator.stopAnimating()
                                                    UIApplication.shared.endIgnoringInteractionEvents()
                                                }
                                            } else {
                                                DispatchQueue.main.async {
                                                    self.activityIndicator.stopAnimating()
                                                    UIApplication.shared.endIgnoringInteractionEvents()
                                                }
                                            }
                                        }
                                    })
                                    
                                    dataTask.resume()
                                } else {
                                    DispatchQueue.main.async {
                                        self.activityIndicator.stopAnimating()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                    }
                                }
                                
                            }
                        } else {
                            //Already captured before
                            
                            DispatchQueue.main.async {
                                
                                self.activityIndicator.stopAnimating()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                
                                let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextCancelAcceptDeclinePaymentView", comment: "Accept Payment"), message: NSLocalizedString("alertViewTextCancelPaymentMessageHolderAcceptDeclinePaymentView", comment: "Transaction payment already accepted."), preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                alert.addAction(OKAction)
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }//here
                } else {
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                }
            }) { (erroro) in
                print(erroro)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        }
        
    }
    
    
    
    func setViewContriants(){
        dataObjectsContanerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        dataObjectsContanerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dataObjectsContanerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dataObjectsContanerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        dataObjectsContanerView.addSubview(statusPlaceHolder)
        dataObjectsContanerView.addSubview(statusDescriptionPlaceHolder)
        dataObjectsContanerView.addSubview(checkImageDescriptionThumbnailImageView)
        dataObjectsContanerView.addSubview(appointmentsButton)
        
        
        statusPlaceHolder.topAnchor.constraint(equalTo: dataObjectsContanerView.topAnchor).isActive = true
        statusPlaceHolder.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        statusPlaceHolder.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -24).isActive = true
        statusPlaceHolder.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        statusDescriptionPlaceHolder.topAnchor.constraint(equalTo: statusPlaceHolder.bottomAnchor, constant: 10).isActive = true
        statusDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        statusDescriptionPlaceHolder.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -24).isActive = true
        statusDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        checkImageDescriptionThumbnailImageView.topAnchor.constraint(equalTo: statusDescriptionPlaceHolder.bottomAnchor, constant: 30).isActive = true
        checkImageDescriptionThumbnailImageView.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        checkImageDescriptionThumbnailImageView.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -72).isActive = true
        checkImageDescriptionThumbnailImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        appointmentsButton.topAnchor.constraint(equalTo: checkImageDescriptionThumbnailImageView.bottomAnchor, constant: 15).isActive = true
        appointmentsButton.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        appointmentsButton.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -72).isActive = true
        appointmentsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc func handleShowAppointView(){
        dismiss(animated: true, completion: nil)
        self.orderDetail.dismissView()
        
    }

}
