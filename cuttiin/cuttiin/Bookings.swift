//
//  Bookings.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/9/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Bookings: NSObject {
    var bookingID: String?
    var customerID: String?
    var bookedBarberID: String?
    var bookedServiceID: String?
    var bookedBarberShopID: String?
    var bookedServiceQuantity: String?
    var bookingStartTime: String?
    var bookingEndTime: String?
    var ConfirmedTotalPrice: String?
    var totalPriceWithCharges: String?
    var ConfirmedTotalTime: String?
    var bookingDescriptionImageName: String?
    var bookingDescriptionImageUrl: String?
    var bookingDescriptionText: String?
    var paymentID: String?
    var isCompleted: String?
    var severDidNotMakeBooking: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
    var bookingCancel: String?
    var cancelDate: String?
    var cancelTimeZone: String?
    var cancelCalendar: String?
    var cancelLocal: String?
    var isRemiderSet: String?
    var isRated: String?
    var actualEarning: String?
    var bookingStatus: String?
}
