//
//  Settings.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/7/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Settings: NSObject {
    var settingsTitle: String?
    var settingsIcon: UIImage?
}
