//
//  CustomBarberTabBarController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

class CustomBarberTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let calendarController = CalendarViewController()
        let firstNavigationController = UINavigationController(rootViewController: calendarController)
        firstNavigationController.title = NSLocalizedString("navigationTitleForCalenderView", comment: "Calendar")
        firstNavigationController.tabBarItem.image = UIImage(named: "calendar_passive")
        
        let appointmentController = AppointmentsViewController()
        let secondNavigationController = UINavigationController(rootViewController: appointmentController)
        secondNavigationController.title = NSLocalizedString("appointmentTabButton", comment: "Appointments")
        secondNavigationController.tabBarItem.image = UIImage(named: "barbers_appointments_passive")
        
        let barberProfileController = BarberProfileViewController()
        let thirdNavigationController = UINavigationController(rootViewController: barberProfileController)
        thirdNavigationController.title = NSLocalizedString("profileTabBarButton", comment: "Profile")
        thirdNavigationController.tabBarItem.image = UIImage(named: "profile_passive")
        
        viewControllers = [firstNavigationController, secondNavigationController, thirdNavigationController]
        
        tabBar.isTranslucent = false
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: 1000, height: 0.5)
        topBorder.backgroundColor = UIColor.white.cgColor
        
        tabBar.layer.addSublayer(topBorder)
        tabBar.clipsToBounds = true
        tabBar.tintColor = UIColor(r: 118, g: 187, b: 220)
        tabBar.unselectedItemTintColor = UIColor.white
        tabBar.barTintColor = UIColor(r: 11, g: 49, b: 68)
        
    }
    
    override func viewWillLayoutSubviews() {
        var newTabBarFrame = tabBar.frame
        
        let newTabBarHeight: CGFloat = 60
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight
        
        tabBar.frame = newTabBarFrame
    }

}
