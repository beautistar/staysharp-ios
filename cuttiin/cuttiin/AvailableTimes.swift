//
//  AvailableTimes.swift
//  cuttiin
//
//  Created by Umoru Joseph on 11/19/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

@objcMembers class AvailableTimes: NSObject {
    var bookingUniqueID: String?
    var bookingStartTimeFirst: DateInRegion?
    var bookingEndTimeSecond: DateInRegion?
    var bookingStartTimeEndTimeString: String?
}
