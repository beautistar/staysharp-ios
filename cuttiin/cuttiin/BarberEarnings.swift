//
//  BarberEarnings.swift
//  cuttiin
//
//  Created by Umoru Joseph on 10/4/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class BarberEarnings: NSObject {
    var barberUUID: String?
    var barberStaffName: String?
    var totalBarberEarning: Double?
    var totalBarberNumbers: Int?
    var profileImage: UIImage?
    var cardPaymentAmount: Double?
    var cardPaymentNumber: Int?
    var mobilePaymentAmount: Double?
    var mobilePaymentNumber: Int?
    var cashPaymentAmount: Double?
    var cashPaymentNumber: Int?
    var totalRefundsAmount: Double?
}
