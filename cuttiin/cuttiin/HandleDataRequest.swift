//
//  HandleDataRequest.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 30/08/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

class HandleDataRequest {
    
    static func handlePaymentsNode(firebaseData: [String: AnyObject]) -> Bool {
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "appDateCreated",
            "appTimezone",
            "appCalendar",
            "appLocale",
            "barberShopUUIDD",
            "barberUUIDD",
            "bookingUUIDD",
            "customerUUID",
            "formattedDateString",
            "paymentID",
            "dateCreated",
            "serviceUUIDD",
            "priceTotal",
            "paymentTypeOperation",
            "payment_qp_status_msg",
            "payment_aq_status_msg",
            
            "captureAppDate",
            "captureAppTimeZone",
            "captureAppCalendar",
            "captureAppLocal",
            "paymentTypeCaptureOperation",
            "paymentCaptureDate",
            "capture_qp_status_msg",
            "capture_aq_status_msg",
            
            "cancelAppDate",
            "cancelAppTimeZone",
            "cancelAppCalendar",
            "cancelAppLocal",
            "paymentTypeCancelOperation",
            "paymentCancelDate",
            "cancel_qp_status_msg",
            "cancel_aq_status_msg",
            
            "refundAppDate",
            "refundAppTimeZone",
            "refundAppCalendar",
            "refundAppLocal",
            "paymentTypeRefundOperation",
            "paymentRefundDate",
            "refund_qp_status_msg",
            "refund_aq_status_msg",
            "amountRefunded",
            
            "notificationSent",
            "methodOfPayment"]
        if let  _  = firebaseData["appDateCreated"] as? String,
            let  _  = firebaseData["appTimezone"] as? String,
            let  _  = firebaseData["appCalendar"] as? String,
            let  _  = firebaseData["appLocale"] as? String,
            let  _  = firebaseData["barberShopUUIDD"] as? String,
            let  _  = firebaseData["barberUUIDD"] as? String,
            let  _  = firebaseData["bookingUUIDD"] as? String,
            let  _  = firebaseData["customerUUID"] as? String,
            let  _  = firebaseData["formattedDateString"] as? String,
            let  _  = firebaseData["paymentID"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["serviceUUIDD"] as? String,
            let  _  = firebaseData["priceTotal"] as? String,
            let  _  = firebaseData["paymentTypeOperation"] as? String,
            let  _  = firebaseData["payment_qp_status_msg"] as? String,
            let  _  = firebaseData["payment_aq_status_msg"] as? String,
            
            let  _  = firebaseData["captureAppDate"] as? String,
            let  _  = firebaseData["captureAppTimeZone"] as? String,
            let  _  = firebaseData["captureAppCalendar"] as? String,
            let  _  = firebaseData["captureAppLocal"] as? String,
            let  _  = firebaseData["paymentTypeCaptureOperation"] as? String,
            let  _  = firebaseData["paymentCaptureDate"] as? String,
            let  _  = firebaseData["capture_qp_status_msg"] as? String,
            let  _  = firebaseData["capture_aq_status_msg"] as? String,
            
            let  _  = firebaseData["cancelAppDate"] as? String,
            let  _  = firebaseData["cancelAppTimeZone"] as? String,
            let  _  = firebaseData["cancelAppCalendar"] as? String,
            let  _  = firebaseData["cancelAppLocal"] as? String,
            let  _  = firebaseData["paymentTypeCancelOperation"] as? String,
            let  _  = firebaseData["paymentCancelDate"] as? String,
            let  _  = firebaseData["cancel_qp_status_msg"] as? String,
            let  _  = firebaseData["cancel_aq_status_msg"] as? String,
            
            let  _  = firebaseData["refundAppDate"] as? String,
            let  _  = firebaseData["refundAppTimeZone"] as? String,
            let  _  = firebaseData["refundAppCalendar"] as? String,
            let  _  = firebaseData["refundAppLocal"] as? String,
            let  _  = firebaseData["paymentTypeRefundOperation"] as? String,
            let  _  = firebaseData["paymentRefundDate"] as? String,
            let  _  = firebaseData["refund_qp_status_msg"] as? String,
            let  _  = firebaseData["refund_aq_status_msg"] as? String,
            let  _  = firebaseData["amountRefunded"] as? String,
            
            let  _  = firebaseData["notificationSent"] as? String,
            let  _  = firebaseData["methodOfPayment"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            
            return true
        } else {
            
            return false
        }
        
        
        
        
    }
    
    static func handleBookingsNode(firebaseData: [String: AnyObject]) -> Bool {
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "bookingID",
            "customerID",
            "bookedBarberID",
            "bookedServiceID",
            "bookedBarberShopID",
            "bookedServiceQuantity",
            "bookingStartTime",
            "bookingEndTime",
            "ConfirmedTotalPrice",
            "totalPriceWithCharges",
            "ConfirmedTotalTime",
            "bookingDescriptionImageName",
            "bookingDescriptionImageUrl",
            "bookingDescriptionText",
            "paymentID",
            "isCompleted",
            "severDidNotMakeBooking",
            "dateCreated",
            "timezone",
            "calendar",
            "local",
            "bookingCancel",
            "cancelDate",
            "cancelTimeZone",
            "cancelCalendar",
            "cancelLocal",
            "isRemiderSet",
            "bookingStatus"]
        
        
        if let  _  = firebaseData["bookingID"] as? String,
            let  _  = firebaseData["customerID"] as? String,
            let  _  = firebaseData["bookedBarberID"] as? String,
            let  _  = firebaseData["bookedServiceID"] as? String,
            let  _  = firebaseData["bookedBarberShopID"] as? String,
            let  _  = firebaseData["bookedServiceQuantity"] as? String,
            let  _  = firebaseData["bookingStartTime"] as? String,
            let  _  = firebaseData["bookingEndTime"] as? String,
            let  _  = firebaseData["ConfirmedTotalPrice"] as? String,
            let  _  = firebaseData["totalPriceWithCharges"] as? String,
            let  _  = firebaseData["ConfirmedTotalTime"] as? String,
            let  _  = firebaseData["bookingDescriptionImageName"] as? String,
            let  _  = firebaseData["bookingDescriptionImageUrl"] as? String,
            let  _  = firebaseData["bookingDescriptionText"] as? String,
            let  _  = firebaseData["paymentID"] as? String,
            let  _  = firebaseData["isCompleted"] as? String,
            let  _  = firebaseData["severDidNotMakeBooking"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String,
            let  _  = firebaseData["bookingCancel"] as? String,
            let  _  = firebaseData["cancelDate"] as? String,
            let  _  = firebaseData["cancelTimeZone"] as? String,
            let  _  = firebaseData["cancelCalendar"] as? String,
            let  _  = firebaseData["cancelLocal"] as? String,
            let  _  = firebaseData["isRemiderSet"] as? String,
            let  _  = firebaseData["bookingStatus"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            
            return true
        } else {
            return false
        }
    }
    
    static func handleServiceNode(firebaseData: [String: AnyObject]) -> Bool {
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "serviceID",
            "serviceTitle",
            "estimatedTime",
            "serviceCost",
            "serviceCostLocal",
            "shortDescription",
            "category",
            "serviceImageName",
            "serviceImageUrl",
            "dateCreated",
            "timezone",
            "calendar",
            "local",
            "localCurrency",
            "isDeleted"]
        if let  _  = firebaseData["serviceID"] as? String,
            let  _  = firebaseData["serviceTitle"] as? String,
            let  _  = firebaseData["estimatedTime"] as? String,
            let  _  = firebaseData["serviceCost"] as? String,
            let  _  = firebaseData["serviceCostLocal"] as? String,
            let  _  = firebaseData["shortDescription"] as? String,
            let  _  = firebaseData["category"] as? String,
            let  _  = firebaseData["serviceImageName"] as? String,
            let  _  = firebaseData["serviceImageUrl"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String,
            let  _  = firebaseData["localCurrency"] as? String,
            let  _  = firebaseData["isDeleted"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        
        } else {
            return false
        }
    }
    
    static func handleBarberShopBarbersNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "barberStaffID",
            "dateCreated",
            "timezone",
            "calendar",
            "local"]
        if let  _  = firebaseData["barberStaffID"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleServiceBarbersNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "barberID",
            "dateCreated",
            "timezone",
            "calendar",
            "local"]
        if let  _  = firebaseData["barberID"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }

    static func handleBarberServicesNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "serviceID",
            "dateCreated",
            "timezone",
            "calendar",
            "local"]
        if let  _  = firebaseData["serviceID"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleWorkhoursNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "openingTimeHour",
            "openingTimeMinute",
            "openingTimeSecond",
            "closingTimeHour",
            "closingTimeMinute",
            "closingTimeSecond",
            "minutesBeforeClosing",
            "dateCreated",
            "timezone",
            "calendar",
            "local"]
        if let  _  = firebaseData["openingTimeHour"] as? String,
            let  _  = firebaseData["openingTimeMinute"] as? String,
            let  _  = firebaseData["openingTimeSecond"] as? String,
            let  _  = firebaseData["closingTimeHour"] as? String,
            let  _  = firebaseData["closingTimeMinute"] as? String,
            let  _  = firebaseData["closingTimeSecond"] as? String,
            let  _  = firebaseData["minutesBeforeClosing"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
            {
            return true
        } else {
            return false
        }
    }
    
    static func handleBookingCompletedNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "barberShopID",
            "bookingID",
            "barberID",
            "customerID",
            "serviceCompletedBarber",
            "serviceCompletedCustomer",
            "reasonForNotCompleted",
            "dateCreated",
            "timezone",
            "calendar",
            "local",
            "notificationSent"]
        if let  _  = firebaseData["barberShopID"] as? String,
            let  _  = firebaseData["bookingID"] as? String,
            let  _  = firebaseData["barberID"] as? String,
            let  _  = firebaseData["customerID"] as? String,
            let  _  = firebaseData["serviceCompletedBarber"] as? String,
            let  _  = firebaseData["serviceCompletedCustomer"] as? String,
            let  _  = firebaseData["reasonForNotCompleted"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String,
            let  _  = firebaseData["notificationSent"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
        
    }
    
    static func handleNumberOfBookingsNode(firebaseData: [String: AnyObject]) -> Bool {
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "numberOfBookings"]
        if let _ = firebaseData["numberOfBookings"] as? NSNumber, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleRatingsNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "bookingID",
            "barberShopID",
            "barberID",
            "customerID",
            "barberRatingVale",
            "barberShopRatingValue",
            "barberRatingComment",
            "barberShopRatingComment",
            "dateCreated",
            "timezone",
            "calendar",
            "local"]
        if let  _  = firebaseData["bookingID"] as? String,
            let  _  = firebaseData["barberShopID"] as? String,
            let  _  = firebaseData["barberID"] as? String,
            let  _  = firebaseData["customerID"] as? String,
            let  _  = firebaseData["barberRatingVale"] as? String,
            let  _  = firebaseData["barberShopRatingValue"] as? String,
            let  _  = firebaseData["barberRatingComment"] as? String,
            let  _  = firebaseData["barberShopRatingComment"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleCustomerBookingsNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "customerID",
            "bookingID",
            "barberID",
            "serviceID",
            "barberShopID"]
        if let  _  = firebaseData["customerID"] as? String,
            let  _  = firebaseData["bookingID"] as? String,
            let  _  = firebaseData["barberID"] as? String,
            let  _  = firebaseData["serviceID"] as? String,
            let  _  = firebaseData["barberShopID"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleCashPaymentVerificationNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "barberShopID",
                         "isCashPaymentAllowed",
            "dateCreated",
            "timezone",
            "calendar",
            "local"]
        if let  _  = firebaseData["barberShopID"] as? String,
            let _ = firebaseData["isCashPaymentAllowed"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleCreditCardPaymentVerificationNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "barberShopID",
                         "isCreditCardPaymentAllowed",
                         "dateCreated",
                         "timezone",
                         "calendar",
                         "local"]
        if let  _  = firebaseData["barberShopID"] as? String,
            let _ = firebaseData["isCreditCardPaymentAllowed"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleBarberShopVerificationNode(firebaseData: [String: AnyObject]) -> Bool{
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "barberShopID",
            "isBarberShopVerified",
            "dateCreated",
            "timezone",
            "calendar",
            "local"]
        if let  _  = firebaseData["barberShopID"] as? String,
            let  _  = firebaseData["isBarberShopVerified"] as? String,
            let  _  = firebaseData["dateCreated"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["local"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            return true
        } else {
            return false
        }
    }
    
    static func handleUserCustomerNode(firebaseData: [String: AnyObject]) -> Bool {
        
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "calendar",
            "cardDateCalendar",
            "cardDateCreated",
            "cardDateLocale",
            "cardDateTimeZone",
            "cardHasBeenDeleted",
            "cardID",
            "dateAccountCreated",
            "email",
            "firstName",
            "lastName",
            "local",
            "mobileNumber",
            "profileImageName",
            "profileImageUrl",
            "role",
            "timezone",
            "uniqueID"]
        if let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["cardDateCalendar"] as? String,
            let  _  = firebaseData["cardDateCreated"] as? String,
            let  _  = firebaseData["cardDateLocale"] as? String,
            let  _  = firebaseData["cardDateTimeZone"] as? String,
            let  _  = firebaseData["cardHasBeenDeleted"] as? String,
            let  _  = firebaseData["cardID"] as? String,
            let  _  = firebaseData["dateAccountCreated"] as? String,
            let  _  = firebaseData["email"] as? String,
            let  _  = firebaseData["firstName"] as? String,
            let  _  = firebaseData["lastName"] as? String,
            let  _  = firebaseData["local"] as? String,
            let  _  = firebaseData["mobileNumber"] as? String,
            let  _  = firebaseData["profileImageName"] as? String,
            let  _  = firebaseData["profileImageUrl"] as? String,
            let  _  = firebaseData["role"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["uniqueID"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            
            return true
            
        } else {
            return false
        }

    }
    
    static func handleUserBarberShop(firebaseData: [String: AnyObject]) -> Bool {
        
        
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "accountNumerKontoNr",
            "accountNumerRegNr",
            "calendar",
            "companyAddressLatitude",
            "companyAddressLongitude",
            "companyCoverPhotoName",
            "companyCoverPhotoUrl",
            "companyFormattedAddress",
            "companyLogoImageName",
            "companyLogoImageUrl",
            "companyName",
            "currencyCode",
            "dateAccountCreated",
            "email",
            "local",
            "mobileNumber",
            "rating",
            "role",
            "timezone",
            "uniqueID"]
        if let  _  = firebaseData["accountNumerKontoNr"] as? String,
            let  _  = firebaseData["accountNumerRegNr"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["companyAddressLatitude"] as? String,
            let  _  = firebaseData["companyAddressLongitude"] as? String,
            let  _  = firebaseData["companyCoverPhotoName"] as? String,
            let  _  = firebaseData["companyCoverPhotoUrl"] as? String,
            let  _  = firebaseData["companyFormattedAddress"] as? String,
            let  _  = firebaseData["companyLogoImageName"] as? String,
            let  _  = firebaseData["companyLogoImageUrl"] as? String,
            let  _  = firebaseData["companyName"] as? String,
            let  _  = firebaseData["currencyCode"] as? String,
            let  _  = firebaseData["dateAccountCreated"] as? String,
            let  _  = firebaseData["email"] as? String,
            let  _  = firebaseData["local"] as? String,
            let  _  = firebaseData["mobileNumber"] as? String,
            let  _  = firebaseData["rating"] as? String,
            let  _  = firebaseData["role"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["uniqueID"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {
            
            return true
        } else {
            return false
        }
    }
    
    static func handleUserBarberNode(firebaseData: [String: AnyObject]) -> Bool  {
        
        let dataHoldFirebase = Array(firebaseData.keys)
        let dataHold = [ "barberShopID",
            "calendar",
            "dateAccountCreated",
            "email",
            "emailSent",
            "firstName",
            "isDeleted",
            "lastName",
            "local",
            "mobileNumber",
            "profileImageName",
            "profileImageUrl",
            "rating",
            "role",
            "timezone",
            "uniqueID"]
        if let  _  = firebaseData["barberShopID"] as? String,
            let  _  = firebaseData["calendar"] as? String,
            let  _  = firebaseData["dateAccountCreated"] as? String,
            let  _  = firebaseData["email"] as? String,
            let  _  = firebaseData["emailSent"] as? String,
            let  _  = firebaseData["firstName"] as? String,
            let  _  = firebaseData["isDeleted"] as? String,
            let  _  = firebaseData["lastName"] as? String,
            let  _  = firebaseData["local"] as? String,
            let  _  = firebaseData["mobileNumber"] as? String,
            let  _  = firebaseData["profileImageName"] as? String,
            let  _  = firebaseData["profileImageUrl"] as? String,
            let  _  = firebaseData["rating"] as? String,
            let  _  = firebaseData["role"] as? String,
            let  _  = firebaseData["timezone"] as? String,
            let  _  = firebaseData["uniqueID"] as? String, dataHoldFirebase.containsSameElements(as: dataHold)
        {   
            return true
        } else {
            return false
        }
    }
    
    
}
