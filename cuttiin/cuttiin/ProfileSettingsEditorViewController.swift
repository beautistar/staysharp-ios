//
//  ProfileSettingsEditorViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/8/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class ProfileSettingsEditorViewController: UIViewController {
    var settingButtons = [Settings]()
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customProfileSettingsEditorCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("navigationTitleTextCustomerProfileView", comment: "Profile")
        view.backgroundColor = UIColor.white
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewConstraints()
        settingViewAlign()
    }
    
    func settingViewAlign(){
        
        
        let valueFirst = Settings()
        valueFirst.settingsTitle = NSLocalizedString("historyButtonProfileSettingsEditorView", comment: "History")
        valueFirst.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(valueFirst)
        
        let value = Settings()
        value.settingsTitle = NSLocalizedString("changePasswordButtonProfileSettingsEditor", comment: "Change Password")
        value.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(value)
        
        let valueNext = Settings()
        valueNext.settingsTitle = NSLocalizedString("logOutButtonProfileSettingsEditor", comment: "Log Out")
        valueNext.settingsIcon = UIImage(named: "logout")
        self.settingButtons.append(valueNext)
    }
    
    @objc func handleCollectionSelction(sender: UIButton){
        switch sender.tag {
        case 0:
            let bookListHis = BookingHistoryViewController()
            let shopListController = UINavigationController(rootViewController: bookListHis)
            present(shopListController, animated: true, completion: nil)
        case 1:
            let changepassword = ChangePasswordViewController()
            navigationController?.pushViewController(changepassword, animated: true)
        case 2:
            self.handleAll()
        default:
            print("Sky High")
        }
    }
    
    func handleAll(){
        FBSDKLoginManager().logOut()
        do{
            try Auth.auth().signOut()
            self.handlePostLogoutDismiss()
        }catch let logoutError {
            print(logoutError)
        }
    }
    
    func handlePostLogoutDismiss(){
        UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
        navigationController?.popViewController(animated: true)
    }
    
    func setupViewConstraints(){
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }

}

class customProfileSettingsEditorCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    lazy var thumbnailImageView: UIButton = {
        let tniv = UIButton()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        tniv.isUserInteractionEnabled = true
        return tniv
    }()
    
    let settingButtonTitlePlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        fnhp.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.isUserInteractionEnabled = true
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(settingButtonTitlePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|-16-[v0(165)][v1(50)]-16-|", views: settingButtonTitlePlaceHolder, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: settingButtonTitlePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
