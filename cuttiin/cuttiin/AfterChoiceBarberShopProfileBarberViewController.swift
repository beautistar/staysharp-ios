//
//  AfterChoiceBarberShopProfileBarberViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/2/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import MGStarRatingView
import SwiftDate


class AfterChoiceBarberShopProfileBarberViewController: UIViewController, StarRatingDelegate {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var barberShopUUID: String?
    var navBar: UINavigationBar = UINavigationBar()
    var barberOne = BarberShop()
    var serviceOneBasedNew = [Service]()
    var barberlistone = [Barber]()
    var serviceCategoryLadies = [Service]()
    var serviceCategoryGents = [Service]()
    var serviceCategoryKids = [Service]()
    var StringListOfBarbers = [String]()
    var selectedServiceOne = [String]()
    var selectedBarberOne = [String]()
    var ladiesSelectedList = [String]()
    var gentsSelectedList = [String]()
    var kidsSelectedList = [String]()
    var selectedServiceOneTotalStringList = [String]()
    var serviceBarberCompList = [ServiceBarbers]()
    var category = "Gents"
    var selectedLadiesGentsKidsButton = "Gents"
    var selectedAvailableTopRated = "Available"
    var openingTimeString: String?
    
    var barbershopTimezone: String?
    var barbershopCalendar: String?
    var barbershopLocale: String?
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white //(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    
    lazy var headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("selectBarbersHeaderAfterChoiceBarberShopBarbersView", comment: "SELECT BARBERS")
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customBarberShopProfileCollectionViewBarberSelectedCell.self, forCellWithReuseIdentifier: "cellId2")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
        setNavBarToTheView()
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.delegate = self
        view.addSubview(headerViewDescriptionPlaceHolder)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContraints()
        getBarberPersonalDetails()
        getOpeningAndCLosingTimeForChoosenDate()
        getThisBarberShopBarbers()
    }
    
    func getBarberPersonalDetails(){
        let firebaseRef = Database.database().reference()
        if let userID = self.barberShopUUID {
            firebaseRef.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshooottt) in
                if let dictionary = snapshooottt.value as? [String: AnyObject] {
                    
                    let barbershopVerific = HandleDataRequest.handleUserBarberShop(firebaseData: dictionary)
                    
                    if barbershopVerific {
                        
                        self.barberOne.setValuesForKeys(dictionary)
                        if let logoURL = self.barberOne.companyLogoImageUrl, let shopName = self.barberOne.companyName, let shopAddress = self.barberOne.companyFormattedAddress, let timezone = self.barberOne.timezone, let calendar = self.barberOne.calendar, let locale = self.barberOne.local  {
                            
                            self.barbershopTimezone = timezone
                            self.barbershopLocale = locale
                            self.barbershopCalendar = calendar
                            
                            self.barberShopLogoImageView.loadImagesUsingCacheWithUrlString(urlString: logoURL)
                            self.barberShopNamePlaceHolder.text = shopName
                            self.barberShopAddressPlaceHolder.text = shopAddress
                            if let ratingValue = self.barberOne.rating, let ratingValueInt = Int(ratingValue) {
                                let rateFloat = CGFloat(integerLiteral: ratingValueInt)
                                self.starView.configure(self.attribute, current: rateFloat, max: 5)
                            } else {
                                let rateFloat = CGFloat(integerLiteral: 0)
                                self.starView.configure(self.attribute, current: rateFloat, max: 5)
                                
                            }
                        }
                    }
                    
                }
            }, withCancel: nil)
        }
        
    }
    
    func getOpeningAndCLosingTimeForChoosenDate(){
        let currentDateGottenNow = DateInRegion()
        let clendarName = DateByUserDeviceInitializer.calenderNow
        let tZoneNow = DateByUserDeviceInitializer.tzone
        let modLocal = "en_US"
        let translatorRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tZoneNow, calenName: clendarName, LocName: modLocal)
        let newTransDateInDiffrentRegion = currentDateGottenNow.convertTo(region: translatorRegion)
        let daySelect = newTransDateInDiffrentRegion.weekdayName(.default)
        
        let firebaseRefere = Database.database().reference()
        if let userID = self.barberShopUUID {
            firebaseRefere.child("workhours").child(userID).child(daySelect).observeSingleEvent(of: .value, with: { (snapshooottss) in
                if let dictionary = snapshooottss.value as? [String: AnyObject]{
                    
                    let workHourVerific = HandleDataRequest.handleWorkhoursNode(firebaseData: dictionary)
                    
                    if workHourVerific {
                        
                        let currentDayWorkHour = Workhours()
                        currentDayWorkHour.setValuesForKeys(dictionary)
                        
                        if let openTimeHour = currentDayWorkHour.openingTimeHour, let openTimeMinute = currentDayWorkHour.openingTimeMinute, let openTimeSecond = currentDayWorkHour.openingTimeSecond {
                            
                            guard let openHour = Int(openTimeHour), let openMinute = Int(openTimeMinute), let openSecond = Int(openTimeSecond) else {
                                return
                            }
                            
                            let correctDateGotten = DateInRegion()
                            
                            if let startTimeDate = correctDateGotten.dateBySet(hour: openHour, min: openMinute, secs: openSecond) {
                                self.openingTimeString = startTimeDate.toString(DateToStringStyles.dateTime(.short))
                            }
                            
                        }
                    }
                    
                }
            }, withCancel: nil)
        }
    }
    
    func getThisBarberShopBarbers(){
        let totalListOfServicesSelected = self.ladiesSelectedList + self.gentsSelectedList + self.kidsSelectedList
        
        if totalListOfServicesSelected.count > 0 {
            for totalSingle in totalListOfServicesSelected {
                self.specifiedBarberFromListOfService(lis: totalSingle)
            }
        }
    }
    
    func specifiedBarberFromListOfService(lis: String){
        let firebaseRefxxx = Database.database().reference()
        firebaseRefxxx.child("serviceBarbers").child(lis).observeSingleEvent(of: .value, with: { (snapshootlocko) in
            if let dictionary = snapshootlocko.value as? [String: AnyObject] {
                for dic in dictionary {
                    guard let dictHolder = dic.value as? [String: AnyObject] else {
                        return
                    }
                    
                    let serviceBarberVerifi = HandleDataRequest.handleServiceBarbersNode(firebaseData: dictHolder)
                    
                    if serviceBarberVerifi {
                        
                        let servbarber = ServiceBarbers()
                        servbarber.setValuesForKeys(dictHolder)
                        self.getCompListOfBarbers(listBarbers: servbarber)
                    }
                }
            }
        }, withCancel: nil)
    }
    
    func getCompListOfBarbers(listBarbers: ServiceBarbers){
        let firebaseReflocko = Database.database().reference()
        if let barberUniqueUUID = listBarbers.barberID {
            firebaseReflocko.child("users").child(barberUniqueUUID).observeSingleEvent(of: .value, with: { (snapshooottt) in
                if let dictionary = snapshooottt.value as? [String: AnyObject] {
                    
                    let barberVeirif = HandleDataRequest.handleUserBarberNode(firebaseData: dictionary)
                    
                    if barberVeirif {
                        
                        let barbersingle = Barber()
                        barbersingle.setValuesForKeys(dictionary)
                        
                        if let barberCheck = barbersingle.isDeleted {
                            if barberCheck == "NO" {
                                if self.barberlistone.contains(barbersingle) {
                                    print("Duplicate value")
                                }else {
                                    
                                    DispatchQueue.global(qos: .background).async {
                                        if let url = URL(string: barbersingle.profileImageUrl!) {
                                            do {
                                                let data = try Data(contentsOf: url)
                                                
                                                if let downloadedImage = UIImage(data: data) {
                                                    barbersingle.profileImage = downloadedImage
                                                    DispatchQueue.main.async {
                                                        self.collectionView.reloadData()
                                                    }
                                                }
                                            } catch {
                                                print("profile picture not available")
                                            }
                                        }
                                    }
                                    
                                    if let ratingValue = barbersingle.rating, let ratingInt = Int(ratingValue) {
                                        barbersingle.ratingValue = CGFloat(integerLiteral: ratingInt)
                                    }else {
                                        barbersingle.ratingValue = CGFloat(integerLiteral: 0)
                                    }
                                    
                                    self.barberlistone.append(barbersingle)
                                }
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    @objc func handleDismissView(){
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopLogoImageView)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(starView)
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopLogoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopLogoImageView.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        starView.topAnchor.constraint(equalTo: barberShopAddressPlaceHolder.bottomAnchor, constant: 5).isActive = true
        starView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        starView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        starView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    func setupViewObjectContraints(){
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: view.topAnchor, constant: 165).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        collectView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -8).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
    func handleMoveToTheNextView(){
        
        if let timezone = self.barbershopTimezone, let calendar = self.barbershopCalendar, let locale = self.barbershopLocale {
            
            let numberCustomer = NumberOfCustomersViewController()
            
            numberCustomer.barberShopUUID = self.barberShopUUID
            numberCustomer.barberShopTimezone = timezone
            numberCustomer.barberShopCalendar = calendar
            numberCustomer.barberShopLocale = locale
            
            let totalListOfStringList = self.ladiesSelectedList + self.gentsSelectedList + self.kidsSelectedList
            numberCustomer.selectedStringServiceOneFinal = totalListOfStringList
            numberCustomer.selectedStringBarbersOneFinal = self.selectedBarberOne
            let navController = UINavigationController(rootViewController: numberCustomer)
            present(navController, animated: true, completion: nil)
        }
    }

}
