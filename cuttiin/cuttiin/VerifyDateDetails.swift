//
//  VerifyDateDetails.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 26/08/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

import Foundation
import SwiftDate

class VerifyDateDetails {
    
    static func checkDateData(timeZone: String, calendar: String, locale: String)-> Bool{
        if let _ = Zones(rawValue: timeZone), let _ = Locales(rawValue: locale)  {
            return true
        } else {
            return false
        }
    }
    
}
