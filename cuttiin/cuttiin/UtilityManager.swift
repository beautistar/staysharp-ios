//
//  UtilityManager.swift
//  cuttiin
//
//  Created by Sevenstar Infotech on 23/05/18.
//  Copyright © 2018 teckdk. All rights reserved.
//

import Foundation
import UIKit





func generateRandomDigit(_ totalDigit : Int) -> Int{
    var place = 1
    var finalNumber = 0;
    
    for _ in 1...totalDigit {
        place *= 10
        let randomNumber = arc4random_uniform(10)
        finalNumber += Int(randomNumber) * place
    }
    return finalNumber
}


//MARK:- UIALERTVIEW

func alertView(_ title : String?, _ message : String, _ buttons : [String], completionHandler : @escaping ((_ index : Int) -> Void)) {
    
    let alertViewController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    
    for (index, _) in buttons.enumerated() {
        alertViewController.addAction(UIAlertAction(title:  buttons[index], style: .default, handler: { (selectedButtonIndex) in completionHandler(index) }))
    }
    UIApplication.topViewController()?.present(alertViewController, animated: true, completion: nil)
}
