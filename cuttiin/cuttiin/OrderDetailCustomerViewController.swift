//
//  OrderDetailCustomerViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/3/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import Firebase
import SwiftDate
import SwiftyJSON
import EventKit

class OrderDetailCustomerViewController: UIViewController, StarRatingDelegate {
    var orderdetailcustomer: AppointmentsCustomer?
    var bookingviewcontroler: BookingsViewController?
    var serviceBooked = [Service]()
    var bookingIDD: String?
    var shopAddressLongitude: String?
    var shopAddressLatitude: String?
    var indexPathSelected: Int?
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var navBar: UINavigationBar = UINavigationBar()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white //(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    //body view objects
    let headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("summaryHeaderNumberOfCustomer", comment: "SUMMARY")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customOrderDetailCustomerCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let costTotalContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let totalTextHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalQuantityPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let costTotalSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    let addressHeaderTextPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("addressLabelTextOrderDetailView", comment: "Address")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var addressDataHoldPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowGoogleMaps)))
        return fnhp
    }()
    
    let contactHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.text = NSLocalizedString("contactLabelTextOrderDetailView", comment: "Contact")
        fnhp.textColor =  UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let phoneLogothumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.image = UIImage(named: "Call-icon")
        return tniv
    }()
    
    let phoneNumberPlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "HelveticaNeue-Mediu", size: 15)
        fnhp.contentHorizontalAlignment = .left
        fnhp.setTitleColor(UIColor.white, for: .normal)
        fnhp.backgroundColor = UIColor.clear
        fnhp.addTarget(self, action: #selector(handleMakeCall), for: .touchUpInside)
        return fnhp
    }()
    
    let openingHoursHeaderTextPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("openingHoursAvailLabelTextOrderDetailView", comment: "Opening hours")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let openingHoursDataHoldPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    
    //booking id bonk gang
    let bookingIDHeaderTextPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("bookingIDAvailLabelTextOrderDetailView", comment: "Order ID")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookingIDDataHoldPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .left
        return fnhp
    }()
    // ends here
    
    lazy var cancelBooking: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("cancelBookingButtonTitleOrderDetailView", comment: "CANCEL BOOKING"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.addTarget(self, action: #selector(handleCancelBooking), for: .touchUpInside)
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        navigationItem.title = NSLocalizedString("bookingNavigationTitleTextOrderDetail", comment: "Booking")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBaction))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("setAlarmNavigationalItemOrderDetailView", comment: "Set reminder"), style: .done, target: self, action: #selector(hamdleSettingAlarmForBooking))
        setNavBarToTheView()
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.type = .half
        starView.tag = 0
        starView.delegate = self
        view.addSubview(scrollView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        getTheSpecificBooking()
        getBarberShopWorkHours()
    }
    
    @objc func hamdleSettingAlarmForBooking(){
        DispatchQueue.global(qos: .background).async {
            if let appointCompData = self.orderdetailcustomer?.bookingUniqueID, let shopIDAX = self.orderdetailcustomer?.barberShopUniqueID, let shopName = self.orderdetailcustomer?.bookedBarberShopName {
                let firebaseReferenceAppointment = Database.database().reference()
                firebaseReferenceAppointment.child("bookings").child(shopIDAX).child(appointCompData).observeSingleEvent(of: .value, with: { (snapshotototCompApp) in
                    if let dictionaryComp = snapshotototCompApp.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryComp) == true {
                        let bookingSingleData = Bookings()
                        bookingSingleData.setValuesForKeys(dictionaryComp)
                        if let bookingStart = bookingSingleData.bookingStartTime, let bookingStop = bookingSingleData.bookingEndTime, let bookingLocal = bookingSingleData.local, let bookingCalendar = bookingSingleData.calendar, let bookingTimeZone = bookingSingleData.timezone, let checkRemider = bookingSingleData.isRemiderSet {
                            
                            let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookingTimeZone, calendar: bookingCalendar, locale: bookingLocal)
                            let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookingTimeZone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingStart)
                            let verifyBookingStopDate = VerifyDateAssociation.checkDateData(timeZone: bookingTimeZone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingStop)
                            
                            if checkRemider == "NO" && verifyBookingData == true && verifyBookingStartDate == true && verifyBookingStopDate == true {
                                
                                let bookingRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bookingTimeZone, calenName: bookingCalendar, LocName: bookingLocal)
                                if let bookingTimeStart = bookingStart.toDate(style: .extended, region: bookingRegion), let bookingTimeStop = bookingStop.toDate(style: .extended, region: bookingRegion) {
                                    let bookingEvenStore: EKEventStore = EKEventStore()
                                    
                                    bookingEvenStore.requestAccess(to: .event, completion: { (granted, errororororororor) in
                                        if let error = errororororororor {
                                            DispatchQueue.main.async {
                                                print(error.localizedDescription)
                                            }
                                        } else {
                                            if granted {
                                                let bookingEvent: EKEvent = EKEvent(eventStore: bookingEvenStore)
                                                bookingEvent.title = NSLocalizedString("reminderTextStringOrderDetailView", comment: "Booking reminder - Stay Sharp App")
                                                bookingEvent.startDate = bookingTimeStart.date
                                                bookingEvent.endDate = bookingTimeStop.date
                                                bookingEvent.notes = NSLocalizedString("reminderNoteTextStringOrderDetailView", comment: "Booked a service at: ") + shopName
                                                let alarmBooking: EKAlarm = EKAlarm(relativeOffset: -3600)
                                                bookingEvent.alarms = [alarmBooking]
                                                bookingEvent.calendar = bookingEvenStore.defaultCalendarForNewEvents
                                                
                                                
                                                do {
                                                    try bookingEvenStore.save(bookingEvent, span: .thisEvent)
                                                    
                                                    let firebaseReminderPut = Database.database().reference()
                                                    firebaseReminderPut.child("bookings").child(shopIDAX).child(appointCompData).child("isRemiderSet").setValue("YES")
                                                    DispatchQueue.main.async {
                                                        let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextOrderDetailView", comment: "Reminder"), message: NSLocalizedString("alertViewMessageSuccessTextOrderDetailView", comment: "You have sucessfully set a reminder for this booking"), preferredStyle: .alert)
                                                        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                                        alert.addAction(OKAction)
                                                        
                                                        self.present(alert, animated: true, completion: nil)
                                                    }
                                                    
                                                } catch {
                                                    
                                                    DispatchQueue.main.async {
                                                        let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextOrderDetailView", comment: "Reminder"), message: NSLocalizedString("alertViewMessageUnsuccessfulTextOrderDetailView", comment: "Unable to set a reminder for this booking"), preferredStyle: .alert)
                                                        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                                        alert.addAction(OKAction)
                                                        
                                                        self.present(alert, animated: true, completion: nil)
                                                    }
                                                }
                                                
                                                
                                            }
                                        }
                                    })
                                }
                                
                            } else {
                                
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextOrderDetailView", comment: "Reminder"), message: NSLocalizedString("alertViewMessageAlreadySetTextOrderDetailView", comment: "The boking already has a Reminder set"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func getTheSpecificBooking(){
        DispatchQueue.global(qos: .background).async {
            if let appointCompData = self.orderdetailcustomer?.bookingUniqueID, let shopIDAX = self.orderdetailcustomer?.barberShopUniqueID {
                let firebaseReferenceAppointment = Database.database().reference()
                firebaseReferenceAppointment.child("bookings").child(shopIDAX).child(appointCompData).observeSingleEvent(of: .value, with: { (snapshotototCompApp) in
                    if let dictionaryComp = snapshotototCompApp.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryComp) == true {
                        let bookingSingleData = Bookings()
                        bookingSingleData.setValuesForKeys(dictionaryComp)
                        if let barberID = bookingSingleData.bookedBarberID, let serviceID = bookingSingleData.bookedServiceID, let totalTime = bookingSingleData.ConfirmedTotalTime, let totalQuantity = bookingSingleData.bookedServiceQuantity, let payIDDS = bookingSingleData.paymentID {
                            DispatchQueue.main.async {
                                self.getBarberDetails(barberIDA: barberID, serviceIDA: serviceID)
                                self.totalQuantityPlaceHolder.text = totalQuantity
                                self.totalTimePlaceHolder.text = totalTime + "min"
                                self.bookingIDDataHoldPlaceHolder.text = appointCompData
                            }
                            
                            if shopIDAX != "" && payIDDS != "" && payIDDS != "nil" {
                                let firebaseReferenceCheckPayment = Database.database().reference()
                                firebaseReferenceCheckPayment.child("payments").child(shopIDAX).child(payIDDS).observeSingleEvent(of: .value, with: { (snapshotDataCheckCurrency) in
                                    if let dictionaryCurrencyCheck = snapshotDataCheckCurrency.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionaryCurrencyCheck) == true {
                                        let paymentsCurrencySingle = Payments()
                                        paymentsCurrencySingle.setValuesForKeys(dictionaryCurrencyCheck)
                                        
                                        if let currentAmount = paymentsCurrencySingle.priceTotal {
                                            
                                            let firebaseReferenceCheckCurrency = Database.database().reference()
                                            firebaseReferenceCheckCurrency.child("users").child(shopIDAX).observeSingleEvent(of: .value, with: { (snapshotDataCheckCurrency) in
                                                if let dictionaryCurrencyCheck = snapshotDataCheckCurrency.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryCurrencyCheck) == true {
                                                    let usersCurrencySingle = BarberShop()
                                                    usersCurrencySingle.setValuesForKeys(dictionaryCurrencyCheck)
                                                    
                                                    if let currencySymbol = usersCurrencySingle.currencyCode {
                                                        DispatchQueue.main.async {
                                                            self.totalPricePlaceHolder.text = currentAmount + currencySymbol
                                                        }
                                                    }
                                                }
                                            })
                                            
                                        }
                                    }
                                })
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func getBarberDetails(barberIDA: String, serviceIDA: String){
        DispatchQueue.global(qos: .background).async {
            let firebaseReferenceBarberDetails = Database.database().reference()
            firebaseReferenceBarberDetails.child("users").child(barberIDA).observeSingleEvent(of: .value, with: { (snapshotBarberShopIDData) in
                if let dictionaryUsers = snapshotBarberShopIDData.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryUsers) == true {
                    let barberUserData = Barber()
                    barberUserData.setValuesForKeys(dictionaryUsers)
                    if let barberName = barberUserData.firstName, let barberShopUniqueID = barberUserData.barberShopID {
                        DispatchQueue.main.async {
                            self.barberShopAddressPlaceHolder.text = barberName
                            self.getBarberShopName(shopIDX: barberShopUniqueID)
                            self.getServiceDetail(shopID: barberShopUniqueID, servName: serviceIDA)
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getBarberShopName(shopIDX: String){
        DispatchQueue.global(qos: .background).async {
            
            let firebaseReferenceBarberShop = Database.database().reference()
            firebaseReferenceBarberShop.child("users").child(shopIDX).observeSingleEvent(of: .value, with: { (snapshotBarberShopIDData) in
                if let dictionaryUsers = snapshotBarberShopIDData.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryUsers) == true {
                    let barberShopUserData = BarberShop()
                    barberShopUserData.setValuesForKeys(dictionaryUsers)
                    if let barberShopName = barberShopUserData.companyName, let barberShopLogoImageURL = barberShopUserData.companyLogoImageUrl, let shopAddress = barberShopUserData.companyFormattedAddress, let phoneNumber = barberShopUserData.mobileNumber, let longiti = barberShopUserData.companyAddressLongitude, let latitu = barberShopUserData.companyAddressLatitude, let rating = barberShopUserData.rating {
                        DispatchQueue.main.async {
                            self.barberShopNamePlaceHolder.text = barberShopName
                            self.barberShopLogoImageView.loadImagesUsingCacheWithUrlString(urlString: barberShopLogoImageURL)
                            self.addressDataHoldPlaceHolder.text = shopAddress
                            self.shopAddressLatitude = latitu
                            self.shopAddressLongitude = longiti
                            self.phoneNumberPlaceHolder.setTitle(phoneNumber, for: .normal)
                            if let ratingInt = Int(rating) {
                                self.starView.configure(self.attribute, current: CGFloat(integerLiteral: ratingInt), max: 5)
                            } else {
                                self.starView.configure(self.attribute, current: CGFloat(integerLiteral: 0), max: 5)
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getServiceDetail(shopID: String, servName: String){
        DispatchQueue.global(qos: .background).async {
            let firebaseRedReference = Database.database().reference()
            firebaseRedReference.child("service").child(shopID).child(servName).observeSingleEvent(of: .value, with: { (snapshososostt) in
                if let dictionary = snapshososostt.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionary) == true {
                    let serDic = Service()
                    serDic.setValuesForKeys(dictionary)
                    self.serviceBooked.append(serDic)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getBarberShopWorkHours(){
        DispatchQueue.global(qos: .background).async {
            
            let curentDate = DateInRegion()
            let clendarName = DateByUserDeviceInitializer.calenderNow
            let tZoneNow = DateByUserDeviceInitializer.tzone
            let modLocal = "en_US"
            let translatorRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tZoneNow, calenName: clendarName, LocName: modLocal)
            let newDate = curentDate.convertTo(region: translatorRegion)
            let selectedDay = newDate.weekdayName(.default)
            
            if let shopIDAS = self.orderdetailcustomer?.barberShopUniqueID {
                let firebaseWorkHourRef = Database.database().reference()
                firebaseWorkHourRef.child("workhours").child(shopIDAS).child(selectedDay).observeSingleEvent(of: .value, with: { (snapshotWorkHour) in
                    if let dictionaryWorkHour = snapshotWorkHour.value as? [String: AnyObject], HandleDataRequest.handleWorkhoursNode(firebaseData: dictionaryWorkHour) == true {
                        let chosenDayWorkhour = Workhours()
                        chosenDayWorkhour.setValuesForKeys(dictionaryWorkHour)
                        
                        if let openTimeHour = chosenDayWorkhour.openingTimeHour, let openTimeMinute = chosenDayWorkhour.openingTimeMinute, let openTimeSecond = chosenDayWorkhour.openingTimeSecond, let closingTimeHour = chosenDayWorkhour.closingTimeHour, let closingTimeMinute = chosenDayWorkhour.closingTimeMinute, let closingTimeSecond = chosenDayWorkhour.closingTimeSecond, let minutesBeforeClosing = chosenDayWorkhour.minutesBeforeClosing{
                            
                            guard let openHour = Int(openTimeHour), let openMinute = Int(openTimeMinute), let openSecond = Int(openTimeSecond), let closeHour = Int(closingTimeHour), let closeMinute = Int(closingTimeMinute), let closeSecond = Int(closingTimeSecond), let minutesBefore = Int(minutesBeforeClosing) else {
                                return
                            }
                            
                            if let startTimeDate = curentDate.dateBySet(hour: openHour, min: openMinute, secs: openSecond), let closeTimeDate = curentDate.dateBySet(hour: closeHour, min: closeMinute, secs: closeSecond) {
                                let newClosingTime = closeTimeDate - minutesBefore.minutes
                                
                                DispatchQueue.main.async {
                                    self.openingHoursDataHoldPlaceHolder.text = "\(startTimeDate.toString(DateToStringStyles.time(.short))) - \(newClosingTime.toString(DateToStringStyles.time(.short)))"
                                }
                                
                            }
                            
                            
                            
                        }
                        
                        
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
    @objc func handleBaction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleDismisView(){
        if let itemPoint = self.indexPathSelected, let specialObject = self.bookingviewcontroler {
            let indexPathHolder = IndexPath(row: itemPoint, section: 0)
            specialObject.collectionView.performBatchUpdates({
                specialObject.collectionView.deleteItems(at: [indexPathHolder])
                specialObject.appointmentsCustomer.remove(at: itemPoint)
            }, completion: { (finished) in
                specialObject.collectionView.reloadItems(at: (specialObject.collectionView.indexPathsForVisibleItems))
                specialObject.getBookingDataAsAppointments()
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    @objc func handleMakeCall(sender: UIButton){
        if let mobNUm = sender.titleLabel?.text {
            if let url = URL(string: "tel://\(mobNUm)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @objc func handleShowGoogleMaps(){
        if let long = self.shopAddressLongitude, let latit = self.shopAddressLatitude {
            guard let longFloat = Float(long), let latFloat = Float(latit) else {
                return
            }
            
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(latFloat),\(longFloat)&directionsmode=driving"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @objc func handleCancelBooking(){
        let alertController = UIAlertController(title: NSLocalizedString("cancelGetRefundTitleOrderDetailView", comment: "Cancel and Get a refund"), message: NSLocalizedString("cancelGetRefundTitleMessageOrderDetailView", comment: "By cancelling this booking. You will get a refund of a specified amount based the time the booking was made."), preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("yesButtonAlertOrderDetail", comment: "Yes"), style: .default) { action in
            self.calculateTimeGetCorrectAmount()
        }
        alertController.addAction(takePhotoAction)
        
        self.present(alertController, animated: true) {
        }
        
    }
    
    func calculateTimeGetCorrectAmount(){
        DispatchQueue.global(qos: .background).async {
            
            if let bookingID = self.orderdetailcustomer?.bookingUniqueID, let barberShopID = self.orderdetailcustomer?.barberShopUniqueID {
                let firebaseRefrenceCheckTimeDifference = Database.database().reference()
                firebaseRefrenceCheckTimeDifference.child("bookings").child(barberShopID).child(bookingID).observeSingleEvent(of: .value, with: { (snapshotCheckTimeDifference) in
                    if let dictionaryCheckTime = snapshotCheckTimeDifference.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryCheckTime) == true {
                        let boookingCheckTimeDiff = Bookings()
                        boookingCheckTimeDiff.setValuesForKeys(dictionaryCheckTime)
                        if let paymentIDD = boookingCheckTimeDiff.paymentID, let bookingStartTimeString = self.orderdetailcustomer?.bookingStartTime, let paidAmount = boookingCheckTimeDiff.ConfirmedTotalPrice {
                            
                            let firebaseReferenceDatePayment = Database.database().reference()
                           
                            if barberShopID != "" && paymentIDD != "" && paymentIDD != "nil" {
                                firebaseReferenceDatePayment.child("payments").child(barberShopID).child(paymentIDD).observeSingleEvent(of: .value, with: { (snapshotPaymentAmount) in
                                    if let paymentObjectHolder = snapshotPaymentAmount.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: paymentObjectHolder) == true {
                                        let paySingle = Payments()
                                        paySingle.setValuesForKeys(paymentObjectHolder)
                                        if let payCaptured = paySingle.paymentTypeCaptureOperation, let captureStatus = paySingle.capture_aq_status_msg ,  let payAmountToCapture = paySingle.priceTotal, let methodOfPaymentChosen = paySingle.methodOfPayment {
                                            
                                            let currentDate = DateInRegion()
                                            
                                            if bookingStartTimeString.isInside(date: currentDate, granularity: .day){
                                                print("same day")
                                                if payCaptured == "capture" && captureStatus == "Approved" {
                                                    
                                                    DispatchQueue.main.async {
                                                        
                                                        let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextCancelPaymentOrderDetailView", comment: "Cancel Payment"), message: NSLocalizedString("alertViewTextCancelPaymentMessageHolderOrderDetailView", comment: "Transaction already completed. Pull to refresh and proceed to the Completed Tab."), preferredStyle: .alert)
                                                        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                                        alert.addAction(OKAction)
                                                        
                                                        self.present(alert, animated: true, completion: nil)
                                                    }
                                                    
                                                } else {
                                                    
                                                    if methodOfPaymentChosen == "cash" {
                                                        
                                                        //bookingSection
                                                        let newDate = DateInRegion()
                                                        let strDate = newDate.toString(.extended)
                                                        let userTimezone = DateByUserDeviceInitializer.tzone
                                                        let userCalender = DateByUserDeviceInitializer.calenderNow
                                                        let userLocal = DateByUserDeviceInitializer.localCode
                                                        
                                                        
                                                        let newUserLocal = "en"
                                                        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                                                        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                                                        let newStrDate = newUserDate.toString(.extended)
                                                        
                                                        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                                                        
                                                        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
                                                        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                                                        
                                                        
                                                        let firebaseReferenceCancelPaymentHold = Database.database().reference()
                                                        
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("bookingCancel").setValue("YES")
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelDate").setValue(correctDateToUse)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelTimeZone").setValue(userTimezone)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelCalendar").setValue(userCalender)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelLocal").setValue(correctLocaleToUSe)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("isCompleted").setValue("YES")
                                                        
                                                        //paymentSection
                                                        let firebaseReferencePaymentSetValueCancelHold = Database.database().reference()
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppDate").setValue(correctDateToUse)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppTimeZone").setValue(userTimezone)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppCalendar").setValue(userCalender)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppLocal").setValue(correctLocaleToUSe)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("paymentTypeCancelOperation").setValue("cancel")
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancel_aq_status_msg").setValue("Approved")
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancel_qp_status_msg").setValue("Approved")
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("paymentCancelDate").setValue(strDate)
                                                        
                                                        DispatchQueue.main.async {
                                                            self.bookingviewcontroler?.getBookingDataAsAppointments()
                                                            self.bookingviewcontroler?.getBookingDataAsAppointmentsIsCompleted()
                                                            self.handleDismisView()
                                                        }
                                                        
                                                    }else {
                                                        
                                                        if let totalAmountInt = Int(payAmountToCapture), let amRefund = Int(paidAmount){
                                                            DispatchQueue.main.async {
                                                                self.handleCaptureTransaction(amountInteger: totalAmountInt, paymentIDDAX: paymentIDD, shopIDSAX: barberShopID, bookingID: bookingID, amountForRefund: amRefund)
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            } else {
                                                //here we check for upcoming cancellation
                                                print("not same day please")
                                                if payCaptured == "capture" && captureStatus == "Approved" {
                                                    
                                                    DispatchQueue.main.async {
                                                        
                                                        let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextCancelPaymentOrderDetailView", comment: "Cancel Payment"), message: NSLocalizedString("alertViewTextCancelPaymentMessageHolderOrderDetailView", comment: "Transaction already completed. Pull to refresh and proceed to the Completed Tab."), preferredStyle: .alert)
                                                        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                                        alert.addAction(OKAction)
                                                        
                                                        self.present(alert, animated: true, completion: nil)
                                                    }
                                                    
                                                } else {
                                                    
                                                    if methodOfPaymentChosen == "cash" {
                                                        
                                                        //bookingSection
                                                        let newDate = DateInRegion()
                                                        let strDate = newDate.toString(.extended)
                                                        let userTimezone = DateByUserDeviceInitializer.tzone
                                                        let userCalender = DateByUserDeviceInitializer.calenderNow
                                                        let userLocal = DateByUserDeviceInitializer.localCode
                                                        
                                                        
                                                        let newUserLocal = "en"
                                                        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                                                        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                                                        let newStrDate = newUserDate.toString(.extended)
                                                        
                                                        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                                                        
                                                        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
                                                        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                                                        
                                                        let firebaseReferenceCancelPaymentHold = Database.database().reference()
                                                        
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("bookingCancel").setValue("YES")
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelDate").setValue(correctDateToUse)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelTimeZone").setValue(userTimezone)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelCalendar").setValue(userCalender)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("cancelLocal").setValue(correctLocaleToUSe)
                                                        firebaseReferenceCancelPaymentHold.child("bookings").child(barberShopID).child(bookingID).child("isCompleted").setValue("YES")
                                                        
                                                        //paymentSection
                                                        let firebaseReferencePaymentSetValueCancelHold = Database.database().reference()
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppDate").setValue(correctDateToUse)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppTimeZone").setValue(userTimezone)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppCalendar").setValue(userCalender)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancelAppLocal").setValue(correctLocaleToUSe)
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("paymentTypeCancelOperation").setValue("cancel")
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancel_aq_status_msg").setValue("Approved")
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("cancel_qp_status_msg").setValue("Approved")
                                                        firebaseReferencePaymentSetValueCancelHold.child("payments").child(barberShopID).child(paymentIDD).child("paymentCancelDate").setValue(strDate)
                                                        
                                                        DispatchQueue.main.async {
                                                            self.bookingviewcontroler?.getBookingDataAsAppointments()
                                                            self.bookingviewcontroler?.getBookingDataAsAppointmentsIsCompleted()
                                                            self.handleDismisView()
                                                        }
                                                        
                                                    }else {
                                                        
                                                        if let totalAmountInt = Int(payAmountToCapture){
                                                            DispatchQueue.main.async {
                                                                let amountRefund = totalAmountInt * 2
                                                                self.handleCaptureTransaction(amountInteger: totalAmountInt, paymentIDDAX: paymentIDD, shopIDSAX: barberShopID, bookingID: bookingID, amountForRefund: amountRefund)
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }
                                        }//from here
                                    }
                                }, withCancel: nil)
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func handleCaptureTransaction(amountInteger: Int, paymentIDDAX: String, shopIDSAX: String, bookingID: String, amountForRefund: Int){
        
        
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.white
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "6646ec47-0ae8-260f-be5f-bf8dd2e42a1e",
                "QuickPay-Callback-Url": "https://us-central1-cuttiin-f6186.cloudfunctions.net/quickPayStayShapCaptureHandle"
            ]
            
            let correctedAmount = amountInteger * 100
            
            let parameters: JSON = ["amount" : correctedAmount]
            
            if let data = try? parameters.rawData() {
                
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.quickpay.net/payments/\(paymentIDDAX)/capture?synchronized=")! as URL,
                                                  cachePolicy: .useProtocolCachePolicy,
                                                  timeoutInterval: 10.0)
                request.httpMethod = "POST"
                request.allHTTPHeaderFields = headers
                request.httpBody = data
                
                let session = URLSession.shared
                let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, errorererererer) -> Void in
                    if let error = errorererererer {
                        DispatchQueue.main.async {
                            print(error.localizedDescription)
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    } else {
                        let httpResponse = response as? HTTPURLResponse
                        if (httpResponse?.statusCode) != nil {
                            let currentTimeDate = DateInRegion().toString(.extended)
                            let userTimezone = DateByUserDeviceInitializer.tzone
                            let userCalender = DateByUserDeviceInitializer.calenderNow
                            let userLocal = DateByUserDeviceInitializer.localCode
                            
                            
                            let newUserLocal = "en"
                            let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                            let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                            let newStrDate = newUserDate.toString(.extended)
                            
                            let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                            
                            let correctDateToUse = (localeTimezoneVerify) ? currentTimeDate : newStrDate
                            let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                            
                            let firebaseUpdatePayment = Database.database().reference()
                            firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppDate").setValue(correctDateToUse)
                            firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppTimeZone").setValue(userTimezone)
                            firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppCalendar").setValue(userCalender)
                            firebaseUpdatePayment.child("payments").child(shopIDSAX).child(paymentIDDAX).child("captureAppLocal").setValue(correctLocaleToUSe)
                            
                            DispatchQueue.main.async {
                                self.activityIndicator.stopAnimating()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                let totalAmountDivide = Double(amountForRefund / 2)
                                let roundUP = totalAmountDivide.roundTo(places: 0)
                                let roundUpInt = Int(roundUP)
                                self.makeActualRefund(amountInteger: roundUpInt, paymentIDDAX: paymentIDDAX, bookingID: bookingID)
                            }
                            
                        } else {
                            DispatchQueue.main.async {
                                self.activityIndicator.stopAnimating()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            }
                        }
                    }
                })
                
                dataTask.resume()
            } else {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        }
    }
    
    func makeActualRefund(amountInteger: Int, paymentIDDAX: String, bookingID: String){
        
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.white
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "content-type": "application/json",
                "quickpay-callback-url": "https://us-central1-cuttiin-f6186.cloudfunctions.net/quickPayStayShapRefundHandle",
                "cache-control": "no-cache",
                "postman-token": "2061a3fc-c9fb-3f71-610d-26aff1447983"
            ]
            
            let correctRefundAmount = amountInteger * 100
            
            let parameters: JSON = ["amount" : correctRefundAmount]
            
            if let data = try? parameters.rawData() {
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.quickpay.net/payments/\(paymentIDDAX)/refund?synchronized=")! as URL,
                                                  cachePolicy: .useProtocolCachePolicy,
                                                  timeoutInterval: 10.0)
                request.httpMethod = "POST"
                request.allHTTPHeaderFields = headers
                request.httpBody = data
                
                let session = URLSession.shared
                let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, errorrreewwerr) -> Void in
                    if let error = errorrreewwerr {
                        DispatchQueue.main.async {
                            print(error.localizedDescription)
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    } else {
                        let httpResponse = response as? HTTPURLResponse
                        if (httpResponse?.statusCode) != nil {
                            if let barberShopID = self.orderdetailcustomer?.barberShopUniqueID {
                                let newDate = DateInRegion()
                                let strDate = newDate.toString(.extended)
                                let userTimezone = DateByUserDeviceInitializer.tzone
                                let userCalender = DateByUserDeviceInitializer.calenderNow
                                let userLocal = DateByUserDeviceInitializer.localCode
                                
                                
                                let newUserLocal = "en"
                                let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                                let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                                let newStrDate = newUserDate.toString(.extended)
                                
                                let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                                
                                let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
                                let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                                
                                
                                let firebaseReferenceSetValueBooking = Database.database().reference()
                                firebaseReferenceSetValueBooking.child("payments").child(barberShopID).child(paymentIDDAX).child("refundAppDate").setValue(correctDateToUse)
                                firebaseReferenceSetValueBooking.child("payments").child(barberShopID).child(paymentIDDAX).child("refundAppTimeZone").setValue(userTimezone)
                                firebaseReferenceSetValueBooking.child("payments").child(barberShopID).child(paymentIDDAX).child("refundAppCalendar").setValue(userCalender)
                                firebaseReferenceSetValueBooking.child("payments").child(barberShopID).child(paymentIDDAX).child("refundAppLocal").setValue(correctLocaleToUSe)
                                
                                let firebaseReferenceSetValue = Database.database().reference()
                                firebaseReferenceSetValue.child("bookings").child(barberShopID).child(bookingID).child("bookingCancel").setValue("YES")
                                firebaseReferenceSetValue.child("bookings").child(barberShopID).child(bookingID).child("cancelDate").setValue(correctDateToUse)
                                firebaseReferenceSetValue.child("bookings").child(barberShopID).child(bookingID).child("cancelTimeZone").setValue(userTimezone)
                                firebaseReferenceSetValue.child("bookings").child(barberShopID).child(bookingID).child("cancelCalendar").setValue(userCalender)
                                firebaseReferenceSetValue.child("bookings").child(barberShopID).child(bookingID).child("cancelLocal").setValue(correctLocaleToUSe)
                                firebaseReferenceSetValue.child("bookings").child(barberShopID).child(bookingID).child("isCompleted").setValue("YES")
                                
                                DispatchQueue.main.async {
                                    self.activityIndicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    self.bookingviewcontroler?.getBookingDataAsAppointments()
                                    self.bookingviewcontroler?.getBookingDataAsAppointmentsIsCompleted()
                                    self.handleDismisView()
                                }
                                
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.activityIndicator.stopAnimating()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            }
                        }
                    }
                })
                
                dataTask.resume()
            } else {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        }
    }
    
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopLogoImageView)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(starView)
        
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopLogoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopLogoImageView.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        starView.topAnchor.constraint(equalTo: barberShopAddressPlaceHolder.bottomAnchor, constant: 5).isActive = true
        starView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
    }
    
    func setupViewObjectContriants(){
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 160).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 405)
        
        scrollView.addSubview(headerViewDescriptionPlaceHolder)
        scrollView.addSubview(collectView)
        scrollView.addSubview(costTotalContanerView)
        scrollView.addSubview(costTotalSeperatorView)
        scrollView.addSubview(addressHeaderTextPlaceHolder)
        scrollView.addSubview(addressDataHoldPlaceHolder)
        scrollView.addSubview(contactHeaderPlaceHolder)
        scrollView.addSubview(phoneLogothumbnailImageView)
        scrollView.addSubview(phoneNumberPlaceHolder)
        scrollView.addSubview(openingHoursHeaderTextPlaceHolder)
        scrollView.addSubview(openingHoursDataHoldPlaceHolder)
        scrollView.addSubview(bookingIDHeaderTextPlaceHolder)
        scrollView.addSubview(bookingIDDataHoldPlaceHolder)
        scrollView.addSubview(cancelBooking)
        
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 5).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        collectView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        costTotalContanerView.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 10).isActive = true
        costTotalContanerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        costTotalContanerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        costTotalContanerView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        costTotalContanerView.addSubview(totalTextHeaderPlaceHolder)
        costTotalContanerView.addSubview(totalQuantityPlaceHolder)
        costTotalContanerView.addSubview(totalTimePlaceHolder)
        costTotalContanerView.addSubview(totalPricePlaceHolder)
        
        totalTextHeaderPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTextHeaderPlaceHolder.leftAnchor.constraint(equalTo: costTotalContanerView.leftAnchor).isActive = true
        totalTextHeaderPlaceHolder.widthAnchor.constraint(equalToConstant: 70).isActive = true
        totalTextHeaderPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalPricePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalPricePlaceHolder.rightAnchor.constraint(equalTo: costTotalContanerView.rightAnchor).isActive = true
        totalPricePlaceHolder.widthAnchor.constraint(equalToConstant: 120).isActive = true
        totalPricePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalTimePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTimePlaceHolder.rightAnchor.constraint(equalTo: totalPricePlaceHolder.leftAnchor).isActive = true
        totalTimePlaceHolder.widthAnchor.constraint(equalToConstant: 90).isActive = true
        totalTimePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalQuantityPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalQuantityPlaceHolder.rightAnchor.constraint(equalTo: totalTimePlaceHolder.leftAnchor).isActive = true
        totalQuantityPlaceHolder.widthAnchor.constraint(equalToConstant: 75).isActive = true
        totalQuantityPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        //
        costTotalSeperatorView.topAnchor.constraint(equalTo: costTotalContanerView.bottomAnchor).isActive = true
        costTotalSeperatorView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        costTotalSeperatorView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        costTotalSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        addressHeaderTextPlaceHolder.topAnchor.constraint(equalTo: costTotalSeperatorView.bottomAnchor, constant: 15).isActive = true
        addressHeaderTextPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        addressHeaderTextPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        addressHeaderTextPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        addressDataHoldPlaceHolder.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 12).isActive = true
        addressDataHoldPlaceHolder.topAnchor.constraint(equalTo: addressHeaderTextPlaceHolder.bottomAnchor).isActive = true
        addressDataHoldPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.5, constant: 30).isActive = true
        addressDataHoldPlaceHolder.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        contactHeaderPlaceHolder.topAnchor.constraint(equalTo: addressDataHoldPlaceHolder.bottomAnchor, constant: 10).isActive = true
        contactHeaderPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contactHeaderPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        contactHeaderPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        phoneLogothumbnailImageView.topAnchor.constraint(equalTo: contactHeaderPlaceHolder.bottomAnchor).isActive = true
        phoneLogothumbnailImageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 12).isActive = true
        phoneLogothumbnailImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        phoneLogothumbnailImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        phoneNumberPlaceHolder.topAnchor.constraint(equalTo: contactHeaderPlaceHolder.bottomAnchor).isActive = true
        phoneNumberPlaceHolder.leftAnchor.constraint(equalTo: phoneLogothumbnailImageView.rightAnchor, constant: 10).isActive = true
        phoneNumberPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -75).isActive = true
        phoneNumberPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        openingHoursHeaderTextPlaceHolder.topAnchor.constraint(equalTo: phoneNumberPlaceHolder.bottomAnchor, constant: 10).isActive = true
        openingHoursHeaderTextPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        openingHoursHeaderTextPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        openingHoursHeaderTextPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        openingHoursDataHoldPlaceHolder.topAnchor.constraint(equalTo: openingHoursHeaderTextPlaceHolder.bottomAnchor, constant: 10).isActive = true
        openingHoursDataHoldPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        openingHoursDataHoldPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        openingHoursDataHoldPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        
        bookingIDHeaderTextPlaceHolder.topAnchor.constraint(equalTo: openingHoursDataHoldPlaceHolder.bottomAnchor, constant: 10).isActive = true
        bookingIDHeaderTextPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bookingIDHeaderTextPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        bookingIDHeaderTextPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        bookingIDDataHoldPlaceHolder.topAnchor.constraint(equalTo: bookingIDHeaderTextPlaceHolder.bottomAnchor, constant: 10).isActive = true
        bookingIDDataHoldPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bookingIDDataHoldPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        bookingIDDataHoldPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        cancelBooking.topAnchor.constraint(equalTo: bookingIDDataHoldPlaceHolder.bottomAnchor, constant: 10).isActive = true
        cancelBooking.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        cancelBooking.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        cancelBooking.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }

}

class customOrderDetailCustomerCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let serviceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = "Service name"
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = "Time"
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = "price"
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    func setupViews(){
        addSubview(serviceNamePlaceHolder)
        addSubview(serviceTimePlaceHolder)
        addSubview(servicePricePlaceHolder)
        addSubview(seperatorView)
        
        //backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|-5-[v0(100)]-15-[v1(100)][v2(120)]-5-|", views: serviceNamePlaceHolder, serviceTimePlaceHolder, servicePricePlaceHolder)
        addContraintsWithFormat(format: "V:|[v0]-5-[v1(1)]|", views: serviceNamePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|[v0]-5-[v1(1)]|", views: serviceTimePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|[v0]-5-[v1(1)]|", views: servicePricePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    override func prepareForReuse() {
        self.serviceNamePlaceHolder.text = ""
        self.serviceTimePlaceHolder.text = ""
        self.servicePricePlaceHolder.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
