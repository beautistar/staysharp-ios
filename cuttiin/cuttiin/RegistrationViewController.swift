//
//  RegistrationViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import Navajo_Swift
import SwiftDate


class RegistrationViewController: UIViewController {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var passwordValid = false
    
    let inputsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let firstNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let firstNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let firstNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let lastNameHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let lastNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let lastNameSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.keyboardType = .emailAddress
        em.autocapitalizationType = .none
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    let passwordHiddenPlaceHolder: UILabel = {
        let php = UILabel()
        php.translatesAutoresizingMaskIntoConstraints = false
        php.text = NSLocalizedString("passwordLabelTextCustomerRegistrationView", comment: "password must have 8 characters or more with characters and digits")
        php.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        php.textColor = UIColor(r: 129, g: 129, b: 129)
        php.isHidden = true
        php.adjustsFontSizeToFitWidth = true
        php.minimumScaleFactor = 0.1
        php.baselineAdjustment = .alignCenters
        php.textAlignment = .left
        return php
    }()
    
    let passwordTextField: UITextField = {
        let ps = UITextField()
        ps.translatesAutoresizingMaskIntoConstraints = false
        ps.textColor = UIColor.black
        ps.placeholder = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
        ps.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ps.isSecureTextEntry = true
        ps.addTarget(self, action: #selector(isValidPassword), for: .editingChanged)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingDidBegin)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingChanged)
        ps.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return ps
    }()
    
    lazy var showPassordTextFieldButton: UIButton = {
        let sptfb = UIButton()
        sptfb.translatesAutoresizingMaskIntoConstraints = false
        sptfb.isUserInteractionEnabled = false
        sptfb.setTitle(NSLocalizedString("showButtonTitleLoginView", comment: "Show"), for: .normal)
        sptfb.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        sptfb.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
        sptfb.backgroundColor = UIColor.white
        sptfb.addTarget(self, action: #selector(handleShowPasswordText), for: .touchUpInside)
        return sptfb
    }()
    
    let passwordSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return view
    }()
    
    lazy var registerButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("registerButtonTitleCustomerRegistrationView", comment: "Register"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
        st.addTarget(self, action: #selector(handleRegisterAccount), for: .touchUpInside)
        st.isEnabled = false
        return st
    }()
    
    lazy var termsAndConditionsButtonLabel: UILabel = {
        let dtv = UILabel()
        dtv.translatesAutoresizingMaskIntoConstraints = false
        dtv.textAlignment = .center
        dtv.numberOfLines = 0
        dtv.isUserInteractionEnabled = true
        dtv.text = NSLocalizedString("termsAndConditionCustomerRegistrationView", comment: "By hitting Register you accept the Terms and Conditions!")
        dtv.font = UIFont(name: "HelveticaNeue", size: 9.0)
        dtv.textColor = UIColor(r: 23, g: 69, b: 90)
        dtv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowWebPopUPWebTermsandCondition)))
        return dtv
    }()
    
    lazy var alreadyAMemberButtonLabel: UILabel = {
        let dtv = UILabel()
        dtv.translatesAutoresizingMaskIntoConstraints = false
        dtv.textAlignment = .center
        dtv.numberOfLines = 0
        dtv.isUserInteractionEnabled = true
        dtv.text = NSLocalizedString("alreadyAMemberButtonTitleCustomerRegistrationView", comment: "Already a member? Log in")
        dtv.font = UIFont(name: "HelveticaNeue", size: 11.0)
        dtv.textColor = UIColor(r: 23, g: 69, b: 90)
        
        dtv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleLoginAction)))
        return dtv
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        emhp.textColor = UIColor(r: 129, g: 129, b: 129)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("registrationNaviagtionTitleCustomerRegistrationView", comment: "Registration")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(inputsContainerView)
        view.addSubview(registerButton)
        view.addSubview(termsAndConditionsButtonLabel)
        view.addSubview(alreadyAMemberButtonLabel)
        view.addSubview(errorMessagePlaceHolder)
        setupInputContainerView()
        setupButtons()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleBackAction(){
        self.hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    //new entry controls
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.firstNameTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.emailTextField.text = ""
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordTextField.text = ""
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.firstNameTextField.text = ""
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameTextField.text = ""
        self.lastNameSeperatorView.backgroundColor = UIColor.black
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleRegisterAccount(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let firstName = firstNameTextField.text, let lastName = lastNameTextField.text,let emailtext = emailTextField.text, let password = passwordTextField.text else {
            return
        }
        
        
        Auth.auth().createUser(withEmail: emailtext, password: password) { (AuthUserData, errororororo) in
            if let error = errororororo {
                UIApplication.shared.endIgnoringInteractionEvents()
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                
                if let errCode = AuthErrorCode(rawValue: error._code){
                    switch errCode {
                    case .networkError:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                    case .invalidEmail:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAddressMalformedErrorCodeLoginView", comment: "email address is malformed")
                    case .operationNotAllowed:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care")
                    case .emailAlreadyInUse:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAlreadyInUseErrorCodeLoginView", comment: "email already in use by an existing account")
                    case .weakPassword:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("weakPasswordErrorCodeCustomerRegistrationView", comment: "weak password")
                    default:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                        
                    }
                }
                self.hideKeyboard()
                return
            }
            
            guard let firebaseUser = AuthUserData?.user else {
                return
            }
            
            //let firebaseUser = Auth.auth().currentUser
            firebaseUser.sendEmailVerification(completion: { (errorrrwewe) in
                if let error = errorrrwewe {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let errCode = AuthErrorCode(rawValue: error._code){
                        switch errCode {
                        case .networkError:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                        case .userNotFound:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("accountNotFoundErrorCodeLoginView", comment: "Account not found")
                        default:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                            
                        }
                    }
                    self.hideKeyboard()
                    
                    firebaseUser.delete(completion: { (errorrr) in
                        if let error = errorrr {
                            print(error.localizedDescription)
                            return
                        }
                        do {
                            try Auth.auth().signOut()
                        } catch let logoutError {
                            print(logoutError)
                        }
                    })
                    return
                }
                
                let newDate = DateInRegion()
                let strDate = newDate.toString(.extended)
                let userTimezone = DateByUserDeviceInitializer.tzone
                let userCalender = DateByUserDeviceInitializer.calenderNow
                let userLocal = DateByUserDeviceInitializer.localCode
                
                
                let newUserLocal = "en"
                let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                let newStrDate = newUserDate.toString(.extended)
                
                let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                
                let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
                let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                
                //sucessfully logged in
                let ref = Database.database().reference()
                let userReference = ref.child("users").child(firebaseUser.uid)
                let values = ["uniqueID": firebaseUser.uid ,"firstName": firstName, "lastName": lastName , "email": emailtext,"role": "customer","profileImageUrl":"nil", "mobileNumber": "1234","profileImageName":"nil", "dateAccountCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe, "cardID": "not available", "cardDateCreated": "not available", "cardDateCalendar": "not available", "cardDateTimeZone": "not available", "cardDateLocale": "not available", "cardHasBeenDeleted":"YES"]
                
                userReference.updateChildValues(values, withCompletionBlock: { (errorr, ref) in
                    if let error = errorr {
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        print(error.localizedDescription)
                        self.hideKeyboard()
                        return
                    }else{
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.handlePresentCustomerChoosePictureView()
                    }
                })
                
            })
        }
    }
    
    func handlePresentCustomerChoosePictureView(){
        registerButton.isEnabled = false
        registerButton.setTitleColor(UIColor.black, for: .normal)
        registerButton.backgroundColor = UIColor.clear
        let choosePView = ChoosePhotoViewController()
        choosePView.customerOrBarberChoice = "Customer"
        let navController = UINavigationController(rootViewController: choosePView)
        self.present(navController, animated: true, completion: nil)
        self.firstNameTextField.text = ""
        self.lastNameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
    }
    
    @objc func handleLoginAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleAccestoApp(){
        let mainView = ViewController()
        let navController = UINavigationController(rootViewController: mainView)
        present(navController, animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(){
        if firstNameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == "" || passwordTextField.text == "" {
            //Disable button
            self.showPassordTextFieldButton.isUserInteractionEnabled = true
            self.passwordTextField.isSecureTextEntry = true
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            registerButton.isEnabled = false
            registerButton.setTitleColor(UIColor.black, for: .normal)
            registerButton.backgroundColor = UIColor.clear
            
        } else {
            //Enable button
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            if emailValid == true && passwordValid == true {
                registerButton.isEnabled = true
                registerButton.setTitleColor(UIColor.white, for: .normal)
                registerButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                self.showPassordTextFieldButton.isUserInteractionEnabled = true
                
            }
        }
    }
    
    @objc func firstNametextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = false
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func lastNametextFieldDidChange(){
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
        self.passwordHiddenPlaceHolder.isHidden = true
        
    }
    
    @objc func passwordtextFieldDidChange(){
        self.passwordSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = false
    }
    
    @objc func handleShowWebPopUPWebTermsandCondition(){
        guard let url = URL(string: "https://teckdk.com/betingelser/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func handleShowPasswordText(){
        passwordTextField.isSecureTextEntry = false
    }
    
    @objc func isValidPassword(){
        let password = passwordTextField.text ?? ""
        let strength = Navajo.strength(ofPassword: password)
        let strengthValue = Navajo.localizedString(forStrength: strength)
        
        if (strengthValue == "Strong" || strengthValue == "Very Strong" || strengthValue == "Reasonable"){
            print("shoes")
            self.passwordValid = true
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
        }else{
            self.passwordValid = false
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
            registerButton.isEnabled = false
            registerButton.setTitleColor(UIColor.black, for: .normal)
            registerButton.backgroundColor = UIColor.clear
        }
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailNotFormattedProperlyPlaceholderLoginView", comment: "Email is not properly formatted")
            registerButton.isEnabled = false
            registerButton.setTitleColor(UIColor.black, for: .normal)
            registerButton.backgroundColor = UIColor.clear
        }
    }
    
    func setupInputContainerView(){
        inputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        inputsContainerView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        
        
        inputsContainerView.addSubview(firstNameHiddenPlaceHolder)
        inputsContainerView.addSubview(firstNameTextField)
        inputsContainerView.addSubview(firstNameSeperatorView)
        inputsContainerView.addSubview(lastNameHiddenPlaceHolder)
        inputsContainerView.addSubview(lastNameTextField)
        inputsContainerView.addSubview(lastNameSeperatorView)
        inputsContainerView.addSubview(emailHiddenPlaceHolder)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeperatorView)
        inputsContainerView.addSubview(passwordHiddenPlaceHolder)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(showPassordTextFieldButton)
        inputsContainerView.addSubview(passwordSeperatorView)
        
        
        firstNameHiddenPlaceHolder.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
        firstNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        firstNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        firstNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        firstNameTextField.topAnchor.constraint(equalTo: firstNameHiddenPlaceHolder.bottomAnchor).isActive = true
        firstNameTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        firstNameSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        firstNameSeperatorView.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        firstNameSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        firstNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        lastNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        lastNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        lastNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        lastNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        lastNameTextField.topAnchor.constraint(equalTo: lastNameHiddenPlaceHolder.bottomAnchor).isActive = true
        lastNameTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        lastNameSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        lastNameSeperatorView.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        lastNameSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        lastNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: lastNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        passwordHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        passwordHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        passwordHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        passwordHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: passwordHiddenPlaceHolder.bottomAnchor).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: inputsContainerView.rightAnchor, constant: -20).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        showPassordTextFieldButton.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        showPassordTextFieldButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        showPassordTextFieldButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        passwordSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordSeperatorView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        passwordSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        passwordSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
    }
    
    func setupButtons(){
        registerButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 10).isActive = true
        registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        termsAndConditionsButtonLabel.topAnchor.constraint(equalTo: registerButton.bottomAnchor, constant: 5).isActive = true
        termsAndConditionsButtonLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        termsAndConditionsButtonLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        termsAndConditionsButtonLabel.heightAnchor.constraint(equalToConstant: 25).isActive =  true
        
        alreadyAMemberButtonLabel.topAnchor.constraint(equalTo: termsAndConditionsButtonLabel.bottomAnchor, constant: 5).isActive = true
        alreadyAMemberButtonLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        alreadyAMemberButtonLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        alreadyAMemberButtonLabel.heightAnchor.constraint(equalToConstant: 20).isActive =  true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: alreadyAMemberButtonLabel.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
    }

}
