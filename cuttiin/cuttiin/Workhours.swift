//
//  Workhours.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/9/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Workhours: NSObject {
    var openingTimeHour: String?
    var openingTimeMinute: String?
    var openingTimeSecond: String?
    var closingTimeHour: String?
    var closingTimeMinute: String?
    var closingTimeSecond: String?
    var minutesBeforeClosing: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
}
