//
//  AppDelegate.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/12/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FBSDKCoreKit
import GooglePlaces
import GoogleMaps
import UserNotifications
import FirebaseMessaging
import SwiftDate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    let mySpecialNotificationKey = "com.teckdkapps.specialNotificationKey"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Firebase configure
        FirebaseApp.configure()
        
        //Facebook Authentication setup
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Google maps and places setup AIzaSyDqJNLVhiZNiWQktqqItY1DlT_5f5ow5I8 //AIzaSyB7ULY1RoDosRyt71Os4krUErM4yprYv3U
//        GMSPlacesClient.provideAPIKey("AIzaSyB7ULY1RoDosRyt71Os4krUErM4yprYv3U")
//        GMSServices.provideAPIKey("AIzaSyB7ULY1RoDosRyt71Os4krUErM4yprYv3U")
        
        // TESTING id
        GMSPlacesClient.provideAPIKey("AIzaSyAAihXBu8GqOKm33Y0iHYjzWMxpGhj4YjM")
        GMSServices.provideAPIKey("AIzaSyAAihXBu8GqOKm33Y0iHYjzWMxpGhj4YjM")

        
        
        //Rootview controller song and dance
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = MainNavigationController()
        //UINavigationController(rootViewController: TestViewController())
        
        //navigation bar background color
        UINavigationBar.appearance().barTintColor = UIColor(r: 23, g: 69, b: 90)
        
        //navigation bar tint color affects the buttons
        UINavigationBar.appearance().tintColor = UIColor.white
        
        //naviagtion bar title text font
        UINavigationBar.appearance().titleTextAttributes = [ NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Bold", size: 22)!]
        
        //set status bar light content
        UIApplication.shared.statusBarStyle = .lightContent
        
        //naviagtion bar opaque
        UINavigationBar.appearance().isTranslucent = false
        
        //navigation bar title text color
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        //IQRKeyboard setup
        IQKeyboardManager.shared.enable = true
        
        //swiftdate
        SwiftDate.defaultRegion = Region.local
        
        
        //application.beginBackgroundTask(withName: "showNotifications", expirationHandler: nil)
        
        //firebase cloud messaging
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("Registration succeeded! Token: ", token)
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        if let uid = Auth.auth().currentUser?.uid, let fcmtokenkey = Messaging.messaging().fcmToken {
            let firebaseRefFCMToken = Database.database().reference()
            firebaseRefFCMToken.child("fcmtoken").child(uid).child("key").setValue(fcmtokenkey)
        }
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // custom code to handle push while app is in the foreground
        //print("Handle push from foreground\(notification.request.content.userInfo)")
        
        if let dict = notification.request.content.userInfo["aps"] as? NSDictionary {
            let d : [String : Any] = dict["alert"] as! [String : Any]
            let body : String = d["body"] as! String
            let title : String = d["title"] as! String
            self.perform(#selector(takeUserToBookingView), with: self, afterDelay: 2)
            self.showAlertAppDelegate(title: title,message:body,buttonTitle:"Ok",window:self.window!)
            
            //make notication check here
            UserDefaults.standard.set(UIApplication.shared.applicationIconBadgeNumber, forKey: "dataPresentOnApplicationBadge")
            if let currentData = UserDefaults.standard.object(forKey: "dataPresentOnApplicationBadge") as? Int {
                UIApplication.shared.applicationIconBadgeNumber = currentData + 1
                NotificationCenter.default.post(name: Notification.Name(rawValue: mySpecialNotificationKey), object: self)
            }
            
        }
        completionHandler([.alert, .sound])
    }
    
    func showAlertAppDelegate(title: String,message : String,buttonTitle: String,window: UIWindow){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        window.rootViewController?.present(alert, animated: false, completion: nil)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed\(response.notification.request.content.userInfo)")
        if let currentData = UserDefaults.standard.object(forKey: "dataPresentOnApplicationBadge") as? Int {
            UIApplication.shared.applicationIconBadgeNumber = currentData + 1
            NotificationCenter.default.post(name: Notification.Name(rawValue: mySpecialNotificationKey), object: self)
        } else {
            UserDefaults.standard.set(UIApplication.shared.applicationIconBadgeNumber, forKey: "dataPresentOnApplicationBadge")
            if let currentData = UserDefaults.standard.object(forKey: "dataPresentOnApplicationBadge") as? Int {
                UIApplication.shared.applicationIconBadgeNumber = currentData + 1
                NotificationCenter.default.post(name: Notification.Name(rawValue: mySpecialNotificationKey), object: self)
            }
        }
        //handle action
        completionHandler()
    }
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    @objc func  takeUserToBookingView(){
        if let mainNavDataHold: UINavigationController = self.window?.rootViewController as? UINavigationController {
            if let mainControl = mainNavDataHold.viewControllers[safe: 0] as? UITabBarController{
                if let uid = Auth.auth().currentUser?.uid {
                    let firebaseBaseConfirmUser = Database.database().reference()
                    firebaseBaseConfirmUser.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshotDataHold) in
                        if let dictionaryUserRoleConfirm = snapshotDataHold.value as? [String: AnyObject] {
                            
                            let CustomerConfirm = HandleDataRequest.handleUserCustomerNode(firebaseData: dictionaryUserRoleConfirm)
                            
                            if CustomerConfirm {
                                let userSingleHold = Customer()
                                userSingleHold.setValuesForKeys(dictionaryUserRoleConfirm)
                                
                                if let userRole = userSingleHold.role {
                                    if userRole == "customer" {
                                        mainControl.selectedIndex = 0
                                    }
                                }
                            }
                            
                            
                        }
                    }, withCancel: { (errororoor) in
                        print(errororoor)
                    })
                }
                
                print("Money Never sleeps")
            } else {
                print("Main controller was not called")
            }
        } else {
            print("Hey mama boy")
        }
    }


}

