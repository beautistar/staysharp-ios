//
//  MainNavigationController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/20/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class MainNavigationController: UINavigationController {
    var calledOnce = true
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isHidden = true
        view.backgroundColor = UIColor.white
        UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
        
//        reachability.whenUnreachable = { _ in
//            print("Not reachable")
//            self.perform(#selector(self.showAlertViewActionSheet), with: self, afterDelay: 0.5)
//        }
//
//        NotificationCenter.default.addObserver(self, selector: #selector(internetChanges), name: Notification.Name.reachabilityChanged, object: reachability)
//        do {
//            try reachability.startNotifier()
//        } catch {
//            print("Unable to start notifier")
//        }
        
        if let uid = Auth.auth().currentUser?.uid {
            print(uid, "manga")
            //delete here.
            //self.handleAllLogOut()
            //self.showLoginView()
            
            let firebaseREF = Database.database().reference()
            firebaseREF.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshootttaa) in
                
                
                if let dictionary = snapshootttaa.value as? [String: AnyObject] {
                    
                    if let userRole = dictionary["role"] as? String { //let userRole = userCLEDATA.role
                        switch userRole {
                        case "customer":
                            let customerController = CustomTabBarController()
                            self.viewControllers = [customerController]
                            UserDefaults.standard.set("YES", forKey: "userHasVerifiedCredantialsAndCanProceed")
                        case "barberShop":
                            let barberShopController = CustomBarberTabBarController()
                            self.viewControllers = [barberShopController]
                            UserDefaults.standard.set("YES", forKey: "userHasVerifiedCredantialsAndCanProceed")
                        case "barberStaff":
                            let barberStaffController = CustomBarberTabBarController()
                            self.viewControllers = [barberStaffController]
                            UserDefaults.standard.set("YES", forKey: "userHasVerifiedCredantialsAndCanProceed")
                        default:
                            print("Sky High")
                            self.handleAllLogOut()
                            self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                        }
                    } else {
                        self.handleAllLogOut()
                        self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                    }
                } else {
                    print("red rubees")
                    FBSDKLoginManager().logOut()
                    do{
                        try Auth.auth().signOut()
                    }catch let logoutError {
                        print(logoutError)
                    }
                    
                    self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                }
            }, withCancel: { (errororrr) in
                self.handleAllLogOut()
                self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                print(errororrr, "raining men all day out here")
            })
        } else {
            print("rocker money")
            self.handleAllLogOut()
            self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
        }
        
        self.calledOnce = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        reachability.whenUnreachable = { _ in
//            print("Not reachable")
//            self.perform(#selector(self.showAlertViewActionSheet), with: self, afterDelay: 0.5)
//        }
        
        if !calledOnce {
            
            if let uid = Auth.auth().currentUser?.uid, let statusAuth = UserDefaults.standard.object(forKey: "userHasVerifiedCredantialsAndCanProceed") as? String {
                if statusAuth == "NO" {
                    let firebaseREF = Database.database().reference()
                    firebaseREF.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshootototoott) in
                        if let dictionary = snapshootototoott.value as? [String: AnyObject] {
                            
                            if let userRole = dictionary["role"] as? String { //let userRole = userGLEData.role
                                switch userRole {
                                case "customer":
                                    let customerController = CustomTabBarController()
                                    self.viewControllers = [customerController]
                                    UserDefaults.standard.set("YES", forKey: "userHasVerifiedCredantialsAndCanProceed")
                                case "barberShop":
                                    let barberShopController = CustomBarberTabBarController()
                                    self.viewControllers = [barberShopController]
                                    UserDefaults.standard.set("YES", forKey: "userHasVerifiedCredantialsAndCanProceed")
                                case "barberStaff":
                                    let barberStaffController = CustomBarberTabBarController()
                                    self.viewControllers = [barberStaffController]
                                    UserDefaults.standard.set("YES", forKey: "userHasVerifiedCredantialsAndCanProceed")
                                default:
                                    print("Sky High")
                                    self.handleAllLogOut()
                                    self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                                }
                            }  else {
                                self.handleAllLogOut()
                                self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                            }
                        } else {
                            print("red roootss")
                            self.handleAllLogOut()
                            self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                        }
                    }, withCancel: { (errororo) in
                        print(errororo)
                        self.handleAllLogOut()
                        self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                    })
                } else {
                    print("no status auth")
                    
                }
            } else {
                print("rocker money")
                self.handleAllLogOut()
                perform(#selector(showLoginView), with: self, afterDelay: 0.03)
            }
        }
        calledOnce = false
    }
    
    @objc func showLoginView(){
        let welcomeviewcontroller = WelcomeViewController() //WelcomeViewController
        present(welcomeviewcontroller, animated: true, completion: nil)
    }
    
//    @objc func internetChanges(note: Notification){
//        if let reachable = note.object as? Reachability {
//            if reachable.connection == .none {
//                self.perform(#selector(self.showAlertViewActionSheet), with: self, afterDelay: 0.5)
//            }
//        }
//    }
//
//    @objc func showAlertViewActionSheet(){
//        let alertController = UIAlertController(title: NSLocalizedString("alertViewActionSheetMainNavView", comment: "Stay Sharp"), message: NSLocalizedString("alertViewActionSheetMessageMainNavView", comment: "No Internet Connection"), preferredStyle: .actionSheet)
//
//        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default) { action in
//            // ...
//        }
//        alertController.addAction(OKAction)
//
//        self.present(alertController, animated: true) {
//            // ...
//        }
//    }
    
    func handleAllLogOut(){
        FBSDKLoginManager().logOut()
        do{
            try Auth.auth().signOut()
            print("Logged out yeah")
        }catch let logoutError {
            print(logoutError)
        }
    }
}
