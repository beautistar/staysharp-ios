//
//  ChangePasswordViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/8/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase

class ChangePasswordViewController: UIViewController {
    var emailValid = false
    
    let topHeaderPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("topHeaderChangePasswordView", comment: "Enter your email address")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor(r: 23, g: 69, b: 90)
        ehp.textAlignment = .center
        return ehp
    }()
    
    let topHeaderDescriptionPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("topHeaderDescriptionChangePasswordView", comment: "Please enter your email to recover your pasword. An email will be sent to you with a link to reset your password")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        ehp.textColor = UIColor(r: 23, g: 69, b: 90)
        ehp.numberOfLines = 0
        ehp.textAlignment = .center
        return ehp
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.keyboardType = .emailAddress
        em.autocapitalizationType = .none
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    lazy var sendEmailButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.white
        st.setTitle(NSLocalizedString("sendButtonChangePasswordView", comment: "SEND"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor.black.cgColor
        st.addTarget(self, action: #selector(handleSendEmail), for: .touchUpInside)
        return st
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("resetNavigationTitleChangePasswordView", comment: "Reset Password")
        view.backgroundColor = UIColor.white
        view.addSubview(topHeaderPlaceHolder)
        view.addSubview(topHeaderDescriptionPlaceHolder)
        view.addSubview(emailHiddenPlaceHolder)
        view.addSubview(emailTextField)
        view.addSubview(emailSeperatorView)
        view.addSubview(sendEmailButton)
        view.addSubview(errorMessagePlaceHolder)
        setupViewObjectCOntraints()
        checkAccountCredentialProvider()
    }
    
    @objc func handleSendEmail(){
        if let email = emailTextField.text {
            Auth.auth().sendPasswordReset(withEmail: email, completion: { (errorr) in
                if let error = errorr {
                    print(error.localizedDescription)
                    
                    if let errCode = AuthErrorCode(rawValue: error._code){
                        switch errCode {
                        case .networkError:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                        case .invalidEmail:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("emailAddressMalformedErrorCodeLoginView", comment: "email address is malformed")
                        case .userDisabled:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care")
                        case .userNotFound:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("accountNotFoundErrorCodeLoginView", comment: "Account not found")
                        default:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                            
                        }
                    }
                    
                    self.emailTextField.text = ""
                    self.sendEmailButton.isEnabled = false
                    self.sendEmailButton.backgroundColor = UIColor.white
                    self.sendEmailButton.setTitleColor(UIColor.black, for: .normal)
                    self.sendEmailButton.layer.borderColor = UIColor.black.cgColor
                    return
                }else{
                    self.emailTextField.text = ""
                    
                    let alert = UIAlertController(title: nil, message: NSLocalizedString("errorMessageTextPasswordChange", comment: "Email sent."), preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: { (alertaction) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            })
        }
    }
    
    func checkAccountCredentialProvider(){
        if let provider = Auth.auth().currentUser?.providerData{
            for access in provider {
                let accessProvider = access.providerID
                if accessProvider == "facebook.com" {
                    self.emailTextField.isUserInteractionEnabled = false
                    self.sendEmailButton.isEnabled = false
                    self.errorMessagePlaceHolder.text = "Password reset is not available to this account.(Facebook)"
                }
            }
        }
    }
    
    //background color UIColor(r: 11, g: 49, b: 68) text color UIColor(r: 118, g: 187, b: 220) border color UIColor(r: 118, g: 187, b: 220).cgColor
    @objc func textFieldDidChange(){
        if emailTextField.text == "" {
            //Disable button
            self.sendEmailButton.isEnabled = false
            self.sendEmailButton.backgroundColor = UIColor.white
            self.sendEmailButton.setTitleColor(UIColor.black, for: .normal)
            self.sendEmailButton.layer.borderColor = UIColor.black.cgColor
            
        } else {
            //Enable button
            if emailValid == true {
                self.sendEmailButton.isEnabled = true
                self.sendEmailButton.backgroundColor = UIColor(r: 11, g: 49, b: 68)
                self.sendEmailButton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
                self.sendEmailButton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
            }
        }
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.emailHiddenPlaceHolder.isHidden = false
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailNotFormattedProperlyPlaceholderLoginView", comment: "Email is not properly formatted")
        }
    }
    
    func setupViewObjectCOntraints(){
        
        topHeaderPlaceHolder.topAnchor.constraint(equalTo: view.topAnchor, constant: 55).isActive = true
        topHeaderPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topHeaderPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        topHeaderPlaceHolder.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        topHeaderDescriptionPlaceHolder.topAnchor.constraint(equalTo: topHeaderPlaceHolder.bottomAnchor, constant: 10).isActive = true
        topHeaderDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topHeaderDescriptionPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        topHeaderDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: topHeaderDescriptionPlaceHolder.bottomAnchor, constant: 20).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: emailTextField.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        sendEmailButton.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 15).isActive = true
        sendEmailButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        sendEmailButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        sendEmailButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: sendEmailButton.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }

}
