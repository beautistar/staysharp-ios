//
//  CreditCardPaymentVerification.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 14/10/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

@objcMembers class CreditCardPaymentVerification: NSObject {
    var barberShopID: String?
    var isCreditCardPaymentAllowed: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
}
