//
//  UpcomingBarberShopBookingViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 26/12/2017.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Firebase

class UpcomingBarberShopBookingViewController: UIViewController {
    var customerMadeBookings = [Bookings]()
    var customerMadeBookingsIsCompleted = [Bookings]()
    var barberShopBookNames = [String]()
    var indexValueMain = 1
    
    var bookingSelectedStartTime = [String]()
    var bookingSelectedCalendar = [String]()
    var bookingSelectedTimeZone = [String]()
    var bookingSelectedLocal = [String]()
    var bookingSelectedTotalTime = [String]()
    var bookingSelectedServiceImageUrl = [String]()
    var bookingSelectedBarberShopName = [String]()
    var bookingSelectedBookingUniqueID = [String]()
    var bookingSelectedBarberShopUniqueID = [String]()
    var appointmentsCustomer = [AppointmentsCustomer]()
    
    var arrayLengthHolder = [Int]()
    var arrayPaymentChecksHolderFOrAppointments = [Payments]()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customUpcomingAndCompletedCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdBAUP")
        cv.register(customUpcomingAndCompletedCancelledCollectionViewCell.self, forCellWithReuseIdentifier: "cellIDXOXBA")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.title = NSLocalizedString("upcomingBookingButtonProfileSettingsEditorView", comment: "Upcoming Bookings")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        getBookingDataAsAppointments()
    }
    
    @objc func handleBackAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        print("ro money call")
        getBookingDataAsAppointments()
        sender.endRefreshing()
    }
    
    @objc func getBookingDataAsAppointments(){
        DispatchQueue.global(qos: .background).async {
            if let customerUUID = Auth.auth().currentUser?.uid {
                let firebaseReferenceCustomerBookingHold = Database.database().reference()
                firebaseReferenceCustomerBookingHold.child("bookings").child(customerUUID).observeSingleEvent(of: .value, with: { (snapshootDataCustomerBooking) in
                    
                    self.bookingSelectedStartTime.removeAll()
                    self.bookingSelectedCalendar.removeAll()
                    self.bookingSelectedTimeZone.removeAll()
                    self.bookingSelectedLocal.removeAll()
                    self.bookingSelectedTotalTime.removeAll()
                    self.bookingSelectedServiceImageUrl.removeAll()
                    self.bookingSelectedBarberShopName.removeAll()
                    self.bookingSelectedBookingUniqueID.removeAll()
                    //self.appointmentsCustomer.removeAll()
                    self.bookingSelectedBarberShopUniqueID.removeAll()
                    
                    if let dictionaryCustomerBooking = snapshootDataCustomerBooking.value as? [String: AnyObject] {
                        for custom in dictionaryCustomerBooking {
                            if let customSingle = custom.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: customSingle) == true {
                                let customerbooking = Bookings()
                                customerbooking.setValuesForKeys(customSingle)
                                
                                if let customerIDXOX = customerbooking.bookedBarberShopID, let userIDXOX = Auth.auth().currentUser?.uid, let payedBook = customerbooking.paymentID, let barberIDX = customerbooking.bookedBarberID, let bookStartString =  customerbooking.bookingStartTime, let bookClandar = customerbooking.calendar, let bookTizne = customerbooking.timezone, let bookLocal = customerbooking.local, let bookServiceUUID = customerbooking.bookedServiceID, let bookkUniqueIDX = customerbooking.bookingID, let bookTotalTmx =  customerbooking.ConfirmedTotalTime, let checkComplete = customerbooking.isCompleted, let shopID = customerbooking.bookedBarberShopID  {
                                    
                                    
                                    let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal)
                                    let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookTizne, calendar: bookClandar, locale: bookLocal, dateData: bookStartString)
                                    
                                    if verifyBookingData == true && verifyBookingStartDate == true {
                                        let bookingDefinedRedion = DateByUserDeviceInitializer.getRegion(TZoneName: bookTizne, calenName: bookClandar, LocName: bookLocal)
                                        
                                        if let bookStartCompare = bookStartString.toDate(style: .extended, region: bookingDefinedRedion) {
                                            let currentDaydateAccessed = DateInRegion()
                                            if bookStartCompare.isAfterDate(currentDaydateAccessed, orEqual: false, granularity: .day) {
                                                if customerIDXOX == userIDXOX && payedBook != "nil" && payedBook != "" && checkComplete == "YES"  {
                                                    let firbaseDatabaseReferencePaymentInstantChecker =  Database.database().reference()
                                                    firbaseDatabaseReferencePaymentInstantChecker.child("payments").child(shopID).child(payedBook).observeSingleEvent(of: .value, with: { (snapshotPayXXccCC) in
                                                        
                                                        if let dictionayRefund = snapshotPayXXccCC.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund) == true {
                                                            let paymentHold = Payments()
                                                            paymentHold.setValuesForKeys(dictionayRefund)
                                                            
                                                            if let payConfirm = paymentHold.payment_aq_status_msg, let payQuickOayConfirm = paymentHold.payment_qp_status_msg {
                                                                if payConfirm == "Approved" && payQuickOayConfirm == "Approved" {
                                                                    
                                                                    let firbaseDatabaseReferenceBarberDetailsCancekked =  Database.database().reference()
                                                                    firbaseDatabaseReferenceBarberDetailsCancekked.child("users").child(barberIDX).observeSingleEvent(of: .value, with: { (snapshotUserDataHold) in
                                                                        if let dictionaryDataHoldCanckked = snapshotUserDataHold.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryDataHoldCanckked) == true {
                                                                            let userDataHold = Barber()
                                                                            userDataHold.setValuesForKeys(dictionaryDataHoldCanckked)
                                                                            
                                                                            if let barberShopName = userDataHold.barberShopID, let barberName = userDataHold.firstName {
                                                                                
                                                                                let firbaseDatabaseReferencServieDetailsService =  Database.database().reference()
                                                                                firbaseDatabaseReferencServieDetailsService.child("service").child(barberShopName).child(bookServiceUUID).observeSingleEvent(of: .value, with: { (snapshoototService) in
                                                                                    if let dictionaryServiceDDE = snapshoototService.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionaryServiceDDE) == true {
                                                                                        let serviceDatHold = Service()
                                                                                        serviceDatHold.setValuesForKeys(dictionaryServiceDDE)
                                                                                        if let serviceImageURL = serviceDatHold.serviceImageUrl {
                                                                                            
                                                                                            self.arrayDefaultCombineCustomerBooking(serviceImageURL: serviceImageURL, bookedBarberShopName: barberName, bookingUniqueIDDD: bookkUniqueIDX, bookingTotalTime: bookTotalTmx, bookStartTSS: bookStartString, bookTzone: bookTizne, bookClandar: bookClandar, bookLocal: bookLocal, shopUniqueID: barberShopName)
                                                                                            
                                                                                        }
                                                                                    }
                                                                                }, withCancel: nil)
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        }
                                    }
                                }//here
                            }
                        }
                    }
                })
                
            }
        }
    }
    
    func arrayDefaultCombineCustomerBooking(serviceImageURL: String, bookedBarberShopName: String , bookingUniqueIDDD: String , bookingTotalTime: String , bookStartTSS: String , bookTzone: String , bookClandar: String , bookLocal: String ,shopUniqueID: String ){
        
        let claxandNow = DateByUserDeviceInitializer.calenderNow
        let tznxNow = DateByUserDeviceInitializer.tzone
        let loclxnNow = DateByUserDeviceInitializer.localCode
        
        let verifyBookingDataCurrent = VerifyDateDetails.checkDateData(timeZone: tznxNow, calendar: claxandNow, locale: loclxnNow)
        
        if verifyBookingDataCurrent == true {
            let currentRegionOfDeviceNow = DateByUserDeviceInitializer.getRegion(TZoneName: tznxNow, calenName: claxandNow, LocName: loclxnNow)
            let bookingUniqueRegionSP = DateByUserDeviceInitializer.getRegion(TZoneName: bookTzone, calenName: bookClandar, LocName: bookLocal)
            
            let appoint = AppointmentsCustomer()
            appoint.serviceImageUrl = serviceImageURL
            appoint.bookedBarberShopName = bookedBarberShopName
            appoint.bookingTotalTime = bookingTotalTime + "min"
            appoint.bookingUniqueID = bookingUniqueIDDD
            appoint.barberShopUniqueID = shopUniqueID
            
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string: serviceImageURL) {
                    do {
                        let data = try Data(contentsOf: url)
                        
                        if let downloadedImage = UIImage(data: data) {
                            appoint.serviceImage = downloadedImage
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    } catch {
                        appoint.serviceImage = UIImage()
                    }
                }
            }
            
            
            if let bookst = bookStartTSS.toDate(style: .extended, region: bookingUniqueRegionSP) {
                let dateData = bookst.convertTo(region: currentRegionOfDeviceNow)
                appoint.bookingStartTimeString = dateData.toString(DateToStringStyles.dateTime(.short))
                appoint.bookingStartTime = dateData
            } else {
                appoint.bookingStartTimeString = ""
            }
            
            if let uuiidBook = appoint.bookingUniqueID {
                if let firstNegative = self.appointmentsCustomer.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    if let indexValue = self.appointmentsCustomer.index(of: firstNegative) {
                        self.appointmentsCustomer[indexValue] = appoint
                    }
                    print("nano squad")
                } else {
                    self.appointmentsCustomer.append(appoint)
                }
            }
            
            self.appointmentsCustomer.sort(by: { (appstx, appsty) -> Bool in
                return appstx.bookingStartTime < appsty.bookingStartTime
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func setupViewObjectContriants(){
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
            collectionView.sendSubview(toBack: refresherController)
        }
    }

}
