//
//  Ratings.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/17/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Ratings: NSObject {
    var bookingID: String?
    var barberShopID: String?
    var barberID: String?
    var customerID: String?
    var barberRatingVale: String?
    var barberShopRatingValue: String?
    var barberRatingComment: String?
    var barberShopRatingComment: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
}
