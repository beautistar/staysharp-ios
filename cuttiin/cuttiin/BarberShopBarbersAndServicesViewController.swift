//
//  BarberShopBarbersAndServicesViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import MGStarRatingView
import SwiftDate

class BarberShopBarbersAndServicesViewController: UIViewController {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    
    var serviceOne = [Service]()
    var barberlistone = [Barber]()
    var serviceCategoryLadies = [Service]()
    var serviceCategoryGents = [Service]()
    var serviceCategoryKids = [Service]()
    var StringListOfBarbers = [String]()
    var selectedServiceOne = [String]()
    var selectedBarberOne = [String]()
    var ladiesSelectedList = [String]()
    var gentsSelectedList = [String]()
    var kidsSelectedList = [String]()
    var availableSelectedList = [String]()
    var topRatedSelectedList = [String]()
    var category = "Gents"
    var selectedLadiesGentsKidsButton = "Gents"
    var selectedAvailableTopRated = "Available"
    var selectedButtonBeforeSwitchingView = "Gents"
    var selectedButtonAfterSwitchingView = "Available"
    var openingTimeString: String?
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
        view.layer.borderWidth = 2
        return view
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "add")
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.clear
        return imageView
    }()
    
    lazy var addBarberAddServiceButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("addBarberButtonBarberShopAndServiceView", comment: "ADD BARBER"), for: .normal)
        st.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 35)
        st.contentHorizontalAlignment = .left
        st.addTarget(self, action: #selector(handleAddingBarber), for: .touchUpInside)
        return st
    }()
    
    lazy var serviceBarberSegmentedControl: UISegmentedControl = {
        let ocsegmentcontrol = UISegmentedControl(items: [NSLocalizedString("barberSegmentedButtonBarberShopAndServiceView", comment: "BARBERS"), NSLocalizedString("servicesSegmentedButtonBarberShopAndServiceView", comment: "SERVICES")])
        ocsegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        ocsegmentcontrol.tintColor = UIColor(r: 23, g: 69, b: 90)
        ocsegmentcontrol.selectedSegmentIndex = 0
        ocsegmentcontrol.addTarget(self, action: #selector(handleServiceBarberChange), for: .valueChanged)
        return ocsegmentcontrol
    }()
    
    let categoryButtonContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ladiesButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle("TOP RATED(0)", for: .normal)
        fbutton.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowAvailableFirst), for: .touchUpInside)
        return fbutton
    }()
    
    let ladiesButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        return fnsv
    }()
    
    lazy var gentsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("gentsTabCategoryBarberShopProfile", comment: "GENTS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowGentsList), for: .touchUpInside)
        fbutton.isHidden = true
        return fbutton
    }()
    
    let gentsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    lazy var kidsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle("AVAILABLE(0)", for: .normal)
        fbutton.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowTopRated), for: .touchUpInside)
        return fbutton
    }()
    
    let kidsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customBarbersBarberShopProfileCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdd")
        cv.register(customBarbersBarberShopProfileCollectionViewBarberSelectedCell.self, forCellWithReuseIdentifier: "cellIdd2")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("serviceAndBarbersButtonSettingsBSShopView", comment: "Barbers & Services")
        view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
        view.addSubview(barberShopHeaderDetailsContainerView)
        view.addSubview(serviceBarberSegmentedControl)
        view.addSubview(categoryButtonContainerView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewConstriants()
        getOpeningAndCLosingTimeForChoosenDate()
        getBarberShopServices()
        getThisBarberShopBarbers()
    }
    
    func getOpeningAndCLosingTimeForChoosenDate(){
        let currentDateGottenNow = DateInRegion()
        let clendarName = DateByUserDeviceInitializer.calenderNow
        let tZoneNow = DateByUserDeviceInitializer.tzone
        let modLocal = "en_US"
        let translatorRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tZoneNow, calenName: clendarName, LocName: modLocal)
        let newTransDateInDiffrentRegion = currentDateGottenNow.convertTo(region: translatorRegion)
        let daySelect = newTransDateInDiffrentRegion.weekdayName(.default)
        
        let firebaseRefere = Database.database().reference()
        if let userID = Auth.auth().currentUser?.uid {
            firebaseRefere.child("workhours").child(userID).child(daySelect).observeSingleEvent(of: .value, with: { (snapshooottss) in
                if let dictionary = snapshooottss.value as? [String: AnyObject], HandleDataRequest.handleWorkhoursNode(firebaseData: dictionary) == true {
                    let currentDayWorkHour = Workhours()
                    currentDayWorkHour.setValuesForKeys(dictionary)
                    
                    if let openTimeHour = currentDayWorkHour.openingTimeHour, let openTimeMinute = currentDayWorkHour.openingTimeMinute, let openTimeSecond = currentDayWorkHour.openingTimeSecond {
                        
                        guard let openHour = Int(openTimeHour), let openMinute = Int(openTimeMinute), let openSecond = Int(openTimeSecond) else {
                            print("guard statement failed in time conversion to Int")
                            return
                        }
                        
                        let correctDateGotten = DateInRegion()
                        
                        
                        if let startTimeDate = correctDateGotten.dateBySet(hour: openHour, min: openMinute, secs: openSecond) {
                            self.openingTimeString = startTimeDate.toString(DateToStringStyles.dateTime(.medium))
                        }
                        
                    }
                }else {
                    print("Day work hours not gotten")
                }
            }, withCancel: nil)
        }
    }
    
    func getBarberShopServices(){
        let firebaseRefere = Database.database().reference()
        if let userID = Auth.auth().currentUser?.uid {
            firebaseRefere.child("service").child(userID).observeSingleEvent(of: .value, with: { (snapshotteerree) in
                
                if let dictionary = snapshotteerree.value as? [String: AnyObject] {
                    for servicDIC in dictionary {
                        if let serveee = servicDIC.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: serveee) == true {
                            
                            let serdic = Service()
                            serdic.setValuesForKeys(serveee)
                            
                            if let cate = serdic.category, let serviceCheck = serdic.isDeleted {
                                if serviceCheck == "NO" {
                                    switch cate {
                                    case "Ladies":
                                        if self.serviceCategoryLadies.contains(serdic){
                                            print("duplicate")
                                        }else {
                                            DispatchQueue.global(qos: .background).async {
                                                if let url = URL(string: serdic.serviceImageUrl!) {
                                                    do {
                                                        let data = try Data(contentsOf: url)
                                                        
                                                        if let downloadedImage = UIImage(data: data) {
                                                            serdic.serviceImage = downloadedImage
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                        }
                                                    } catch {
                                                        print("profile picture not available")
                                                    }
                                                }
                                            }
                                            self.serviceCategoryLadies.append(serdic)
                                        }
                                    case "Gents":
                                        if self.serviceCategoryGents.contains(serdic) && self.serviceOne.contains(serdic){
                                            print("duplicate")
                                        }else {
                                            DispatchQueue.global(qos: .background).async {
                                                if let url = URL(string: serdic.serviceImageUrl!) {
                                                    do {
                                                        let data = try Data(contentsOf: url)
                                                        
                                                        if let downloadedImage = UIImage(data: data) {
                                                            serdic.serviceImage = downloadedImage
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                        }
                                                    } catch {
                                                        print("profile picture not available")
                                                    }
                                                }
                                            }
                                            self.serviceCategoryGents.append(serdic)
                                            self.serviceOne.append(serdic)
                                        }
                                    case "Kids":
                                        if self.serviceCategoryKids.contains(serdic){
                                            print("duplicate")
                                        }else {
                                            DispatchQueue.global(qos: .background).async {
                                                if let url = URL(string: serdic.serviceImageUrl!) {
                                                    do {
                                                        let data = try Data(contentsOf: url)
                                                        
                                                        if let downloadedImage = UIImage(data: data) {
                                                            serdic.serviceImage = downloadedImage
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                        }
                                                    } catch {
                                                        print("profile picture not available")
                                                    }
                                                }
                                            }
                                            self.serviceCategoryKids.append(serdic)
                                        }
                                    default:
                                        print("Sky high")
                                    }
                                }
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getThisBarberShopBarbers(){
        let firebaseRef23 = Database.database().reference()
        if let userID = Auth.auth().currentUser?.uid {
            firebaseRef23.child("barberShop-Barbers").child(userID).observeSingleEvent(of: .value, with: { (snapshootthhss) in
                if let dictionary = snapshootthhss.value as? [String: AnyObject] {
                    
                    for barberDic in dictionary {
                        if let barberSingle = barberDic.value as? [String: AnyObject], HandleDataRequest.handleBarberShopBarbersNode(firebaseData: barberSingle) == true {
                            let barbershopbarbersss = BarberShopBarbers()
                            barbershopbarbersss.setValuesForKeys(barberSingle)
                            guard let staffID = barbershopbarbersss.barberStaffID else {
                                print("error occured")
                                return
                            }
                            self.getListOfBarbersImmediate(barberID: staffID)
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getListOfBarbersImmediate(barberID: String){
        let firebaseRefxll = Database.database().reference()
        firebaseRefxll.child("users").child(barberID).observeSingleEvent(of: .value, with: { (snapshooottt) in
            if let dictionary = snapshooottt.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionary) == true {
                let barbersingle = Barber()
                barbersingle.setValuesForKeys(dictionary)
                
                self.StringListOfBarbers.append(barberID)
                if self.barberlistone.contains(barbersingle) {
                    print("Duplicate value")
                }else {
                    
                    DispatchQueue.global(qos: .background).async {
                        if let url = URL(string: barbersingle.profileImageUrl!) {
                            do {
                                let data = try Data(contentsOf: url)
                                
                                if let downloadedImage = UIImage(data: data) {
                                    barbersingle.profileImage = downloadedImage
                                    DispatchQueue.main.async {
                                        self.collectionView.reloadData()
                                    }
                                }
                            } catch {
                                print("profile picture not available")
                            }
                        }
                    }
                    
                    if let ratingValue = barbersingle.rating, let ratingInt = Int(ratingValue) {
                        barbersingle.ratingValue = CGFloat(integerLiteral: ratingInt)
                    }else {
                        barbersingle.ratingValue = CGFloat(integerLiteral: 0)
                    }
                    
                    self.barberlistone.append(barbersingle)
                    
                    if self.barberlistone.count > 0{
                        self.barberlistone.sort(by: { (appstx, appsty) -> Bool in
                            return (appstx.ratingValue?.isLessThanOrEqualTo(appsty.ratingValue!))!
                        })
                    }
                    self.ladiesButton.setTitle("TOP RATED (\(self.barberlistone.count))", for: .normal)
                    self.kidsButton.setTitle("AVAILABLE (\(self.barberlistone.count))", for: .normal)
                }
            }
        }, withCancel: nil)
    }
    
    func specifiedBarberFromList(){
        let firebaseRefxxx = Database.database().reference()
        if self.StringListOfBarbers.count > 0 {
            for lis in self.StringListOfBarbers {
                firebaseRefxxx.child("users").child(lis).observeSingleEvent(of: .value, with: { (snapshooottt) in
                    if let dictionary = snapshooottt.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionary) == true {
                        let barbersingle = Barber()
                        barbersingle.setValuesForKeys(dictionary)
                        if self.barberlistone.contains(barbersingle) {
                            print("Duplicate value")
                        }else {
                            self.barberlistone.append(barbersingle)
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }else {
                        print("Data did not get back")
                    }
                }, withCancel: nil)
            }
        }
        
    }
    
    @objc func handleDismissView(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleAddingBarber(){
        print("Add Barber")
        let addbarber = AddBarberViewController()
        addbarber.notRegisteringANewBarberShop = false
        addbarber.fromSelection = false
        let navController = UINavigationController(rootViewController: addbarber)
        present(navController, animated: true, completion: nil)
    }
    
    @objc func handleAddingService(){
        print("Add service")
        let addservice = AddServiceViewController()
        addservice.notRegisteringANewBarberShop = false
        addservice.notEditingNotRegistering = false
        let navController = UINavigationController(rootViewController: addservice)
        present(navController, animated: true, completion: nil)
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
        print(value)
    }
    
    @objc func handleServiceBarberChange(){
        self.selectedButtonBeforeSwitchingView = self.selectedLadiesGentsKidsButton
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            self.addBarberAddServiceButton.removeTarget(nil, action: nil, for: .allEvents)
            self.addBarberAddServiceButton.setTitle(NSLocalizedString("addServiceButtonBarberShopAndServiceView", comment: "ADD SERVICE"), for: .normal)
            self.addBarberAddServiceButton.addTarget(self, action: #selector(handleAddingService), for: .touchUpInside)
            self.selectedBarberOne.removeAll()
            self.availableSelectedList.removeAll()
            self.topRatedSelectedList.removeAll()
            
            self.categoryButtonContainerView.isHidden = false
            self.categoryButtonContainerViewHeightAnchor?.isActive = false
            self.categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 40)
            self.categoryButtonContainerViewHeightAnchor?.isActive = true
            
            self.selectedLadiesGentsKidsButton = self.selectedButtonBeforeSwitchingView
            
            self.ladiesButton.isHidden = false
            self.ladiesButton.removeTarget(nil, action: nil, for: .allEvents)
            self.ladiesButton.setTitle(NSLocalizedString("ladiesCartegoryTabBarberShopProfileView", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))", for: .normal)
            self.ladiesButton.addTarget(self, action: #selector(handleShowLadiesList), for: .touchUpInside)
            
            self.gentsButton.isHidden = false
            self.gentsButton.setTitle(NSLocalizedString("gentsCartegoryTabBarberShopProfileView", comment: "GENTS") + "(\(self.serviceCategoryGents.count))", for: .normal)
            
            self.kidsButton.isHidden = false
            self.kidsButton.removeTarget(nil, action: nil, for: .allEvents)
            self.kidsButton.setTitle(NSLocalizedString("kidsCartegoryTabBarberShopProfileView", comment: "KIDS") + "(\(self.serviceCategoryKids.count))", for: .normal)
            self.kidsButton.addTarget(self, action: #selector(handleShowKidsList), for: .touchUpInside)
            //self.collectionView.allowsMultipleSelection = true
            self.changeButtonCounter()
            
        }else {
            
            self.addBarberAddServiceButton.removeTarget(nil, action: nil, for: .allEvents)
            self.addBarberAddServiceButton.setTitle(NSLocalizedString("addBarberButtonBarberShopAndServiceView", comment: "ADD BARBER"), for: .normal)
            self.addBarberAddServiceButton.addTarget(self, action: #selector(handleAddingBarber), for: .touchUpInside)
            
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            
            self.categoryButtonContainerView.isHidden = true
            self.categoryButtonContainerViewHeightAnchor?.isActive = false
            self.categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 0)
            self.categoryButtonContainerViewHeightAnchor?.isActive = true
            
            self.ladiesButton.removeTarget(nil, action: nil, for: .allEvents)
            self.ladiesButton.setTitle("TOP RATED (\(self.barberlistone.count))", for: .normal)
            self.ladiesButton.addTarget(self, action: #selector(handleShowAvailableFirst), for: .touchUpInside)
            
            self.gentsButton.isHidden = true
            
            self.kidsButton.removeTarget(nil, action: nil, for: .allEvents)
            self.kidsButton.setTitle("AVAILABLE (\(self.barberlistone.count))", for: .normal)
            self.kidsButton.addTarget(self, action: #selector(handleShowTopRated), for: .touchUpInside)
            self.changeButtonCounterBarber()
            
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    @objc func handleShowAvailableFirst(){
        print(self.availableSelectedList)
        print(self.barberlistone.count)
        self.selectedAvailableTopRated = "Available"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.white
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.changeButtonCounterBarber()
    }
    
    @objc func handleShowTopRated(){
        print(topRatedSelectedList)
        print(self.barberlistone.count)
        self.selectedAvailableTopRated = "Top Rated"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.white
        self.changeButtonCounterBarber()
    }
    
    @objc func handleShowLadiesList(){
        print(serviceCategoryLadies)
        self.selectedLadiesGentsKidsButton = "Ladies"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        if self.serviceCategoryLadies.count > 0 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            for serv in self.serviceCategoryLadies {
                if serv.category == "Ladies" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    @objc func handleShowGentsList(){
        self.selectedLadiesGentsKidsButton = "Gents"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        if self.serviceCategoryGents.count > 0 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            for serv in self.serviceCategoryGents {
                if serv.category == "Gents" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }
    
    @objc func handleShowKidsList(){
        self.selectedLadiesGentsKidsButton = "Kids"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        if self.serviceCategoryKids.count > 0 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            for serv in self.serviceCategoryKids {
                if serv.category == "Kids" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }
    
    func changeButtonCounter(){
        print(self.selectedLadiesGentsKidsButton)
        switch self.selectedLadiesGentsKidsButton {
        case "Ladies":
            let buttonTitle = NSLocalizedString("ladiesCartegoryTabBarberShopProfileView", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))"
            self.ladiesButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Ladies"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
            
        case "Gents":
            let buttonTitle = NSLocalizedString("gentsCartegoryTabBarberShopProfileView", comment: "GENTS") + "(\(self.serviceCategoryGents.count))"
            self.gentsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Gents"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        case "Kids":
            let buttonTitle = NSLocalizedString("kidsCartegoryTabBarberShopProfileView", comment: "KIDS") + "(\(self.serviceCategoryKids.count))"
            self.kidsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Kids"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        default:
            print("Sky high")
        }
    }
    
    func changeButtonCounterBarber(){
        switch self.selectedAvailableTopRated {
        case "Available":
            let buttonTitle = "TOP RATED (\(self.barberlistone.count))"
            self.ladiesButton.setTitle(buttonTitle, for: .normal)
            self.selectedAvailableTopRated = "Available"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
            print(self.barberlistone.count)
        case "Top Rated":
            let buttonTitle = "AVAILABLE (\(self.barberlistone.count))"
            self.kidsButton.setTitle(buttonTitle, for: .normal)
            self.selectedAvailableTopRated = "Top Rated"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
            print(self.barberlistone.count)
        default:
            print("Sky high")
        }
    }
    
    func handleMoveToTheNextView(){
        if self.ladiesSelectedList.count > 0 || self.gentsSelectedList.count > 0 || self.kidsSelectedList.count > 0 {
            if let uid = Auth.auth().currentUser?.uid, let selServiceID = self.selectedServiceOne.first {
                let addserviceEdit = AddServiceViewController()
                addserviceEdit.barberShopID = uid
                addserviceEdit.noteditingService = false
                addserviceEdit.serviceID = selServiceID
                let navController = UINavigationController(rootViewController: addserviceEdit)
                present(navController, animated: true, completion: nil)
            }
        }
    }
    
    func handleBarberMoveToNextView(barberUniqueID: String){
        let addBarber = AddBarberViewController()
        addBarber.notAddingNewBarber = false
        addBarber.selectedBarberID = barberUniqueID
        let navController = UINavigationController(rootViewController: addBarber)
        present(navController, animated: true, completion: nil)
    }
    
    var categoryButtonContainerViewHeightAnchor: NSLayoutConstraint?
    
    func setupViewConstriants(){
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(selectImageView)
        barberShopHeaderDetailsContainerView.addSubview(addBarberAddServiceButton)
        
        selectImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor, constant: 5).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor, multiplier: 0.25, constant: -20).isActive = true
        selectImageView.bottomAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.bottomAnchor, constant: -5).isActive = true
        
        addBarberAddServiceButton.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor, constant: 5).isActive = true
        addBarberAddServiceButton.leftAnchor.constraint(equalTo: selectImageView.rightAnchor).isActive = true
        addBarberAddServiceButton.rightAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.rightAnchor, constant: 5).isActive = true
        addBarberAddServiceButton.bottomAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.bottomAnchor, constant: -5).isActive = true
        
        serviceBarberSegmentedControl.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.bottomAnchor, constant: 10).isActive = true
        serviceBarberSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        serviceBarberSegmentedControl.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        serviceBarberSegmentedControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        categoryButtonContainerView.topAnchor.constraint(equalTo: serviceBarberSegmentedControl.bottomAnchor, constant: 10).isActive = true
        categoryButtonContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        categoryButtonContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 0)
        categoryButtonContainerViewHeightAnchor?.isActive = true
        
        categoryButtonContainerView.addSubview(ladiesButton)
        categoryButtonContainerView.addSubview(ladiesButtonSeperatorView)
        categoryButtonContainerView.addSubview(gentsButton)
        categoryButtonContainerView.addSubview(gentsButtonSeperatorView)
        categoryButtonContainerView.addSubview(kidsButton)
        categoryButtonContainerView.addSubview(kidsButtonSeperatorView)
        
        ladiesButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        ladiesButton.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        ladiesButtonSeperatorView.topAnchor.constraint(equalTo: ladiesButton.bottomAnchor).isActive = true
        ladiesButtonSeperatorView.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        kidsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        kidsButton.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        kidsButtonSeperatorView.topAnchor.constraint(equalTo: kidsButton.bottomAnchor).isActive = true
        kidsButtonSeperatorView.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        gentsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        gentsButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButton.leftAnchor.constraint(equalTo: ladiesButton.rightAnchor).isActive = true
        gentsButton.rightAnchor.constraint(equalTo: kidsButton.leftAnchor).isActive = true
        gentsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        gentsButtonSeperatorView.topAnchor.constraint(equalTo: gentsButton.bottomAnchor).isActive = true
        gentsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButtonSeperatorView.leftAnchor.constraint(equalTo: ladiesButtonSeperatorView.rightAnchor).isActive = true
        gentsButtonSeperatorView.rightAnchor.constraint(equalTo: kidsButtonSeperatorView.leftAnchor).isActive = true
        gentsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        self.categoryButtonContainerView.isHidden = true
        
        collectView.topAnchor.constraint(equalTo: categoryButtonContainerView.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -8).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
    }
}

class customBarbersBarberShopProfileCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "babershop0")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    let textBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    let barberShopServiceLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopServiceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let barberShopServiceDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceTimeTakenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 11)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 11)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    func setupViews(){
        print("yellow")
        addSubview(barberShopCoverImageView)
        addSubview(textBackgroundView)
        addSubview(seperatorView)
        addSubview(barberShopServiceLogoImageView)
        addSubview(barberShopServiceNamePlaceHolder)
        addSubview(barberShopServiceDescriptionPlaceHolder)
        addSubview(serviceTimeTakenPlaceHolder)
        addSubview(servicePricePlaceHolder)
        addSubview(thumbnailImageView)
        thumbnailImageView.image = UIImage()
        
        addContraintsWithFormat(format: "H:|[v0]|", views: barberShopCoverImageView)
        addContraintsWithFormat(format: "V:|[v0][v1(5)]|", views: barberShopCoverImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: textBackgroundView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: textBackgroundView)
        addContraintsWithFormat(format: "H:|-30-[v0(100)]|", views: barberShopServiceLogoImageView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: barberShopServiceLogoImageView)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopServiceNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0(20)][v1]-30-|", views: barberShopServiceNamePlaceHolder,barberShopServiceDescriptionPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopServiceDescriptionPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0(50)][v1(50)]-10-[v2(20)]-5-|", views: serviceTimeTakenPlaceHolder,servicePricePlaceHolder,thumbnailImageView)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: serviceTimeTakenPlaceHolder)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: servicePricePlaceHolder)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: thumbnailImageView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customBarbersBarberShopProfileCollectionViewBarberSelectedCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopBarberCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "babershop0")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    let textBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    let barberShopBarberLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopBarberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.text = "Fade Cut"
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let barberAvailableFromPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("availableFromBarberShopProfile", comment: "Available from")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let barberTimeToBeAvailablePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.text = "time things"
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 15,spacing: 5, emptyColor: .black, fillColor: .white)
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    func setupViews(){
        addSubview(barberShopBarberCoverImageView)
        addSubview(textBackgroundView)
        addSubview(seperatorView)
        addSubview(barberShopBarberLogoImageView)
        addSubview(barberShopBarberNamePlaceHolder)
        addSubview(barberAvailableFromPlaceHolder)
        addSubview(barberTimeToBeAvailablePlaceHolder)
        starView.configure(attribute, current: 3, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        addSubview(starView)
        addSubview(thumbnailImageView)
        
        addContraintsWithFormat(format: "H:|[v0]|", views: barberShopBarberCoverImageView)
        addContraintsWithFormat(format: "V:|[v0][v1(5)]|", views: barberShopBarberCoverImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: textBackgroundView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: textBackgroundView)
        addContraintsWithFormat(format: "H:|-30-[v0(100)]|", views: barberShopBarberLogoImageView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: barberShopBarberLogoImageView)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopBarberNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberAvailableFromPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberTimeToBeAvailablePlaceHolder)
        addContraintsWithFormat(format: "H:|-170-[v0]-5-[v1(20)]-5-|", views: starView, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-5-[v0(30)][v1(20)][v2(20)][v3(20)]-5-|", views: barberShopBarberNamePlaceHolder,barberAvailableFromPlaceHolder,barberTimeToBeAvailablePlaceHolder,starView)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: thumbnailImageView)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


















