//
//  BarberShopsViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MGStarRatingView
import Firebase
import UserNotifications
import FirebaseMessaging
import SwiftDate
import FBSDKLoginKit

class BarberShopsViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UITabBarControllerDelegate {
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var locationManager = CLLocationManager()
    var cordinatesPlaces: CLLocationCoordinate2D?
    var mapView = GMSMapView()
    let firebaseRef = Database.database().reference()
    var bMarkers = [GMSMarker]()
    var barbermarkers = [BarberMarker]()
    var searchedLocMarker = [GMSMarker]()
    let mySpecialNotificationKey = "com.teckdkapps.specialNotificationKey"
    var modalViewCOntrollerHolderForView = ModalViewController()
    
    let searchBarContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor(r: 11, g: 49, b: 68).cgColor
        view.layer.borderWidth = 1
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        return view
    }()
    
    lazy var searchBarSearchImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "search")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowPlaceAutoComplete)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var searchBarTextInputPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.text = NSLocalizedString("searchBarTextBarberShopsView", comment: "Search")
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowPlaceAutoComplete)))
        return fnhp
    }()
    
    lazy var searchBarGPSLocationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "gps_on")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowCurrentLocation)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var searchBarShowNearbyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "view_list")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowNearbyShops)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    
    var isCurrentLocationSet = false
    var dictionaryTotalCount = 0
    var calledCount = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.isCurrentLocationSet = false
            case .authorizedAlways, .authorizedWhenInUse:
                self.isCurrentLocationSet = true
            }
        } else {
            self.isCurrentLocationSet = false
        }
        
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.white
        mapView = GMSMapView(frame: self.view.bounds)
        mapView.delegate = self
        mapView.accessibilityElementsHidden = false
        mapView.settings.myLocationButton = false
        mapView.isMyLocationEnabled = true
        mapView.settings.scrollGestures = true
        mapView.settings.zoomGestures = true
        let mapInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 60.0, right: 0.0)
        mapView.padding = mapInsets
        view.addSubview(mapView)
        view.addSubview(searchBarContainerView)
        view.bringSubview(toFront: searchBarContainerView)
        setupObjectContraints()
        //location manager to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        self.getAllMarkers()
         self.tabBarController?.delegate = self
        UserDefaults.standard.set(false, forKey: "theBookingProcessIsComplete")
        postUserSpecialToken()
        NotificationCenter.default.addObserver(self, selector: #selector(updateTabBarBadgeNumbers), name: NSNotification.Name(rawValue: mySpecialNotificationKey), object: nil)
        //getBookingWithNoTimes()
    }
    
    func getBookingWithNoTimes(){
        print("red bubles")
        DispatchQueue.global(qos: .background).async {
            
            let firebaseGetNotimeBooking = Database.database().reference()
            firebaseGetNotimeBooking.child("bookings").child("HNLmAey6MhNcr51NmZtxJ7lmkLq1").observeSingleEvent(of: .value) { (snapshotDataMissing) in
                if let dictionaryDataMising = snapshotDataMissing.value as? [String: AnyObject] {
                    for singleMissingData in dictionaryDataMising {
                        if let singleMadeDataPoint = singleMissingData.value as? [String: AnyObject] {
                            
                            let bookingConfirm = HandleDataRequest.handleBookingsNode(firebaseData: singleMadeDataPoint)
                            
                            if bookingConfirm {
                                let bookingHolderSaint = Bookings()
                                bookingHolderSaint.setValuesForKeys(singleMadeDataPoint)
                                
                                if let bookingID = bookingHolderSaint.bookingID, let dateCreatedBook = bookingHolderSaint.dateCreated, let timeZone = bookingHolderSaint.timezone, let localTT = bookingHolderSaint.local, let calendxx = bookingHolderSaint.calendar, let bookingStartTime = bookingHolderSaint.bookingStartTime, let payIDDX = bookingHolderSaint.paymentID, let isComPLXX = bookingHolderSaint.isCompleted, let customerID = bookingHolderSaint.customerID {
                                    let bookingRegion = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: calendxx, LocName: localTT)
                                    
                                    let verifyDateData = VerifyDateDetails.checkDateData(timeZone: timeZone, calendar: calendxx, locale: localTT)
                                    let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: timeZone, calendar: calendxx, locale: localTT, dateData: bookingStartTime)
                                    
                                    if verifyDateData == true && verifyBookingStartDate == true {
                                        if let dateCreatedSoap = dateCreatedBook.toDate(style: .extended, region: bookingRegion) {
                                            let currentDateChosen = DateInRegion()
                                            
                                            //.isInSameDayOf(date: currentDateChosen)
                                            
                                            if dateCreatedSoap.isInside(date: currentDateChosen, granularity: .day) && bookingStartTime == "not available" && payIDDX != "not available" && isComPLXX == "YES" && payIDDX != "nil" && payIDDX != "nil" {
                                                print("bookingID: ",bookingID, "customerID: ", customerID)
                                                let firebaseUserInfoRequest = Database.database().reference()
                                                firebaseUserInfoRequest.child("users").child(customerID).observeSingleEvent(of: .value, with: { (snapshotototototototototototot) in
                                                    if let dictionaryUserDataHold = snapshotototototototototototot.value as? [String: AnyObject] {
                                                        
                                                        let userConfirm = HandleDataRequest.handleUserCustomerNode(firebaseData: dictionaryUserDataHold)
                                                        
                                                        if userConfirm {
                                                            let singleUser = Customer()
                                                            singleUser.setValuesForKeys(dictionaryUserDataHold)
                                                            print("user detail")
                                                            
                                                            print("bookingID: ",bookingID, "customerID: ", customerID)
                                                            
                                                            if let firstName = singleUser.firstName {
                                                                print("First name: ", firstName)
                                                            }
                                                            
                                                            if let lastName = singleUser.lastName {
                                                                print("Last name: ", lastName)
                                                            }
                                                        }
                                                        
                                                    }
                                                })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getEmail(){
        let firebaseReferenceEmail = Database.database().reference()
        firebaseReferenceEmail.child("users").observeSingleEvent(of: .value, with: { (snapshotdatathold) in
            if let dictionary = snapshotdatathold.value as? [String: AnyObject] {
                for userEmailSingle in dictionary {
                    if let useremailOnce = userEmailSingle.value as? [String: AnyObject] {
                        
                        if let onceEmail = useremailOnce["email"] as? String, let uniqueID = useremailOnce["uniqueID"] as? String {
                            print(onceEmail, "unique ID: ", uniqueID)
                        }
                    }
                }
            }
        })
    }
    
    func preventWrongLocal(){
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        switch userCalender {
        case "gregorian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
            
        case "buddhist":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "chinese":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "coptic":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "ethiopicAmeteMihret":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "hebrew":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "iso8601":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "indian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamic":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamicCivil":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "japanese":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "persian":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "republicOfChina":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamicTabular":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        case "islamicUmmAlQura":
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        default:
            print("Sky High")
            if let _ = Locales(rawValue: userLocal), let _ = Zones(rawValue: userTimezone) {
                print("good local and time zone")
            } else {
                print("red zone part 2")
                self.handleWrongLocalAndDate()
            }
        }
    }
    
    func handleWrongLocalAndDate(){
        print("get kicked out")
        
        UserDefaults.standard.set("wrongLocal", forKey: "localeForThisDevice")
        
        FBSDKLoginManager().logOut()
        do{
            try Auth.auth().signOut()
            self.tabBarController?.selectedIndex = 1
            let welcomeviewcontroller = WelcomeViewController()
            self.present(welcomeviewcontroller, animated: true, completion: nil)
            welcomeviewcontroller.handleWrongLocalAndDate()
            
        }catch let logoutError {
            print(logoutError, "red dealings")
            
            self.activityIndicator.removeFromSuperview()
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()

        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            //do your stuff
            tabBarController.tabBar.items?[0].badgeValue = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    @objc func updateTabBarBadgeNumbers(){
        tabBarController?.tabBar.items?[0].badgeValue = "\(UIApplication.shared.applicationIconBadgeNumber)"
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        UserDefaults.standard.set(false, forKey: "theBookingProcessIsComplete")

        self.getAllMarkers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//
//        if let index = self.bMarkers.index(of: marker) {
//            let modalViewController = ModalViewController()
//            modalViewController.indexMarker = index
//            modalViewController.barbermarkerrrr = self.barbermarkers
//            modalViewController.modalPresentationStyle = .overCurrentContext
//            present(modalViewController, animated: true, completion: nil)
//
//        }
//
//        return true
//    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let index = self.bMarkers.index(of: marker) {
            let modalViewController = ModalViewController()
            modalViewController.indexMarker = index
            modalViewController.barbermarkerrrr = self.barbermarkers
            modalViewController.modalPresentationStyle = .overCurrentContext
            present(modalViewController, animated: false, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.isCurrentLocationSet {
            let location = locations.last
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude:(location?.coordinate.longitude)!, zoom:15)
            mapView.animate(to: camera)
            
            self.isCurrentLocationSet = false
            //        self.locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
            self.isCurrentLocationSet = false
            self.locationManager.requestAlwaysAuthorization()
            
        case .denied:
            print("User denied access to location.")
            self.isCurrentLocationSet = false
            self.locationManager.requestAlwaysAuthorization()
            
        case .notDetermined:
            self.isCurrentLocationSet = false
            print("Location access notDetermined.")
            self.locationManager.requestAlwaysAuthorization()
            
        case .authorizedAlways: fallthrough
            
        case .authorizedWhenInUse:
            self.isCurrentLocationSet = true
            print("Location status is OK.")
        }
    }
    
    func desiredLocationMarker(loc: CLLocationCoordinate2D){
        DispatchQueue.main.async {
            let desiredSearchMarker = GMSMarker(position: loc)
            desiredSearchMarker.icon = GMSMarker.markerImage(with: UIColor(r: 230, g: 76, b: 78))
            desiredSearchMarker.map = self.mapView
            self.searchedLocMarker.append(desiredSearchMarker)
        }
    }
    
    func removeSearchedLocMarker(){
        for  vMarker in self.searchedLocMarker {
            if let index = searchedLocMarker.index(of: vMarker) {
                let serchMark = searchedLocMarker[index]
                serchMark.map = nil
                searchedLocMarker = searchedLocMarker.filter { $0 != serchMark }
            }
            
        }
    }
    
    @objc func handleShowPlaceAutoComplete(){
        self.removeSearchedLocMarker()
        
        if let countryCodeForMapFilter = Locale.current.regionCode {
            let autocompletecontroller = GMSAutocompleteViewController()
            autocompletecontroller.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .establishment  //suitable filter type
            filter.country = countryCodeForMapFilter
            
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.autocompleteFilter = filter
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
            
        } else {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
        
    }
    
    @objc func handleShowCurrentLocation(){
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 15)
        self.mapView.animate(to: camera)
    }
    
    @objc func handleShowNearbyShops(){
        let nearbyview = NearByBarberShopViewController()
        navigationController?.pushViewController(nearbyview, animated: true)
    }
    
    func getAllMarkers(){
        DispatchQueue.global(qos: .background).async {
            self.barbermarkers.removeAll()
            let firebaseReferenceVerificationCheck = Database.database().reference()
            firebaseReferenceVerificationCheck.child("barberShopVerification").observeSingleEvent(of: .value, with: { (snapshototoAllData) in
                if let dictionaryVer = snapshototoAllData.value as? [String: AnyObject] {
                    self.barbermarkers.removeAll()
                    self.bMarkers.removeAll()
                    DispatchQueue.main.async {
                        self.mapView.clear()
                    }
                    for dataHold in dictionaryVer {
                        if let verificDataHold = dataHold.value as? [String: AnyObject] {
                            
                            let barberShopVerificationConfirm = HandleDataRequest.handleBarberShopVerificationNode(firebaseData: verificDataHold)
                            
                            if barberShopVerificationConfirm {
                                let shopCheckHold = BarberShopVerification()
                                shopCheckHold.setValuesForKeys(verificDataHold)
                                
                                
                                if let verificationChoice = shopCheckHold.isBarberShopVerified, let shopIDD = shopCheckHold.barberShopID {
                                    if verificationChoice == "YES" {
                                        let firebaseReferenceBarberHop = Database.database().reference()
                                        firebaseReferenceBarberHop.child("users").child(shopIDD).observeSingleEvent(of: .value, with: { (snapshotototData) in
                                            if let userDataDictionary = snapshotototData.value as? [String: AnyObject] {
                                                
                                                let userBarbershopConfirm = HandleDataRequest.handleUserBarberShop(firebaseData: userDataDictionary)
                                                
                                                if userBarbershopConfirm {
                                                    let userSingleData = BarberShop()
                                                    userSingleData.setValuesForKeys(userDataDictionary)
                                                    
                                                    if let lon = userSingleData.companyAddressLongitude, let lat = userSingleData.companyAddressLatitude, let formAddress = userSingleData.companyFormattedAddress, let imageURL = userSingleData.companyLogoImageUrl, let cmpName = userSingleData.companyName, let timezoneHold = userSingleData.timezone  {
                                                        let markerB = BarberMarker()
                                                        
                                                        
                                                        let latitude = (lat as NSString).doubleValue
                                                        let longitude = (lon as NSString).doubleValue
                                                        markerB.companyID = shopIDD
                                                        markerB.companyName = cmpName
                                                        markerB.companyLogoImageUrl = imageURL
                                                        markerB.companyFormattedAddress = formAddress
                                                        markerB.companyAddressLongitude = lon
                                                        markerB.companyAddressLatitude = lat
                                                        markerB.timezone = timezoneHold
                                                        
                                                        
                                                        if let ratingValue = userSingleData.rating, let ratingInt = Int(ratingValue) {
                                                            markerB.ratingValue = CGFloat(integerLiteral: ratingInt)
                                                        } else {
                                                            markerB.ratingValue = CGFloat(integerLiteral: 0)
                                                        }
                                                        
                                                        if let _ = self.barbermarkers.first(where: { $0.companyID ==  shopIDD }) {
                                                            print("runaway man")
                                                        } else {
                                                            print("kimonos redede")
                                                            self.barbermarkers.append(markerB)
                                                            let locValue:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
                                                            DispatchQueue.main.async{
                                                                let desiredMarker = GMSMarker(position: locValue)
                                                                desiredMarker.icon = GMSMarker.markerImage(with: UIColor(r: 118, g: 187, b: 220))
                                                                desiredMarker.title = cmpName
                                                                desiredMarker.snippet = formAddress
                                                                desiredMarker.map = self.mapView
                                                                self.bMarkers.append(desiredMarker)
                                                            }
                                                            
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                        
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                }
            })
        }
    }
    
    func postUserSpecialToken(){
        if let uid = Auth.auth().currentUser?.uid, let fcmtokenkey = Messaging.messaging().fcmToken {
            let firebaseRefFCMToken = Database.database().reference()
            firebaseRefFCMToken.child("fcmtoken").child(uid).child("key").setValue(fcmtokenkey)
        }
    }
    
    func setupObjectContraints(){
        searchBarContainerView.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 25).isActive = true
        searchBarContainerView.centerXAnchor.constraint(equalTo: mapView.centerXAnchor).isActive = true
        searchBarContainerView.widthAnchor.constraint(equalTo: mapView.widthAnchor, constant: -48).isActive = true
        searchBarContainerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        searchBarContainerView.addSubview(searchBarSearchImageView)
        searchBarContainerView.addSubview(searchBarGPSLocationImageView)
        searchBarContainerView.addSubview(searchBarShowNearbyImageView)
        searchBarContainerView.addSubview(searchBarTextInputPlaceHolder)
        
        searchBarSearchImageView.topAnchor.constraint(equalTo: searchBarContainerView.topAnchor).isActive = true
        searchBarSearchImageView.leftAnchor.constraint(equalTo: searchBarContainerView.leftAnchor).isActive = true
        searchBarSearchImageView.widthAnchor.constraint(equalTo: searchBarContainerView.widthAnchor, multiplier: 0.25, constant: -40).isActive = true
        searchBarSearchImageView.heightAnchor.constraint(equalTo: searchBarContainerView.heightAnchor).isActive = true
        
        searchBarShowNearbyImageView.topAnchor.constraint(equalTo: searchBarContainerView.topAnchor).isActive = true
        searchBarShowNearbyImageView.rightAnchor.constraint(equalTo: searchBarContainerView.rightAnchor).isActive = true
        searchBarShowNearbyImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        searchBarShowNearbyImageView.heightAnchor.constraint(equalTo: searchBarContainerView.heightAnchor).isActive = true
        
        searchBarGPSLocationImageView.topAnchor.constraint(equalTo: searchBarContainerView.topAnchor).isActive = true
        searchBarGPSLocationImageView.rightAnchor.constraint(equalTo: searchBarShowNearbyImageView.leftAnchor).isActive = true
        searchBarGPSLocationImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        searchBarGPSLocationImageView.heightAnchor.constraint(equalTo: searchBarContainerView.heightAnchor).isActive = true
        
        searchBarTextInputPlaceHolder.topAnchor.constraint(equalTo: searchBarContainerView.topAnchor).isActive = true
        searchBarTextInputPlaceHolder.leftAnchor.constraint(equalTo: searchBarSearchImageView.rightAnchor).isActive = true
        searchBarTextInputPlaceHolder.rightAnchor.constraint(equalTo: searchBarGPSLocationImageView.leftAnchor).isActive = true
        searchBarTextInputPlaceHolder.heightAnchor.constraint(equalTo: searchBarContainerView.heightAnchor).isActive = true
    }

}


extension BarberShopsViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //print("Place name: \(place.name)")
        //print("Place address: \(place.formattedAddress)")
        //print("Place attributions: \(place.attributions)")
        guard let formAddress = place.formattedAddress else{
            return
        }
        self.mapView.camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15)
        self.searchBarTextInputPlaceHolder.text = formAddress
        self.desiredLocationMarker(loc: place.coordinate)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}



//modal view

class ModalViewController: UIViewController, StarRatingDelegate {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 10,spacing: 5, emptyColor: .black, fillColor: .white)
    var indexMarker: Int?
    var barbermarkerrrr = [BarberMarker]()
    var currentUserIdentification: String?
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let modalViewBarContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowMoreShopDetails)))
        return view
    }()
    
    let modalViewTextContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowMoreShopDetails)))
        return view
    }()
    
    lazy var barberShopCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "barbershop1")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowMoreShopDetails)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        //imageView.image = UIImage(named: "view_list")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 40
        imageView.layer.masksToBounds = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowMoreShopDetails)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowMoreShopDetails)))
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowMoreShopDetails)))
        return fnhp
    }()
    
    override func viewDidLoad() {
        
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
    
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        view.isOpaque = false
        view.addSubview(modalViewBarContainerView)
        setupViewContraints()
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDismissView))
        view.addGestureRecognizer(tapGesture)
        inputDataIntoViews()
        UserDefaults.standard.set(false, forKey: "theBookingProcessIsComplete")
        let mapViewHolder = BarberShopsViewController()
        mapViewHolder.modalViewCOntrollerHolderForView = self
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(false, forKey: "theBookingProcessIsComplete")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.handleDismissView()
    }
    
    func inputDataIntoViews(){
        if let index = self.indexMarker {
            if self.barbermarkerrrr.count > 0 && index <= self.barbermarkerrrr.count {
                let tappedMarker = self.barbermarkerrrr[index]
                guard let compName = tappedMarker.companyName, let compAddress = tappedMarker.companyFormattedAddress, let logoURL = tappedMarker.companyLogoImageUrl, let currentUserUUID = tappedMarker.companyID else {
                    return
                }
                self.barberShopLogoImageView.loadImagesUsingCacheWithUrlString(urlString: logoURL)
                self.barberShopNamePlaceHolder.text = compName
                self.barberShopAddressPlaceHolder.text = compAddress
                self.currentUserIdentification = currentUserUUID
                self.getRatingValue(shopID: currentUserUUID)
            }
        }
    }
    
    func getRatingValue(shopID: String){
        DispatchQueue.global(qos: .background).async {
            
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            
            let firebaseReferenceRating = Database.database().reference()
            firebaseReferenceRating.child("users").child(shopID).observeSingleEvent(of: .value, with: { (snapshootRatingValue) in
                
                self.activityIndicator.removeFromSuperview()
                self.activityIndicator.stopAnimating()
                
                if let dictonaryRatingVal = snapshootRatingValue.value as? [String: AnyObject]{
                    
                    if let ratingValue = dictonaryRatingVal["rating"] as? String, let ratingValueInt = Int(ratingValue) {
                        DispatchQueue.main.async {
                            self.starView.configure(self.attribute, current: CGFloat(integerLiteral: ratingValueInt), max: 5)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.starView.configure(self.attribute, current: CGFloat(integerLiteral: 0), max: 5)
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    @objc func handleDismissView(){
        dismiss(animated: false, completion: nil)
    }
    
    func setupViewContraints(){
        modalViewBarContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        modalViewBarContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        modalViewBarContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        modalViewBarContainerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        modalViewBarContainerView.addSubview(barberShopCoverImageView)
        modalViewBarContainerView.addSubview(modalViewTextContainerView)
        modalViewBarContainerView.addSubview(barberShopLogoImageView)
        modalViewBarContainerView.bringSubview(toFront: barberShopLogoImageView)
        
        
        barberShopCoverImageView.topAnchor.constraint(equalTo: modalViewBarContainerView.topAnchor).isActive = true
        barberShopCoverImageView.rightAnchor.constraint(equalTo: modalViewBarContainerView.rightAnchor).isActive = true
        //barberShopCoverImageView.leftAnchor.constraint(equalTo: barberShopNamePlaceHolder.rightAnchor).isActive = true
        barberShopCoverImageView.widthAnchor.constraint(equalTo: modalViewBarContainerView.widthAnchor, multiplier: 0.5).isActive = true
        barberShopCoverImageView.heightAnchor.constraint(equalTo: modalViewBarContainerView.heightAnchor).isActive = true
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: modalViewBarContainerView.topAnchor, constant: 10).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: modalViewBarContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        barberShopLogoImageView.bottomAnchor.constraint(equalTo: modalViewBarContainerView.bottomAnchor, constant: -10).isActive = true
        
        modalViewTextContainerView.topAnchor.constraint(equalTo: modalViewBarContainerView.topAnchor).isActive = true
        modalViewTextContainerView.leftAnchor.constraint(equalTo: modalViewBarContainerView.leftAnchor).isActive = true
        modalViewTextContainerView.rightAnchor.constraint(equalTo: barberShopCoverImageView.leftAnchor, constant: -40).isActive = true
        modalViewTextContainerView.heightAnchor.constraint(equalTo: modalViewBarContainerView.heightAnchor) .isActive = true
        
        modalViewTextContainerView.addSubview(barberShopNamePlaceHolder)
        modalViewTextContainerView.addSubview(barberShopAddressPlaceHolder)
        modalViewTextContainerView.addSubview(starView)
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: modalViewTextContainerView.topAnchor).isActive = true
        barberShopNamePlaceHolder.leftAnchor.constraint(equalTo: modalViewTextContainerView.leftAnchor, constant: 2).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: modalViewTextContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.leftAnchor.constraint(equalTo: modalViewTextContainerView.leftAnchor, constant: 2).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: modalViewTextContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        starView.topAnchor.constraint(equalTo: barberShopAddressPlaceHolder.bottomAnchor, constant: 10).isActive = true
        starView.leftAnchor.constraint(equalTo: modalViewTextContainerView.leftAnchor, constant: 2).isActive = true
        starView.widthAnchor.constraint(equalTo: modalViewTextContainerView.widthAnchor).isActive = true
        starView.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
    @objc func handleShowMoreShopDetails(){
        if let userSelID = self.currentUserIdentification {
            let barberprofile = BarberShopProfileViewController()
            barberprofile.barberShopUUID = userSelID
            let navController = UINavigationController(rootViewController: barberprofile)
            present(navController, animated: true, completion: nil)
        }
    }
}

















