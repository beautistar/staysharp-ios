//
//  BookingHistoryBarberShopDetailViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 23/12/2017.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate

class BookingHistoryBarberShopDetailViewController: UIViewController {
    
    var specificAppintment: AppointmentsCustomer?
    var bookingHolder = [Bookings]()
    var serviceBooked = [Service]()
    var barberShopID: String?
    var serviceSoleNameReference: String?
    var bookingUniqueIDDD: String?
    var appointmentsviewhold: BookingHistoryBarberShopViewController?
    var bookedBarberUUIDAX: String?
    var paymentIdentification: String?
    var paymentAmountString: String?
    var indexPathSelected: Int?
    
    
    lazy var scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.isUserInteractionEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    lazy var headerContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let headerTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("headerTitleTextOrderView", comment: "DETAIL")
        ht.font = UIFont(name: "Arch-LightCond", size: 25)
        ht.textColor = UIColor.white
        ht.textAlignment = .center
        return ht
    }()
    
    lazy var headerSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.isUserInteractionEnabled = true
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    let moreDetailContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        return view
    }()
    
    let topMoreDetailContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let detailthumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        
        return tniv
    }()
    
    let clientNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let openingAndClosingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookedBarberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let methodOfPaymentNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.red
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("summaryHeaderNumberOfCustomer", comment: "SUMMARY")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let headerDescriptionSeperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.white
        return sv
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customOrderDetailCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdXBA")
        return cv
    }()
    
    let costTotalSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    let costTotalContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let totalTextHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalQuantityPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let serviceDescriptionContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var serviceDescriptionThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.layer.cornerRadius = 5
        tniv.layer.masksToBounds = true
        tniv.layer.borderWidth = 1
        tniv.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        tniv.contentMode = .scaleToFill
        tniv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showViewToEnableZoom)))
        tniv.isUserInteractionEnabled = true
        return tniv
    }()
    
    let serviceDescriptionTextPlaceHolder: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.white
        em.backgroundColor = UIColor.clear
        em.layer.cornerRadius = 5
        em.layer.masksToBounds = true
        em.layer.borderWidth = 1
        em.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        em.isUserInteractionEnabled = true
        em.isEditable = false
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        em.textAlignment = .justified
        return em
    }()
    
    let contactHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.text = NSLocalizedString("contactLabelTextOrderDetailView", comment: "Contact")
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let phoneLogothumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.image = UIImage(named: "Call-icon")
        return tniv
    }()
    
    let phoneNumberPlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "HelveticaNeue-Mediu", size: 15)
        fnhp.contentHorizontalAlignment = .left
        fnhp.setTitleColor(UIColor.white, for: .normal)
        fnhp.backgroundColor = UIColor.clear
        fnhp.addTarget(self, action: #selector(handleMakeCall), for: .touchUpInside)
        return fnhp
    }()
    
    let bookingIDHeaderTextPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("bookingIDAvailLabelTextOrderDetailView", comment: "Order ID")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookingIDDataHoldPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .left
        return fnhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.title = NSLocalizedString("appointmentTabButton", comment: "Appointments")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBaction))
        view.addSubview(scrollView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContriants()
        inputDataIntoViewObject()
        getBookingData()
        getCustomerUniqueIdentifier()
        
    }
    
    @objc func handleBaction(){
        self.dismiss(animated: true, completion: nil)
    }
    
//    func dismissView(){
//        if let itemPoint = self.indexPathSelected, let specialObject = self.appointmentsviewhold {
//            let indexPathHolder = IndexPath(row: itemPoint, section: 0)
//            specialObject.collectionView.performBatchUpdates({
//                specialObject.collectionView.deleteItems(at: [indexPathHolder])
//                specialObject.appointments.remove(at: itemPoint)
//            }, completion: { (finished) in
//                specialObject.collectionView.reloadItems(at: (specialObject.collectionView.indexPathsForVisibleItems))
//                specialObject.getPaidBookings()
//                self.navigationController?.popViewController(animated: true)
//            })
//        }
//    }
    
    @objc func showViewToEnableZoom(){
        let modalViewController = ZoomImageViewController()
        if self.serviceDescriptionThumbnailImageView.image != nil {
            modalViewController.tappedImage = self.serviceDescriptionThumbnailImageView.image
            modalViewController.modalPresentationStyle = .overCurrentContext
            present(modalViewController, animated: true, completion: nil)
        }
    }
    
    func getCustomerUniqueIdentifier(){
        if let shopIX = self.barberShopID,  let bookingIDXOO = self.bookingUniqueIDDD {
            let firebaseRefCustomerIdentifier = Database.database().reference()
            firebaseRefCustomerIdentifier.child("bookings").child(shopIX).child(bookingIDXOO).observeSingleEvent(of: .value, with: { (snapshotootottIdentifier) in
                if let dictionIDX = snapshotootottIdentifier.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionIDX) == true {
                    let bookingByCustomer = Bookings()
                    bookingByCustomer.setValuesForKeys(dictionIDX)
                    if let customerIDXHolder = bookingByCustomer.customerID, let paymentIDDAX = bookingByCustomer.paymentID {
                        
                        if shopIX != "" && paymentIDDAX != "" && paymentIDDAX != "nil" {
                            self.getCustomerMobileNumber(customerIdentification: customerIDXHolder)
                            UserDefaults.standard.set(customerIDXHolder, forKey: "dataForCustomerIdentifierOnOrderDetailAccpetDecline")
                            self.paymentIdentification = paymentIDDAX
                            let firebaseRefePayment = Database.database().reference()
                            firebaseRefePayment.child("payments").child(shopIX).child(paymentIDDAX).observeSingleEvent(of: .value, with: { (snapshoopPayment) in
                                if let paymentHold = snapshoopPayment.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: paymentHold) == true {
                                    let paymentDataHold = Payments()
                                    paymentDataHold.setValuesForKeys(paymentHold)
                                    
                                    if let amountPaid = paymentDataHold.priceTotal, let methodPay = paymentDataHold.methodOfPayment {
                                        self.paymentAmountString = amountPaid
                                        self.methodOfPaymentNamePlaceHolder.text = NSLocalizedString("paymentMethodTextHolderOrderDetailView", comment: "payment method: ") + "\(methodPay)"
                                    }
                                }
                            }, withCancel: nil)
                        }
                    }
                }
            })
        }
    }
    
    func getCustomerMobileNumber(customerIdentification: String){
        let firebaseMobileNumerReference = Database.database().reference()
        firebaseMobileNumerReference.child("users").child(customerIdentification).observeSingleEvent(of: .value, with: { (snapshooottttMobile) in
            if let dictionaryMob = snapshooottttMobile.value as? [String: AnyObject], HandleDataRequest.handleUserCustomerNode(firebaseData: dictionaryMob) == true{
                let customerDataHold = Customer()
                customerDataHold.setValuesForKeys(dictionaryMob)
                
                if let mobileNumberGotten = customerDataHold.mobileNumber {
                    self.phoneNumberPlaceHolder.setTitle(mobileNumberGotten, for: .normal)
                }
            }
        }, withCancel: nil)
    }
    
    func inputDataIntoViewObject(){
        if let apData = self.specificAppintment {
            DispatchQueue.main.async {
                
                if let bookingUniqueID = apData.bookingUniqueID, let shopID = self.barberShopID {
                    let firebaseDataRefrenceDataDetails = Database.database().reference()
                    firebaseDataRefrenceDataDetails.child("bookings").child(shopID).child(bookingUniqueID).observeSingleEvent(of: .value, with: { (snapshotDatatata) in
                        if let dictionaryDataDetails = snapshotDatatata.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryDataDetails) == true {
                            let bookingSingle = Bookings()
                            bookingSingle.setValuesForKeys(dictionaryDataDetails)
                            
                            if let bookingStart = bookingSingle.bookingStartTime, let bookingEnd = bookingSingle.bookingEndTime, let tzone = bookingSingle.timezone, let bookingCalendar = bookingSingle.calendar, let bookingLocal = bookingSingle.local, let barberUniqueID = bookingSingle.bookedBarberID, let customerUniqueID = bookingSingle.customerID {
                                
                                let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal)
                                let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingStart)
                                let verifyBookingStopDate = VerifyDateAssociation.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingEnd)
                                let userTimezone = DateByUserDeviceInitializer.tzone
                                let userCalender = DateByUserDeviceInitializer.calenderNow
                                let userLocal = DateByUserDeviceInitializer.localCode
                                
                                let verifyBookingCurrentData = VerifyDateDetails.checkDateData(timeZone: userTimezone, calendar: userCalender, locale: userLocal)
                                
                                if verifyBookingData == true && verifyBookingStartDate == true && verifyBookingStopDate == true && verifyBookingCurrentData == true {
                                    let bookingRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tzone, calenName: bookingCalendar, LocName: bookingLocal)
                                    
                                    //DateInRegion(string: bookingEnd, format: .extended, fromRegion: bookingRegion)
                                    
                                    if let bookingStartDate = bookingStart.toDate(style: .extended, region: bookingRegion) , let bookingEndDate = bookingEnd.toDate(style: .extended, region: bookingRegion) {
                                        
                                        let currentUserTimeRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: userLocal)
                                        let currentBookingStartDate = bookingStartDate.convertTo(region: currentUserTimeRegion)
                                        let currentBookingEndDate = bookingEndDate.convertTo(region: currentUserTimeRegion)
                                        
                                        
                                        let comp = "\(currentBookingStartDate.toString(DateToStringStyles.time(.short))) - \(currentBookingEndDate.toString(DateToStringStyles.time(.short)))"
                                        self.openingAndClosingTimePlaceHolder.text = comp
                                        self.getBarberUniqueName(userID: barberUniqueID)
                                        self.getCustomerUniqueName(userID: customerUniqueID)
                                    }
                                }
                            }
                        }
                    }, withCancel: { (errorororr) in
                        print(errorororr)
                    })
                }
            }
        }
    }
    
    func getBarberUniqueName(userID: String){
        let firebaseDataRefrenceBarberDetails = Database.database().reference()
        firebaseDataRefrenceBarberDetails.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshotPersonalDetails) in
            if let dictionaryPersonalDetails = snapshotPersonalDetails.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryPersonalDetails) == true {
                let userSpecific = Barber()
                userSpecific.setValuesForKeys(dictionaryPersonalDetails)
                
                if let userName = userSpecific.firstName {
                    self.bookedBarberNamePlaceHolder.text = userName
                }
            }
        }) { (errororororor) in
            print(errororororor)
        }
    }
    
    func getCustomerUniqueName(userID: String){
        let firebaseDataRefrenceBarberDetails = Database.database().reference()
        firebaseDataRefrenceBarberDetails.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshotPersonalDetails) in
            if let dictionaryPersonalDetails = snapshotPersonalDetails.value as? [String: AnyObject], HandleDataRequest.handleUserCustomerNode(firebaseData: dictionaryPersonalDetails) == true {
                let userSpecific = Customer()
                userSpecific.setValuesForKeys(dictionaryPersonalDetails)
                
                if let userName = userSpecific.firstName, let imageUserURl = userSpecific.profileImageUrl {
                    self.clientNamePlaceHolder.text = userName
                    self.detailthumbnailImageView.loadImagesUsingCacheWithUrlString(urlString: imageUserURl)
                    
                }
            }
        }) { (errororororor) in
            print(errororororor)
        }
    }
    
    
    func getBookingData(){
        let firebaseRedRef = Database.database().reference()
        if let apDataBook = self.specificAppintment, let shopID = self.barberShopID {
            guard let bookID = apDataBook.bookingUniqueID else {
                print("Error in guard statement booking")
                return
            }
            
            firebaseRedRef.child("bookings").child(shopID).child(bookID).observeSingleEvent(of: .value, with: { (snapshootosos) in
                if let dictionary = snapshootosos.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionary) == true {
                    let bookingData = Bookings()
                    bookingData.setValuesForKeys(dictionary)
                    self.bookingHolder.append(bookingData)
                    if let descText = bookingData.bookingDescriptionText, let descURL = bookingData.bookingDescriptionImageUrl, let servName = bookingData.bookedServiceID, let bookTtime = bookingData.ConfirmedTotalTime, let quanServe = bookingData.bookedServiceQuantity, let payIDDS = bookingData.paymentID {
                        self.serviceDescriptionTextPlaceHolder.text = descText
                        self.serviceDescriptionThumbnailImageView.loadImagesUsingCacheWithUrlString(urlString: descURL)
                        self.totalTimePlaceHolder.text = bookTtime + "min"
                        self.totalQuantityPlaceHolder.text = quanServe
                        self.bookingIDDataHoldPlaceHolder.text = bookID
                        
                        
                        if shopID != "" && payIDDS != "" && payIDDS != "nil" {
                            let firebaseReferenceCheckPayment = Database.database().reference()
                            firebaseReferenceCheckPayment.child("payments").child(shopID).child(payIDDS).observeSingleEvent(of: .value, with: { (snapshotDataCheckCurrency) in
                                if let dictionaryCurrencyCheck = snapshotDataCheckCurrency.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionaryCurrencyCheck) == true {
                                    let paymentsCurrencySingle = Payments()
                                    paymentsCurrencySingle.setValuesForKeys(dictionaryCurrencyCheck)
                                    
                                    if let currentAmount = paymentsCurrencySingle.priceTotal {
                                        
                                        let firebaseReferenceCheckCurrency = Database.database().reference()
                                        firebaseReferenceCheckCurrency.child("users").child(shopID).observeSingleEvent(of: .value, with: { (snapshotDataCheckCurrency) in
                                            if let dictionaryCurrencyCheck = snapshotDataCheckCurrency.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryCurrencyCheck) == true {
                                                let usersCurrencySingle = BarberShop()
                                                usersCurrencySingle.setValuesForKeys(dictionaryCurrencyCheck)
                                                
                                                if let currencySymbol = usersCurrencySingle.currencyCode {
                                                    self.totalPricePlaceHolder.text = currentAmount + currencySymbol
                                                }
                                            }
                                        })
                                        
                                    }
                                }
                            })
                        }
                        
                        
                        let firebaseRedReference = Database.database().reference()
                        firebaseRedReference.child("service").child(shopID).child(servName).observeSingleEvent(of: .value, with: { (snapshososostt) in
                            if let dictionary = snapshososostt.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionary) == true {
                                let serDic = Service()
                                serDic.setValuesForKeys(dictionary)
                                self.serviceBooked.append(serDic)
                                DispatchQueue.main.async {
                                    self.collectionView.reloadData()
                                }
                            }
                        }, withCancel: nil)
                    }
                }
            }, withCancel: nil)
        }
    }
    
    @objc func handleMakeCall(sender: UIButton){
        if let mobNUm = sender.titleLabel?.text {
            if let url = URL(string: "tel://\(mobNUm)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func setupViewContriants(){
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 528) //550
        
        scrollView.addSubview(headerContanerView)
        scrollView.addSubview(headerSeperatorView)
        scrollView.addSubview(moreDetailContanerView)
        
        headerContanerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        headerContanerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerContanerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        headerContanerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        headerContanerView.addSubview(headerTitle)
        
        headerTitle.topAnchor.constraint(equalTo: headerContanerView.topAnchor).isActive = true
        headerTitle.centerXAnchor.constraint(equalTo: headerContanerView.centerXAnchor).isActive = true
        headerTitle.widthAnchor.constraint(equalTo: headerContanerView.widthAnchor).isActive = true
        headerTitle.heightAnchor.constraint(equalTo: headerContanerView.heightAnchor).isActive = true
        
        headerSeperatorView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        headerSeperatorView.topAnchor.constraint(equalTo: headerContanerView.bottomAnchor).isActive = true
        headerSeperatorView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        headerSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        moreDetailContanerView.topAnchor.constraint(equalTo: headerSeperatorView.bottomAnchor, constant: 12).isActive = true
        moreDetailContanerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        moreDetailContanerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -12).isActive = true
        moreDetailContanerView.heightAnchor.constraint(equalToConstant: scrollView.contentSize.height).isActive = true
        //constraint(equalTo: scrollView.heightAnchor, multiplier: 1, constant: 25).isActive = true
        
        moreDetailContanerView.addSubview(topMoreDetailContanerView)
        moreDetailContanerView.addSubview(headerViewDescriptionPlaceHolder)
        moreDetailContanerView.addSubview(headerDescriptionSeperatorView)
        moreDetailContanerView.addSubview(collectView)
        moreDetailContanerView.addSubview(costTotalSeperatorView)
        moreDetailContanerView.addSubview(costTotalContanerView)
        moreDetailContanerView.addSubview(serviceDescriptionContanerView)
        moreDetailContanerView.addSubview(contactHeaderPlaceHolder)
        moreDetailContanerView.addSubview(phoneLogothumbnailImageView)
        moreDetailContanerView.addSubview(phoneNumberPlaceHolder)
        moreDetailContanerView.addSubview(bookingIDHeaderTextPlaceHolder)
        moreDetailContanerView.addSubview(bookingIDDataHoldPlaceHolder)
        
        topMoreDetailContanerView.topAnchor.constraint(equalTo: moreDetailContanerView.topAnchor).isActive = true
        topMoreDetailContanerView.centerXAnchor.constraint(equalTo: moreDetailContanerView.centerXAnchor).isActive = true
        topMoreDetailContanerView.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, constant: -24).isActive = true
        topMoreDetailContanerView.heightAnchor.constraint(equalToConstant: 105).isActive = true //25
        
        topMoreDetailContanerView.addSubview(detailthumbnailImageView)
        topMoreDetailContanerView.addSubview(clientNamePlaceHolder)
        topMoreDetailContanerView.addSubview(openingAndClosingTimePlaceHolder)
        topMoreDetailContanerView.addSubview(bookedBarberNamePlaceHolder)
        topMoreDetailContanerView.addSubview(methodOfPaymentNamePlaceHolder)
        
        detailthumbnailImageView.topAnchor.constraint(equalTo: topMoreDetailContanerView.topAnchor).isActive = true
        detailthumbnailImageView.leftAnchor.constraint(equalTo: topMoreDetailContanerView.leftAnchor).isActive = true
        detailthumbnailImageView.widthAnchor.constraint(equalTo: topMoreDetailContanerView.widthAnchor, multiplier: 0.25, constant: 10).isActive = true
        detailthumbnailImageView.heightAnchor.constraint(equalTo: topMoreDetailContanerView.heightAnchor).isActive = true
        
        clientNamePlaceHolder.topAnchor.constraint(equalTo: topMoreDetailContanerView.topAnchor, constant: 5).isActive = true
        clientNamePlaceHolder.leftAnchor.constraint(equalTo: detailthumbnailImageView.rightAnchor, constant: 10).isActive = true
        clientNamePlaceHolder.rightAnchor.constraint(equalTo: topMoreDetailContanerView.rightAnchor, constant: -30).isActive = true
        clientNamePlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        openingAndClosingTimePlaceHolder.topAnchor.constraint(equalTo: clientNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        openingAndClosingTimePlaceHolder.leftAnchor.constraint(equalTo: detailthumbnailImageView.rightAnchor, constant: 10).isActive = true
        openingAndClosingTimePlaceHolder.rightAnchor.constraint(equalTo: topMoreDetailContanerView.rightAnchor, constant: -30).isActive = true
        openingAndClosingTimePlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        bookedBarberNamePlaceHolder.topAnchor.constraint(equalTo: openingAndClosingTimePlaceHolder.bottomAnchor, constant: 5).isActive = true
        bookedBarberNamePlaceHolder.leftAnchor.constraint(equalTo: detailthumbnailImageView.rightAnchor, constant: 10).isActive = true
        bookedBarberNamePlaceHolder.rightAnchor.constraint(equalTo: topMoreDetailContanerView.rightAnchor, constant: -30).isActive = true
        bookedBarberNamePlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        methodOfPaymentNamePlaceHolder.topAnchor.constraint(equalTo: bookedBarberNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        methodOfPaymentNamePlaceHolder.leftAnchor.constraint(equalTo: detailthumbnailImageView.rightAnchor, constant: 10).isActive = true
        methodOfPaymentNamePlaceHolder.rightAnchor.constraint(equalTo: topMoreDetailContanerView.rightAnchor, constant: -30).isActive = true
        methodOfPaymentNamePlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: topMoreDetailContanerView.bottomAnchor, constant: 5).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: moreDetailContanerView.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, constant: -24).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        headerDescriptionSeperatorView.leftAnchor.constraint(equalTo: moreDetailContanerView.leftAnchor).isActive = true
        headerDescriptionSeperatorView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor).isActive = true
        headerDescriptionSeperatorView.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor).isActive = true
        headerDescriptionSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        collectView.topAnchor.constraint(equalTo: headerDescriptionSeperatorView.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: moreDetailContanerView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        costTotalSeperatorView.leftAnchor.constraint(equalTo: moreDetailContanerView.leftAnchor).isActive = true
        costTotalSeperatorView.topAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true
        costTotalSeperatorView.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor).isActive = true
        costTotalSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        costTotalContanerView.topAnchor.constraint(equalTo: costTotalSeperatorView.bottomAnchor, constant: 5).isActive = true
        costTotalContanerView.centerXAnchor.constraint(equalTo: moreDetailContanerView.centerXAnchor).isActive = true
        costTotalContanerView.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, constant: -12).isActive = true
        costTotalContanerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        costTotalContanerView.addSubview(totalTextHeaderPlaceHolder)
        costTotalContanerView.addSubview(totalQuantityPlaceHolder)
        costTotalContanerView.addSubview(totalTimePlaceHolder)
        costTotalContanerView.addSubview(totalPricePlaceHolder)
        
        totalTextHeaderPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTextHeaderPlaceHolder.leftAnchor.constraint(equalTo: costTotalContanerView.leftAnchor).isActive = true
        totalTextHeaderPlaceHolder.widthAnchor.constraint(equalToConstant: 70).isActive = true
        totalTextHeaderPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalPricePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalPricePlaceHolder.rightAnchor.constraint(equalTo: costTotalContanerView.rightAnchor).isActive = true
        totalPricePlaceHolder.widthAnchor.constraint(equalToConstant: 120).isActive = true
        totalPricePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalTimePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTimePlaceHolder.rightAnchor.constraint(equalTo: totalPricePlaceHolder.leftAnchor).isActive = true
        totalTimePlaceHolder.widthAnchor.constraint(equalToConstant: 90).isActive = true
        totalTimePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalQuantityPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalQuantityPlaceHolder.rightAnchor.constraint(equalTo: totalTimePlaceHolder.leftAnchor).isActive = true
        totalQuantityPlaceHolder.widthAnchor.constraint(equalToConstant: 75).isActive = true
        totalQuantityPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        //
        serviceDescriptionContanerView.topAnchor.constraint(equalTo: costTotalContanerView.bottomAnchor).isActive = true
        serviceDescriptionContanerView.centerXAnchor.constraint(equalTo: moreDetailContanerView.centerXAnchor).isActive = true
        serviceDescriptionContanerView.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, constant: -12).isActive = true
        serviceDescriptionContanerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        serviceDescriptionContanerView.addSubview(serviceDescriptionThumbnailImageView)
        serviceDescriptionContanerView.addSubview(serviceDescriptionTextPlaceHolder)
        
        serviceDescriptionThumbnailImageView.topAnchor.constraint(equalTo: serviceDescriptionContanerView.topAnchor).isActive = true
        serviceDescriptionThumbnailImageView.leftAnchor.constraint(equalTo: serviceDescriptionContanerView.leftAnchor).isActive = true
        serviceDescriptionThumbnailImageView.widthAnchor.constraint(equalTo: serviceDescriptionContanerView.widthAnchor, multiplier: 0.25, constant: 15).isActive = true
        serviceDescriptionThumbnailImageView.heightAnchor.constraint(equalTo: serviceDescriptionContanerView.heightAnchor).isActive = true
        
        serviceDescriptionTextPlaceHolder.topAnchor.constraint(equalTo: serviceDescriptionContanerView.topAnchor).isActive = true
        serviceDescriptionTextPlaceHolder.leftAnchor.constraint(equalTo: serviceDescriptionThumbnailImageView.rightAnchor, constant: 10).isActive = true
        serviceDescriptionTextPlaceHolder.rightAnchor.constraint(equalTo: serviceDescriptionContanerView.rightAnchor, constant: -30).isActive = true
        serviceDescriptionTextPlaceHolder.heightAnchor.constraint(equalTo: serviceDescriptionContanerView.heightAnchor).isActive = true
        
        contactHeaderPlaceHolder.topAnchor.constraint(equalTo: serviceDescriptionContanerView.bottomAnchor, constant: 10).isActive = true
        contactHeaderPlaceHolder.centerXAnchor.constraint(equalTo: moreDetailContanerView.centerXAnchor).isActive = true
        contactHeaderPlaceHolder.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, constant: -12).isActive = true
        contactHeaderPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        phoneLogothumbnailImageView.topAnchor.constraint(equalTo: contactHeaderPlaceHolder.bottomAnchor).isActive = true
        phoneLogothumbnailImageView.leftAnchor.constraint(equalTo: moreDetailContanerView.leftAnchor, constant: 6).isActive = true
        phoneLogothumbnailImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        phoneLogothumbnailImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        phoneNumberPlaceHolder.topAnchor.constraint(equalTo: contactHeaderPlaceHolder.bottomAnchor).isActive = true
        phoneNumberPlaceHolder.leftAnchor.constraint(equalTo: phoneLogothumbnailImageView.rightAnchor, constant: 10).isActive = true
        phoneNumberPlaceHolder.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, constant: -75).isActive = true
        phoneNumberPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        bookingIDHeaderTextPlaceHolder.topAnchor.constraint(equalTo: phoneLogothumbnailImageView.bottomAnchor, constant: 5).isActive = true
        bookingIDHeaderTextPlaceHolder.centerXAnchor.constraint(equalTo: moreDetailContanerView.centerXAnchor).isActive = true
        bookingIDHeaderTextPlaceHolder.widthAnchor.constraint(equalTo: moreDetailContanerView.widthAnchor, constant: -24).isActive = true
        bookingIDHeaderTextPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        bookingIDDataHoldPlaceHolder.topAnchor.constraint(equalTo: bookingIDHeaderTextPlaceHolder.bottomAnchor, constant: 10).isActive = true
        bookingIDDataHoldPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bookingIDDataHoldPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        bookingIDDataHoldPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }

}
