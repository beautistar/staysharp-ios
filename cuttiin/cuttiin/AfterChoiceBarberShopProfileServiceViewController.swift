//
//  AfterChoiceBarberShopProfileServiceViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/2/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import MGStarRatingView

class AfterChoiceBarberShopProfileServiceViewController: UIViewController, StarRatingDelegate {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var barberShopUUID: String?
    var navBar: UINavigationBar = UINavigationBar()
    var barberOne = BarberShop()
    var serviceOne = [Service]()
    var serviceCategoryLadies = [Service]()
    var serviceCategoryGents = [Service]()
    var serviceCategoryKids = [Service]()
    var StringListOfBarbers = [String]()
    var selectedServiceOne = [String]()
    var selectedBarberOne = [String]()
    var ladiesSelectedList = [String]()
    var gentsSelectedList = [String]()
    var kidsSelectedList = [String]()
    
    var availableSelectedList = [String]()
    var topRatedSelectedList = [String]()
    
    var category = "Gents"
    var selectedLadiesGentsKidsButton = "Gents"
    var selectedButtonBeforeSwitchingView = "Gents"
    var selectedButtonAfterSwitchingView = "Available"
    
    
    var barbershopTimezone: String?
    var barbershopCalendar: String?
    var barbershopLocale: String?
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white //(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("selectBarbersHeaderAfterChoiceBarberShopServiceView", comment: "SELECT SERVICES")
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let categoryButtonContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ladiesButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("ladiesTapCartegoryAfterBarberShopProfileService", comment: "LADIES(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowLadiesList), for: .touchUpInside)
        return fbutton
    }()
    
    let ladiesButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    lazy var gentsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("gentsTabCategoryBarberShopProfile", comment: "GENTS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowGentsList), for: .touchUpInside)
        return fbutton
    }()
    
    let gentsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    lazy var kidsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("kidsTapCartegoryAfterBarberShopProfileService", comment: "KIDS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowKidsList), for: .touchUpInside)
        return fbutton
    }()
    
    let kidsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customBarberShopProfileCollectionViewCell.self, forCellWithReuseIdentifier: "cellId3")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
        setNavBarToTheView()
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.delegate = self
        view.addSubview(headerViewDescriptionPlaceHolder)
        view.addSubview(categoryButtonContainerView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContraints()
        getBarberPersonalDetails()
        getListBarberUUID()
        // Do any additional setup after loading the view.
    }
    
    func getBarberPersonalDetails(){
        let firebaseRef = Database.database().reference()
        if let userID = self.barberShopUUID {
            firebaseRef.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshooottt) in
                if let dictionary = snapshooottt.value as? [String: AnyObject] {
                    
                    let barbershopVerifi = HandleDataRequest.handleUserBarberShop(firebaseData: dictionary)
                    
                    if barbershopVerifi {
                        
                        self.barberOne.setValuesForKeys(dictionary)
                        if let logoURL = self.barberOne.companyLogoImageUrl, let shopName = self.barberOne.companyName, let shopAddress = self.barberOne.companyFormattedAddress, let ratingValueHold = self.barberOne.rating, let timezone = self.barberOne.timezone, let calendar = self.barberOne.calendar, let locale = self.barberOne.local {
                            
                            
                            self.barbershopTimezone = timezone
                            self.barbershopLocale = locale
                            self.barbershopCalendar = calendar
                            
                            DispatchQueue.main.async {
                                self.barberShopLogoImageView.loadImagesUsingCacheWithUrlString(urlString: logoURL)
                                self.barberShopNamePlaceHolder.text = shopName
                                self.barberShopAddressPlaceHolder.text = shopAddress
                                if let ratingInt = Int(ratingValueHold) {
                                    self.starView.configure(self.attribute, current: CGFloat(integerLiteral: ratingInt), max: 5)
                                } else {
                                    self.starView.configure(self.attribute, current: CGFloat(integerLiteral: 0), max: 5)
                                }
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
        
    }
    
    func getListBarberUUID(){
        let totalListOfBarbersSelected = self.availableSelectedList + self.topRatedSelectedList
        
        if totalListOfBarbersSelected.count > 0 {
            for totalSingle in totalListOfBarbersSelected {
                self.getBarberBBServices(barberID: totalSingle)
            }
        }
    }
    
    func getBarberBBServices(barberID: String){
        let firebaseRefere = Database.database().reference()
        
        firebaseRefere.child("barberServices").child(barberID).observeSingleEvent(of: .value, with: { (snapshoootshh) in
            if let dictionary = snapshoootshh.value as? [String: AnyObject] {
                for dic in dictionary {
                    guard let dictHolder = dic.value as? [String: AnyObject] else {
                        return
                    }
                    let barberserv = BarberServices()
                    barberserv.setValuesForKeys(dictHolder)
                    if let serveID = barberserv.serviceID {
                        self.getBarberServices(userID: serveID)
                    }
                }
            }
        }, withCancel: nil)
    }
    
    func getBarberServices(userID: String){
        let firebaseReferetwirl = Database.database().reference()
        if let barberShopUUUIIDD = self.barberShopUUID {
            firebaseReferetwirl.child("service").child(barberShopUUUIIDD).child(userID).observeSingleEvent(of: .value, with: { (snapshoootttttxxzz) in
                if let dictionary = snapshoootttttxxzz.value as? [String: AnyObject] {
                    
                    let serviceVerification = HandleDataRequest.handleServiceNode(firebaseData: dictionary)
                    
                    if serviceVerification {
                        let serveee = Service()
                        serveee.setValuesForKeys(dictionary)
                        guard let category = serveee.category, let serviceCheck = serveee.isDeleted else {
                            return
                        }
                        if self.serviceOne.contains(serveee) {
                            print("Duplicate")
                        }else {
                            if serveee.category == "Gents" && serviceCheck == "NO" {
                                
                                DispatchQueue.global(qos: .background).async {
                                    if let url = URL(string: serveee.serviceImageUrl!) {
                                        do {
                                            let data = try Data(contentsOf: url)
                                            
                                            if let downloadedImage = UIImage(data: data) {
                                                serveee.serviceImage = downloadedImage
                                                DispatchQueue.main.async {
                                                    self.collectionView.reloadData()
                                                }
                                            }
                                        } catch {
                                            print("profile picture not available")
                                        }
                                    }
                                }
                                self.serviceOne.append(serveee)
                            }
                        }
                        
                        if serviceCheck == "NO" {
                            switch category {
                            case "Ladies"  :
                                if self.serviceCategoryLadies.contains(serveee) {
                                    print("Duplicate")
                                }else {
                                    DispatchQueue.global(qos: .background).async {
                                        if let url = URL(string: serveee.serviceImageUrl!) {
                                            do {
                                                let data = try Data(contentsOf: url)
                                                
                                                if let downloadedImage = UIImage(data: data) {
                                                    serveee.serviceImage = downloadedImage
                                                }
                                            } catch {
                                                print("profile picture not available")
                                            }
                                        }
                                    }
                                    self.serviceCategoryLadies.append(serveee)
                                    self.ladiesButton.setTitle(NSLocalizedString("ladiesCartegoryTabBarberShopProfileView", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))", for: .normal)
                                }
                            case "Gents"  :
                                if self.serviceCategoryGents.contains(serveee) {
                                    print("Duplicate")
                                }else {
                                    DispatchQueue.global(qos: .background).async {
                                        if let url = URL(string: serveee.serviceImageUrl!) {
                                            do {
                                                let data = try Data(contentsOf: url)
                                                
                                                if let downloadedImage = UIImage(data: data) {
                                                    serveee.serviceImage = downloadedImage
                                                }
                                            } catch {
                                                print("profile picture not available")
                                            }
                                        }
                                    }
                                    self.serviceCategoryGents.append(serveee)
                                    self.gentsButton.setTitle(NSLocalizedString("gentsCartegoryTabBarberShopProfileView", comment: "GENTS") + "(\(self.serviceCategoryGents.count))", for: .normal)
                                }
                            case "Kids"  :
                                if self.serviceCategoryKids.contains(serveee) {
                                    print("Duplicate")
                                }else {
                                    DispatchQueue.global(qos: .background).async {
                                        if let url = URL(string: serveee.serviceImageUrl!) {
                                            do {
                                                let data = try Data(contentsOf: url)
                                                
                                                if let downloadedImage = UIImage(data: data) {
                                                    serveee.serviceImage = downloadedImage
                                                }
                                            } catch {
                                                print("profile picture not available")
                                            }
                                        }
                                    }
                                    self.serviceCategoryKids.append(serveee)
                                    self.kidsButton.setTitle(NSLocalizedString("kidsCartegoryTabBarberShopProfileView", comment: "KIDS") + "(\(self.serviceCategoryKids.count))", for: .normal)
                                }
                            default :
                                print( "Sky high")
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    @objc func handleDismissView(){
        dismiss(animated: true, completion: nil)
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopLogoImageView)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(starView)
        
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopLogoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopLogoImageView.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        starView.topAnchor.constraint(equalTo: barberShopAddressPlaceHolder.bottomAnchor, constant: 5).isActive = true
        starView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        starView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        starView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    func setupViewObjectContraints(){
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: view.topAnchor, constant: 165).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        categoryButtonContainerView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        categoryButtonContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        categoryButtonContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        categoryButtonContainerView.addSubview(ladiesButton)
        categoryButtonContainerView.addSubview(ladiesButtonSeperatorView)
        categoryButtonContainerView.addSubview(gentsButton)
        categoryButtonContainerView.addSubview(gentsButtonSeperatorView)
        categoryButtonContainerView.addSubview(kidsButton)
        categoryButtonContainerView.addSubview(kidsButtonSeperatorView)
        
        ladiesButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        ladiesButton.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        ladiesButtonSeperatorView.topAnchor.constraint(equalTo: ladiesButton.bottomAnchor).isActive = true
        ladiesButtonSeperatorView.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        kidsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        kidsButton.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        kidsButtonSeperatorView.topAnchor.constraint(equalTo: kidsButton.bottomAnchor).isActive = true
        kidsButtonSeperatorView.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        gentsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        gentsButton.leftAnchor.constraint(equalTo: ladiesButton.rightAnchor).isActive = true
        gentsButton.rightAnchor.constraint(equalTo: kidsButton.leftAnchor).isActive = true
        gentsButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        gentsButtonSeperatorView.topAnchor.constraint(equalTo: gentsButton.bottomAnchor).isActive = true
        gentsButtonSeperatorView.leftAnchor.constraint(equalTo: ladiesButtonSeperatorView.rightAnchor).isActive = true
        gentsButtonSeperatorView.rightAnchor.constraint(equalTo: kidsButtonSeperatorView.leftAnchor).isActive = true
        gentsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        collectView.topAnchor.constraint(equalTo: categoryButtonContainerView.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -8).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
    @objc func handleShowLadiesList(){
        self.selectedLadiesGentsKidsButton = "Ladies"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.white
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        self.selectedServiceOne.removeAll()
        self.ladiesSelectedList.removeAll()
        self.gentsSelectedList.removeAll()
        self.kidsSelectedList.removeAll()
        if self.serviceCategoryLadies.count > 0 {
            for serv in self.serviceCategoryLadies {
                if serv.category == "Ladies" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    @objc func handleShowGentsList(){
        self.selectedLadiesGentsKidsButton = "Gents"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.white
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        self.selectedServiceOne.removeAll()
        self.ladiesSelectedList.removeAll()
        self.gentsSelectedList.removeAll()
        self.kidsSelectedList.removeAll()
        if self.serviceCategoryGents.count > 0 {
            for serv in self.serviceCategoryGents {
                if serv.category == "Gents" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }
    
    @objc func handleShowKidsList(){
        self.selectedLadiesGentsKidsButton = "Kids"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.white
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        self.selectedServiceOne.removeAll()
        self.ladiesSelectedList.removeAll()
        self.gentsSelectedList.removeAll()
        self.kidsSelectedList.removeAll()
        if self.serviceCategoryKids.count > 0 {
            for serv in self.serviceCategoryKids {
                if serv.category == "Kids" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }
    
    func changeButtonCounter(){
        switch self.selectedLadiesGentsKidsButton {
        case "Ladies":
            let buttonTitle = NSLocalizedString("ladiesCartegoryTabBarberShopProfileView", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))"
            self.ladiesButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Ladies"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.white
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
            
        case "Gents":
            let buttonTitle = NSLocalizedString("gentsCartegoryTabBarberShopProfileView", comment: "GENTS") + "(\(self.serviceCategoryGents.count))"
            self.gentsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Gents"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.white
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        case "Kids":
            let buttonTitle = NSLocalizedString("kidsCartegoryTabBarberShopProfileView", comment: "KIDS") + "(\(self.serviceCategoryKids.count))"
            self.kidsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Kids"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.white
        default:
            print("Sky high")
        }
    }
    
    func handleMoveToTheNextView(){
        
        if let timezone = self.barbershopTimezone, let calendar = self.barbershopCalendar, let locale = self.barbershopLocale {
            
            let numberCustomer = NumberOfCustomersViewController()
            
            numberCustomer.barberShopUUID = self.barberShopUUID
            numberCustomer.barberShopTimezone = timezone
            numberCustomer.barberShopCalendar = calendar
            numberCustomer.barberShopLocale = locale
            
            
            numberCustomer.selectedStringServiceOneFinal = self.selectedServiceOne
            let totalListOfBarbersSelectedxx = self.availableSelectedList + self.topRatedSelectedList
            numberCustomer.selectedStringBarbersOneFinal = totalListOfBarbersSelectedxx
            let navController = UINavigationController(rootViewController: numberCustomer)
            present(navController, animated: true, completion: nil)
            
        }
    }

}
