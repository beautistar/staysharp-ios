//
//  ShoppingListViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/7/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import SwiftDate
import LocalAuthentication

class ShoppingListViewController: UIViewController {
    var navBar: UINavigationBar = UINavigationBar()
    
    var BarberShopUUID: String?
    var bookingUUID: String?
    var bookedServiceArry = [Service]()
    var bookingDateViewDataObject = BookingDateViewController()
    
    var strCardID = ""
    
    var arrAllCards : JSON?
    
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = UIColor.clear
        return v
    }()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var headerNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("paymentButtonTitleShoppingListView", comment: "Payment")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    //main view objects
    let paymentDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var paymentMethodsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "payment-methods")
        return imageView
    }()
    
    let headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.text = NSLocalizedString("summaryHeaderNumberOfCustomer", comment: "SUMMARY")
        fnhp.backgroundColor = UIColor.clear
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customShoppingListCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let collectionViewSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        return fnsv
    }()
    
    let costTotalContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let totalTextHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let buttonContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var payFoBookingButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false

//        st.setImage(UIImage(named: "credit_card"), for: .normal)
        st.setImage(UIImage(named: "mobile_phone_outline"), for: .normal)
        st.imageEdgeInsets = UIEdgeInsets(top: 8,left: 130,bottom: 8,right: 15)
        st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -45,bottom: 0,right: 30) //-180 34
        
        st.backgroundColor = UIColor.white //(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("payButtonTitleShoppingListViewPictureButton", comment: "MOBILEPAY"), for: .normal)
        st.setTitleColor(UIColor(r: 11, g: 49, b: 68), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 19)
        st.contentHorizontalAlignment = .left
        st.tag = 0
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        
        //check here
        st.isEnabled = false
        st.isHidden = true
        
        st.addTarget(self, action: #selector(handleShowActionSheet(sender:)), for: .touchUpInside)
        return st
    }()
    
    lazy var payFoSavedCardButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        
        st.setImage(UIImage(named: "credit_card"), for: .normal)
        st.imageEdgeInsets = UIEdgeInsets(top: 8,left: 100,bottom: 8,right: 15)
        st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -125,bottom: 0,right: 30) //-180 34

        st.backgroundColor = UIColor.white //(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("payCreditCardButtonTitleShoppingListViewPictureButton", comment: "CREDIT CARD"), for: .normal)
        st.setTitleColor(UIColor(r: 11, g: 49, b: 68), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 19)
        st.contentHorizontalAlignment = .left
        st.tag = 2
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        
        //check here
        st.isEnabled = false
        st.isHidden = true
        
        st.addTarget(self, action: #selector(handleShowActionSheet(sender:)), for: .touchUpInside)
        return st
    }()
    
    lazy var payFoBookingButtonWithCash: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        
        st.setImage(UIImage(named: "cash_"), for: .normal)
        st.imageEdgeInsets = UIEdgeInsets(top: 8,left: 100,bottom: 8,right: 15)
        st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -125,bottom: 0,right: 30)
        //st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -180,bottom: 0,right: 34)
        st.backgroundColor = UIColor.white //(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("payWithCashButtonTitleShoppingListView", comment: "CASH") , for: .normal)
        st.setTitleColor(UIColor(r: 11, g: 49, b: 68), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 19)
        st.contentHorizontalAlignment = .left
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        
        //check here
        st.isEnabled = false
        st.isHidden = true
        
        st.addTarget(self, action: #selector(handleShowActionSheet(sender:)), for: .touchUpInside)
        return st
    }()
    
    var didShoppingVCBackButtonPressedBlock : (() -> Void)!
    


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(checkAlertPresent))
        navigationItem.rightBarButtonItem?.isEnabled = false
        view.addSubview(scrollView)
        setNavBarToTheView()
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViews()
        getBarberShopName()
        getBookingDetails()
        getCashPaymentVerification()
        getCardPaymentVerification()

        UserDefaults.standard.set(false, forKey: "alertViewForMobileNumbersPresentedShoppingListView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getAllCards()
        
        print("RED ROSES RUN DRY")
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: false, completion: nil)
            } else {
                print("no true user default")
            }
        } else {
            print("no data from user default")
        }
    }
    
    func dissmisviewAfterCashPayment(){
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: false, completion: nil)
            } else {
                print("no true user default")
            }
        } else {
            print("no data from user default")
        }
    }
    
    func getCashPaymentVerification(){
        DispatchQueue.global(qos: .background).async {
            if let shopID = self.BarberShopUUID{
                let firebaseReferenceCashPay = Database.database().reference()
                firebaseReferenceCashPay.child("cashPaymentVerification").child(shopID).observeSingleEvent(of: .value, with: { (snapshotDataHold) in
                    if let dictionaryCashCheck = snapshotDataHold.value as? [String: AnyObject], HandleDataRequest.handleCashPaymentVerificationNode(firebaseData: dictionaryCashCheck) == true {
                        let cashVerification = CashPaymentVerification()
                        cashVerification.setValuesForKeys(dictionaryCashCheck)
                        if let checkData = cashVerification.isCashPaymentAllowed {
                            if checkData == "YES" {
                                //check here
                                self.payFoBookingButtonWithCash.isEnabled = true
                                self.payFoBookingButtonWithCash.isHidden = false
                            }
                        }
                    }
                })
            }
        }
    }
    
    func getCardPaymentVerification(){
        DispatchQueue.global(qos: .background).async {
            if let shopID = self.BarberShopUUID{
                let firebaseReferenceCashPay = Database.database().reference()
                firebaseReferenceCashPay.child("creditCardPaymentVerification").child(shopID).observeSingleEvent(of: .value, with: { (snapshotDataHold) in
                    if let dictionaryCashCheck = snapshotDataHold.value as? [String: AnyObject], HandleDataRequest.handleCreditCardPaymentVerificationNode(firebaseData: dictionaryCashCheck) == true {
                        let cashVerification = CreditCardPaymentVerification()
                        cashVerification.setValuesForKeys(dictionaryCashCheck)
                        if let checkData = cashVerification.isCreditCardPaymentAllowed {
                            if checkData == "YES" {
                                //check here
                                print("Mango run wild")
                                self.payFoBookingButton.isEnabled = true
                                self.payFoBookingButton.isHidden = false
                                
                                self.payFoSavedCardButton.isEnabled = true
                                self.payFoSavedCardButton.isHidden = false
                            }
                        }
                    }
                })
            }
        }
    }
    
    func getBarberShopName(){
        DispatchQueue.global(qos: .background).async {
            if let shopID = self.BarberShopUUID{
                let firebaseDatabaseRefenceBarberShop = Database.database().reference()
                firebaseDatabaseRefenceBarberShop.child("users").child(shopID).observeSingleEvent(of: .value, with: { (snapshotBarberShopUUID) in
                    if let dictionaryBarberShop = snapshotBarberShopUUID.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryBarberShop) {
                        let barberShopDetails = BarberShop()
                        barberShopDetails.setValuesForKeys(dictionaryBarberShop)
                        if let shopDeails = barberShopDetails.companyName {
                            DispatchQueue.main.async {
                                self.barberShopNamePlaceHolder.text = shopDeails
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func getBookingDetails(){
        DispatchQueue.global(qos: .background).async {
            if let shopID = self.BarberShopUUID, let bookingID = self.bookingUUID {
                let firebaseDatabaseRefence = Database.database().reference()
                firebaseDatabaseRefence.child("bookings").child(shopID).child(bookingID).observeSingleEvent(of: .value, with: { (snapshotBookingDeatails) in
                    if let dictionaryDetails = snapshotBookingDeatails.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryDetails) == true {
                        let bookDetails = Bookings()
                        bookDetails.setValuesForKeys(dictionaryDetails)
                        if let serviceID = bookDetails.bookedServiceID, let confirmedTotal = bookDetails.ConfirmedTotalPrice, let barberIDAX = bookDetails.bookedBarberID {
                            firebaseDatabaseRefence.child("service").child(shopID).child(serviceID).observeSingleEvent(of: .value, with: { (snapshotService) in
                                if let dictionaryService = snapshotService.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionaryService) == true {
                                    let bookService = Service()
                                    bookService.setValuesForKeys(dictionaryService)
                                    self.bookedServiceArry.append(bookService)
                                    self.getBarberName(barberID: barberIDAX)
                                    if let tprDouble = Double(confirmedTotal) {
                                        let totalPrice = self.addCompanyCharges(tPriceDouble: tprDouble)
                                        let bookServiceNext = Service()
                                        bookServiceNext.serviceCost = String(totalPrice - tprDouble)
                                        bookServiceNext.serviceTitle = "Gebyr"
                                        self.bookedServiceArry.append(bookServiceNext)
                                        let totalPriceIntHold = Int(totalPrice)
                                        self.totalPricePlaceHolder.text = String(totalPriceIntHold)
                                    }
                                    DispatchQueue.main.async {
                                        self.collectionView.reloadData()
                                    }
                                }
                            }, withCancel: nil)
                        }
                    }
                }, withCancel: nil)
                
            }
        }
    }
    
    func addCompanyCharges(tPriceDouble: Double)-> Double{
        let percentageAmount = tPriceDouble * 0.10
        let totalAmount = tPriceDouble + percentageAmount
        return totalAmount
    }
    
    func getBarberName(barberID: String){
        DispatchQueue.global(qos: .background).async {
            
            let firebaseDatabaseRefenceBarber = Database.database().reference()
            firebaseDatabaseRefenceBarber.child("users").child(barberID).observeSingleEvent(of: .value, with: { (snapshotBarberUUID) in
                if let dictionaryBarber = snapshotBarberUUID.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryBarber) == true {
                    let barberDetails = Barber()
                    barberDetails.setValuesForKeys(dictionaryBarber)
                    if let shopDeails = barberDetails.lastName {
                        DispatchQueue.main.async {
                            self.barberNamePlaceHolder.text = shopDeails
                        }
                    }
                }
            }, withCancel: nil)
            
        }
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 5).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(headerNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberNamePlaceHolder)
        
        headerNamePlaceHolder.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        headerNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        headerNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        headerNamePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: headerNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberNamePlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
    }
    
    func setupViews(){
        
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 325)
        
        
        scrollView.addSubview(paymentDetailsContainerView)
        scrollView.addSubview(buttonContanerView)
        
        paymentDetailsContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        paymentDetailsContainerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        paymentDetailsContainerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        paymentDetailsContainerView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        paymentDetailsContainerView.addSubview(paymentMethodsImageView)
        paymentDetailsContainerView.addSubview(headerViewDescriptionPlaceHolder)
        paymentDetailsContainerView.addSubview(collectView)
        paymentDetailsContainerView.addSubview(collectionViewSeperatorView)
        paymentDetailsContainerView.addSubview(costTotalContanerView)
        
        paymentMethodsImageView.topAnchor.constraint(equalTo: paymentDetailsContainerView.topAnchor).isActive = true
        paymentMethodsImageView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        paymentMethodsImageView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        paymentMethodsImageView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: paymentMethodsImageView.bottomAnchor, constant: 10).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        collectView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor).isActive = true
        collectView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        collectionViewSeperatorView.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 10).isActive = true
        collectionViewSeperatorView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        collectionViewSeperatorView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, constant: -24).isActive = true
        collectionViewSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        costTotalContanerView.topAnchor.constraint(equalTo: collectionViewSeperatorView.bottomAnchor, constant: 10).isActive = true
        costTotalContanerView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        costTotalContanerView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, constant: -24).isActive = true
        costTotalContanerView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        costTotalContanerView.addSubview(totalTextHeaderPlaceHolder)
        costTotalContanerView.addSubview(totalPricePlaceHolder)
        
        totalTextHeaderPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTextHeaderPlaceHolder.leftAnchor.constraint(equalTo: costTotalContanerView.leftAnchor).isActive = true
        totalTextHeaderPlaceHolder.widthAnchor.constraint(equalToConstant: 70).isActive = true
        totalTextHeaderPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalPricePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalPricePlaceHolder.rightAnchor.constraint(equalTo: costTotalContanerView.rightAnchor).isActive = true
        totalPricePlaceHolder.widthAnchor.constraint(equalToConstant: 75).isActive = true
        totalPricePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        buttonContanerView.topAnchor.constraint(equalTo: paymentDetailsContainerView.bottomAnchor, constant: 10).isActive = true
        buttonContanerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        buttonContanerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        buttonContanerView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        buttonContanerView.addSubview(payFoBookingButton)
//        self.scrollView.addSubview(self.lblPayForBooking)
//        self.scrollView.addSubview(self.imgPayForBooking)
        
        buttonContanerView.addSubview(payFoBookingButtonWithCash)
        
        payFoBookingButton.topAnchor.constraint(equalTo: buttonContanerView.topAnchor).isActive = true
        payFoBookingButton.leftAnchor.constraint(equalTo: buttonContanerView.leftAnchor).isActive = true
        payFoBookingButton.widthAnchor.constraint(equalTo: buttonContanerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        payFoBookingButton.heightAnchor.constraint(equalTo: buttonContanerView.heightAnchor).isActive = true
        
//        self.lblPayForBooking.leftAnchor.constraint(equalTo: payFoBookingButton.leftAnchor).isActive = true
//        self.lblPayForBooking.centerXAnchor.constraint(equalTo: self.payFoBookingButton.centerXAnchor).isActive = true
//
//        self.imgPayForBooking.topAnchor.constraint(equalTo: payFoBookingButton.topAnchor).isActive = true
//        self.imgPayForBooking.heightAnchor.constraint(equalTo: payFoBookingButton.heightAnchor).isActive = true
//
//        self.imgPayForBooking.frame = CGRect(x: self.imgPayForBooking.frame.size.width * 0.7, y: self.imgPayForBooking.frame.origin.y, width: self.payFoBookingButton.frame.size.height, height: self.payFoBookingButton.frame.size.height)
        
        payFoBookingButtonWithCash.topAnchor.constraint(equalTo: buttonContanerView.topAnchor).isActive = true
        payFoBookingButtonWithCash.rightAnchor.constraint(equalTo: buttonContanerView.rightAnchor).isActive = true
        payFoBookingButtonWithCash.widthAnchor.constraint(equalTo: buttonContanerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        payFoBookingButtonWithCash.heightAnchor.constraint(equalTo: buttonContanerView.heightAnchor).isActive = true
        
        
        self.scrollView.addSubview(self.payFoSavedCardButton)
        
        self.payFoSavedCardButton.topAnchor.constraint(equalTo: self.buttonContanerView.bottomAnchor, constant: 10).isActive = true
        self.payFoSavedCardButton.centerXAnchor.constraint(equalTo: self.scrollView.centerXAnchor).isActive = true
        self.payFoSavedCardButton.widthAnchor.constraint(equalTo: self.buttonContanerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        self.payFoSavedCardButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
    }
    
    @objc func moveToPaymentView(_ isSaveCard : Bool) {
        if let shopID = self.BarberShopUUID, let bookingIID = self.bookingUUID, let totalMainPrice = self.totalPricePlaceHolder.text {
            let fireBaseAddTotalPrice = Database.database().reference()
            fireBaseAddTotalPrice.child("bookings").child(shopID).child(bookingIID).child("totalPriceWithCharges").setValue(totalMainPrice)
            let paymentView = PaymentViewController()
            paymentView.BarberShopUUID = shopID
            paymentView.bookingUUID = bookingIID
            paymentView.isSaveCard  = isSaveCard
            paymentView.arrAllCards = self.arrAllCards
            paymentView.strCardID   = self.strCardID
            
            paymentView.didBackButtonPressed = { () in
                self.handleDismissView()
                self.bookingDateViewDataObject.handleDismsiss()
            }
            
            let navController = UINavigationController(rootViewController: paymentView)
            present(navController, animated: true, completion: nil)
        }
    }
    
    @objc func moveToPaymentWithCashView(){
        if let shopID = self.BarberShopUUID, let bookingIID = self.bookingUUID, let totalMainPrice = self.totalPricePlaceHolder.text {
            let modalAccpetDeclineViewController = CashPaymentVerifyViewController()
            modalAccpetDeclineViewController.barberShopUniqueID = shopID
            modalAccpetDeclineViewController.bookingUUID = bookingIID
            modalAccpetDeclineViewController.totalPriceWithCharges = totalMainPrice
            modalAccpetDeclineViewController.shoppingListViewDataObjectConstant = self
            modalAccpetDeclineViewController.modalPresentationStyle = .overCurrentContext
            present(modalAccpetDeclineViewController, animated: true, completion: nil)
        }
    }
    
    @objc func checkAlertPresent(){
        print("red moom")
        
        let firebaseReferenceDataBooking = Database.database().reference()
        
        self.bookingDateViewDataObject.handleDismsiss()
        
        if let shopID = self.BarberShopUUID {
            
            print("shopID            :: \(shopID)")
            print("self.bookingUUID! :: \(self.bookingUUID!)")
            
            UserDefaults.standard.set(false, forKey: "numberOfNewBookingsHasChanged")
            firebaseReferenceDataBooking.child("bookings").child(shopID).child(self.bookingUUID!).removeValue(completionBlock: { (errorrr, databaseReference) in

                if let error = errorrr {
                    print("Error : \(error.localizedDescription)")
                }
                
            })
            
            if self.didShoppingVCBackButtonPressedBlock != nil {
                self.didShoppingVCBackButtonPressedBlock()
            }
        }

        
        if let netAvail = UserDefaults.standard.object(forKey: "alertViewForMobileNumbersPresentedShoppingListView") as? Bool{
            if netAvail {
                print("supposed to work")
                self.dismiss(animated: true, completion: nil) 
                self.handleDismissView()
            } else {
                print("off set work")
                self.handleDismissView()
            }
        }
    }
    
    @objc func handleDismissView(){
        self.bookingDateViewDataObject.handleDismsiss()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleShowActionSheet(sender: UIButton){
        
        let alertController = UIAlertController(title: nil, message: NSLocalizedString("actionSheetMessageShoppingListView", comment: "Please enter your mobile number, to ensure the BarberShop can contact you."), preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = NSLocalizedString("actionSheetTextPlaceHoldeShoppingListView", comment: "Mobile number")
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .default) { action in
            // ...
            UserDefaults.standard.set(false, forKey: "alertViewForMobileNumbersPresentedShoppingListView")
        }
        
        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default) { action in
            // ...
            if let loginTextField = alertController.textFields![safe: 0], let numberText = loginTextField.text {
                
                
                if let uid = Auth.auth().currentUser?.uid {
                    let firebaseUpdateMobileNumber = Database.database().reference()
                    firebaseUpdateMobileNumber.child("users").child(uid).child("mobileNumber").setValue(numberText)
                    UserDefaults.standard.set(false, forKey: "alertViewForMobileNumbersPresentedShoppingListView")
                   
                    switch sender.tag {
                    case 0:
                        self.moveToPaymentView(false)
//                        self.moveToPaymentView()
                    case 1:
                        self.moveToPaymentWithCashView()
                        
                    case 2:
                        if let strCardID = UserDefaults.standard.value(forKey: "savedCardID") as? String {
                            
                            let isContain = self.arrAllCards!.array?.contains(where: { (json) -> Bool in
                                print("json :: \(json)")
                                return json["id"].stringValue == strCardID
                            })
                            
                            if isContain! {
//                                    self.savedCardAlert()
                                self.strCardID = strCardID
                                self.moveToPaymentView(false)
                            }
                            else {
                                self.arrAllCards = nil
                                self.saveCardAlert()
                            }
                        } else {
                            self.arrAllCards = nil
                            self.saveCardAlert()
                        }
                        
                    default:
                        print("Sky High")
                    }

                }
            }
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
            UserDefaults.standard.set(true, forKey: "alertViewForMobileNumbersPresentedShoppingListView")
        }
    }
    
    
    func saveCardAlert() {
        let alertController = UIAlertController(title: nil, message: NSLocalizedString("actionSheetMessageSaveCard", comment: "Do you want save your card info?"), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("noButtonAlertOrderDetail", comment: "No"), style: .default) { action in
//            self.moveToPaymentView(false)
        }
        
        let OKAction = UIAlertAction(title: NSLocalizedString("yesButtonAlertOrderDetail", comment: "Yes"), style: .default) { action in
            self.moveToPaymentView(true)
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

    func savedCardAlert() {
        let alertController = UIAlertController(title: nil, message: NSLocalizedString("actionSheetMessageSavedCard", comment: "Do you want to pay from saved card?"), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .default) { action in
//            self.moveToPaymentView(false)
        }
        
        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default) { action in
            self.moveToPaymentView(true)
        }
        
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getAllCards() {
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
//                "authorization": "Basic ef52f58a03b8180bc4ff8db95ce0345a71a75f58fa6e7b8da40bd1fbe3e8691e=",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "9e5f5b15-dd1b-041c-a26d-eddc40cdadcc"
            ]
            
            
            let dataPostURLString = "https://api.quickpay.net/cards"
            
            let request = NSMutableURLRequest(url: NSURL(string: dataPostURLString)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
            
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                
                //                self.payFoBookingButtonWithCash.isEnabled = true
                //                self.payFoBookingButtonWithCash.isEnabled = true
                
                if let errorrr = error {
                    print("Could make post",errorrr.localizedDescription)
                } else {
                    if let jsData = data {
                        do {
                            let jsonData = try JSON(data: jsData)
                            self.arrAllCards = jsonData
                            
                        } catch {
                            print("error in getCardAuthorizationLink")
                        }
                    }
                }
            })
            dataTask.resume()
        }
        
    }

}



class customShoppingListCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let serviceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.white
        return sv
    }()
    
    func setupViews(){
        addSubview(serviceNamePlaceHolder)
        addSubview(servicePricePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|-5-[v0(100)]-15-[v1(100)]-5-|", views: serviceNamePlaceHolder, servicePricePlaceHolder)
        addContraintsWithFormat(format: "V:|[v0]-5-[v1(1)]|", views: serviceNamePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|[v0]-5-[v1(1)]|", views: servicePricePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
