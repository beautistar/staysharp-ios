//
//  DateByUserDeviceInitializer.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/27/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class DateByUserDeviceInitializer {
    static var tzone: String { return TimeZone.current.identifier }
    static var calenderNow: String { return "\(Calendar.current.identifier)" }
    static var localCode: String { return "\(Locale.current.identifier)"} //Locale.preferredLanguages[0] //Locale.current.identifier
    static var currencyCode: String {return "\(String(describing: Locale.current.currencyCode))"}
    
    
    static func getRegion(TZoneName: String, calenName: String, LocName: String)-> Region{
        switch calenName {
        case "gregorian":
            let rangeData = Region(calendar: Calendars.gregorian, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "buddhist":
            let rangeData = Region(calendar: Calendars.buddhist, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "chinese":
            let rangeData = Region(calendar: Calendars.chinese, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "coptic":
            let rangeData = Region(calendar: Calendars.coptic, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "ethiopicAmeteMihret":
            let rangeData = Region(calendar: Calendars.ethiopicAmeteMihret, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "hebrew":
            let rangeData = Region(calendar: Calendars.hebrew, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "iso8601":
            let rangeData = Region(calendar: Calendars.iso8601, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "indian":
            let rangeData = Region(calendar: Calendars.indian, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "islamic":
            let rangeData = Region(calendar: Calendars.islamic, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "islamicCivil":
            let rangeData = Region(calendar: Calendars.islamicCivil, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "japanese":
            let rangeData = Region(calendar: Calendars.japanese, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "persian":
            let rangeData = Region(calendar: Calendars.persian, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "republicOfChina":
            let rangeData = Region(calendar: Calendars.republicOfChina, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "islamicTabular":
            let rangeData = Region(calendar: Calendars.islamicTabular, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        case "islamicUmmAlQura":
            let rangeData = Region(calendar: Calendars.islamicUmmAlQura, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        default:
            print("Sky High")
            let rangeData = Region(calendar: Calendars.gregorian, zone: Zones(rawValue: TZoneName)!, locale: Locales(rawValue: LocName)!)
            return rangeData
        }
    }
}
