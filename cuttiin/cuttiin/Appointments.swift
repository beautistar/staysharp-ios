//
//  Appointments.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/20/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

@objcMembers class Appointments: NSObject {
    var profileImageUrl: String?
    var profileImage: UIImage?
    var clientName: String?
    var bookingStartTime = DateInRegion()
    var bookingStartTimeString: String?
    var bookingEndTime = DateInRegion()
    var bookingEndTimeString: String?
    var bookedBarberName: String?
    var bookedBarberUUID: String?
    var bookedServicePrice: String?
    var bookingUniqueID: String?
}
