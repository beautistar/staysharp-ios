//
//  WorkHourButtons.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class WorkHourButtons: NSObject {
    var dayOfTheWeek: String?
    var openingClosingTimeString: String?
}
