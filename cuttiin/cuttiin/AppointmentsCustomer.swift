//
//  AppointmentsCustomer.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/2/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

@objcMembers class AppointmentsCustomer: NSObject {
    var serviceImageUrl: String?
    var bookedBarberShopName: String?
    var bookingUniqueID: String?
    var bookingTotalTime: String?
    var bookingStartTime = DateInRegion()
    var bookingStartTimeString: String?
    var bookingCalendar: String?
    var bookingLocal: String?
    var bookingTimeZone: String?
    var barberShopUniqueID: String?
    var serviceImage: UIImage?
}
