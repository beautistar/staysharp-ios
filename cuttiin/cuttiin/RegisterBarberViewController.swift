//
//  RegisterBarberViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/17/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Navajo_Swift
import Firebase
import SwiftDate

class RegisterBarberViewController: UIViewController, UITextFieldDelegate {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var passwordValid = false
    
    let inputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    let passwordHiddenPlaceHolder: UILabel = {
        let php = UILabel()
        php.translatesAutoresizingMaskIntoConstraints = false
        php.text = NSLocalizedString("passwordLabelTextCustomerRegistrationView", comment: "password must have 8 characters or more with characters and digits")
        php.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        php.textColor = UIColor(r: 129, g: 129, b: 129)
        php.isHidden = true
        php.adjustsFontSizeToFitWidth = true
        php.minimumScaleFactor = 0.1
        php.baselineAdjustment = .alignCenters
        php.textAlignment = .left
        return php
    }()
    
    let passwordTextField: UITextField = {
        let ps = UITextField()
        ps.translatesAutoresizingMaskIntoConstraints = false
        ps.textColor = UIColor.black
        ps.placeholder = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
        ps.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ps.isSecureTextEntry = true
        ps.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingDidBegin)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingChanged)
        ps.addTarget(self, action: #selector(isValidPassword), for: .editingChanged)
        return ps
    }()
    
    lazy var showPassordTextFieldButton: UIButton = {
        let sptfb = UIButton()
        sptfb.translatesAutoresizingMaskIntoConstraints = false
        sptfb.isUserInteractionEnabled = false
        sptfb.setTitle(NSLocalizedString("showButtonTitleLoginView", comment: "Show"), for: .normal)
        sptfb.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        sptfb.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
        sptfb.backgroundColor = UIColor.white
        sptfb.addTarget(self, action: #selector(handleShowPasswordText), for: .touchUpInside)
        return sptfb
    }()
    
    let passwordSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return view
    }()
    
//    let cvrNumberHiddenPlaceHolder: UILabel = {
//        let cvrhp = UILabel()
//        cvrhp.translatesAutoresizingMaskIntoConstraints = false
//        cvrhp.text = "CVR number (optional)"
//        cvrhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
//        cvrhp.textColor = UIColor(r: 129, g: 129, b: 129)
//        cvrhp.isHidden = true
//        cvrhp.adjustsFontSizeToFitWidth = true
//        cvrhp.minimumScaleFactor = 0.1
//        cvrhp.baselineAdjustment = .alignCenters
//        cvrhp.textAlignment = .left
//        return cvrhp
//    }()
    
//    let cvrNumberTextField: UITextField = {
//        let cvrtf = UITextField()
//        cvrtf.translatesAutoresizingMaskIntoConstraints = false
//        cvrtf.textColor = UIColor.black
//        cvrtf.placeholder = "CVR Number"
//        cvrtf.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
//        cvrtf.keyboardType = .numberPad
//        cvrtf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
//        cvrtf.addTarget(self, action: #selector(cvrTextFieldDidChange), for: .editingDidBegin)
//        cvrtf.addTarget(self, action: #selector(cvrTextFieldDidChange), for: .editingChanged)
//        return cvrtf
//    }()
//    
//    let cvrNumberSeperatorView: UIView = {
//        let cvrsv = UIView()
//        cvrsv.translatesAutoresizingMaskIntoConstraints = false
//        cvrsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
//        return cvrsv
//    }()
    
    let mobileNumberInputView: UIView = {
        let mniv = UIView()
        mniv.translatesAutoresizingMaskIntoConstraints = false
        return mniv
    }()
    
    let mobileNumberHiddenPlaceHolder: UILabel = {
        let cvrhp = UILabel()
        cvrhp.translatesAutoresizingMaskIntoConstraints = false
        cvrhp.text = NSLocalizedString("phoneNumberTextRegistrationView", comment: "Phone Number")
        cvrhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        cvrhp.textColor = UIColor(r: 129, g: 129, b: 129)
        cvrhp.isHidden = true
        cvrhp.adjustsFontSizeToFitWidth = true
        cvrhp.minimumScaleFactor = 0.1
        cvrhp.baselineAdjustment = .alignCenters
        cvrhp.textAlignment = .left
        return cvrhp
    }()
    
    let mobileNumberTextField: UITextField = {
        let mntf = UITextField()
        mntf.translatesAutoresizingMaskIntoConstraints = false
        mntf.textColor = UIColor.black
        mntf.placeholder = NSLocalizedString("phoneNumberTextRegistrationView", comment: "Phone Number")
        mntf.textAlignment = .left
        mntf.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        mntf.keyboardType = .phonePad
        mntf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        mntf.addTarget(self, action: #selector(mobileNumberTextFieldDidChange), for: .editingDidBegin)
        mntf.addTarget(self, action: #selector(mobileNumberTextFieldDidChange), for: .editingChanged)
        return mntf
    }()
    
    let mobileNumberSeperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black
        return view
    }()
    
    lazy var registerButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("continueButtonTextRegistrationView", comment: "Continue"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
        st.addTarget(self, action: #selector(handleRegisterBarbershop), for: .touchUpInside)
        st.isEnabled = false
        return st
    }()
    
    lazy var termsAndConditionsButtonLabel: UILabel = {
        let dtv = UILabel()
        dtv.translatesAutoresizingMaskIntoConstraints = false
        dtv.textAlignment = .center
        dtv.numberOfLines = 0
        dtv.isUserInteractionEnabled = true
        dtv.text = NSLocalizedString("termsAndConditionCustomerRegistrationView", comment: "By hitting Register you accept the Terms and Conditions!")
        dtv.font = UIFont(name: "HelveticaNeue", size: 9.0)
        dtv.textColor = UIColor(r: 23, g: 69, b: 90)
        dtv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowWebPopUPWebTermsandCondition)))
        return dtv
    }()
    
    lazy var alreadyAMemberButtonLabel: UILabel = {
        let dtv = UILabel()
        dtv.translatesAutoresizingMaskIntoConstraints = false
        dtv.textAlignment = .center
        dtv.numberOfLines = 0
        dtv.isUserInteractionEnabled = true
        dtv.text = NSLocalizedString("alreadyAMemberButtonTitleCustomerRegistrationView", comment: "Already a member? Log in")
        dtv.font = UIFont(name: "HelveticaNeue", size: 11.0)
        dtv.textColor = UIColor(r: 23, g: 69, b: 90)
        
        dtv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleLoginAction)))
        return dtv
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        emhp.textColor = UIColor(r: 129, g: 129, b: 129)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("naviagtionTitleBarberRegistrationView", comment: "Barbershop Registration")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(inputsContainerView)
        view.addSubview(registerButton)
        view.addSubview(termsAndConditionsButtonLabel)
        view.addSubview(alreadyAMemberButtonLabel)
        view.addSubview(errorMessagePlaceHolder)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        self.mobileNumberTextField.delegate = self
        setupInputContainerViews()
        setupButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.emailTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.emailTextField.text = ""
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordTextField.text = ""
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberTextField.text = ""
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
    }
    
    @objc func textFieldDidChange(){
        if emailTextField.text == "" || passwordTextField.text == "" || mobileNumberTextField.text == "" {
            //Disable button
            self.showPassordTextFieldButton.isUserInteractionEnabled = true
            self.passwordTextField.isSecureTextEntry = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
            registerButton.isEnabled = false
            registerButton.setTitleColor(UIColor.black, for: .normal)
            registerButton.backgroundColor = UIColor.clear
            
        } else {
            //Enable button
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
            //self.checkNumberOfCharactersInMobileNumber()
            if emailValid == true && passwordValid == true {
                registerButton.isEnabled = true
                registerButton.setTitleColor(UIColor.white, for: .normal)
                registerButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                self.showPassordTextFieldButton.isUserInteractionEnabled = true
                self.checkNumberOfCharactersInMobileNumber()
                
            }
        }
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.passwordSeperatorView.backgroundColor = UIColor.black
        //self.cvrNumberSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.emailHiddenPlaceHolder.isHidden = false
        self.passwordHiddenPlaceHolder.isHidden = true
        //self.cvrNumberHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        
    }
    
    @objc func passwordtextFieldDidChange(){
        self.passwordSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.emailSeperatorView.backgroundColor = UIColor.black
        //self.cvrNumberSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.passwordHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        //self.cvrNumberHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
//    func cvrTextFieldDidChange(){
//        self.cvrNumberSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
//        self.passwordSeperatorView.backgroundColor = UIColor.black
//        self.emailSeperatorView.backgroundColor = UIColor.black
//        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
//        self.cvrNumberHiddenPlaceHolder.isHidden = false
//        self.passwordHiddenPlaceHolder.isHidden = true
//        self.emailHiddenPlaceHolder.isHidden = true
//        self.mobileNumberHiddenPlaceHolder.isHidden = true
//        
//    }
    
    @objc func mobileNumberTextFieldDidChange(){
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.emailSeperatorView.backgroundColor = UIColor.black
        //self.cvrNumberSeperatorView.backgroundColor = UIColor.black
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        //self.cvrNumberHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func isValidPassword(){
        let password = passwordTextField.text ?? ""
        let strength = Navajo.strength(ofPassword: password)
        let strengthValue = Navajo.localizedString(forStrength: strength)
        
        if (strengthValue == "Strong" || strengthValue == "Very Strong" || strengthValue == "Reasonable"){
            self.passwordValid = true
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
        }else{
            self.passwordValid = false
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
            registerButton.isEnabled = false
            registerButton.setTitleColor(UIColor.black, for: .normal)
            registerButton.backgroundColor = UIColor.clear
        }
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailNotFormattedProperlyPlaceholderLoginView", comment: "Email is not properly formatted")
            registerButton.isEnabled = false
            registerButton.setTitleColor(UIColor.black, for: .normal)
            registerButton.backgroundColor = UIColor.clear
        }
    }
    
    func checkNumberOfCharactersInMobileNumber(){
        if let character = self.mobileNumberTextField.text {
            if character.count  >= 6 {
                self.registerButton.isEnabled = true
                registerButton.setTitleColor(UIColor.white, for: .normal)
                registerButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
            }else {
                self.registerButton.isEnabled = false
                registerButton.isEnabled = false
                registerButton.setTitleColor(UIColor.black, for: .normal)
                registerButton.backgroundColor = UIColor.clear
            }
            
        }
    }
    
    @objc func handleShowWebPopUPWebTermsandCondition(){
        guard let url = URL(string: "https://teckdk.com/betingelser/") else {
        return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func handleLoginAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleShowPasswordText(){
        passwordTextField.isSecureTextEntry = false
    }
    
    @objc func handleBackAction(){
        self.hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleRegisterBarbershop(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let emailtext = emailTextField.text, let password = passwordTextField.text, let mobileNumber = mobileNumberTextField.text, let crCode = Locale.current.currencyCode  else {
            print("error in guard statement registration")
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            return
        }
        
        Auth.auth().createUser(withEmail: emailtext, password: password) { (authUserDataHold, erroroorrr) in
            if let error = erroroorrr {
                UIApplication.shared.endIgnoringInteractionEvents()
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                
                if let errCode = AuthErrorCode(rawValue: error._code){
                    switch errCode {
                    case .networkError:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                    case .invalidEmail:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAddressMalformedErrorCodeLoginView", comment: "email address is malformed")
                    case .operationNotAllowed:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care")
                    case .emailAlreadyInUse:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAlreadyInUseErrorCodeLoginView", comment: "email already in use by an existing account")
                    case .weakPassword:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("weakPasswordErrorCodeCustomerRegistrationView", comment: "weak password")
                    default:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                        
                    }
                }
                self.hideKeyboard()
                return
            }
            
            guard let firebaseUser = authUserDataHold?.user else {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            
            firebaseUser.sendEmailVerification(completion: { (errorrrwewe) in
                if let error = errorrrwewe {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let errCode = AuthErrorCode(rawValue: error._code){
                        switch errCode {
                        case .networkError:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                        case .userNotFound:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("accountNotFoundErrorCodeLoginView", comment: "Account not found")
                        default:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                            
                        }
                    }
                    self.hideKeyboard()
                    
                    firebaseUser.delete(completion: { (errorrr) in
                        if let error = errorrr {
                            print(error.localizedDescription)
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        do {
                            try Auth.auth().signOut()
                        } catch let logoutError {
                            print(logoutError)
                        }
                    })
                    return
                }
                let uid = firebaseUser.uid
                
                let newDate = DateInRegion()
                let strDate = newDate.toString(.extended)
                let userTimezone = DateByUserDeviceInitializer.tzone
                let userCalender = DateByUserDeviceInitializer.calenderNow
                let userLocal = DateByUserDeviceInitializer.localCode
                
                
                let newUserLocal = "en"
                let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                let newStrDate = newUserDate.toString(.extended)
                
                let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                
                let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
                let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                
                //sucessfully logged in
                let ref = Database.database().reference()
                let userReference = ref.child("users").child(uid)
                let values = ["uniqueID": uid,"companyName":"nil", "email": emailtext,"role": "barberShop","companyLogoImageUrl":"nil", "mobileNumber": mobileNumber,"rating": "0","companyLogoImageName":"nil","companyCoverPhotoName":"nil","companyCoverPhotoUrl":"nil", "accountNumerRegNr":"nil","accountNumerKontoNr":"nil","companyFormattedAddress": "nil", "companyAddressLongitude": "nil", "companyAddressLatitude": "nil", "dateAccountCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe, "currencyCode": crCode]
                userReference.updateChildValues(values, withCompletionBlock: { (errorr, ref) in
                    if let error = errorr {
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        print(error.localizedDescription)
                        return
                    }else{
                        self.createPersonalBookingCounter(barberShopID: uid)
                        self.createBarberCashPaymentVerification(barberShopID: uid)
                        self.createBarberCreditCardPaymentVerification(barberShopID: uid)
                        self.createBarberShopVerification(barberShopID: uid)
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.handlePresentBarberChoosePictureView()
                    }
                })
            })
        }
        
    }
    
    func createPersonalBookingCounter(barberShopID: String){
        let fireBaseNumberOfBooking = Database.database().reference()
        let valuesz = ["numberOfBookings": 0]
        fireBaseNumberOfBooking.child("totalNumberOfBookings").child(barberShopID).updateChildValues(valuesz) { (errorrrrrrrrrr, dataReff) in
            if let error = errorrrrrrrrrr {
                print(error.localizedDescription)
                return
            }
            print("counter sucessfully created")
        }
    }
    
    func createBarberCashPaymentVerification(barberShopID: String) {
        
        let newDate = DateInRegion()
        let strDate = newDate.toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        let newUserLocal = "en"
        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
        let newStrDate = newUserDate.toString(.extended)
        
        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
        
        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
        
        let firebaseReferencePaymentVerification = Database.database().reference()
        let acshValue = ["barberShopID": barberShopID, "isCashPaymentAllowed": "NO", "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
        firebaseReferencePaymentVerification.child("cashPaymentVerification").child(barberShopID).updateChildValues(acshValue) { (errorrrr, dataRefef) in
            if let error = errorrrr {
                print(error.localizedDescription)
                return
            }else {
                print("Data not posted")
            }
        }
    }
    
    func createBarberCreditCardPaymentVerification(barberShopID: String) {
        
        let newDate = DateInRegion()
        let strDate = newDate.toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        let newUserLocal = "en"
        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
        let newStrDate = newUserDate.toString(.extended)
        
        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
        
        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
        
        let firebaseReferencePaymentVerification = Database.database().reference()
        let acshValue = ["barberShopID": barberShopID, "isCreditCardPaymentAllowed": "NO", "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
        firebaseReferencePaymentVerification.child("creditCardPaymentVerification").child(barberShopID).updateChildValues(acshValue) { (errorrrr, dataRefef) in
            if let error = errorrrr {
                print(error.localizedDescription)
                return
            }else {
                print("Data not posted")
            }
        }
    }
    
    func createBarberShopVerification(barberShopID: String) {
        
        let newDate = DateInRegion()
        let strDate = newDate.toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        
        let newUserLocal = "en"
        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
        let newStrDate = newUserDate.toString(.extended)
        
        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
        
        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
        
        let firebaseReferencePaymentVerification = Database.database().reference()
        let acshValue = ["barberShopID": barberShopID, "isBarberShopVerified": "NO", "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
        firebaseReferencePaymentVerification.child("barberShopVerification").child(barberShopID).updateChildValues(acshValue) { (errorrrr, dataRefef) in
            if let error = errorrrr {
                print(error.localizedDescription)
                return
            }else {
                print("Data not posted")
            }
        }
    }
    
    func handlePresentBarberChoosePictureView(){
        registerButton.isEnabled = false
        registerButton.setTitleColor(UIColor.black, for: .normal)
        registerButton.backgroundColor = UIColor.clear
        let chooseBarBerPView = ChoosePhotoViewController()
        let navController = UINavigationController(rootViewController: chooseBarBerPView)
        chooseBarBerPView.customerOrBarberChoice = "Barber"
        self.present(navController, animated: true, completion: nil)
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
    }
    
    func setupInputContainerViews(){
        inputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        inputsContainerView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        
        inputsContainerView.addSubview(emailHiddenPlaceHolder)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeperatorView)
        inputsContainerView.addSubview(passwordHiddenPlaceHolder)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(showPassordTextFieldButton)
        inputsContainerView.addSubview(passwordSeperatorView)
        //inputsContainerView.addSubview(cvrNumberHiddenPlaceHolder)
        //inputsContainerView.addSubview(cvrNumberTextField)
        //inputsContainerView.addSubview(cvrNumberSeperatorView)
        inputsContainerView.addSubview(mobileNumberInputView)
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: inputsContainerView.topAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        passwordHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        passwordHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        passwordHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        passwordHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: passwordHiddenPlaceHolder.bottomAnchor).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: inputsContainerView.rightAnchor, constant: -20).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        showPassordTextFieldButton.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        showPassordTextFieldButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        showPassordTextFieldButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        passwordSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordSeperatorView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        passwordSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        passwordSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
//        cvrNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: passwordSeperatorView.bottomAnchor, constant: 5).isActive = true
//        cvrNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
//        cvrNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
//        cvrNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
//        
//        cvrNumberTextField.topAnchor.constraint(equalTo: cvrNumberHiddenPlaceHolder.bottomAnchor).isActive = true
//        cvrNumberTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
//        cvrNumberTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
//        cvrNumberTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
//        
//        cvrNumberSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
//        cvrNumberSeperatorView.topAnchor.constraint(equalTo: cvrNumberTextField.bottomAnchor).isActive = true
//        cvrNumberSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
//        cvrNumberSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        mobileNumberInputView.topAnchor.constraint(equalTo: passwordSeperatorView.bottomAnchor, constant: 5).isActive = true
        mobileNumberInputView.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        mobileNumberInputView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        mobileNumberInputView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        mobileNumberInputView.addSubview(mobileNumberHiddenPlaceHolder)
        //mobileNumberInputView.addSubview(countryFlagImage)
        mobileNumberInputView.addSubview(mobileNumberTextField)
        mobileNumberInputView.addSubview(mobileNumberSeperatorView)
        
        mobileNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: mobileNumberInputView.topAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: mobileNumberInputView.centerXAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        mobileNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: mobileNumberInputView.widthAnchor).isActive = true
        
//        countryFlagImage.topAnchor.constraint(equalTo: mobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
//        countryFlagImage.leftAnchor.constraint(equalTo: mobileNumberInputView.leftAnchor).isActive = true
//        countryFlagImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        countryFlagImage.heightAnchor.constraint(equalTo: mobileNumberInputView.heightAnchor, constant: -15).isActive = true
        
        mobileNumberTextField.topAnchor.constraint(equalTo: mobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
        mobileNumberTextField.leftAnchor.constraint(equalTo: mobileNumberInputView.leftAnchor).isActive = true
        mobileNumberTextField.rightAnchor.constraint(equalTo: mobileNumberInputView.rightAnchor).isActive = true
        mobileNumberTextField.heightAnchor.constraint(equalTo: mobileNumberInputView.heightAnchor, constant: -15).isActive = true
        
        mobileNumberSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        mobileNumberSeperatorView.topAnchor.constraint(equalTo: mobileNumberInputView.bottomAnchor).isActive = true
        mobileNumberSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        mobileNumberSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
    }
    
    func setupButtons(){
        registerButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 10).isActive = true
        registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        termsAndConditionsButtonLabel.topAnchor.constraint(equalTo: registerButton.bottomAnchor, constant: 5).isActive = true
        termsAndConditionsButtonLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        termsAndConditionsButtonLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        termsAndConditionsButtonLabel.heightAnchor.constraint(equalToConstant: 25).isActive =  true
        
        alreadyAMemberButtonLabel.topAnchor.constraint(equalTo: termsAndConditionsButtonLabel.bottomAnchor, constant: 5).isActive = true
        alreadyAMemberButtonLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        alreadyAMemberButtonLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -85).isActive = true
        alreadyAMemberButtonLabel.heightAnchor.constraint(equalToConstant: 20).isActive =  true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: alreadyAMemberButtonLabel.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
    }

}
