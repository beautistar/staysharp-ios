//
//  ProfileEditorViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/7/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class ProfileEditorViewController: UIViewController, UIImagePickerControllerDelegate, UITextFieldDelegate, UINavigationControllerDelegate {
    
    let picker = UIImagePickerController()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var passwordValid = false
    
    let imageAndFirstLastContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let firstNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let firstNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let firstNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let lastNameHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let lastNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let lastNameSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    let firstNameLastNameContainerView: UIView = {
        let fnlncview = UIView()
        fnlncview.translatesAutoresizingMaskIntoConstraints = false
        return fnlncview
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    
    let mobileNumberHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("mobileNumberLabelTectProfileEditView", comment: "Mobile Number")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let mobileNumberTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("mobileNumberLabelTectProfileEditView", comment: "Mobile Number")
        em.keyboardType = .phonePad
        em.addTarget(self, action: #selector(mobileNumbertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(mobileNumbertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let mobileNumberSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("navigationTitleTextCustomerProfileView", comment: "Profile")
        picker.delegate = self
        self.mobileNumberTextField.delegate = self
        view.backgroundColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("barButtonTitleText", comment: "Save"), style: .done, target: self, action: #selector(checkAuthenticationType))
        navigationItem.rightBarButtonItem?.isEnabled = false
        view.addSubview(imageAndFirstLastContainerView)
        view.addSubview(emailHiddenPlaceHolder)
        view.addSubview(emailTextField)
        view.addSubview(emailSeperatorView)
        view.addSubview(mobileNumberHiddenPlaceHolder)
        view.addSubview(mobileNumberTextField)
        view.addSubview(mobileNumberSeperatorView)
        view.addSubview(errorMessagePlaceHolder)
        setupViewConstriants()
        getUserData()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    func getUserData(){
        DispatchQueue.global(qos: .background).async {
            if let userUUID = Auth.auth().currentUser?.uid {
                let firebaseReferenceUserDataEdit = Database.database().reference()
                firebaseReferenceUserDataEdit.child("users").child(userUUID).observeSingleEvent(of: .value, with: { (snapshotUserDataEdit) in
                    if let dictionaryUserDataEdit = snapshotUserDataEdit.value as? [String: AnyObject], HandleDataRequest.handleUserCustomerNode(firebaseData: dictionaryUserDataEdit) == true {
                        let userDataEdit = Customer()
                        userDataEdit.setValuesForKeys(dictionaryUserDataEdit)
                        
                        if let firstName = userDataEdit.firstName, let lastName = userDataEdit.lastName, let email = userDataEdit.email, let mobNumber = userDataEdit.mobileNumber, let imgURL = userDataEdit.profileImageUrl, let imageName = userDataEdit.profileImageName {
                            DispatchQueue.main.async {
                                self.firstNameTextField.text = firstName
                                self.lastNameTextField.text = lastName
                                self.emailTextField.text = email
                                self.mobileNumberTextField.text = mobNumber
                                self.selectImageView.loadImagesUsingCacheWithUrlString(urlString: imgURL)
                                UserDefaults.standard.set(imageName, forKey: "profileImageFileNameIncludingJPEGEnding")
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func setupViewConstriants(){
        imageAndFirstLastContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        imageAndFirstLastContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageAndFirstLastContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        imageAndFirstLastContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        imageAndFirstLastContainerView.addSubview(selectImageView)
        imageAndFirstLastContainerView.addSubview(firstNameLastNameContainerView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: imageAndFirstLastContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: imageAndFirstLastContainerView.widthAnchor, multiplier: 0.25).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        firstNameLastNameContainerView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        firstNameLastNameContainerView.rightAnchor.constraint(equalTo: imageAndFirstLastContainerView.rightAnchor).isActive = true
        firstNameLastNameContainerView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.addSubview(firstNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(firstNameTextField)
        firstNameLastNameContainerView.addSubview(firstNameSeperatorView)
        firstNameLastNameContainerView.addSubview(lastNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(lastNameTextField)
        firstNameLastNameContainerView.addSubview(lastNameSeperatorView)
        
        firstNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.topAnchor).isActive = true
        firstNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        firstNameTextField.topAnchor.constraint(equalTo: firstNameHiddenPlaceHolder.bottomAnchor).isActive = true
        firstNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        firstNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        firstNameSeperatorView.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        firstNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        lastNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        lastNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        lastNameTextField.topAnchor.constraint(equalTo: lastNameHiddenPlaceHolder.bottomAnchor).isActive = true
        lastNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        lastNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        lastNameSeperatorView.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        lastNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: emailTextField.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        mobileNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        mobileNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        mobileNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        mobileNumberTextField.topAnchor.constraint(equalTo: mobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
        mobileNumberTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mobileNumberTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        mobileNumberTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        mobileNumberSeperatorView.topAnchor.constraint(equalTo: mobileNumberTextField.bottomAnchor).isActive = true
        mobileNumberSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mobileNumberSeperatorView.widthAnchor.constraint(equalTo: mobileNumberTextField.widthAnchor).isActive = true
        mobileNumberSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: mobileNumberSeperatorView.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func textFieldDidChange(){
        if firstNameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == "" || mobileNumberTextField.text == ""{
            //Disable button
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            navigationItem.rightBarButtonItem?.isEnabled = false
            
        } else {
            //Enable button
            self.isValidEmail()
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            if emailValid == true {
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
    }
    
    @objc func firstNametextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = false
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func lastNametextFieldDidChange(){
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func mobileNumbertextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = false
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailNotFormattedProperlyPlaceholderLoginView", comment: "Email is not properly formatted")
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("takePhotoAlertViewProfileEdit", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotAlertViewProfileEdit", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            self.selectImageView.contentMode = .scaleAspectFit
            self.selectImageView.image = selectedImage
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = NSLocalizedString("errorMessageNoCameraChoosePhotoView", comment: "Sorry, this device has no camera")
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @objc func checkAuthenticationType(){
        if let provider = Auth.auth().currentUser?.providerData{
            for access in provider {
                let accessProvider = access.providerID
                switch accessProvider {
                case "password":
                    self.handleEditProfileData()
                case "facebook.com":
                    self.handleFacebookEditProfileData()
                default:
                    print("Sky high")
                }
            }
        }
    }
    
    func handleFacebookEditProfileData(){
        let user = Auth.auth().currentUser
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        
        if let userCredData = user {
            userCredData.reauthenticateAndRetrieveData(with: credential, completion: { (authDataResult, Errorororor) in
                if let error = Errorororor {
                    self.errorMessagePlaceHolder.text = error.localizedDescription
                } else {
                    // User re-authenticated.
                    //self.handleSaveChanges()
                    self.handleReUploadingData()
                    
                }
            })
        }
    }
    
    func handleEditProfileData(){
        let alert = UIAlertController(title: NSLocalizedString("reauthenticateUserProfileEdit", comment: "Re-authenticate user"), message: NSLocalizedString("reauthenticateUserMessageProfileEdit", comment: "Please Insert old email & password."), preferredStyle: .alert)
        
        // Login button
        let loginAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: { (action) -> Void in
            // Get TextFields text
            
            let usernameTxt = alert.textFields![0]
            let passwordTxt = alert.textFields![1]
            
            guard let email = usernameTxt.text, let currentPassword = passwordTxt.text else {
                self.errorMessagePlaceHolder.text = NSLocalizedString("reauthenticateUserErrorMessageEnterValueProfileEdit", comment: "Did not enter value")
                return
            }
            let user = Auth.auth().currentUser
            let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
            
            // Prompt the user to re-provide their sign-in credentials
            
            if let userCredData = user {
                userCredData.reauthenticateAndRetrieveData(with: credential, completion: { (authDataHold, errororoororo) in
                    if let error = errororoororo {
                        print(error.localizedDescription)
                    } else {
                        // User re-authenticated.
                        self.handleReUploadingData()
                        
                    }
                })
            }
        })
        
        let cancel = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel, handler: { (action) -> Void in })
        
        // Add 1 textField (for username)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardType = .emailAddress
            textField.autocorrectionType = .default
            textField.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
            textField.textColor = UIColor.black
        }
        
        // Add 2nd textField (for password)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardType = .default
            textField.placeholder = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
            textField.isSecureTextEntry = true
            textField.textColor = UIColor.black
        }
        
        // Add action buttons and present the Alert
        alert.addAction(loginAction)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
    }
    
    func handleReUploadingData(){
        if let imageName = UserDefaults.standard.object(forKey: "profileImageFileNameIncludingJPEGEnding") as? String {let storageRef = Storage.storage().reference().child("profileImages")
            
            let desertRef = storageRef.child(imageName)
            
            desertRef.delete { error in
                if let error = error {
                    if imageName == "no_image"{
                        self.handleSaveChanges()
                    }else{
                        print(error.localizedDescription)
                        self.handleSaveChanges()
                    }
                } else {
                    self.handleSaveChanges()
                }
            }
            
        }
    }
    
    func handleSaveChanges(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let imageName = NSUUID().uuidString
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference().child("profileImages")
        let currentUser = Auth.auth().currentUser
        
        if let profileImage = self.selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1), let firstName = self.firstNameTextField.text, let lastName = self.lastNameTextField.text, let mNumber = self.mobileNumberTextField.text, let userEmail =  self.emailTextField.text {
            
            storageRef.child("\(imageName).jpeg").putData(uploadData, metadata: nil, completion: { (stMetaData, erroroorrrrooorrrr) in
                if erroroorrrrooorrrr != nil {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    return
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                currentUser?.updateEmail(to: userEmail) { (errorrr) in
                    if errorrr != nil {
                        return
                    } else {
                        
                        storageRef.downloadURL(completion: { (urlllllll, errroorororor) in
                            guard let downloadURL = urlllllll?.absoluteString else {
                                // Uh-oh, an error occurred!
                                return
                            }
                            ref.child("users").child(uid).child("profileImageUrl").setValue(downloadURL)
                            ref.child("users").child(uid).child("firstName").setValue(firstName)
                            ref.child("users").child(uid).child("lastName").setValue(lastName)
                            ref.child("users").child(uid).child("mobileNumber").setValue(mNumber)
                            ref.child("users").child(uid).child("email").setValue(userEmail)
                            ref.child("users").child(uid).child("profileImageName").setValue("\(imageName).jpeg")
                            self.navigationItem.rightBarButtonItem?.isEnabled = false
                        })
                        
                    }
                    
                }
                
            })
        }
    }

}
