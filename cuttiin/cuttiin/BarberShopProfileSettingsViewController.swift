//
//  BarberShopProfileSettingsViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class BarberShopProfileSettingsViewController: UIViewController {
    var settingButtons = [Settings]()
    var userRole: String?
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customBarbershopProfileSettingsEditorCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("settingButtonCollectionViewProfileView", comment: "Settings")
        view.backgroundColor = UIColor.white
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewConstraints()
        getUserRole()
    }
    
    
    
    func getUserRole(){
        if let uid = Auth.auth().currentUser?.uid {
            let firebaseReferenceUserRole = Database.database().reference()
            firebaseReferenceUserRole.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshotUsrRole) in
                if let dictionaryUserRole = snapshotUsrRole.value as? [String: AnyObject] {
                    
                    let BarberShopCOnfirm = HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryUserRole)
                    let barberConfirm = HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryUserRole)
                    
                    if BarberShopCOnfirm {
                        let userRoleHolder = BarberShop()
                        userRoleHolder.setValuesForKeys(dictionaryUserRole)
                        if let userRole = userRoleHolder.role, userRole == "barberShop" {
                            self.settingsViewAlignShop()
                            self.userRole = "barberShop"
                            
                        }
                        
                    } else if barberConfirm {
                        let userBarberRoleHolder = Barber()
                        userBarberRoleHolder.setValuesForKeys(dictionaryUserRole)
                        
                        if let userRole = userBarberRoleHolder.role, userRole == "barberStaff" {
                            self.settingViewAlign()
                            self.userRole = "barberStaff"
                            
                        }
                    }
                }
            })
            
        }
    }
    
    func settingViewAlign(){
        let value = Settings()
        value.settingsTitle = NSLocalizedString("changePasswordButtonProfileSettingsEditor", comment: "Change Password")
        value.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(value)
        
        let valueNext = Settings()
        valueNext.settingsTitle = NSLocalizedString("logOutButtonProfileSettingsEditor", comment: "Log Out")
        valueNext.settingsIcon = UIImage(named: "logout")
        self.settingButtons.append(valueNext)
        DispatchQueue.main.async {
            self.userRole = "barberStaff"
            self.collectionView.reloadData()
        }
    }
    
    func settingsViewAlignShop(){
//        let valueone = Settings()
//        valueone.settingsTitle = NSLocalizedString("paymentButtonTitleShoppingListView", comment: "Payment")
//        valueone.settingsIcon = UIImage(named: "payments")
//        self.settingButtons.append(valueone)
        
        let valueone = Settings()
        valueone.settingsTitle = NSLocalizedString("openingHoursAvailLabelTextOrderDetailView", comment: "Opening hours")
        valueone.settingsIcon = UIImage(named: "opening_hours")
        self.settingButtons.append(valueone)
      
        
        let valuethree = Settings()
        valuethree.settingsTitle = NSLocalizedString("serviceAndBarbersButtonSettingsBSShopView", comment: "Barbers & Services")
        valuethree.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(valuethree)
        
        let valueFour = Settings()
        valueFour.settingsTitle = NSLocalizedString("historyButtonProfileSettingsEditorView", comment: "History")
        valueFour.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(valueFour)
        
        let valueFive = Settings()
        valueFive.settingsTitle = NSLocalizedString("upcomingBookingButtonProfileSettingsEditorView", comment: "Upcoming Bookings")
        valueFive.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(valueFive)
        
        let valueSix = Settings()
        valueSix.settingsTitle = NSLocalizedString("changePasswordButtonProfileSettingsEditor", comment: "Change Password")
        valueSix.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(valueSix)
        
        let valueNext = Settings()
        valueNext.settingsTitle = NSLocalizedString("logOutButtonProfileSettingsEditor", comment: "Log Out")
        valueNext.settingsIcon = UIImage(named: "logout")
        self.settingButtons.append(valueNext)
        
        DispatchQueue.main.async {
            self.userRole = "barberShop"
            self.collectionView.reloadData()
        }
    }
    
    @objc func handleCollectionSelction(sender: UIButton){
        if self.userRole == "barberShop" {
            switch sender.tag {
//            case 0:
//                let payoutview = PayOutViewControllerViewController()
//                let navController = UINavigationController(rootViewController: payoutview)
//                present(navController, animated: true, completion: nil)
            case 0:
                let openinghours = OpeningHoursViewController()
                openinghours.notRegisteringANewBarberShop = false
                let navController = UINavigationController(rootViewController: openinghours)
                present(navController, animated: true, completion: nil)

            case 1:
                let barberservice = BarberShopBarbersAndServicesViewController()
                let navController = UINavigationController(rootViewController: barberservice)
                present(navController, animated: true, completion: nil)
            case 2:
                let barberserviceHistory = BookingHistoryBarberShopViewController()
                let navController = UINavigationController(rootViewController: barberserviceHistory)
                present(navController, animated: true, completion: nil)
                
            case 3:
                let upcomingBookings = UpcomingBarberShopBookingViewController()
                let navController = UINavigationController(rootViewController: upcomingBookings)
                present(navController, animated: true, completion: nil)
                
            case 4:
                let changepassword = ChangePasswordViewController()
                navigationController?.pushViewController(changepassword, animated: true)
                
            case 5:
                self.handleAll()
            default:
                print("Sky High")
            }
            
        } else {
            switch sender.tag {
            case 0:
                let changepassword = ChangePasswordViewController()
                navigationController?.pushViewController(changepassword, animated: true)
            case 1:
                self.handleAll()
            default:
                print("Sky High")
            }
            
        }
    }
    
    func handleAll(){
        FBSDKLoginManager().logOut()
        do{
            try Auth.auth().signOut()
            print("Logged out yeah")
            self.handlePostLogoutDismiss()
        }catch let logoutError {
            print(logoutError)
        }
    }
    
    func handlePostLogoutDismiss(){
        UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
        navigationController?.popViewController(animated: true)
    }
    
    func setupViewConstraints(){
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }

}

class customBarbershopProfileSettingsEditorCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    lazy var thumbnailImageView: UIButton = {
        let tniv = UIButton()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let settingButtonTitlePlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        fnhp.setTitle("one hand", for: .normal)
        fnhp.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.isUserInteractionEnabled = true
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(settingButtonTitlePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|-16-[v0(200)][v1(50)]-16-|", views: settingButtonTitlePlaceHolder, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: settingButtonTitlePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

