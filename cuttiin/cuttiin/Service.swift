//
//  Service.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Service: NSObject {
    var serviceID: String?
    var serviceTitle: String?
    var estimatedTime: String?
    var serviceCost: String?
    var serviceCostLocal: String?
    var shortDescription: String?
    var category: String?
    var serviceImageName: String?
    var serviceImageUrl: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
    var localCurrency: String?
    
    var serviceImage: UIImage?
    
    var isDeleted: String?
}
