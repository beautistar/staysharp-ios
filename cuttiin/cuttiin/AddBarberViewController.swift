//
//  AddBarberViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate

class AddBarberViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    let picker = UIImagePickerController()
    var notRegisteringANewBarberShop = true
    var fromSelection = true
    var notAddingNewBarber = true
    var addserviceviewcontrl = AddServiceViewController()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let firebaseRef = Database.database().reference()
    var services = [Service]()
    var selectedServices = [String]()
    var selectedServiceID = [String]()
    var selectedBarberID: String?
    var serviceBarberArrayForLink = [ServiceBarbers]()
    var barberServiceArrayForLink = [BarberServices]()
    
    var emailValid = false
    var passwordValid = false
    
    let imageAndFirstLastContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_photo_smallest")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let firstNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let firstNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let firstNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let lastNameHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let lastNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let lastNameSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    let firstNameLastNameContainerView: UIView = {
        let fnlncview = UIView()
        fnlncview.translatesAutoresizingMaskIntoConstraints = false
        return fnlncview
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    let offeredServiceTextLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("collectionViewYopHeaderTextAddService", comment: "OFFERED SERVICE")
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let offeredServiceSeperatorView: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        tpcview.backgroundColor = UIColor.black
        return tpcview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.white
        cv.allowsMultipleSelection = true
        cv.register(customCollectionViewCellBarbers.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        picker.delegate = self
        view.addSubview(imageAndFirstLastContainerView)
        view.addSubview(emailHiddenPlaceHolder)
        view.addSubview(emailTextField)
        view.addSubview(emailSeperatorView)
        view.addSubview(offeredServiceTextLabel)
        view.addSubview(offeredServiceSeperatorView)
        view.addSubview(collectView)
        view.addSubview(errorMessagePlaceHolder)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectsConstraints()
        if !notAddingNewBarber {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
            
            let deleteButtonHold = UIBarButtonItem(title: NSLocalizedString("deleteButtonNavigationButtonAddServiceView", comment: "Hide"), style: .done, target: self, action: #selector(handleDeleActions))
            deleteButtonHold.tintColor = UIColor.red
            let doneButtonHold = UIBarButtonItem(title: NSLocalizedString("doneButtonPaymentView", comment: "Done"), style: .done, target: self, action: #selector(handleUpdatingBarberServiceLink))
            navigationItem.rightBarButtonItems = [doneButtonHold, deleteButtonHold]
            self.getBarberDataImmutable()
            
        } else {
            navigationItem.title = NSLocalizedString("navigationTitleTextAddBarberView", comment: "Add a Barber: Step 5 of 5")
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("doneButtonPaymentView", comment: "Done"), style: .done, target: self, action: #selector(handleCreatingABarber))
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
        getServices()
        if !fromSelection {
            navigationItem.title = NSLocalizedString("navigationTitleAddABarerAddBarberView", comment: "Add a Barber")
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Auth.auth().currentUser?.uid == nil {
            dismiss(animated: false, completion: nil)
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let barberID = self.selectedBarberID {
            let firebaseReferenceBarberService = Database.database().reference()
            firebaseReferenceBarberService.child("barberServices").child(barberID).observeSingleEvent(of: .value, with: { (snapshootbarberServiceValue) in
                if let dictionaryBarberService = snapshootbarberServiceValue.value as? [String: AnyObject] {
                    for dictionSB in dictionaryBarberService {
                        if let servicebarbersingle = dictionSB.value as? [String: AnyObject], HandleDataRequest.handleBarberServicesNode(firebaseData: servicebarbersingle) == true {
                            let barberServiceHold = BarberServices()
                            barberServiceHold.setValuesForKeys(servicebarbersingle)
                            
                            if let serviceID = barberServiceHold.serviceID {
                                print(serviceID, "know how")
                                if let arrayIndex = self.services.index(where: {$0.serviceID == serviceID}) {
                                    print(arrayIndex,"hey now it was a match", serviceID)
                                    let indexPathForFirstRow = IndexPath(row: arrayIndex, section: 0)
                                    self.collectionView.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: UICollectionViewScrollPosition(rawValue: 0))
                                    if let cell = self.collectionView.cellForItem(at: indexPathForFirstRow) as? customCollectionViewCellBarbers {
                                        print("zuko fire burn them")
                                        cell.thumbnailImageView.image = UIImage(named: "check_box_active")
                                        if let selSer =  self.services[indexPathForFirstRow.row].serviceTitle, let selSerID =  self.services[indexPathForFirstRow.row].serviceID {
                                            self.selectedServices.append(selSer)
                                            self.selectedServiceID.append(selSerID)
                                        }
                                    } else {
                                        print(serviceID, "Q beats all men")
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    @objc func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func getServices(){
        guard let uid = Auth.auth().currentUser?.uid else {
            print("An errror occured")
            return
        }
        
        firebaseRef.child("service").child(uid).observeSingleEvent(of: .value, with: { (snapshoootttt) in
            if let dictionary = snapshoootttt.value as? [String : AnyObject] {
                for dic in dictionary {
                    if let di = dic.value as? [String : AnyObject], HandleDataRequest.handleServiceNode(firebaseData: di) == true {
                        let servicesAvail = Service()
                        servicesAvail.setValuesForKeys(di)
                        
                        if let serviceCheck = servicesAvail.isDeleted, let serviceIDD = servicesAvail.serviceID {
                            if serviceCheck == "NO" {
                                self.services.append(servicesAvail)
                                self.handleGettingBarberService(serviceIDDAX: serviceIDD)
                                self.handleGettingServiceBarber(serviceIDDAX: serviceIDD)
                                DispatchQueue.main.async {
                                    self.collectionView.reloadData()
                                }
                            }
                        }
                    }
                    
                }
            }
        }, withCancel: nil)
    }
    
    func handleGettingBarberService(serviceIDDAX: String){
        let firebaseReferenceServiceBarbers = Database.database().reference()
        firebaseReferenceServiceBarbers.child("serviceBarbers").child(serviceIDDAX).observeSingleEvent(of: .value, with: { (snapshootServiceBarbers) in
            if let dictionaryServBarb = snapshootServiceBarbers.value as? [String: AnyObject] {
                for serBar in dictionaryServBarb {
                    if let dataHold = serBar.value as? [String: AnyObject], HandleDataRequest.handleServiceBarbersNode(firebaseData: dataHold) == true {
                        let barberServiceSingleHold = ServiceBarbers()
                        barberServiceSingleHold.setValuesForKeys(dataHold)
                        barberServiceSingleHold.objectID = serBar.key
                        
                        if let barberID = barberServiceSingleHold.barberID, let barberIDAXZ = self.selectedBarberID {
                            if barberID == barberIDAXZ {
                                self.serviceBarberArrayForLink.append(barberServiceSingleHold)
                                print(self.serviceBarberArrayForLink.count, "Money talks")
                            }
                        }
                        
                    }
                }
            }
        }, withCancel: nil)
    }
    
    func handleGettingServiceBarber(serviceIDDAX: String) {
        if let barberID = self.selectedBarberID {
            let firebaseReferenceBarberServices = Database.database().reference()
            firebaseReferenceBarberServices.child("barberServices").child(barberID).observeSingleEvent(of: .value, with: { (snapshoootBarberServices) in
                if let dictionaryBarberServices = snapshoootBarberServices.value as? [String: AnyObject] {
                    for barSer in dictionaryBarberServices {
                        if let barberServiceHold = barSer.value as? [String: AnyObject], HandleDataRequest.handleBarberServicesNode(firebaseData: barberServiceHold) == true {
                            let barberSelService = BarberServices()
                            barberSelService.setValuesForKeys(barberServiceHold)
                            barberSelService.objectID = barSer.key
                            
                            if let serviceUniqueID = barberSelService.serviceID {
                                if serviceUniqueID == serviceIDDAX {
                                    self.barberServiceArrayForLink.append(barberSelService)
                                    print(self.barberServiceArrayForLink.count, "Charlie sheen")
                                }
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    
    @objc func handleUpdatingBarberServiceLink(){
        if let barberID = self.selectedBarberID {
            if self.selectedServiceID.count > 0 {
                self.handleDeletingUnSelectedBarberServiceLink()
                for serveID in self.selectedServiceID {
                    if self.barberServiceArrayForLink.first(where: { $0.serviceID ==  serveID }) != nil {
                        print("already exist")
                        
                    } else {
                        
                        let newDate = DateInRegion()
                        let strDate = newDate.toString(.extended)
                        let userTimezone = DateByUserDeviceInitializer.tzone
                        let userCalender = DateByUserDeviceInitializer.calenderNow
                        let userLocal = DateByUserDeviceInitializer.localCode
                        
                        
                        let newUserLocal = "en"
                        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
                        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
                        let newStrDate = newUserDate.toString(.extended)
                        
                        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                        
                        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
                        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
                        
                        let databaseref = Database.database().reference()
                        
                        let objKey = NSUUID().uuidString
                        let serviceBarberRefrence = databaseref.child("serviceBarbers").child(serveID).child(objKey)
                        let values = ["barberID": barberID, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                        serviceBarberRefrence.updateChildValues(values, withCompletionBlock: { (errrorrrrr, refffz) in
                            if let error = errrorrrrr {
                                print(error.localizedDescription)
                                return
                            }
                            let barberServiceRefrence = databaseref.child("barberServices").child(barberID).child(objKey)
                            let value2 = ["serviceID":serveID, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                            barberServiceRefrence.updateChildValues(value2, withCompletionBlock: { (errrorrri, refffss) in
                                if let error = errrorrri {
                                    print(error.localizedDescription)
                                    return
                                }
                                print("servce and barbers sccessfully created")//
                                
                            })
                        })
                    }
                }
            }
        }
    }
    
    func handleDeletingUnSelectedBarberServiceLink(){
        if let barberID = self.selectedBarberID {
            if self.services.count > 0 {
                let counterStopper = self.services.count
                var counterXX = 0
                for servIDD in self.services {
                    counterXX += 1
                    if self.selectedServiceID.first(where: { $0 ==  servIDD.serviceID }) != nil {
                        
                    } else {
                        if let seriDDHOLD = servIDD.serviceID {
                            if let firstCall = self.barberServiceArrayForLink.first(where: { $0.serviceID ==  seriDDHOLD }) {
                                if let objectKey = firstCall.objectID {
                                    let firebaseDeleteUnselectedService = Database.database().reference()
                                    firebaseDeleteUnselectedService.child("barberServices").child(barberID).child(objectKey).removeValue(completionBlock: { (errorrrr, dataReff) in
                                        if let error = errorrrr {
                                            print(error.localizedDescription)
                                            return
                                        }
                                        
                                        print("Action completed barber services")
                                        
                                        if let secondCall = self.serviceBarberArrayForLink.first(where: { $0.barberID ==  barberID }) {
                                            if let objectKeyzzz = secondCall.objectID {
                                                let firebaseReferenceDeleteServiceBarbers = Database.database().reference()
                                                print(seriDDHOLD, objectKeyzzz, "measurement")
                                                
                                                firebaseReferenceDeleteServiceBarbers.child("serviceBarbers").child(seriDDHOLD).child(objectKeyzzz).removeValue(completionBlock: { (errororror, dataRefereere) in
                                                    if let error = errororror {
                                                        print(error.localizedDescription)
                                                        return
                                                    }
                                                    print("Action completed service barbers")
                                                })
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    }
                    
                    if counterXX == counterStopper {
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                    
                }
            }
        }
    }
    
    func getBarberDataImmutable(){
        if let barberID = self.selectedBarberID {
            let firebaseReferenceBarberDetails = Database.database().reference()
            firebaseReferenceBarberDetails.child("users").child(barberID).observeSingleEvent(of: .value, with: { (snapshootUserDeatails) in
                if let dictionaryUserDetails = snapshootUserDeatails.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryUserDetails) == true {
                    let userDetails = Barber()
                    userDetails.setValuesForKeys(dictionaryUserDetails)
                    
                    if let firstName = userDetails.firstName, let lastName = userDetails.lastName, let userEmail = userDetails.email, let profileImageURL = userDetails.profileImageUrl, let checkStatus = userDetails.isDeleted {
                        self.firstNameTextField.text = firstName
                        self.firstNameTextField.isUserInteractionEnabled = false
                        self.lastNameTextField.text = lastName
                        self.lastNameTextField.isUserInteractionEnabled = false
                        self.emailTextField.text = userEmail
                        self.emailTextField.isUserInteractionEnabled = false
                        self.selectImageView.loadImagesUsingCacheWithUrlString(urlString: profileImageURL)
                        self.selectImageView.isUserInteractionEnabled = false
                        
                        if checkStatus == "YES" {
                            self.navigationItem.rightBarButtonItems?.remove(at: 1)
                            let newUIButton = UIBarButtonItem(title: NSLocalizedString("deleteButtonNavigationButtonAddServiceViewUnHide", comment: "Unhide"), style: .done, target: self, action: #selector(self.handleDeleActionsUnhide))
                            newUIButton.tintColor = UIColor.white
                            self.navigationItem.rightBarButtonItems?.append(newUIButton)
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    @objc func handleDeleActions(){
        let alertController = UIAlertController(title: NSLocalizedString("hideBarberPopUpViewAddBarberView", comment: "Hide this Barber!"), message: NSLocalizedString("hideBarberPopUpViewMessageAddBarberView", comment: "Are you sure you want to hide this barber, as your customers won't be able to book this barber"), preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("yesButtonAlertOrderDetail", comment: "Yes"), style: .default) { action in
            self.handleDeletingABarber()
        }
        alertController.addAction(takePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    @objc func handleDeleActionsUnhide(){
        let alertController = UIAlertController(title: NSLocalizedString("hideBarberPopUpViewAddBarberViewUnhide", comment: "Unhide this Barber"), message: NSLocalizedString("hideBarberPopUpViewMessageAddBarberViewUnhide", comment: "Are you sure you want to unhide this barber, as your customers will be able to book this barber"), preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("yesButtonAlertOrderDetail", comment: "Yes"), style: .default) { action in
            self.handleDeletingABarberUnhide()
        }
        alertController.addAction(takePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func handleDeletingABarber(){
        if let userID = self.selectedBarberID {
            let firebaseReferenceDeleteUser = Database.database().reference()
            firebaseReferenceDeleteUser.child("users").child(userID).child("isDeleted").setValue("YES")
            print("user deleted")
            self.navigationItem.rightBarButtonItems?.remove(at: 1)
            let newUIButton = UIBarButtonItem(title: NSLocalizedString("deleteButtonNavigationButtonAddServiceViewUnHide", comment: "Unhide"), style: .done, target: self, action: #selector(self.handleDeleActionsUnhide))
            newUIButton.tintColor = UIColor.white
            self.navigationItem.rightBarButtonItems?.append(newUIButton)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func handleDeletingABarberUnhide(){
        if let userID = self.selectedBarberID {
            let firebaseReferenceDeleteUser = Database.database().reference()
            firebaseReferenceDeleteUser.child("users").child(userID).child("isDeleted").setValue("NO")
            print("user deleted")
            
            self.navigationItem.rightBarButtonItems?.remove(at: 1)
            let newUIButton = UIBarButtonItem(title: NSLocalizedString("deleteButtonNavigationButtonAddServiceView", comment: "Hide"), style: .done, target: self, action: #selector(handleDeleActions))
            newUIButton.tintColor = UIColor.red
            self.navigationItem.rightBarButtonItems?.append(newUIButton)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setupViewObjectsConstraints(){
        imageAndFirstLastContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        imageAndFirstLastContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageAndFirstLastContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        imageAndFirstLastContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        imageAndFirstLastContainerView.addSubview(selectImageView)
        imageAndFirstLastContainerView.addSubview(firstNameLastNameContainerView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: imageAndFirstLastContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: imageAndFirstLastContainerView.widthAnchor, multiplier: 0.25).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        firstNameLastNameContainerView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        firstNameLastNameContainerView.rightAnchor.constraint(equalTo: imageAndFirstLastContainerView.rightAnchor).isActive = true
        firstNameLastNameContainerView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.addSubview(firstNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(firstNameTextField)
        firstNameLastNameContainerView.addSubview(firstNameSeperatorView)
        firstNameLastNameContainerView.addSubview(lastNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(lastNameTextField)
        firstNameLastNameContainerView.addSubview(lastNameSeperatorView)
        
        firstNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.topAnchor).isActive = true
        firstNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        firstNameTextField.topAnchor.constraint(equalTo: firstNameHiddenPlaceHolder.bottomAnchor).isActive = true
        firstNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        firstNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        firstNameSeperatorView.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        firstNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        lastNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        lastNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        lastNameTextField.topAnchor.constraint(equalTo: lastNameHiddenPlaceHolder.bottomAnchor).isActive = true
        lastNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        lastNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        lastNameSeperatorView.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        lastNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: emailTextField.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        
        offeredServiceTextLabel.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 15).isActive = true
        offeredServiceTextLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        offeredServiceTextLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        offeredServiceTextLabel.heightAnchor.constraint(equalToConstant: 25)
        
        offeredServiceSeperatorView.topAnchor.constraint(equalTo: offeredServiceTextLabel.bottomAnchor).isActive = true
        offeredServiceSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        offeredServiceSeperatorView.widthAnchor.constraint(equalTo: offeredServiceTextLabel.widthAnchor).isActive = true
        offeredServiceSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        collectView.topAnchor.constraint(equalTo: offeredServiceSeperatorView.bottomAnchor).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleCreatingABarber(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let uid = Auth.auth().currentUser?.uid, let firstName = firstNameTextField.text, let lastName = lastNameTextField.text, let emailtext = emailTextField.text else {
            return
        }
        let bundle = Bundle.main
        let path = bundle.path(forResource: "GoogleService-Info", ofType: "plist")!
        let options = FirebaseOptions.init(contentsOfFile: path)
        FirebaseApp.configure(name: "Secondary", options: options!)
        let secondary_app = FirebaseApp.app(name: "Secondary")  //.init(named: "Secondary")
        let second_auth = Auth.auth(app: secondary_app!)
        let password = "theCuttingAppPassKey123"
        second_auth.createUser(withEmail: emailtext, password: password)
        {
            (authUserData,errorrrr) in
            if let error = errorrrr {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                
                if let errCode = AuthErrorCode(rawValue: error._code){
                    switch errCode {
                    case .networkError:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                    case .invalidEmail:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAddressMalformedErrorCodeLoginView", comment: "email address is malformed")
                    case .operationNotAllowed:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care")
                    case .emailAlreadyInUse:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("emailAlreadyInUseErrorCodeLoginView", comment: "email already in use by an existing account")
                    case .weakPassword:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("weakPasswordErrorCodeCustomerRegistrationView", comment: "weak password")
                    default:
                        self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                        
                    }
                }
                self.hideKeyboard()
                return
            }
            
            guard let userxx = authUserData?.user else{
                print("could not get new user")
                return
            }
            
            userxx.sendEmailVerification(completion: { (errorrrr) in
                if let error = errorrrr {
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let errCode = AuthErrorCode(rawValue: error._code){
                        switch errCode {
                        case .networkError:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation")
                        case .userNotFound:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("accountNotFoundErrorCodeLoginView", comment: "Account not found")
                        default:
                            self.errorMessagePlaceHolder.text = NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again")
                            
                        }
                    }
                    self.hideKeyboard()
                
                    userxx.delete(completion: { (errorrr) in
                        if let error = errorrr {
                            print(error.localizedDescription)
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        do {
                            try Auth.auth().signOut()
                        } catch let logoutError {
                            print(logoutError)
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    })
                    return
                }
            })
            
            let newUser = userxx.uid
            let newDate = DateInRegion()
            let strDate = newDate.toString(.extended)
            let userTimezone = DateByUserDeviceInitializer.tzone
            let userCalender = DateByUserDeviceInitializer.calenderNow
            let userLocal = DateByUserDeviceInitializer.localCode
            
            
            let newUserLocal = "en"
            let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
            let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
            let newStrDate = newUserDate.toString(.extended)
            
            let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
            
            let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
            let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
            
            let databaseref = Database.database().reference()
            //print(Auth.auth().currentUser?.email ?? "default")
            let imageName = NSUUID().uuidString
            let storageRef = Storage.storage().reference().child("profileImages").child("\(imageName).jpeg")
            
            if let profileImage = self.selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
                storageRef.putData(uploadData, metadata: nil, completion: { (metaData, errorrr) in
                    if let error = errorrr {
                        self.errorMessagePlaceHolder.text = error.localizedDescription
                        print(error.localizedDescription)
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    storageRef.downloadURL(completion: { (urllll, errooroorrrr) in
                        guard let downloadURL = urllll?.absoluteString else {
                            // Uh-oh, an error occurred!
                            return
                        }
                        
                        let userDatabaseReference = databaseref.child("users").child(newUser)
                        let values = ["uniqueID": newUser ,"firstName": firstName, "lastName": lastName , "email": emailtext,"role": "barberStaff", "barberShopID":uid,"profileImageUrl":downloadURL, "mobileNumber": "1234","profileImageName": "\(imageName).jpeg", "dateAccountCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe, "rating": "0", "isDeleted":"NO"]
                        userDatabaseReference.updateChildValues(values, withCompletionBlock: { (errorr, refff) in
                            if let error = errorr {
                                self.activityIndicator.stopAnimating()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                print(error.localizedDescription)
                                return
                            }else{
                                
                                let keyStash = NSUUID().uuidString
                                let oldUserDatabaseReference = databaseref.child("barberShop-Barbers").child(uid).child(keyStash)
                                let valuesss = ["barberStaffID": newUser, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                                oldUserDatabaseReference.updateChildValues(valuesss, withCompletionBlock: { (errrorrrrree, refrrrr) in
                                    if let error = errrorrrrree {
                                        print(error.localizedDescription)
                                        self.activityIndicator.stopAnimating()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        return
                                    }
                                    
                                    for ser in self.selectedServiceID{
                                        let objKey = NSUUID().uuidString
                                        let serviceBarberRefrence = databaseref.child("serviceBarbers").child(ser).child(objKey)
                                        let values = ["barberID": newUser, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                                        serviceBarberRefrence.updateChildValues(values, withCompletionBlock: { (errrorrrrr, refffz) in
                                            if let error = errrorrrrr {
                                                print(error.localizedDescription)
                                                self.activityIndicator.stopAnimating()
                                                UIApplication.shared.endIgnoringInteractionEvents()
                                                return
                                            }
                                            let barberServiceRefrence = databaseref.child("barberServices").child(newUser).child(objKey)
                                            let value2 = ["serviceID":ser, "dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                                            barberServiceRefrence.updateChildValues(value2, withCompletionBlock: { (errrorrri, refffss) in
                                                if let error = errrorrri {
                                                    print(error.localizedDescription)
                                                    self.activityIndicator.stopAnimating()
                                                    UIApplication.shared.endIgnoringInteractionEvents()
                                                    return
                                                }
                                                print("servce and barbers sccessfully created")
                                                //self.dismiss(animated: true, completion: nil)
                                            })
                                        })
                                    }
                                    //innerfunc()
                                    self.moveToNextView()
                                })
                                
                            }
                        })
                    })
                    
                })
            }
        }
    }
    
    func moveToNextView(){
        if !notRegisteringANewBarberShop {
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.dismiss(animated: true, completion: nil)
            self.addserviceviewcontrl.handleDismissView()
        } else {
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            let cmainView = CustomBarberTabBarController()
            self.present(cmainView, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func textFieldDidChange(){
        if firstNameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == "" || selectedServices.isEmpty == true{
            //Disable button
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            navigationItem.rightBarButtonItem?.isEnabled = false
            
        } else {
            //Enable button
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            if emailValid == true {
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
    }
    
    @objc func firstNametextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = false
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
    }
    
    @objc func lastNametextFieldDidChange(){
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}" //[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = "Email"
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = "Email is not properly formatted"
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        print("Hello therer baby")
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("takePhotoAlertViewProfileEdit", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotAlertViewProfileEdit", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        print("Hello")
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            print("Image selected ########")
            selectImageView.contentMode = .scaleAspectFit
            selectImageView.image = selectedImage
            navigationItem.rightBarButtonItem?.isEnabled = true
        }else {
            print("image not selected")
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = "Sorry, this device has no camera"
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }

}

class customCollectionViewCellBarbers: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.white
        tniv.contentMode = .scaleAspectFit
        tniv.image = UIImage(named: "check_box_inactive")
        return tniv
    }()
    
    let servicesAvailable: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.text = "Hello"
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.black
        return sv
    }()
    
    func setupViews(){
        addSubview(servicesAvailable)
        addSubview(thumbnailImageView)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.white
        addContraintsWithFormat(format: "H:|-16-[v0]-16-[v1(20)]-16-|", views: servicesAvailable, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: servicesAvailable,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
