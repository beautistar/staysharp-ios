//
//  BarberShopBarbers.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/31/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import Foundation

@objcMembers class BarberShopBarbers: NSObject {
    var barberStaffID: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
}
