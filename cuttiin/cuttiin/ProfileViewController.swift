//
//  ProfileViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class ProfileViewController: UIViewController {
    var settingButtons = [Settings]()
    var calledOnceHolder = true
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showEditProfileView))
        tcview.addGestureRecognizer(tapGesture)
        return tcview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let userNameAndTextInputsContainerView: UIView = {
    let tcview = UIView()
    tcview.translatesAutoresizingMaskIntoConstraints = false
    return tcview
    }()
    
    let userNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let userNameDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("viewAndEditProfileButton", comment: "View and Edit Profile")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let buildVersionDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 11)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let upperSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        return fnsv
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customProfileSettingsCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("navigationTitleTextCustomerProfileView", comment: "Profile")
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjects()
        settingViewAlign()
        handleSHowUserData()
        calledOnceHolder = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !calledOnceHolder {
            self.handleSHowUserData()
        }
        calledOnceHolder = false
        
        if let userIDDXX = Auth.auth().currentUser?.uid {
            //Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            self.buildVersionDescriptionPlaceHolder.text = NSLocalizedString("userSpecialIDProfileView", comment: "Unique ID: ") + userIDDXX
        }
        
        if Auth.auth().currentUser?.uid == nil {
            self.tabBarController?.selectedIndex = 1
            let welcomeviewcontroller = WelcomeViewController()
            self.present(welcomeviewcontroller, animated: true, completion: nil)
        }
    }
    
    @objc func showEditProfileView(){
        let profileedit = ProfileEditorViewController()
        navigationController?.pushViewController(profileedit, animated: true)
    }
    
    func handleSHowUserData(){
        DispatchQueue.global(qos: .background).async {
            if let userUID = Auth.auth().currentUser?.uid {
                let firebaseUserReference = Database.database().reference()
                firebaseUserReference.child("users").child(userUID).observeSingleEvent(of: .value, with: { (snapshotUserData) in
                    if let dictionaryUserData = snapshotUserData.value as? [String: AnyObject] {
                        
                        if let firstName = dictionaryUserData["firstName"] as? String, let lastName = dictionaryUserData["lastName"] as? String, let imageURL = dictionaryUserData["profileImageUrl"] as? String {
                            DispatchQueue.main.async {
                                self.userNamePlaceHolder.text = firstName + " " + lastName
                            }
                            
                            if let url = URL(string: imageURL) {
                                do {
                                    let data = try Data(contentsOf: url)
                                    
                                    if let downloadedImage = UIImage(data: data) {
                                        DispatchQueue.main.async {
                                            self.selectImageView.image = downloadedImage
                                        }
                                    }
                                } catch {
                                    print("profile picture not available")
                                }
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func setupViewObjects(){
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        upperInputsContainerView.addSubview(selectImageView)
        upperInputsContainerView.addSubview(userNamePlaceHolder)
        upperInputsContainerView.addSubview(userNameDescriptionPlaceHolder)
        upperInputsContainerView.addSubview(buildVersionDescriptionPlaceHolder)
        
        selectImageView.topAnchor.constraint(equalTo: upperInputsContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: upperInputsContainerView.widthAnchor, multiplier: 0.25, constant: 5).isActive = true
        selectImageView.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: -10).isActive = true
        
        userNamePlaceHolder.topAnchor.constraint(equalTo: upperInputsContainerView.topAnchor, constant: 5).isActive = true
        userNamePlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        userNamePlaceHolder.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        userNamePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        userNameDescriptionPlaceHolder.topAnchor.constraint(equalTo: userNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        userNameDescriptionPlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        userNameDescriptionPlaceHolder.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        userNameDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        buildVersionDescriptionPlaceHolder.topAnchor.constraint(equalTo: userNameDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        buildVersionDescriptionPlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 15).isActive = true
        buildVersionDescriptionPlaceHolder.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        buildVersionDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 13).isActive = true
        
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        collectView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    func settingViewAlign(){
        let value = Settings()
        value.settingsTitle = NSLocalizedString("settingButtonCollectionViewProfileView", comment: "Settings")
        value.settingsIcon = UIImage(named: "settings")
        self.settingButtons.append(value)
        
    }
}

class customProfileSettingsCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let settingButtonTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        fnhp.text = "one hand"
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(settingButtonTitlePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|-16-[v0(150)][v1(50)]-16-|", views: settingButtonTitlePlaceHolder, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: settingButtonTitlePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
