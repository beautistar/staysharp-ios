//
//  CustomerBookings.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/21/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class CustomerBookings: NSObject {
    var customerID: String?
    var bookingID: String?
    var barberID: String?
    var serviceID: String?
    var barberShopID: String?
}

//refrence - bookingID
