//
//  PaymentViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/12/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON
import SwiftDate

class PaymentViewController: UIViewController, UIWebViewDelegate {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var BarberShopUUID: String?
    var bookingUUID: String?
    
    
    var isSaveCard = false
    var arrAllCards : JSON?
    var dictCard : JSON?
    var id = ""
    var saveCardWebViewFinishCount = 0
    var totalAmount = ""
    
    var n = 0
    var strPaymentID : String?
    var strNavigationBarBTNTitleCancel = "Cancel"
    var strNavigationBarBTNTitleDone   = "Done"
    
    let webViewWindow: UIWebView = {
        let webvieww = UIWebView()
        webvieww.translatesAutoresizingMaskIntoConstraints = false
        webvieww.keyboardDisplayRequiresUserAction = true
        return webvieww
    }()

    
    var rightNavigationBarButton : UIBarButtonItem?
    
    var didBackButtonPressed :(() -> Void)!
    var shoppingListVC = ShoppingListViewController()
    
    var didPaymentVCBackButtonPressedBlock : (() -> Void)!
    var strCardID = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("doneButtonPaymentView", comment: "Done"), style: .done, target: self, action: #selector(handleDismisss))

        
        self.rightNavigationBarButton = UIBarButtonItem(title: NSLocalizedString("cancelButtonPaymentView", comment: self.strNavigationBarBTNTitleCancel), style: .plain, target: self, action: #selector(handleDismisss))
        
        navigationItem.rightBarButtonItem = self.rightNavigationBarButton

        
        webViewWindow.delegate = self;
        view.addSubview(webViewWindow)
        setupViewsContraints()
        
        if self.isSaveCard {
            self.webViewWindow.tag = 0
            
            if self.arrAllCards == nil || (self.arrAllCards?.count == 2 && self.arrAllCards![0]["error"] == nil) || self.arrAllCards?.count == 0 {
                self.getCardAPI()
            }
            else {
                self.webViewWindow.tag = 1
                
                if self.strCardID != "" {
                    self.getPaymentToken(self.strCardID)
                } else {
                    let alert = UIAlertController(title: nil, message: "Something went wrong!, Please try again", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        if self.didBackButtonPressed != nil {
                            self.didBackButtonPressed()
                        }
                        self.dismissThisView()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            self.webViewWindow.tag = 1
            self.getBookingData()
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("URL :: \(request.url?.path ?? "URL NOT FOUND")")

        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        let scrollPoint = CGPoint(x: 0, y: webView.scrollView.contentSize.height - webView.frame.size.height)
        webView.scrollView.setContentOffset(scrollPoint, animated: true)
        
        
        if webView.tag == 1 {
            if self.n == 2 {
                if let url = webView.request?.mainDocumentURL {
                    do {
                        let htmlString = try String(contentsOf: url, encoding: .utf8)
                        print("HTML Content :: \(htmlString)")
                        
                        if htmlString.contains("You have completed the payment") {
                            print("Transaction Successfull")
                            if self.strPaymentID != nil && self.strPaymentID != "" {
                                self.postToAppDatabase(paymentID: self.strPaymentID!)
                                
                                self.rightNavigationBarButton?.title = NSLocalizedString("doneButtonPaymentView", comment: self.strNavigationBarBTNTitleDone)
                            }
                        } else {
                            print("Transaction Failed")
                            self.rightNavigationBarButton?.title = NSLocalizedString("cancelButtonPaymentView", comment: self.strNavigationBarBTNTitleCancel)
                        }
                    }
                    catch let err {
                        print("Error :: \(err.localizedDescription)")
                        self.rightNavigationBarButton?.title = NSLocalizedString("cancelButtonPaymentView", comment: self.strNavigationBarBTNTitleCancel)
                    }
                }
            }
            self.n = self.n + 1
        }
        
        if webView.tag == 0 {
            
            if self.saveCardWebViewFinishCount == 2 {
                if let url = webView.request?.mainDocumentURL {
                    do {
                        let htmlString = try String(contentsOf: url, encoding: .utf8)
                        print("HTML Content :: \(htmlString)")
                        
                        if htmlString.contains("You have completed the payment") {
                            self.getPaymentToken(self.id)
                            self.webViewWindow.tag = 0
                        } else {
                            print("Something went wrong")
                        }
                    }
                    catch let err {
                        print("Error :: \(err.localizedDescription)")
                    }
                }
            }
            self.saveCardWebViewFinishCount = self.saveCardWebViewFinishCount + 1
        }
    }

    
    func getCardAPI() {
        DispatchQueue.global(qos: .background).async {
        
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "9e5f5b15-dd1b-041c-a26d-eddc40cdadcc"
            ]
            
            let dataPostURLString = "https://api.quickpay.net/cards"
            
            let request = NSMutableURLRequest(url: NSURL(string: dataPostURLString)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let errorrr = error {
                    print("Could make post",errorrr.localizedDescription)
                } else {
                    if let jsData = data {
                        do {
                            let jsonData = try JSON(data: jsData)
                            
                            self.dictCard = jsonData
                            print("self.dictCard :: \(self.dictCard ?? "JSON Empty")")

                            let id = jsonData["id"].stringValue
                            self.id = id
                            UserDefaults.standard.set(id, forKey: "savedCardID")
                            self.getCardAuthorizationLink(id)
                            
                        } catch {
                            print("error in creating payment")
                        }
                    }
                }
            })
            
            dataTask.resume()
        }
    }

    
    func getCardAuthorizationLink(_ id : String) {
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "9e5f5b15-dd1b-041c-a26d-eddc40cdadcc"
            ]
            
            let dataPostURLString = "https://api.quickpay.net/cards/\(id)/link"
            
            let request = NSMutableURLRequest(url: NSURL(string: dataPostURLString)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "PUT"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let errorrr = error {
                    print("Could make post",errorrr.localizedDescription)
                } else {
                    if let jsData = data {
                        do {
                            let jsonData = try JSON(data: jsData)
                            let strURL = jsonData["url"].stringValue
                            
                            if let addr = URL(string: strURL){
                                let addrREQ = URLRequest(url: addr)
                                
                                DispatchQueue.main.async {
                                    self.webViewWindow.tag = 0
                                    self.webViewWindow.loadRequest(addrREQ)
                                }
                            }
                        } catch {
                            print("error in getCardAuthorizationLink")
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
    
    func getPaymentToken(_ strID :String) {
        
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "9e5f5b15-dd1b-041c-a26d-eddc40cdadcc"
            ]
            
            let dataPostURLString = "https://api.quickpay.net/cards/\(strID)/tokens"
            
            let request = NSMutableURLRequest(url: NSURL(string: dataPostURLString)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let errorrr = error {
                    print("Could make post",errorrr.localizedDescription)
                } else {
                    if let jsData = data {
                        do {
                            let jsonData = try JSON(data: jsData)
                            let strToken = jsonData["token"].stringValue
                        
                            UserDefaults.standard.set(strToken, forKey: "quickPayPaymentToken")
                            if strToken != "" {
                                self.getBookingData()
                            }
                            else {
                                let alert = UIAlertController(title: nil, message: "Something went wrong, please try again", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                    self.dismiss(animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                                print("Something went wrong while getting QuickPay payment token")
                            }
                        } catch {
                            print("error in getCardAuthorizationLink")
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
    
    
    func getBookingData(){
        DispatchQueue.global(qos: .background).async {
            let firebaseReferee = Database.database().reference().child("bookings")
            if let shopID = self.BarberShopUUID, let bookID = self.bookingUUID {
                firebaseReferee.child(shopID).child(bookID).observeSingleEvent(of: .value, with: { (snapshoootsss) in
                    if let dictionary = snapshoootsss.value as? [String : AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionary) == true {
                        let currentBook = Bookings()
                        currentBook.setValuesForKeys(dictionary)
                        if let totalServicePrice = currentBook.totalPriceWithCharges, let barberUUID = currentBook.bookedBarberID, let serviceID = currentBook.bookedServiceID, let bookUUID = currentBook.bookingID, let customerIDDAX = currentBook.customerID {
                            let firebaseRefereeService = Database.database().reference()
                            firebaseRefereeService.child("service").child(shopID).child(serviceID).observeSingleEvent(of: .value, with: { (snapshoServiceABP) in
                                if let dictionaryServiceABp = snapshoServiceABP.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionaryServiceABp) == true {
                                    let servicePaid = Service()
                                    servicePaid.setValuesForKeys(dictionaryServiceABp)
                                    if let serviceTitle = servicePaid.serviceTitle, let currencyG = servicePaid.localCurrency{
                                        
                                        self.totalAmount = totalServicePrice
                                        
                                        self.createPayment(totalPrice: totalServicePrice, serviceUUID: serviceID, serviceTitle: serviceTitle, barberShopUUID: shopID, barberUUID: barberUUID, bookingUUID: bookUUID, customerIX: customerIDDAX, currency: currencyG)
                                    }
                                }
                            }, withCancel: nil)
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func createPayment(totalPrice: String, serviceUUID: String, serviceTitle: String, barberShopUUID: String, barberUUID: String, bookingUUID: String, customerIX: String, currency: String){
        
        DispatchQueue.global(qos: .background).async {
            //  ef52f58a03b8180bc4ff8db95ce0345a71a75f58fa6e7b8da40bd1fbe3e8691e

//            let headers = [
//                "accept-version": "v10",
//                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
//                "content-type": "application/json",
//                "cache-control": "no-cache",
//                "postman-token": "9e5f5b15-dd1b-041c-a26d-eddc40cdadcc"
//            ]
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "content-type": "application/json",
                "cache-control": "no-cache",
                "postman-token": "9e5f5b15-dd1b-041c-a26d-eddc40cdadcc"
            ]

            let myCode = ShortCodeGenerator.getCode(length: 6)
            let newDate = DateInRegion()
            let strDate = newDate.toString(.extended)
            let userTimezone = DateByUserDeviceInitializer.tzone
            let userCalender = DateByUserDeviceInitializer.calenderNow
            let userLocal = DateByUserDeviceInitializer.localCode
            
            
            let newUserLocal = "en"
            let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
            let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
            let newStrDate = newUserDate.toString(.extended)
            
            let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
            
            let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
            let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
            
            
            
            
            //customerUUID
            
            let strindDemandEscape = "currency=\(currency)&order_id=\(myCode)&basket[][qty]=1&basket[][item_no]=\(serviceUUID)&basket[][item_name]=\(serviceTitle)&basket[][item_price]=\(totalPrice)&basket[][vat_rate]=0.25&text_on_statement=Teckdk.com&variables[barberID]=\(barberUUID)&variables[barberShopID]=\(barberShopUUID)&variables[bookingUUID]=\(bookingUUID)&variables[dateCreated]=\(correctDateToUse)&variables[timezone]=\(userTimezone)&variables[calendar]=\(userCalender)&variables[local]=\(correctLocaleToUSe)&variables[customerUUID]=\(customerIX)"
            
            guard let escapedString = strindDemandEscape.addingPercentEncoding(withAllowedCharacters: .rfc3986Unreserved) else {
                return
            }
            
            let dataPostURLString = "https://api.quickpay.net/payments/?\(escapedString)"
            //"https://api.quickpay.net/payments/?currency=dkk&order_id=\(myCode)"
            
            let request = NSMutableURLRequest(url: NSURL(string: dataPostURLString)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let errorrr = error {
                    print("Could make post",errorrr.localizedDescription)
                } else {
                    if let jsData = data {
                        do {
                            
                            let jsonData = try JSON(data: jsData)
                            let payIdentification = jsonData["id"].stringValue
                            
                            self.strPaymentID = payIdentification
                            
                            if self.isSaveCard {
                                self.createPaymentFromSavedCard()
                            }
                            else{
                                self.createPaymentWindowLink(totalP: totalPrice, payID: payIdentification)
//                            self.postToAppDatabase(paymentID: payIdentification)
                            }
                        } catch {
                            print("error in creating payment")
                        }
                    }
                }
            })
            
            dataTask.resume()
        }
    }
    
    func createPaymentFromSavedCard(){
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "cache-control": "no-cache",
                "postman-token": "25fc18d9-d151-78f8-71d9-5db3020c4706"
            ]
            
            
            var dict = [String : String]()
            var token = ""
            if let strToken  = UserDefaults.standard.value(forKey: "quickPayPaymentToken") as? String {
                dict["card[token]"] = strToken
                token = strToken
            }
            dict["amount"] = self.totalAmount
            
            
            let url = "https://api.quickpay.net/payments/\(String(describing: self.strPaymentID!))/authorize/?"
            
            let request = NSMutableURLRequest()
            request.url = URL(string: url)
            request.httpMethod = "POST"
            request.timeoutInterval = 10.0
            request.allHTTPHeaderFields = headers

            var strParams = "" //"card[token]=\(token)&amount=\(self.totalAmount)"
            
            if token != "" {
                strParams = "card[token]=\(token)&amount=\(self.totalAmount)"
            } else {
                strParams = "amount=\(self.totalAmount)"
            }
            
            request.httpBody = strParams.data(using: String.Encoding.utf8)
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
               
                if let errorrr = error {
                    print(errorrr.localizedDescription)
                }
                else {
                    if let jsData = data {
                        do {
                            let jsonData = try JSON(data: jsData)
                            print("dict :: \(jsonData)")
                            
                            if jsonData["accepted"].stringValue == "false" {
                                self.verifyPayment()
                            }
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
    
    
    func verifyPayment(){
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "cache-control": "no-cache",
                "postman-token": "25fc18d9-d151-78f8-71d9-5db3020c4706"
            ]
            
            
            let dataPostURLString = "https://api.quickpay.net/payments/\(String(describing: self.strPaymentID!))/"
            let request = NSMutableURLRequest(url: NSURL(string: dataPostURLString)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
            
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers
            
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                if let errorrr = error {
                    print(errorrr.localizedDescription)
                }
                else {
                    if let jsData = data {
                        do {
                            let jsonData = try JSON(data: jsData)
                            print("dict :: \(jsonData)")
                            print("\(jsonData["accepted"].stringValue)")
                            
                            let isDone = jsonData["accepted"].stringValue

                            self.rightNavigationBarButton?.title = isDone == "true" ? NSLocalizedString("doneButtonPaymentView", comment: self.strNavigationBarBTNTitleDone) : NSLocalizedString("cancelButtonPaymentView", comment: self.strNavigationBarBTNTitleCancel)
                            
                            
                            if isDone == "true" {
                                if self.strPaymentID != nil && self.strPaymentID != "" {
                                    self.postToAppDatabase(paymentID: self.strPaymentID!)
                                    
                                    self.rightNavigationBarButton?.title = NSLocalizedString("doneButtonPaymentView", comment: self.strNavigationBarBTNTitleDone)
                                }
                            }
                            
                            
                            let alert = UIAlertController(title: nil, message: isDone == "true" ? "Payment successfully done" : "Payment successfully not done", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                
                                if self.didBackButtonPressed != nil {
                                    self.didBackButtonPressed()
                                }
                                self.dismissThisView()
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
    
    func createPaymentWindowLink(totalP: String, payID: String){
        DispatchQueue.global(qos: .background).async {
            
            let headers = [
                "accept-version": "v10",
                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
//                "authorization": "Basic OmM3NDdhZjcxY2I5MjZlMjRmYWI0NWYyNmM5NWI5ZTM5MmU3M2M4NWVhYTMzN2NjMDY0OGI5OTc5MGNkNDJkYzI=",
                "cache-control": "no-cache",
                "postman-token": "25fc18d9-d151-78f8-71d9-5db3020c4706"
            ]
            
            let request = NSMutableURLRequest(url: NSURL(string: "https://api.quickpay.net/payments/\(payID)/link?amount=\(totalP)00")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "PUT"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let errorrr = error {
                    print(errorrr.localizedDescription)
                } else {
                    if let jsData = data {
                        do {
                            let jsonData = try JSON(data: jsData)
                            let address = jsonData["url"].stringValue
                            if let addr = URL(string: address){
                                let addrREQ = URLRequest(url: addr)
                                
                                DispatchQueue.main.async {
                                    self.webViewWindow.tag = 1
                                    self.webViewWindow.loadRequest(addrREQ)
                                }
                            }
                        } catch {
                            print("error in json conversion")
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
    
    
    func postToAppDatabase(paymentID: String){
        DispatchQueue.global(qos: .background).async {
            let ref = Database.database().reference().child("bookings")
            
            if let shopID = self.BarberShopUUID, let bookid = self.bookingUUID {
//                ref.child(shopID).child(bookid).child("paymentID").setValue(paymentID)
                
                ref.child(shopID).child(bookid).child("paymentID").setValue(paymentID)
                ref.child(shopID).child(bookid).child("isCompleted").setValue("YES")
            }
        }
        
    }
    
    @objc func handleDismisss(){
        let alertController = UIAlertController(title: NSLocalizedString("closeViewButtonTitlePaymentView", comment: "Close this view?"), message: NSLocalizedString("closeButtonDescriptionPaymentView", comment: "Note: If transaction is not completed and this view is closed. The transaction will be terminated."), preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("yesButtonAlertOrderDetail", comment: "Yes"), style: .default) { action in

            
            self.shoppingListVC.handleDismissView()
            if self.didBackButtonPressed != nil {
                self.didBackButtonPressed()
            }
            
            if let btnTitle = self.rightNavigationBarButton?.title
            {
                if btnTitle == self.strNavigationBarBTNTitleCancel {
                    UserDefaults.standard.set(false, forKey: "theBookingProcessIsComplete")
                    
                    if let shopID = self.BarberShopUUID {
                        UserDefaults.standard.set(false, forKey: "numberOfNewBookingsHasChanged")
                        
                        let firebaseReferenceDataBooking = Database.database().reference()
                        firebaseReferenceDataBooking.child("bookings").child(shopID).child(self.bookingUUID!).removeValue(completionBlock: { (error, databaseReference) in
                            
                            print("Error : \(error?.localizedDescription ?? "ERROR")")
                        })
                    }
                } else {
                    UserDefaults.standard.set(true, forKey: "theBookingProcessIsComplete")
                }
            }
            self.dismissThisView()
        }
        alertController.addAction(takePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func dismissThisView(){
        dismiss(animated: false, completion: nil)
    }
    
    func setupViewsContraints(){
        webViewWindow.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        webViewWindow.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        webViewWindow.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        webViewWindow.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }

}
