//
//  CustomYearHolder.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 02/12/2017.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class CustomYearHolder: NSObject {
    var yearIntValue: Int?
    var yearStringValue: String?
}
