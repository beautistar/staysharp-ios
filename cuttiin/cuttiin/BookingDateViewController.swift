//
//  BookingDateViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/7/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate
import Alamofire
import EventKit



class BookingDateViewController: UIViewController {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var barberShopUUID: String?
    var bookingID: String?
    var barberID: String?
    var pictureFileName: String?
    var barberOne = BarberShop()
    var navBar: UINavigationBar = UINavigationBar()
    var presentDay: Int?
    var newDay: Int?
    var totalTimeForServicePushed: Int?
    var totalTimeForService: Int?
    var isDateClashing = false
    var counterInitValueForPostToServer = 0
    var firstLoad = false
    var bookingStartStopHold = [Bookings]()
    var availableTimesCalled = [AvailableTimes]()
    var availableTimesCalledSecond = [AvailableTimes]()
    var availableTimesCalledSecondCall = [AvailableTimes]()
    var inbetweenavailableTimes = [IntermediaryAvailableTimes]()
    var bookingTimeIntervalMainArray = [BookingsTimeInterval]()
    var bookingTimeIntervalMainArraySecondCall = [BookingsTimeInterval]()
    var bookingTimeIntervalMainArraySecond = [BookingsTimeInterval]()
    
    
    var barbershopTimezone: String?
    var barbershopCalendar: String?
    var barbershopLocale: String?
    
    var presentDayVeri: Int?
    var newDayVeri: Int?
    
    var todaysBooking = [Bookings]()
    var todaysBookingsForVerification = [Bookings]()
    var thisBooking = Bookings()
    var timeSelectorCurrentDayWorkHour = Workhours()
    
    
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = UIColor.clear
        return v
    }()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let instructionHeaderHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("intructionHeaderTextHold", comment: "Suggested available hours, slide left for more")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        cview.layer.borderWidth = 2
        return cview
    }()
    
    let collectViewBackgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "button_slider_updated")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customBookingDateCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdxx")
        return cv
    }()
    
    let statusOfAvailableBookingLable: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        ehp.textColor = UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    let estimatedServiceTimeHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        ehp.textColor = UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    let serviceTimeStartTimePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .dateAndTime
        
        //
        opentime.setValue(UIColor.white, forKey: "textColor")
        opentime.setValue(false, forKeyPath: "highlightsToday")
        //
        let currentDate = Date()
        opentime.date = currentDate
        opentime.minimumDate = currentDate
        opentime.addTarget(self, action: #selector(handleServiceTimeStartTimePickerChange), for: .valueChanged)
        return opentime
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 216, g: 127, b: 15)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionTitleBookingDateView", comment: "Cancellation Policy!")
        ht.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        ht.textColor = UIColor(r: 118, g: 187, b: 220)
        ht.textAlignment = .center
        return ht
    }()
    
    let descriptionBody: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionBodyBookingDateView", comment: "Remember if you cancel the same day as your appointment is due, you will still be charged for the cancellation(50% of your total transaction)")
        ht.numberOfLines = 0
        ht.font = UIFont(name: "HelveticaNeue", size: 13)
        ht.textColor = UIColor(r: 118, g: 187, b: 220)
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .center
        return ht
    }()
    
    let chosenDatePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = ""
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var bookTheDateButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        fbutton.setTitle(NSLocalizedString("bookButtonBookingDateView", comment: "BOOK"), for: .normal)
        fbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.layer.borderWidth = 2
        fbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        fbutton.isEnabled = false
        fbutton.addTarget(self, action: #selector(handleCallToPostMethod), for: .touchUpInside)
        return fbutton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear


        // viewDidLoad//
        view.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismsiss))
        setNavBarToTheView()
        view.addSubview(scrollView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViews()

        
        UserDefaults.standard.set(false, forKey: "isDateClashing")
        UserDefaults.standard.set(counterInitValueForPostToServer, forKey: "intValueHoldForCounterToEnsureServerSideWrite")

        getBarberPersonalDetails()
        handleServiceTimeStartTimePickerChange()
        
        //data monitor
        UserDefaults.standard.set(true, forKey: "monitorBookingBarbershopCounterChanges")
        //        UserDefaults.standard.set(false, forKey: "numberOfNewBookingsHasChanged")
        monitorChangesInNumberBarberShopBooking()
        self.firstLoad = true

        UserDefaults.standard.set(false, forKey: "numberOfNewBookingsHasChanged")
        
        //viewDidLoad//
        
        
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
        
        if !self.firstLoad {
            print("first load second call")
            
            counterInitValueForPostToServer = 0
            UserDefaults.standard.set(false, forKey: "isDateClashing")
            UserDefaults.standard.set(counterInitValueForPostToServer, forKey: "intValueHoldForCounterToEnsureServerSideWrite")
            handleServiceTimeStartTimePickerChange()
            
            //data monitor
            UserDefaults.standard.set(true, forKey: "monitorBookingBarbershopCounterChanges")
            UserDefaults.standard.set(false, forKey: "numberOfNewBookingsHasChanged")
            monitorChangesInNumberBarberShopBooking()
            getBookingStartandEndDates()
        }
        self.firstLoad = false
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
    }
    
    func setupViews(){
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 110).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 520) //450
        
        
        scrollView.addSubview(instructionHeaderHolder)
        scrollView.addSubview(collectView)
        scrollView.addSubview(statusOfAvailableBookingLable)
        scrollView.addSubview(estimatedServiceTimeHolder)
        scrollView.addSubview(serviceTimeStartTimePicker)
        scrollView.addSubview(errorMessagePlaceHolder)
        scrollView.addSubview(descriptionTitle)
        scrollView.addSubview(descriptionBody)
        scrollView.addSubview(bookTheDateButton)
        scrollView.addSubview(chosenDatePlaceHolder)
        
        
        instructionHeaderHolder.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 5).isActive = true
        instructionHeaderHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        instructionHeaderHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        instructionHeaderHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        collectView.topAnchor.constraint(equalTo: instructionHeaderHolder.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -72).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        collectView.addSubview(collectViewBackgroundImageView)
        
        collectViewBackgroundImageView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectViewBackgroundImageView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectViewBackgroundImageView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectViewBackgroundImageView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        collectViewBackgroundImageView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectViewBackgroundImageView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectViewBackgroundImageView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectViewBackgroundImageView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectViewBackgroundImageView.heightAnchor).isActive = true
        
        
        statusOfAvailableBookingLable.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 5).isActive = true
        statusOfAvailableBookingLable.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        statusOfAvailableBookingLable.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        statusOfAvailableBookingLable.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        //
        estimatedServiceTimeHolder.topAnchor.constraint(equalTo: statusOfAvailableBookingLable.bottomAnchor, constant: 5).isActive = true
        estimatedServiceTimeHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        estimatedServiceTimeHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        estimatedServiceTimeHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        serviceTimeStartTimePicker.topAnchor.constraint(equalTo: estimatedServiceTimeHolder.bottomAnchor, constant: 5).isActive = true
        serviceTimeStartTimePicker.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        serviceTimeStartTimePicker.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -12).isActive = true
        serviceTimeStartTimePicker.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: serviceTimeStartTimePicker.bottomAnchor, constant: 5).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        descriptionTitle.topAnchor.constraint(equalTo: errorMessagePlaceHolder.bottomAnchor, constant: 10).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        descriptionBody.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        descriptionBody.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionBody.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionBody.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        chosenDatePlaceHolder.topAnchor.constraint(equalTo: descriptionBody.bottomAnchor, constant: 15).isActive = true
        chosenDatePlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        chosenDatePlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        chosenDatePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        bookTheDateButton.topAnchor.constraint(equalTo: chosenDatePlaceHolder.bottomAnchor, constant: 5).isActive = true
        bookTheDateButton.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        bookTheDateButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bookTheDateButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        bookTheDateButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20)
        
    }
    
    
    func showTappedDate(dateTapped: Date){
        if let minDate = self.serviceTimeStartTimePicker.minimumDate {
            
            let currentDate = DateInRegion().date
            
            
            if dateTapped.isAfterDate(minDate, orEqual: true, granularity: .minute) && dateTapped.isAfterDate(currentDate, orEqual: true, granularity: .minute) {
                print("it was passed sucessfully")
                self.serviceTimeStartTimePicker.setDate(dateTapped, animated: true)
                self.handleServiceTimeStartTimePickerChange()
            } else {
                print("improper date comparism")
            }
        } else {
            print("no min max date")
        }
    }
    
    func getBarberPersonalDetails(){
        DispatchQueue.global(qos: .background).async {
            let firebaseRef = Database.database().reference()
            if let userID = self.barberShopUUID {
                firebaseRef.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshooottt) in
                    if let dictionary = snapshooottt.value as? [String: AnyObject] {
                        
                        let barberShopVerifi = HandleDataRequest.handleUserBarberShop(firebaseData: dictionary)
                        
                        if barberShopVerifi {
                            
                            self.barberOne.setValuesForKeys(dictionary)
                            if let shopName = self.barberOne.companyName, let address = self.barberOne.companyFormattedAddress {
                                DispatchQueue.main.async {
                                    self.barberShopNamePlaceHolder.text = shopName
                                    self.barberShopAddressPlaceHolder.text = address
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    @objc func handleServiceTimeStartTimePickerChange(){
        
        
        self.activityIndicator.center = CGPoint(x: self.view.bounds.size.width/2, y: self.view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.white
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        self.errorMessagePlaceHolder.text = ""
        self.bookTheDateButton.isEnabled = false
        self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.getBookingStartandEndDates()
        let correctDateGotten = self.serviceTimeStartTimePicker.date.inDefaultRegion() //inLocalRegion()
        let clendarName = DateByUserDeviceInitializer.calenderNow
        
        var tZoneNow = DateByUserDeviceInitializer.tzone
        
        if let barberTimeZone = UserDefaults.standard.value(forKey: "selectedBarberTimeZone") as? String {
            if barberTimeZone != "" {
                tZoneNow = barberTimeZone
            }
        }
        
        let modLocal = "en_US"
        let translatorRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tZoneNow, calenName: clendarName, LocName: modLocal)
        let pickerDateHolderAwaitingTrans = self.serviceTimeStartTimePicker.date.inDefaultRegion()
        
        let newTransDateInDiffrentRegion = pickerDateHolderAwaitingTrans.convertTo(region: translatorRegion) //toRegion(translatorRegion)
        let selectDateDay = newTransDateInDiffrentRegion.weekdayName(.default)
        
        
        DispatchQueue.global(qos: .background).async {
            
            if let userID = self.barberShopUUID, let bTimezone = self.barbershopTimezone {
                if let zot = Zones(rawValue: bTimezone) {
                    DispatchQueue.main.async {
                        self.serviceTimeStartTimePicker.timeZone = zot.toTimezone()
                    }
                }
                
                DispatchQueue.main.async {
                    self.view.isUserInteractionEnabled = true
                }
                
                let firebaseRefereOpeningClosingDate = Database.database().reference()
                
                firebaseRefereOpeningClosingDate.child("workhours").child(userID).child(selectDateDay).observeSingleEvent(of: .value, with: { (snapshooottss) in
                    
                    if let dictionaryOpeningClosingDate = snapshooottss.value as? [String: AnyObject], HandleDataRequest.handleWorkhoursNode(firebaseData: dictionaryOpeningClosingDate){
                        let currentDayWorkHour = Workhours()
                        currentDayWorkHour.setValuesForKeys(dictionaryOpeningClosingDate)
                        
                        if let openTimeHour = currentDayWorkHour.openingTimeHour, let openTimeMinute = currentDayWorkHour.openingTimeMinute, let openTimeSecond = currentDayWorkHour.openingTimeSecond, let closingTimeHour = currentDayWorkHour.closingTimeHour, let closingTimeMinute = currentDayWorkHour.closingTimeMinute, let closingTimeSecond = currentDayWorkHour.closingTimeSecond, let minutesBeforeClosing = currentDayWorkHour.minutesBeforeClosing, let totalTimeInt = self.totalTimeForServicePushed {
                            
                            if let openHour = Int(openTimeHour), let openMinute = Int(openTimeMinute), let openSecond = Int(openTimeSecond), let closeHour = Int(closingTimeHour), let closeMinute = Int(closingTimeMinute), let closeSecond = Int(closingTimeSecond), let minutesBefore = Int(minutesBeforeClosing) {
                                
                                
                                if let startTimeDate = correctDateGotten.dateBySet(hour: openHour, min: openMinute, secs: openSecond), let closeTimeDate = correctDateGotten.dateBySet(hour: closeHour, min: closeMinute, secs: closeSecond) {
                                    
                                    self.totalTimeForService = totalTimeInt
                                    let addedTime = closeTimeDate - minutesBefore.minutes
                                    
                                    let endTimeDate = correctDateGotten + totalTimeInt.minutes
                                    
                                    DispatchQueue.main.async {
                                        self.estimatedServiceTimeHolder.text = NSLocalizedString("estimatedServiceTimeBookingDateView", comment: "Estimated service time: ") + "\(totalTimeInt) min"
                                    }
                                    
                                    
                                    if correctDateGotten.isInRange(date: startTimeDate, and: addedTime, orEqual: true, granularity: .minute) && endTimeDate.isInRange(date: startTimeDate, and: addedTime, orEqual: true, granularity: .minute) {
                                        
                                        DispatchQueue.main.async {
                                            UserDefaults.standard.set(false, forKey: "isDateClashing")
                                            self.handleGetBookingDataForVerification(selStart: correctDateGotten, selEnd: endTimeDate)
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            self.errorMessagePlaceHolder.text = NSLocalizedString("chosenDateOutOfRangeBookingDateView", comment: "Unfortunately the selected time is outside of the barberShop Opening and Closing time.")
                                            self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                                            self.bookTheDateButton.isEnabled = false
                                            self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                                            self.chosenDatePlaceHolder.text = ""
                                            
                                            self.activityIndicator.stopAnimating()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                        }
                                    }
                                }
                            } else {
                                
                                DispatchQueue.main.async {
                                    print("could not convert to int")
                                    self.bookTheDateButton.isEnabled = false
                                    self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                                    self.chosenDatePlaceHolder.text = ""
                                    
                                    self.activityIndicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                print("Error in data check")
                                self.errorMessagePlaceHolder.text = NSLocalizedString("chosenDateOutOfRangeBookingDateView", comment: "Unfortunately the selected time is outside of the barberShop Opening and Closing time.")
                                self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                                self.bookTheDateButton.isEnabled = false
                                self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                                self.chosenDatePlaceHolder.text = ""
                                
                                self.activityIndicator.stopAnimating()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            }
                        }
                        
                    } else {// read rain
                        //here
                        DispatchQueue.main.async {
                            self.errorMessagePlaceHolder.text = NSLocalizedString("chosenDateOutOfRangeBookingDateView", comment: "Unfortunately the selected time is outside of the barberShop Opening and Closing time.")
                            self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                            self.bookTheDateButton.isEnabled = false
                            self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                            self.chosenDatePlaceHolder.text = ""
                            
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                })
                
            }
        }
    }
    
    
    func handleGetBookingDataForVerification(selStart: DateInRegion,selEnd: DateInRegion){
        DispatchQueue.global(qos: .background).async {
            let firebaseRefxx = Database.database().reference()
       
            if let shopID = self.barberShopUUID, let barberID = self.barberID {
            
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "E d-MMM-yyyy"
                let strDate = dateFormatter.string(from: Date())
                print("Selected date :: \(strDate)")
            
                
                firebaseRefxx.child("bookings").child(shopID)/*.queryOrdered(byChild: "bookingStartTime").queryStarting(atValue: "Fri 01-Jun-2018")*/.observeSingleEvent(of: .value, with: { (snapshootootot) in
                    if let dictionary = snapshootootot.value as? [String: AnyObject] {
                        self.todaysBookingsForVerification.removeAll()
                        let numberOfItems = dictionary.count
                        var numberCounter = 0
                        for data in dictionary {
                            numberCounter = numberCounter + 1
                            if let dictoo = data.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictoo) == true {
                                
                                let bookingData = Bookings()
                                bookingData.setValuesForKeys(dictoo)
                                print("runing shoes")
                                if let currentBarber = bookingData.bookedBarberID, let bookCompleted = bookingData.isCompleted, let isCancelled = bookingData.bookingCancel, let bookingStatus = bookingData.bookingStatus {
                                    if barberID == currentBarber && bookCompleted == "YES" && isCancelled == "NO" && bookingStatus == "completed" ||  bookingStatus == "booked" || bookingStatus == "pending"  {
                                        self.todaysBookingsForVerification.append(bookingData)
                                        
                                        if let bookingStart = bookingData.bookingStartTime, let bookingEnd = bookingData.bookingEndTime, let tzone = bookingData.timezone, let bookingCalendar = bookingData.calendar, let bookingLocal = bookingData.local {
                                            
                                            let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal)
                                            let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingStart)
                                            let verifyBookingStopDate = VerifyDateAssociation.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingEnd)
                                            
                                            if verifyBookingData == true && verifyBookingStartDate == true && verifyBookingStopDate == true {
                                                self.checkForBookingAvailability(selectStartDate: selStart, selectEndDate: selEnd, bookingStartDate: bookingStart, bookingEndDate: bookingEnd, timeZne: tzone, clendar: bookingCalendar, defLocal: bookingLocal)
                                            } else {
                                                DispatchQueue.main.async {
                                                    self.activityIndicator.stopAnimating()
                                                    UIApplication.shared.endIgnoringInteractionEvents()
                                                }
                                            }
                                            
                                        }
                                        else {
                                            DispatchQueue.main.async {
                                                self.activityIndicator.stopAnimating()
                                                UIApplication.shared.endIgnoringInteractionEvents()
                                            }
                                        }//presido
                                    } else {
                                        DispatchQueue.main.async {
                                            self.activityIndicator.stopAnimating()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        self.activityIndicator.stopAnimating()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                    }
                                }
                                
                            } else {
                                DispatchQueue.main.async {
                                    self.activityIndicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    if numberOfItems == numberCounter, let isClash = UserDefaults.standard.object(forKey: "isDateClashing") as? Bool, !isClash {
                                        self.errorMessagePlaceHolder.text = NSLocalizedString("bookingDateSelectionCheckStatusOkBookingDateView", comment: "The chosen time is available to book a service.")
                                        self.errorMessagePlaceHolder.textColor = UIColor(r: 25, g: 192, b: 41)
                                        self.bookTheDateButton.isEnabled = true
                                        self.bookTheDateButton.backgroundColor = UIColor(r: 11, g: 49, b: 68)
                                    }
                                }
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                })
            } else {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        }
    }
    
    //monitor number booking for the barber shop
    let firebaseNumberOfBookingReferenceSpecial = Database.database().reference()
    
    func monitorChangesInNumberBarberShopBooking() {
        
        if let shopID = self.barberShopUUID {
            
            firebaseNumberOfBookingReferenceSpecial.child("totalNumberOfBookings").child(shopID).observe(.value, with: { (snapshoootNumberValue) in
                
                if snapshoootNumberValue.value as? [String: AnyObject] != nil {
                    print("Sing bended knee", snapshoootNumberValue.key)
                    
                    if let monitorChangeValue = UserDefaults.standard.object(forKey: "monitorBookingBarbershopCounterChanges") as? Bool {
                        if !monitorChangeValue {
                            UserDefaults.standard.set(true, forKey: "numberOfNewBookingsHasChanged")
                        } else {
                            UserDefaults.standard.set(false, forKey: "monitorBookingBarbershopCounterChanges")
                        }
                    }
                }
                
            })
        }
    }
    
    @objc func handleCallToPostMethod(){
        self.activityIndicator.center = CGPoint(x: self.view.bounds.size.width/2, y: self.view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.white
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        let randomNumDouble  = Double.random(min: 0.00, max: 3.00)
        let roundUpRandom = randomNumDouble.roundTo(places: 2)
        self.bookTheDateButton.isEnabled = false
        self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.perform(#selector(self.postSelectedTimeToDataBase), with: self, afterDelay: roundUpRandom)
    }
    
    @objc func transactionDataPost(){
        DispatchQueue.global(qos: .background).async {
            if let shopID = self.barberShopUUID {
                let fireref = Database.database().reference()
                
                fireref.child("totalNumberOfBookings").child(shopID).runTransactionBlock({ (currentData) -> TransactionResult in
                    //input
                    
                    if let newData = currentData.value as? [String: AnyObject], HandleDataRequest.handleNumberOfBookingsNode(firebaseData: newData) == true {
                        
                        
                        let tNumber = NumberOfBookings()
                        tNumber.setValuesForKeys(newData)
                        if let counter = tNumber.numberOfBookings {
                            let counterValue = counter.intValue
                            let valueNumber = counterValue + 1
                            currentData.value = ["numberOfBookings": valueNumber]
                            return TransactionResult.success(withValue: currentData)
                        }
                        
                    }
                    return TransactionResult.success(withValue: currentData)
                }, andCompletionBlock: { (errorrrrrrr, commited, snapshotFinalVlaue) in
                    if let error = errorrrrrrr {
                        DispatchQueue.main.async {
                            print(error.localizedDescription, "error in transaction killer")
                        }
                    }
                    if commited {
                        DispatchQueue.main.async {
                            print("booking added to inventory inventroy")
                        }
                    }
                })
                
            }
        }
    }
    
    @objc func postSelectedTimeToDataBase(){
        if let isClash = UserDefaults.standard.object(forKey: "isDateClashing") as? Bool, let totalTimeInt = self.totalTimeForServicePushed, let checker = UserDefaults.standard.object(forKey: "intValueHoldForCounterToEnsureServerSideWrite") as? Int, let finalCheck = UserDefaults.standard.object(forKey: "numberOfNewBookingsHasChanged") as? Bool, let bTimezone = self.barbershopTimezone, let bCalendar = self.barbershopCalendar, let bLocale = self.barbershopLocale {
            
            if !finalCheck {
                
                let barberShopRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bTimezone, calenName: bCalendar, LocName: bLocale)
                let newUserLocal = "en"
                let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bTimezone, calenName: bCalendar, LocName: newUserLocal)
                
                let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
                
                let selStart = self.serviceTimeStartTimePicker.date.convertTo(region: barberShopRegion)
                let engSelStart = selStart.convertTo(region: englishDateRegion)
                let newSelStart = (localeTimezoneVerify) ? selStart : engSelStart
                
                let selEnd = selStart + totalTimeInt.minutes
                let engSelEnd = selEnd.convertTo(region: englishDateRegion)
                let newSelEnd = (localeTimezoneVerify) ? selEnd : engSelEnd
                
                let selStartString = newSelStart.toString(.extended)
                let selEndString = newSelEnd.toString(.extended)
                let paramValue = "yesServeDidNot\(checker)"
                
                UserDefaults.standard.set(false, forKey: "numberOfNewBookingsHasChanged")
                
                if let shopID = self.barberShopUUID, let bookIDD = self.bookingID {
                    let firebaseReferenceDataBooking = Database.database().reference()
                    
                    if !isClash {
                        firebaseReferenceDataBooking.child("bookings").child(shopID).child(bookIDD).child("bookingStartTime").setValue(selStartString)
                        firebaseReferenceDataBooking.child("bookings").child(shopID).child(bookIDD).child("bookingEndTime").setValue(selEndString)
                        firebaseReferenceDataBooking.child("bookings").child(shopID).child(bookIDD).child("severDidNotMakeBooking").setValue(paramValue)
                       
                        print("red madness")
                        
                        let inVail = checker + 1
                        
                        if inVail >= 100 {
                            UserDefaults.standard.set(self.counterInitValueForPostToServer, forKey: "intValueHoldForCounterToEnsureServerSideWrite")
                        } else {
                            UserDefaults.standard.set(inVail, forKey: "intValueHoldForCounterToEnsureServerSideWrite")
                        }
                        
                        self.transactionDataPost()
                        self.manageChangesToNewBooking()
                        //self.perform(#selector(handlePostDataDelete), with: self, afterDelay: 1200)
                    } else {
                        print("blue madness")
                        self.manageChangesToNewBooking()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                }
            } else {
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    //
                    self.errorMessagePlaceHolder.text = NSLocalizedString("chosenDateOutOfRangeBookingDateView", comment: "Unfortunately the selected time is outside of the barberShop Opening and Closing time.")
                    self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                    self.bookTheDateButton.isEnabled = false
                    self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                    self.chosenDatePlaceHolder.text = ""
                    UserDefaults.standard.set(false, forKey: "numberOfNewBookingsHasChanged")
                    self.handleServiceTimeStartTimePickerChange()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
    }
    
    
    
    @objc func handlePostDataDelete(shopID: String, bookingID: String){
        let firebaseReference = Database.database().reference()
        firebaseReference.child("bookings").child(shopID).child(bookingID).observeSingleEvent(of: .value) { (snapshotData) in
            if let dictionary = snapshotData.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionary) == true {
                let bookingHolder = Bookings()
                bookingHolder.setValuesForKeys(dictionary)
                
                if let bookStatus = bookingHolder.bookingStatus {
                    if bookStatus == "pending" {
                        let firebaseReferenceUpdate = Database.database().reference()
                        firebaseReferenceUpdate.child("bookings").child(shopID).child(bookingID).setValue("not available")
                    }
                }
            }
        }
    }
    
    func checkForBookingAvailability(selectStartDate: DateInRegion, selectEndDate: DateInRegion, bookingStartDate: String, bookingEndDate: String, timeZne: String, clendar: String, defLocal: String){
        if let isClash = UserDefaults.standard.object(forKey: "isDateClashing") as? Bool{
            if isClash == false {
                
                let bookingRegion = DateByUserDeviceInitializer.getRegion(TZoneName: timeZne, calenName: clendar, LocName: defLocal)
                
                //DateInRegion(string: bookingEndDate, format: .extended, fromRegion: bookingRegion)
                if let bookingDateStart = bookingStartDate.toDate(style: .extended, region: bookingRegion)  , let bookingDateEnd = bookingEndDate.toDate(style: .extended, region: bookingRegion) {
                    
                    
                    switch selectStartDate.compare(toDate: bookingDateStart, granularity: .minute) {
                    case .orderedAscending:
                        //print("Date A is earlier than date B")
                        //selectEndDate < bookingDateStart
                        
                        if selectEndDate.isBeforeDate(bookingDateStart, orEqual: false, granularity: .minute)  {
                            //print("the dates did not clash")
                            UserDefaults.standard.set(false, forKey: "isDateClashing")
                            DispatchQueue.main.async {
                                self.errorMessagePlaceHolder.text = NSLocalizedString("bookingDateSelectionCheckStatusOkBookingDateView", comment: "The chosen time is available to book a service.")
                                print("The chosen time is available to book a service.")
                                print("orderedAscending bookingDateStart :: \(bookingDateStart) || bookingEndDate :: \(bookingEndDate)")
                                self.errorMessagePlaceHolder.textColor = UIColor(r: 25, g: 192, b: 41)
                                self.bookTheDateButton.isEnabled = true
                                self.bookTheDateButton.backgroundColor = UIColor(r: 11, g: 49, b: 68)
                            }
                        }else {
                            //print(" the dates clash")
                            UserDefaults.standard.set(true, forKey: "isDateClashing")
                            DispatchQueue.main.async {
                                self.errorMessagePlaceHolder.text = NSLocalizedString("bookingDateSelectionCheckStatusNotOkBookingDateView", comment: "Unfortunately the selected time is already booked.")
                                print("Unfortunately the selected time is already booked.")
                                print("else bookingDateStart :: \(bookingDateStart) || bookingEndDate :: \(bookingEndDate)")
                                self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                                self.bookTheDateButton.isEnabled = false
                                self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                            }
                        }
                    case .orderedDescending:
                        //print("Date A is later than date B")
                        //selectStartDate > bookingDateEnd

                         if selectStartDate.isAfterDate(bookingDateEnd, orEqual: false, granularity: .minute) {
                            //print("dates did not clash")
                            
                            UserDefaults.standard.set(false, forKey: "isDateClashing")
                            DispatchQueue.main.async {
                                self.errorMessagePlaceHolder.text = NSLocalizedString("bookingDateSelectionCheckStatusOkBookingDateView", comment: "The chosen time is available to book a service.")
                                print("The chosen time is available to book a service.")
                                print("orderedDescending selectStartDate :: \(selectStartDate.date) || bookingEndDate :: \(bookingDateEnd.date)")
                                self.errorMessagePlaceHolder.textColor = UIColor(r: 25, g: 192, b: 41)
                                self.bookTheDateButton.isEnabled = true
                                self.bookTheDateButton.backgroundColor = UIColor(r: 11, g: 49, b: 68)
                            }
                        } else {
                            //print(" the dates clash")
                            UserDefaults.standard.set(true, forKey: "isDateClashing")
                            DispatchQueue.main.async {
                                self.errorMessagePlaceHolder.text = NSLocalizedString("bookingDateSelectionCheckStatusNotOkBookingDateView", comment: "Unfortunately the selected time is already booked.")
                                self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                                self.bookTheDateButton.isEnabled = false
                                self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                            }
                        }
                    case .orderedSame:
                        //print("The two dates are the same")
                        UserDefaults.standard.set(true, forKey: "isDateClashing")
                        DispatchQueue.main.async {
                            self.errorMessagePlaceHolder.text = NSLocalizedString("bookingDateSelectionCheckStatusNotOkBookingDateView", comment: "Unfortunately the selected time is already booked.")
                            self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                            self.bookTheDateButton.isEnabled = false
                            self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                        }
                    }
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
                else {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            } else {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        } else {
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    
    @objc func handleDismsiss(){
        if let shopID = self.barberShopUUID {
            self.firebaseNumberOfBookingReferenceSpecial.child("totalNumberOfBookings").child(shopID).removeAllObservers()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func manageChangesToNewBooking(){
        if let shopID = self.barberShopUUID, let bookID = self.bookingID {
            let firebaseRefxxmanage = Database.database().reference()
            firebaseRefxxmanage.child("bookings").child(shopID).child(bookID).observeSingleEvent(of: .value, with: { (snapshotttthshs) in
                print("kriss kaliko", bookID, HandleDataRequest.handleBookingsNode(firebaseData: (snapshotttthshs.value as? [String: AnyObject])!))
                if let dataDicionary = snapshotttthshs.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dataDicionary) == true {
                    let bookedDataSingle = Bookings()
                    bookedDataSingle.setValuesForKeys(dataDicionary)
                    print("it worked properly")
                    if let UID = bookedDataSingle.bookingID {
                        print("Data variation: ", UID, bookID)
                        if UID == bookID {
                            
                            if let startTimeChanged = bookedDataSingle.bookingStartTime, let endTimeChaned = bookedDataSingle.bookingEndTime, let shopID = self.barberShopUUID {
                                if startTimeChanged != "not available" && endTimeChaned != "not available" {
                                    print("move to new view")
                                    self.activityIndicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    
                                    let shoplistview = ShoppingListViewController()
                                    shoplistview.bookingDateViewDataObject = self
                                    shoplistview.BarberShopUUID = shopID
                                    shoplistview.bookingUUID = UID
                                    
                                    shoplistview.didShoppingVCBackButtonPressedBlock = { () in
                                        self.handleDismsiss()
                                    }
                                    
                                    let shopListController = UINavigationController(rootViewController: shoplistview)
                                    self.firebaseNumberOfBookingReferenceSpecial.child("totalNumberOfBookings").child(shopID).removeAllObservers()
                                    self.present(shopListController, animated: true, completion: nil)
                                    
                                    
                                } else {
                                    print("some where chilling")
                                    self.activityIndicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    self.errorMessagePlaceHolder.text = NSLocalizedString("afterPostRequestBookingStatusBookingDateView", comment: "Sorry the booking time slot has already been taken")
                                    self.errorMessagePlaceHolder.textColor = UIColor(r: 216, g: 127, b: 15)
                                    self.bookTheDateButton.isEnabled = false
                                    self.bookTheDateButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                                    self.chosenDatePlaceHolder.text = ""
                                }
                            } else {
                                self.activityIndicator.stopAnimating()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            }
                        } else {
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    } else {
                        self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                } else {
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            })
        }
    }
    
    
    //Available times getter
    
    func getBookingStartandEndDates(){
        self.statusOfAvailableBookingLable.text = ""
        DispatchQueue.global(qos: .background).async {
            let firebaseRefxxBookDates = Database.database().reference()
            if let shopID = self.barberShopUUID {
                firebaseRefxxBookDates.child("bookings").child(shopID).observeSingleEvent(of: .value, with: { (snapshootoototBookDates) in
                    if let dictionary = snapshootoototBookDates.value as? [String: AnyObject] {
                        let dictionaryCount = dictionary.count
                        var dictionaryCounterHold = 0
                        
                        self.bookingStartStopHold.removeAll()
                        self.bookingTimeIntervalMainArray.removeAll()
                        //self.availableTimesCalledSecond.removeAll()
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                        
                        for data in dictionary {
                            dictionaryCounterHold += 1
                            if let dictoo = data.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictoo) == true {
                                let bookingData = Bookings()
                                bookingData.setValuesForKeys(dictoo)
                                if let uiidd = bookingData.bookingID {
                                    if let _ = self.bookingStartStopHold.first(where: { $0.bookingID ==  uiidd }) {
                                        print("runaway dragon getBookingStart")
                                    } else {
                                        self.bookingStartStopHold.append(bookingData)
                                    }
                                }
                            }
                            
                            if dictionaryCount == dictionaryCounterHold {
                                dictionaryCounterHold = 0
                                if !self.bookingStartStopHold.isEmpty {
                                    DispatchQueue.main.async {
                                        self.perform(#selector(self.parseBaokingDates), with: self, afterDelay: 0.05)
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        self.statusOfAvailableBookingLable.text = ""
                                    }
                                }
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.statusOfAvailableBookingLable.text = ""
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    @objc func parseBaokingDates(){
        let pickerChosenTineDate = self.serviceTimeStartTimePicker.date.inDefaultRegion()
        DispatchQueue.global(qos: .background).async {
            if let barberID = self.barberID {
                if self.bookingStartStopHold.count > 0 {
                    let arrayMainHolderCounter = self.bookingStartStopHold.count
                    var arrayHolderStop = 0
                    
                    self.bookingTimeIntervalMainArray.removeAll()
                    for startStopSingle in self.bookingStartStopHold {
                        arrayHolderStop += 1
                        if let currentBarber = startStopSingle.bookedBarberID, let bookCompleted = startStopSingle.isCompleted, let isCancelled = startStopSingle.bookingCancel, let bookingStart = startStopSingle.bookingStartTime, let bookingEnd = startStopSingle.bookingEndTime, let tzone = startStopSingle.timezone, let bookingCalendar = startStopSingle.calendar, let bookingLocal = startStopSingle.local, let bookingUniqueIDD = startStopSingle.bookingID {
                            
                            
                            let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal)
                            let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingStart)
                            let verifyBookingStopDate = VerifyDateAssociation.checkDateData(timeZone: tzone, calendar: bookingCalendar, locale: bookingLocal, dateData: bookingEnd)
                            
                            if barberID == currentBarber && bookCompleted == "YES" && isCancelled == "NO" && verifyBookingData == true && verifyBookingStartDate == true && verifyBookingStopDate == true { //YES
                                let bookingRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tzone, calenName: bookingCalendar, LocName: bookingLocal)
                                
                                if let bookingStartDateRegion = bookingStart.toDate(style: .extended, region: bookingRegion), let bookingStopDateRegion = bookingEnd.toDate(style: .extended, region: bookingRegion) {
                                    let currentDateAndTime = DateInRegion()
                                    let userTimezone = DateByUserDeviceInitializer.tzone
                                    let userCalender = DateByUserDeviceInitializer.calenderNow
                                    let userLocal = DateByUserDeviceInitializer.localCode
                                    let currentUserRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: userLocal)
                                    
                                    
                                    if bookingStartDateRegion.isAfterDate(currentDateAndTime, orEqual: true, granularity: .minute) && bookingStartDateRegion.isInside(date: pickerChosenTineDate, granularity: .day) {
                                        let bookingtimeinterval = BookingsTimeInterval()
                                        bookingtimeinterval.bookingStartTime = bookingStartDateRegion.convertTo(region: currentUserRegion)
                                        bookingtimeinterval.bookingEndTime = bookingStopDateRegion.convertTo(region: currentUserRegion)
                                        bookingtimeinterval.bookingUniqueID = bookingUniqueIDD
                                        
                                        if let uiididx = bookingtimeinterval.bookingUniqueID {
                                            if let _ = self.bookingTimeIntervalMainArray.first(where: { $0.bookingUniqueID ==  uiididx }) {
                                                print("runaway dragon parseBooking", self.bookingTimeIntervalMainArray.count, uiididx)
                                            } else {
                                                print("gizarelii")
                                                self.bookingTimeIntervalMainArray.append(bookingtimeinterval)
                                                self.bookingTimeIntervalMainArray.sort(by: { (appstx, appsty) -> Bool in
                                                    if let mannerx = appstx.bookingStartTime,let mannery = appsty.bookingStartTime {
                                                        return mannerx.isBeforeDate(mannery, orEqual: true, granularity: .minute)
                                                    } else {
                                                        return false
                                                    }
                                                })
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                        if arrayHolderStop == arrayMainHolderCounter {
                            arrayHolderStop = 0
                            if !self.bookingTimeIntervalMainArray.isEmpty {
                                print("runing mad crazy")
                                DispatchQueue.main.async {
                                    self.perform(#selector(self.getOpeningAndClosingHours), with: self, afterDelay: 0.05)
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.statusOfAvailableBookingLable.text = ""
                                }
                            }
                        }
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.statusOfAvailableBookingLable.text = ""
                    }
                }
            }
        }
    }
    
    
    @objc func getOpeningAndClosingHours(){
        print("blame me")
        let clendarName = DateByUserDeviceInitializer.calenderNow
        let tZoneNow = DateByUserDeviceInitializer.tzone
        let modLocal = "en_US"
        let translatorRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tZoneNow, calenName: clendarName, LocName: modLocal)
        let pickerDateHolderAwaitingTrans = self.serviceTimeStartTimePicker.date.inDefaultRegion()
        let newTransDateInDiffrentRegion = pickerDateHolderAwaitingTrans.convertTo(region: translatorRegion)
        let selectDateDay = newTransDateInDiffrentRegion.weekdayName(.default)
        let uniqueIDD = UUID().uuidString
        let uniqueIDDSecond = UUID().uuidString
        
        DispatchQueue.global(qos: .background).async {
            let firebaseRefere = Database.database().reference()
            if let userID = self.barberShopUUID {
                firebaseRefere.child("workhours").child(userID).child(selectDateDay).observeSingleEvent(of: .value, with: { (snapshooottss) in
                    if let dictionary = snapshooottss.value as? [String: AnyObject], HandleDataRequest.handleWorkhoursNode(firebaseData: dictionary) == true {
                        self.timeSelectorCurrentDayWorkHour.setValuesForKeys(dictionary)
                        print("gary peyton")
                        if let openTimeHour = self.timeSelectorCurrentDayWorkHour.openingTimeHour, let openTimeMinute = self.timeSelectorCurrentDayWorkHour.openingTimeMinute, let openTimeSecond = self.timeSelectorCurrentDayWorkHour.openingTimeSecond, let closingTimeHour = self.timeSelectorCurrentDayWorkHour.closingTimeHour, let closingTimeMinute = self.timeSelectorCurrentDayWorkHour.closingTimeMinute, let closingTimeSecond = self.timeSelectorCurrentDayWorkHour.closingTimeSecond, let minutesBeforeClosing = self.timeSelectorCurrentDayWorkHour.minutesBeforeClosing{
                            if let openHour = Int(openTimeHour), let openMinute = Int(openTimeMinute), let openSecond = Int(openTimeSecond), let closeHour = Int(closingTimeHour), let closeMinute = Int(closingTimeMinute), let closeSecond = Int(closingTimeSecond), let minutesBefore = Int(minutesBeforeClosing) {
                                print("suge knight")
                                if !self.bookingTimeIntervalMainArray.isEmpty {
                                    if let firstStartDate = self.bookingTimeIntervalMainArray.first?.bookingStartTime, let lastClosingDate = self.bookingTimeIntervalMainArray.last?.bookingEndTime {
                                        
                                        
                                        if let openingTimeDate = firstStartDate.dateBySet(hour: openHour, min: openMinute, secs: openSecond), let closingTimeDateInitial = lastClosingDate.dateBySet(hour: closeHour, min: closeMinute, secs: closeSecond) {
                                            let closingTimeDate = closingTimeDateInitial - minutesBefore.minutes
                                            
                                            
                                            if closingTimeDate.isAfterDate(lastClosingDate, granularity: .minute){
                                                
                                                
                                                print("upper cut")
                                                let bookingtimeintervalLastCall = BookingsTimeInterval()
                                                bookingtimeintervalLastCall.bookingStartTime = lastClosingDate
                                                bookingtimeintervalLastCall.bookingEndTime = closingTimeDate //here
                                                bookingtimeintervalLastCall.bookingUniqueID = uniqueIDD
                                                
                                                if let uiididx = bookingtimeintervalLastCall.bookingUniqueID {
                                                    if let _ = self.bookingTimeIntervalMainArray.first(where: { $0.bookingUniqueID ==  uiididx }) {
                                                        print("runaway dragon")
                                                    } else {
                                                        self.bookingTimeIntervalMainArray.append(bookingtimeintervalLastCall)
                                                    }
                                                }
                                                
                                                print("closing date added")
                                                
                                                let bookingtimeintervalfirstCall = BookingsTimeInterval()
                                                bookingtimeintervalfirstCall.bookingEndTime = firstStartDate
                                                bookingtimeintervalfirstCall.bookingStartTime = openingTimeDate
                                                bookingtimeintervalfirstCall.bookingUniqueID = uniqueIDDSecond
                                                
                                                
                                                if let uiididx = bookingtimeintervalfirstCall.bookingUniqueID {
                                                    if let _ = self.bookingTimeIntervalMainArray.first(where: { $0.bookingUniqueID ==  uiididx }) {
                                                        print("runaway dragon")
                                                    } else {
                                                        self.bookingTimeIntervalMainArray.append(bookingtimeintervalfirstCall)
                                                        self.bookingTimeIntervalMainArray.sort(by: { (appstx, appsty) -> Bool in
                                                            if let mannerx = appstx.bookingStartTime,let mannery = appsty.bookingStartTime {
                                                                return mannerx.isBeforeDate(mannery, orEqual: true, granularity: .minute) //<
                                                            } else {
                                                                return false
                                                            }
                                                        })
                                                        print("opening date added and sorted")
                                                    }
                                                }
                                                
                                                self.perform(#selector(self.createInBetweenTimeInterval), with: self, afterDelay: 0.03)
                                                
                                            } else if closingTimeDate.isBeforeDate(lastClosingDate, granularity: .minute)  {
                                                
                                                print("lower cut")
                                                let bookingtimeintervalLastCallBefore = BookingsTimeInterval()
                                                bookingtimeintervalLastCallBefore.bookingStartTime = closingTimeDate
                                                bookingtimeintervalLastCallBefore.bookingEndTime = lastClosingDate
                                                bookingtimeintervalLastCallBefore.bookingUniqueID = uniqueIDD
                                                
                                                if let uiididx = bookingtimeintervalLastCallBefore.bookingUniqueID {
                                                    if let _ = self.bookingTimeIntervalMainArray.first(where: { $0.bookingUniqueID ==  uiididx }) {
                                                        print("runaway dragon")
                                                    } else {
                                                        self.bookingTimeIntervalMainArray.append(bookingtimeintervalLastCallBefore)
                                                    }
                                                }
                                                print("closing date added")
                                                
                                                let bookingtimeintervalfirstCallBefore = BookingsTimeInterval()
                                                bookingtimeintervalfirstCallBefore.bookingStartTime = openingTimeDate
                                                bookingtimeintervalfirstCallBefore.bookingEndTime = firstStartDate
                                                bookingtimeintervalfirstCallBefore.bookingUniqueID = uniqueIDDSecond
                                                
                                                if let uiididx = bookingtimeintervalfirstCallBefore.bookingUniqueID {
                                                    if let _ = self.bookingTimeIntervalMainArray.first(where: { $0.bookingUniqueID ==  uiididx }) {
                                                        print("runaway dragon")
                                                    } else {
                                                        self.bookingTimeIntervalMainArray.append(bookingtimeintervalfirstCallBefore)
                                                        self.bookingTimeIntervalMainArray.sort(by: { (appstx, appsty) -> Bool in
                                                            if let mannerx = appstx.bookingStartTime,let mannery = appsty.bookingStartTime {
                                                                return mannerx.isBeforeDate(mannery, orEqual: true, granularity: .minute) //<
                                                            } else {
                                                                return false
                                                            }
                                                        })
                                                    }
                                                }
                                                print("opening date added")
                                                
                                                self.perform(#selector(self.createInBetweenTimeInterval), with: self, afterDelay: 0.03)
                                                
                                            }
                                        }
                                    }
                                }
                                
                            }
                            
                        }
                    }else {
                        self.timeSelectorCurrentDayWorkHour = Workhours()
                        DispatchQueue.main.async {
                            self.statusOfAvailableBookingLable.text = ""
                        }
                        print("Day work hours not gotten")
                    }
                })
            }
        }
    }
    
    
    @objc func createInBetweenTimeInterval(){
        self.bookingTimeIntervalMainArraySecond.removeAll()
        self.inbetweenavailableTimes.removeAll()
        if !self.bookingTimeIntervalMainArray.isEmpty {
            let numberOfItems = self.bookingTimeIntervalMainArray.count
            var numberOfItemsCounter = 0
            for btinterval in self.bookingTimeIntervalMainArray {
                numberOfItemsCounter += 1
                if self.inbetweenavailableTimes.isEmpty {
                    if let uniqueID = btinterval.bookingUniqueID, let bookingEndTime = btinterval.bookingEndTime {
                        let inbetweenHolder = IntermediaryAvailableTimes()
                        inbetweenHolder.bookingEndTimeFirst = bookingEndTime
                        inbetweenHolder.bookingUniqueIDFirst = uniqueID
                        self.inbetweenavailableTimes.append(inbetweenHolder)
                    }
                    
                    
                } else {
                    if let endTimeFirst = self.inbetweenavailableTimes.first, let endTimeSel = endTimeFirst.bookingEndTimeFirst, let bookingStartTime = btinterval.bookingStartTime, let bookingEndTimeKey = btinterval.bookingEndTime, let bookingUniqueIDKEY = btinterval.bookingUniqueID {
                        
                        let uniqueIDInBetween = UUID().uuidString
                        let bookingtimeintervalInBetween = BookingsTimeInterval()
                        bookingtimeintervalInBetween.bookingStartTime = endTimeSel
                        bookingtimeintervalInBetween.bookingEndTime = bookingStartTime
                        bookingtimeintervalInBetween.bookingUniqueID = uniqueIDInBetween
                        
                        self.bookingTimeIntervalMainArraySecond.append(bookingtimeintervalInBetween)
                        self.bookingTimeIntervalMainArraySecond.sort(by: { (appstx, appsty) -> Bool in
                            if let mannerx = appstx.bookingStartTime,let mannery = appsty.bookingStartTime {
                                return mannerx.isBeforeDate(mannery, orEqual: true, granularity: .minute) //<
                            } else {
                                return false
                            }
                        })
                        self.inbetweenavailableTimes.removeAll()
                        let inbetweenHolderShoe = IntermediaryAvailableTimes()
                        inbetweenHolderShoe.bookingEndTimeFirst = bookingEndTimeKey
                        inbetweenHolderShoe.bookingUniqueIDFirst = bookingUniqueIDKEY
                        self.inbetweenavailableTimes.append(inbetweenHolderShoe)
                    }
                }
                
                if numberOfItemsCounter == numberOfItems {
                    numberOfItemsCounter = 0
                    if !self.bookingTimeIntervalMainArraySecond.isEmpty {
                        self.bookingTimeIntervalMainArray.append(contentsOf: bookingTimeIntervalMainArraySecond)
                        self.bookingTimeIntervalMainArray.sort(by: { (appstx, appsty) -> Bool in
                            if let mannerx = appstx.bookingStartTime,let mannery = appsty.bookingStartTime {
                                return mannerx.isBeforeDate(mannery, orEqual: true, granularity: .minute) //<
                            } else {
                                return false
                            }
                        })
                        self.bookingTimeIntervalMainArraySecond.removeAll()
                        
                        DispatchQueue.main.async {
                            self.perform(#selector(self.removeTakenBooking), with: self, afterDelay: 0.03)
                        }
                    }
                }
                
            }
        }
    }
    
    @objc func removeTakenBooking(){
        
        DispatchQueue.global(qos: .background).async {
            
            let initialArrayHolder = self.bookingTimeIntervalMainArray.count
            var initialArrayHolderCheck = 0
            
            self.availableTimesCalledSecond.removeAll()
            self.bookingTimeIntervalMainArraySecondCall.removeAll()
            
            for intervalSingleRemove in self.bookingTimeIntervalMainArray {
                initialArrayHolderCheck += 1
                if let bookingIDDX = intervalSingleRemove.bookingUniqueID, let shopIDDXOX = self.barberShopUUID {
                    
                    let firebaseReferenceRemoveBooking = Database.database().reference()
                    firebaseReferenceRemoveBooking.child("bookings").child(shopIDDXOX).child(bookingIDDX).observeSingleEvent(of: .value, with: { (snapshototototo) in
                        if snapshototototo.value as? [String: AnyObject] == nil {
                            if let bookingIDDX = intervalSingleRemove.bookingUniqueID, let startTimx = intervalSingleRemove.bookingStartTime, let endTimexx = intervalSingleRemove.bookingEndTime , let serviceTimeInterval = self.totalTimeForServicePushed {
                                let currentTimeWitness = DateInRegion()
                                
                                
                                if endTimexx.isAfterDate(currentTimeWitness, orEqual: true, granularity: .minute) {
                                    
                                    let checkTime = startTimx + serviceTimeInterval.minutes
                                    
                                    
                                    if checkTime.isInRange(date: startTimx, and: endTimexx, orEqual: true, granularity: .minute) {
                                        
                                        let availTime = AvailableTimes()
                                        availTime.bookingUniqueID = bookingIDDX
                                        availTime.bookingStartTimeFirst = startTimx
                                        availTime.bookingEndTimeSecond = endTimexx
                                        //.string(dateStyle: .short, timeStyle: .short)
                                        
                                        let combineString = startTimx.toString(.dateTime(.short)) + " - " + endTimexx.toString(.dateTime(.short))
                                        availTime.bookingStartTimeEndTimeString = combineString
                                        
                                        if let yruiidd = availTime.bookingUniqueID {
                                            
                                            if let firstNegative = self.availableTimesCalled.first(where: { $0.bookingUniqueID ==  yruiidd }) {
                                                if let indexValue = self.availableTimesCalled.index(of: firstNegative) {
                                                    self.availableTimesCalled[indexValue] = availTime
                                                }
                                                print("runaway dragon")
                                            } else {
                                                self.availableTimesCalled.append(availTime)
                                                print("red nose")
                                            }
                                            
                                            //second call
                                            if let _ = self.availableTimesCalledSecond.first(where: { $0.bookingUniqueID ==  yruiidd }) {
                                                print("runaway dragon")
                                            } else {
                                                self.availableTimesCalledSecond.append(availTime)
                                                print("green nose")
                                            }
                                        }
                                        
                                        DispatchQueue.main.async {
                                            self.collectionView.reloadData()
                                        }
                                        
                                    }
                                }
                                
                                
                            }
                            
                        }
                    })
                }
                
                if initialArrayHolderCheck == initialArrayHolder {
                    DispatchQueue.main.async {
                        self.perform(#selector(self.searchAndDeleteOldArrayItems), with: self, afterDelay: 3)
                    }
                }
            }
        }
    }

    
    @objc func searchAndDeleteOldArrayItems(){
        if !self.availableTimesCalledSecond.isEmpty {
            for singleApp in self.availableTimesCalled {
                if let uuiidBook = singleApp.bookingUniqueID {
                    
                    if let _ = self.availableTimesCalledSecond.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                        print("good data")
                    } else {
                        print("deleted once")
                        self.availableTimesCalled = self.availableTimesCalled.filter { $0.bookingUniqueID != uuiidBook }
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
        } else {
            print("the shogun calls")
            self.statusOfAvailableBookingLable.text = NSLocalizedString("bookingStatusTextBookingDateView", comment: "Fully booked")
            self.availableTimesCalled.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}


class customBookingDateCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let openStartTimeHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        ehp.textColor = UIColor.white
        ehp.textAlignment = .center
        return ehp
    }()
    
    func setupViews(){
        addSubview(openStartTimeHolder)
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|[v0]|", views: openStartTimeHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-|", views: openStartTimeHolder)
        
    }
    
    override func prepareForReuse() {
        self.openStartTimeHolder.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
