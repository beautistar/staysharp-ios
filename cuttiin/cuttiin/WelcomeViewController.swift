//
//  WelcomeViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/12/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import SwiftDate

class WelcomeViewController: UIViewController, FBSDKLoginButtonDelegate {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let firebaseREF = Database.database().reference()
    
    var fBookloginButton = FBSDKLoginButton()
    
    lazy var backgroundImageView: UIImageView = {
        let bImageView = UIImageView()
        bImageView.translatesAutoresizingMaskIntoConstraints = false
        bImageView.contentMode = .scaleAspectFill
        bImageView.image = UIImage(named: "stay_sharp_frontpage_bg")
        return bImageView
    }()
    
    let backgroundImageTintCoverView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear //.black.withAlphaComponent(0.5)
        return view
    }()
    
    let mainLogoImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.image = UIImage(named: "mainLogo")
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleToFill
        return tniv
    }()
    
    let lowerInputContainerView: UIView = {
        let licview = UIView()
        licview.translatesAutoresizingMaskIntoConstraints = false
        licview.backgroundColor = UIColor.clear
        return licview
    }()
    
    
    lazy var loginButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        fbutton.setTitle(NSLocalizedString("logInButtonTitleWelcomeView", comment: "LOGIN"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.addTarget(self, action: #selector(handleShowLoginView), for: .touchUpInside)
        return fbutton
    }()
    
    lazy var createAccountButton: UIButton = {
        let cbutton = UIButton()
        cbutton.translatesAutoresizingMaskIntoConstraints = false
        cbutton.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        cbutton.setTitle(NSLocalizedString("createAnAccountButtonTitleWelcomeView", comment: "CREATE AN ACCOUNT"), for: .normal)
        cbutton.setTitleColor(UIColor.white, for: .normal)
        cbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        cbutton.layer.cornerRadius = 5
        cbutton.layer.masksToBounds = true
        cbutton.addTarget(self, action: #selector(handleCreatingAnAccount), for: .touchUpInside)
        return cbutton
    }()
    
    let facebookLoginButtonContainerView: UIView = {
        let fBookview = UIView()
        fBookview.translatesAutoresizingMaskIntoConstraints = false
        return fBookview
    }()
    
    lazy var customFaceBookLoginButton: UIButton = {
        let customFBButton = UIButton()
        customFBButton.translatesAutoresizingMaskIntoConstraints = false
        customFBButton.backgroundColor = UIColor(r: 59, g: 89, b: 152)
        customFBButton.layer.cornerRadius = 5
        customFBButton.layer.masksToBounds = true
        customFBButton.setTitle(NSLocalizedString("facebookButtonTitle", comment: "CONTINUE WITH FACEBOOK") , for: .normal)
        customFBButton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        customFBButton.setTitleColor(.white, for: .normal)
        customFBButton.addTarget(self, action: #selector(handleFaceBookCredentialsLogin), for: .touchUpInside)
        return customFBButton
    }()
    
    lazy var forBarbersSectionButton: UIButton = {
        let fbbutton = UIButton()
        fbbutton.translatesAutoresizingMaskIntoConstraints = false
        fbbutton.backgroundColor = UIColor(r: 5, g: 22, b: 31)
        fbbutton.setTitle(NSLocalizedString("forBarbersButtonWelcomeView", comment: "FOR BARBERS!"), for: .normal)
        fbbutton.setTitleColor(UIColor.white, for: .normal)
        fbbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        fbbutton.addTarget(self, action: #selector(handleForBarberSection), for: .touchUpInside)
        return fbbutton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.white
        view.addSubview(backgroundImageView)
        view.addSubview(backgroundImageTintCoverView)
        view.bringSubview(toFront: backgroundImageTintCoverView)
        setupViewObjectsAutoContriaints()
        fBookloginButton.delegate = self
        fBookloginButton.readPermissions = ["email", "public_profile"]
    }
    
    func handleTimedDismiss(){
        perform(#selector(dismissViewAfterLogin), with: self, afterDelay: 0.02)
    }
    
    @objc func dismissViewAfterLogin(){
        print("view dismissed")
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //checks of user is logged in
        if Auth.auth().currentUser?.uid != nil {
            UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    var faceBookLoginButtonContainerViewBottomAnchor: NSLayoutConstraint?
    var createAccountButtonBottomAnchor: NSLayoutConstraint?
    
    func setupViewObjectsAutoContriaints(){
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        backgroundImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        backgroundImageTintCoverView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageTintCoverView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        backgroundImageTintCoverView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImageTintCoverView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        backgroundImageTintCoverView.addSubview(lowerInputContainerView)
        backgroundImageTintCoverView.addSubview(mainLogoImageView)
        
        lowerInputContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        lowerInputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        lowerInputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lowerInputContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5, constant: -40).isActive = true
        
        
        mainLogoImageView.bottomAnchor.constraint(equalTo: lowerInputContainerView.topAnchor).isActive = true
        mainLogoImageView.centerXAnchor.constraint(equalTo: backgroundImageTintCoverView.centerXAnchor).isActive = true
        mainLogoImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        mainLogoImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        lowerInputContainerView.addSubview(loginButton)
        lowerInputContainerView.addSubview(createAccountButton)
        lowerInputContainerView.addSubview(facebookLoginButtonContainerView)
        lowerInputContainerView.addSubview(forBarbersSectionButton)
        
        forBarbersSectionButton.bottomAnchor.constraint(equalTo: lowerInputContainerView.bottomAnchor).isActive = true
        forBarbersSectionButton.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor).isActive = true
        forBarbersSectionButton.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        forBarbersSectionButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        faceBookLoginButtonContainerViewBottomAnchor = facebookLoginButtonContainerView.bottomAnchor.constraint(equalTo: forBarbersSectionButton.topAnchor, constant: -11)//22
        faceBookLoginButtonContainerViewBottomAnchor?.isActive = true
        facebookLoginButtonContainerView.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        facebookLoginButtonContainerView.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        facebookLoginButtonContainerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        facebookLoginButtonContainerView.addSubview(customFaceBookLoginButton)
        
        //fBookloginButton.frame = CGRect(x: 0, y: 0, width: view.frame.width - 24, height: 48)
        customFaceBookLoginButton.topAnchor.constraint(equalTo: facebookLoginButtonContainerView.topAnchor).isActive = true
        customFaceBookLoginButton.centerXAnchor.constraint(equalTo: facebookLoginButtonContainerView.centerXAnchor).isActive = true
        customFaceBookLoginButton.widthAnchor.constraint(equalTo: facebookLoginButtonContainerView.widthAnchor).isActive = true
        customFaceBookLoginButton.heightAnchor.constraint(equalTo: facebookLoginButtonContainerView.heightAnchor).isActive = true
        
        createAccountButtonBottomAnchor = createAccountButton.bottomAnchor.constraint(equalTo: facebookLoginButtonContainerView.topAnchor, constant: -11)
        createAccountButtonBottomAnchor?.isActive = true
        createAccountButton.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        createAccountButton.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        createAccountButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        loginButton.bottomAnchor.constraint(equalTo: createAccountButton.topAnchor, constant: -11).isActive = true
        loginButton.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Did log out of facebook")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        //print("Successfully logged in with facebook", result)
        handleFaceBookCredentialsLogin()
    }
    
    @objc func handleFaceBookCredentialsLogin(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, err) in
            
            if let actionChosen = result?.isCancelled {
                if actionChosen {
                    print("User selected the cancel button")
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
            }
            
            print("Break you off smooth")
            if let error = err {
                print("Custom login Failed: ", error.localizedDescription)
                print("red moon dancer")
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            } else {
                print("get money quick")
                if let tokenFBSDKL = FBSDKAccessToken.current().tokenString {
                    let credential = FacebookAuthProvider.credential(withAccessToken: tokenFBSDKL)
                    
                    Auth.auth().signInAndRetrieveData(with: credential, completion: { (AuthUserData, errrorororor) in
                        print("sign in started")
                        if let error = errrorororor {
                            print("Design waldo cages")
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                            if let errCode = AuthErrorCode(rawValue: error._code){
                                switch errCode {
                                case .networkError:
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("networkErrorCodeLoginView", comment: "network error occurred during the operation"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                case .invalidCredential:
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("invalidCredentialsErrorCodeLoginView", comment: "login credentials supplied is invalid"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                case .operationNotAllowed:
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                case .emailAlreadyInUse:
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("emailAlreadyInUseErrorCodeLoginView", comment: "email already in use by an existing account"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                case .userDisabled:
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("accountDisabledErrorCodeLoginView", comment: "Account disabled please contact Customer care"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                case .userNotFound:
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("accountNotFoundEasilyErrorCodeLoginView", comment: "Account disabled please contact Customer care - mising link"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                case .accountExistsWithDifferentCredential:
                                    print("Lions never die my guy")
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("accountAlreadyExistWithDifferentCreditentialErrorCodeLoginView", comment: "An account already exists with the same email address but different sign-in credentials."), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    
                                    let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("defaultErrorCodeLoginView", comment: "Ooops went wrong please restart the app and try again"), preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                }
                            }
                            return
                        }
                        
                        if let userUID = AuthUserData?.user.uid, let userEmail = AuthUserData?.user.email, let userDisplayName = AuthUserData?.user.displayName {
                            print("EMAIL: ", userEmail)
                            let dataref = Database.database().reference()
                            dataref.child("users").child(userUID).observeSingleEvent(of: .value, with: { (snapshottsds) in
                                let dataSnapshot = snapshottsds.value as? [String: AnyObject]
                                
                                if dataSnapshot == nil {
                                    let firebaseUser = Auth.auth().currentUser
                                    
                                    firebaseUser?.sendEmailVerification(completion: { (errorrrrr) in
                                        if let error = errorrrrr {
                                            print(error.localizedDescription)
                                            self.activityIndicator.stopAnimating()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                            
                                            firebaseUser?.delete(completion: { (errorrr) in
                                                if let error = errorrr {
                                                    print(error.localizedDescription)
                                                    return
                                                }
                                                do {
                                                    try Auth.auth().signOut()
                                                } catch let logoutError {
                                                    print(logoutError)
                                                }
                                            })
                                            return
                                        }
                                        
                                        FBSDKGraphRequest(graphPath: "me/picture?type=normal&redirect=false", parameters: nil).start { (connection, result, errorr) in
                                            if let error = errorr {
                                                print("could not get picture: ", error.localizedDescription)
                                                self.activityIndicator.stopAnimating()
                                                UIApplication.shared.endIgnoringInteractionEvents()
                                                return
                                            }
                                            
                                            guard let imageData = result as? [String: AnyObject] else {
                                                return
                                            }
                                            
                                            print("got picture url from graph", imageData)
                                            
                                            guard let imageURL = imageData["data"] as? NSDictionary else {
                                                return
                                            }
                                            let picURL = imageURL.object(forKey: "url") as! String
                                            
                                            DispatchQueue.global(qos: .background).async {
                                                guard let url = URL(string: picURL) else {
                                                    self.activityIndicator.stopAnimating()
                                                    UIApplication.shared.endIgnoringInteractionEvents()
                                                    return
                                                }
                                                
                                                do {
                                                    let data = try Data(contentsOf: url)
                                                    
                                                    DispatchQueue.main.async {
                                                        if let downloadedImage = UIImage(data: data) {
                                                            
                                                            let imageName = NSUUID().uuidString
                                                            let storageRef = Storage.storage().reference().child("profileImages").child("\(imageName).jpeg")
                                                            
                                                            if let uploadData = UIImageJPEGRepresentation(downloadedImage, 0.1) {
                                                                storageRef.putData(uploadData, metadata: nil, completion: { (metaData, errorrr) in
                                                                    if let error = errorrr {
                                                                        print(error.localizedDescription)
                                                                        self.activityIndicator.stopAnimating()
                                                                        UIApplication.shared.endIgnoringInteractionEvents()
                                                                        return
                                                                    }
                                                                    
                                                                    
                                                                    storageRef.downloadURL(completion: { (urlllll, erririri) in
                                                                        guard let downloadURL = urlllll?.absoluteString else {
                                                                            // Uh-oh, an error occurred!
                                                                            return
                                                                        }
                                                                        let newDate = DateInRegion()
                                                                        let strDate = newDate.toString(.extended)
                                                                        let userTimezone = DateByUserDeviceInitializer.tzone
                                                                        let userCalender = DateByUserDeviceInitializer.calenderNow
                                                                        let userLocal = DateByUserDeviceInitializer.localCode
                                                                        
                                                                        //sucessfully logged in
                                                                        let ref = Database.database().reference()
                                                                        let userReference = ref.child("users").child(userUID)
                                                                        
                                                                        let values = ["uniqueID": userUID ,"firstName": userDisplayName, "lastName": "last name" , "email": userEmail,"role": "customer","profileImageUrl": downloadURL, "mobileNumber": "1234","profileImageName": "\(imageName).jpeg", "dateAccountCreated": strDate, "timezone": userTimezone, "calendar":userCalender, "local":userLocal, "cardID": "not available", "cardDateCreated": "not available", "cardDateCalendar": "not available", "cardDateTimeZone": "not available", "cardDateLocale": "not available", "cardHasBeenDeleted":"NO"]
                                                                        
                                                                        userReference.updateChildValues(values, withCompletionBlock: { (errorr, ref) in
                                                                            if let error = errorr {
                                                                                //UIApplication.shared.endIgnoringInteractionEvents()
                                                                                print(error.localizedDescription)
                                                                                self.activityIndicator.stopAnimating()
                                                                                UIApplication.shared.endIgnoringInteractionEvents()
                                                                                return
                                                                            }else{
                                                                                //self.activityIndicator.startAnimating()
                                                                                self.activityIndicator.stopAnimating()
                                                                                UIApplication.shared.endIgnoringInteractionEvents()
                                                                                UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
                                                                                self.dismiss(animated: false, completion: nil)
                                                                            }
                                                                        })
                                                                        
                                                                        
                                                                    })
                                                                    
                                                                })
                                                            }
                                                        }
                                                    }
                                                } catch {
                                                    print("profile picture not available")
                                                    self.activityIndicator.stopAnimating()
                                                    UIApplication.shared.endIgnoringInteractionEvents()
                                                }
                                            }
                                        }
                                        
                                        
                                    })
                                } else {
                                    print("BRING OG GAMO")
                                    self.activityIndicator.stopAnimating()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
                                    self.dismiss(animated: false, completion: nil)
                                }
                                
                                
                            }, withCancel: nil)
                            
                        } else {
                            print("Data missing #####")
                            self.activityIndicator.stopAnimating()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                            if let userRX = Auth.auth().currentUser {
                                userRX.delete { error in
                                    if error != nil {
                                        // An error happened.
                                        print("Shear money hold")
                                        FBSDKLoginManager().logOut()
                                        do{
                                            try Auth.auth().signOut()
                                            
                                            let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("noEmailSuppliedBackFromFacebookAuthenticationErrorCodeLoginView", comment: "login credentials supplied is invalid, Tap the CREATE AN ACCOUNT button to create an account using your email and Password"), preferredStyle: .alert)
                                            let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                            alert.addAction(OKAction)
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        }catch let logoutError {
                                            print(logoutError)
                                        }
                                    } else {
                                        // Account deleted.
                                        print("Shear money hold hold one a little")
                                        FBSDKLoginManager().logOut()
                                        do{
                                            try Auth.auth().signOut()
                                            
                                            let alert = UIAlertController(title: NSLocalizedString("alertViewHeaderTextWelcomeErrorHandle", comment: "Error"), message: NSLocalizedString("noEmailSuppliedBackFromFacebookAuthenticationErrorCodeLoginView", comment: "login credentials supplied is invalid, Tap the CREATE AN ACCOUNT button to create an account using your email and Password"), preferredStyle: .alert)
                                            let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: nil)
                                            alert.addAction(OKAction)
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        }catch let logoutError {
                                            print(logoutError)
                                        }
                                    }
                                }
                            }
                            
                            
                            
                        }
                    })
                    
                } else {
                    //here we say why
                    print("invalid token string given due to user actions")
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        }
    }
    
    @objc func handleShowLoginView(){
        let loginview = LoginViewController()
        loginview.welcomeviewholder = self
        let navController = UINavigationController(rootViewController: loginview)
        present(navController, animated: true, completion: nil)
        
    }
    
    @objc func handleCreatingAnAccount(){
        let registerview = RegistrationViewController()
        let navController = UINavigationController(rootViewController: registerview)
        present(navController, animated: true, completion: nil)
    }
    
    @objc func handleForBarberSection(){
        self.createAccountButton.removeTarget(nil, action: nil, for: .allEvents)
        self.createAccountButton.setTitle(NSLocalizedString("registerButtonTitleWelcomeScreen", comment: "REGISTER"), for: .normal)
        self.createAccountButton.addTarget(self, action: #selector(handleShowBarberRegister), for: .touchUpInside)
        
        self.customFaceBookLoginButton.isHidden = true
        self.faceBookLoginButtonContainerViewBottomAnchor?.isActive = false
        faceBookLoginButtonContainerViewBottomAnchor = facebookLoginButtonContainerView.bottomAnchor.constraint(equalTo: forBarbersSectionButton.topAnchor, constant: 0)
        self.faceBookLoginButtonContainerViewBottomAnchor?.isActive = true
        
        
        self.createAccountButtonBottomAnchor?.isActive = false
        createAccountButtonBottomAnchor = createAccountButton.bottomAnchor.constraint(equalTo: facebookLoginButtonContainerView.topAnchor, constant: 0)
        self.createAccountButtonBottomAnchor?.isActive = true
        
        self.forBarbersSectionButton.removeTarget(nil, action: nil, for: .allEvents)
        self.forBarbersSectionButton.setTitle(NSLocalizedString("goBackButtonTitleWelcomeScreen", comment: "GO BACK!"), for: .normal)
        self.forBarbersSectionButton.addTarget(self, action: #selector(handleGoBackActions), for: .touchUpInside)
    }
    
    @objc func handleShowBarberRegister(){
        let registerViewBarber = RegisterBarberViewController()
        let navController = UINavigationController(rootViewController: registerViewBarber)
        present(navController, animated: true, completion: nil)
        
    }
    
    @objc func handleGoBackActions(){
        self.createAccountButton.removeTarget(nil, action: nil, for: .allEvents)
        self.createAccountButton.setTitle(NSLocalizedString("createAnAccountButtonTitleWelcomeView", comment: "CREATE AN ACCOUNT"), for: .normal)
        self.createAccountButton.addTarget(self, action: #selector(handleCreatingAnAccount), for: .touchUpInside)
        
        self.customFaceBookLoginButton.isHidden = false
        self.faceBookLoginButtonContainerViewBottomAnchor?.isActive = false
        faceBookLoginButtonContainerViewBottomAnchor = facebookLoginButtonContainerView.bottomAnchor.constraint(equalTo: forBarbersSectionButton.topAnchor, constant: -11) //
        
        self.faceBookLoginButtonContainerViewBottomAnchor?.isActive = true
        
        self.createAccountButtonBottomAnchor?.isActive = false
        createAccountButtonBottomAnchor = createAccountButton.bottomAnchor.constraint(equalTo: facebookLoginButtonContainerView.topAnchor, constant: -11)
        self.createAccountButtonBottomAnchor?.isActive = true
        
        self.forBarbersSectionButton.removeTarget(nil, action: nil, for: .allEvents)
        self.forBarbersSectionButton.setTitle(NSLocalizedString("forBarbersButtonWelcomeView", comment: "FOR BARBERS!"), for: .normal)
        self.forBarbersSectionButton.addTarget(self, action: #selector(handleForBarberSection), for: .touchUpInside)
        
    }
    
    func printAllFont(){
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
    func handleWrongLocalAndDate(){
        
        let alert = UIAlertController(title: nil, message: NSLocalizedString("invalidLocaleAndTimeZoneErrorMessageTextPasswordChange", comment: "Your current device Language and Time zone are not curently supported on this device"), preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: { (alertaction) in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
