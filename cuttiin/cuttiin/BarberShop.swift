//
//  BarberShop.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 19/09/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

@objcMembers class BarberShop: NSObject {
    
    var accountNumerKontoNr: String?
    var accountNumerRegNr: String?
    var calendar: String?
    var companyAddressLatitude: String?
    var companyAddressLongitude: String?
    var companyCoverPhotoName: String?
    var companyCoverPhotoUrl: String?
    var companyFormattedAddress: String?
    var companyLogoImageName: String?
    var companyLogoImageUrl: String?
    var companyName: String?
    var currencyCode: String?
    var dateAccountCreated: String?
    var email: String?
    var local: String?
    var mobileNumber: String?
    var rating: String?
    var role: String?
    var timezone: String?
    var uniqueID: String?
    
}
