//
//  ChoosePhotoViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces
import SwiftDate

class ChoosePhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    var companyAddressLong: String?
    var companyAddressLat: String?
    
    var customerOrBarberChoice: String?
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let picker = UIImagePickerController()
    
    let headerTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("headerTitleTextChoosePhotView", comment: "Welcome")
        ht.font = UIFont(name: "HelveticaNeue-Bold", size: 22)
        ht.textColor = UIColor(r: 23, g: 69, b: 90)
        ht.textAlignment = .center
        return ht
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_picture")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.layer.cornerRadius = 100
        imageView.layer.masksToBounds = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let textContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    let companyNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let companyNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let companyNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    lazy var addressSearchLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("barberShopAddressTextBarberProfileEditorView", comment: "Address")
        ht.textColor = UIColor.gray
        ht.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ht.isUserInteractionEnabled = true
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        ht.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowGoogleAutocomple)))
        return ht
    }()
    
    let addressSearchSeperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black
        return view
    }()
    
    let accountDetailsContainerView: UIView = {
        let adcview = UIView()
        adcview.translatesAutoresizingMaskIntoConstraints = false
        return adcview
    }()
    
    let regNrHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("regNrKontorNrTextChoosePhotoView", comment: "Reg nr and Konto nr")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let regNrTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.keyboardType = .numberPad
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("regNumberTextChoosePhotoView", comment: "Reg nr")
        em.addTarget(self, action: #selector(regNrtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(charMarLimit), for: .editingChanged)
        em.addTarget(self, action: #selector(regNrtextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let regNrSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    let kontoNrTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.keyboardType = .numberPad
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("kontorNumberTextChoosePhotoView", comment: "Konto nr.")
        em.addTarget(self, action: #selector(kontoNrtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(kontoNrtextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let kontoNrSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionTitleTextChoosePhotoView", comment: "Please upload your BarberShop logo!")
        ht.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
        ht.textColor = UIColor(r: 23, g: 69, b: 90)
        ht.textAlignment = .center
        return ht
    }()
    
    let descriptionBody: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionBodyTextChoosePhotoView", comment: "This is will be shown on your babrbershop profile. You can also update it later by accessing your profile page")
        ht.numberOfLines = 0
        ht.font = UIFont(name: "HelveticaNeue", size: 13)
        ht.textColor = UIColor(r: 23, g: 69, b: 90)
        ht.textAlignment = .center
        return ht
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        picker.delegate = self
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(handleUpdateBarberProfileWithURL))
        if let chosenViewToDisplay = customerOrBarberChoice {
            if chosenViewToDisplay == "Customer" {
                navigationItem.title = NSLocalizedString("navigationTitleTextChoosePhotoView", comment: "Registration - Final")
                self.descriptionTitle.text = NSLocalizedString("descriptionTitleTextChoosePhotoViewChangeHold", comment: "Please upload a profile a picture")
                self.descriptionBody.text = NSLocalizedString("descriptionBodyTextChoosePhotoViewChangeHold", comment: "This is will be shown on your profile. You can also update it later by accessing your profile page")
                navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(handleUpdateProfileWithURL))
                navigationItem.rightBarButtonItem?.isEnabled = false
                view.addSubview(headerTitle)
                view.addSubview(selectImageView)
                view.addSubview(textContainerView)
                view.addSubview(errorMessagePlaceHolder)
                setupInputContainerView()
            }else {
                navigationItem.title = NSLocalizedString("navigationTitleTextOnceChoosePhotoView", comment: "Registration: Step 2 of 5")
                navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(handleUpdateBarberProfileWithURL))
                navigationItem.rightBarButtonItem?.isEnabled = false
                view.addSubview(selectImageView)
                view.addSubview(textContainerView)
                view.addSubview(errorMessagePlaceHolder)
                setupBarberInputContainerView()
            }
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Auth.auth().currentUser?.uid == nil {
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.errorMessagePlaceHolder.text = ""
        
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }

    
    func setupInputContainerView(){
        headerTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        headerTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerTitle.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        headerTitle.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        selectImageView.topAnchor.constraint(equalTo: headerTitle.bottomAnchor, constant: 10).isActive = true
        selectImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        selectImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        textContainerView.topAnchor.constraint(equalTo: selectImageView.bottomAnchor, constant: 30).isActive = true
        textContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        textContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        textContainerView.addSubview(descriptionTitle)
        textContainerView.addSubview(descriptionBody)
        
        descriptionTitle.topAnchor.constraint(equalTo: textContainerView.topAnchor).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        descriptionBody.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        descriptionBody.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        descriptionBody.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        descriptionBody.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: descriptionBody.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
    }
    
    func setupBarberInputContainerView(){
        selectImageView.contentMode = .scaleToFill
        selectImageView.layer.cornerRadius = 0
        
        selectImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        selectImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        selectImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        textContainerView.topAnchor.constraint(equalTo: selectImageView.bottomAnchor, constant: 15).isActive = true
        textContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        textContainerView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        textContainerView.addSubview(companyNameHiddenPlaceHolder)
        textContainerView.addSubview(companyNameTextField)
        textContainerView.addSubview(companyNameSeperatorView)
        textContainerView.addSubview(addressSearchLabel)
        textContainerView.addSubview(addressSearchSeperatorView)
        textContainerView.addSubview(accountDetailsContainerView)
        textContainerView.addSubview(descriptionTitle)
        textContainerView.addSubview(descriptionBody)
        
        companyNameHiddenPlaceHolder.topAnchor.constraint(equalTo: textContainerView.topAnchor).isActive = true
        companyNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        companyNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        companyNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        
        companyNameTextField.topAnchor.constraint(equalTo: companyNameHiddenPlaceHolder.bottomAnchor).isActive = true
        companyNameTextField.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        companyNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        companyNameTextField.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        
        companyNameSeperatorView.leftAnchor.constraint(equalTo: textContainerView.leftAnchor).isActive = true
        companyNameSeperatorView.topAnchor.constraint(equalTo: companyNameTextField.bottomAnchor).isActive = true
        companyNameSeperatorView.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        companyNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        
        addressSearchLabel.topAnchor.constraint(equalTo: companyNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        addressSearchLabel.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        addressSearchLabel.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        addressSearchLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        addressSearchSeperatorView.leftAnchor.constraint(equalTo: textContainerView.leftAnchor).isActive = true
        addressSearchSeperatorView.topAnchor.constraint(equalTo: addressSearchLabel.bottomAnchor).isActive = true
        addressSearchSeperatorView.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        addressSearchSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        accountDetailsContainerView.topAnchor.constraint(equalTo: addressSearchSeperatorView.bottomAnchor, constant: 5).isActive = true
        accountDetailsContainerView.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        accountDetailsContainerView.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        accountDetailsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        accountDetailsContainerView.addSubview(regNrHiddenPlaceHolder)
        accountDetailsContainerView.addSubview(regNrTextField)
        accountDetailsContainerView.addSubview(regNrSeperatorView)
        accountDetailsContainerView.addSubview(kontoNrTextField)
        accountDetailsContainerView.addSubview(kontoNrSeperatorView)
        
        regNrHiddenPlaceHolder.topAnchor.constraint(equalTo: accountDetailsContainerView.topAnchor).isActive = true
        regNrHiddenPlaceHolder.centerXAnchor.constraint(equalTo: accountDetailsContainerView.centerXAnchor).isActive = true
        regNrHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        regNrHiddenPlaceHolder.widthAnchor.constraint(equalTo: accountDetailsContainerView.widthAnchor).isActive = true
        
        regNrTextField.topAnchor.constraint(equalTo: regNrHiddenPlaceHolder.bottomAnchor).isActive = true
        regNrTextField.leftAnchor.constraint(equalTo: accountDetailsContainerView.leftAnchor).isActive = true
        regNrTextField.widthAnchor.constraint(equalToConstant: 80).isActive = true
        regNrTextField.heightAnchor.constraint(equalTo: accountDetailsContainerView.heightAnchor, constant: -15).isActive = true
        
        regNrSeperatorView.leftAnchor.constraint(equalTo: accountDetailsContainerView.leftAnchor).isActive = true
        regNrSeperatorView.topAnchor.constraint(equalTo: regNrTextField.bottomAnchor).isActive = true
        regNrSeperatorView.widthAnchor.constraint(equalTo: regNrTextField.widthAnchor).isActive = true
        regNrSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        
        kontoNrTextField.topAnchor.constraint(equalTo: regNrHiddenPlaceHolder.bottomAnchor).isActive = true
        kontoNrTextField.leftAnchor.constraint(equalTo: regNrTextField.rightAnchor, constant: 10).isActive = true
        kontoNrTextField.rightAnchor.constraint(equalTo: accountDetailsContainerView.rightAnchor).isActive = true
        kontoNrTextField.heightAnchor.constraint(equalTo: accountDetailsContainerView.heightAnchor, constant: -15).isActive = true
        
        
        kontoNrSeperatorView.leftAnchor.constraint(equalTo: regNrSeperatorView.rightAnchor, constant: 10).isActive = true
        kontoNrSeperatorView.topAnchor.constraint(equalTo: kontoNrTextField.bottomAnchor).isActive = true
        kontoNrSeperatorView.widthAnchor.constraint(equalTo: kontoNrTextField.widthAnchor).isActive = true
        kontoNrSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        
        descriptionTitle.topAnchor.constraint(equalTo: accountDetailsContainerView.bottomAnchor, constant: 10).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        descriptionBody.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        descriptionBody.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        descriptionBody.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        descriptionBody.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: descriptionBody.bottomAnchor, constant: 5).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
    }
    
    @objc func textFieldDidChange(){
        if companyNameTextField.text == "" || addressSearchLabel.text == "" || regNrTextField.text == "" || kontoNrTextField.text == "" {
            //Disable button
            self.companyNameHiddenPlaceHolder.text = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
            navigationItem.rightBarButtonItem?.isEnabled = false
            
        } else {
            //Enable button
            self.companyNameHiddenPlaceHolder.text = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    @objc func companyNametextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.addressSearchSeperatorView.backgroundColor = UIColor.black
        self.regNrSeperatorView.backgroundColor = UIColor.black
        self.kontoNrSeperatorView.backgroundColor = UIColor.black
        self.companyNameHiddenPlaceHolder.isHidden = false
        self.regNrHiddenPlaceHolder.isHidden = true
    }
    
    @objc func regNrtextFieldDidChange(){
        self.regNrSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.addressSearchSeperatorView.backgroundColor = UIColor.black
        self.companyNameSeperatorView.backgroundColor = UIColor.black
        self.kontoNrSeperatorView.backgroundColor = UIColor.black
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.regNrHiddenPlaceHolder.isHidden = false
    }
    
    @objc func kontoNrtextFieldDidChange(){
        self.kontoNrSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.addressSearchSeperatorView.backgroundColor = UIColor.black
        self.companyNameSeperatorView.backgroundColor = UIColor.black
        self.regNrSeperatorView.backgroundColor = UIColor.black
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.regNrHiddenPlaceHolder.isHidden = false
        
    }
    
    @objc func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = NSLocalizedString("errorMessageNoCameraChoosePhotoView", comment: "Sorry, this device has no camera")
        }
    }
    
    @objc func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("takePhotoAlertViewProfileEdit", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotAlertViewProfileEdit", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    @objc func handleUpdateProfileWithURL(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let imageName = NSUUID().uuidString
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference().child("profileImages").child("\(imageName).jpeg")
        
        if let profileImage = selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
            storageRef.putData(uploadData, metadata: nil, completion: { (metaData, errorrr) in
                if let error = errorrr {
                    self.errorMessagePlaceHolder.text = error.localizedDescription
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                storageRef.downloadURL(completion: { (urllll, errorororor) in
                    guard let downloadURL = urllll?.absoluteString else {
                        // Uh-oh, an error occurred!
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    ref.child("users").child(uid).child("profileImageUrl").setValue(downloadURL)
                    ref.child("users").child(uid).child("profileImageName").setValue("\(imageName).jpeg")
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.moveToNextView()
                })
                
            })
        }
    }
    
    @objc func handleUpdateBarberProfileWithURL(){
        self.activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        self.activityIndicator.color = UIColor.darkGray
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let uid = Auth.auth().currentUser?.uid, let companyName = companyNameTextField.text, let companyAddress = self.addressSearchLabel.text, let calo = self.companyAddressLong, let cala = self.companyAddressLat, let accountNumerRegNr = regNrTextField.text, let accountNumerKontoNr = kontoNrTextField.text else {// changes to User object from users to customer
            return
        }
        
        let imageName = NSUUID().uuidString
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference().child("companyLogo").child("\(imageName).jpeg")
        
        if let profileImage = selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
            storageRef.putData(uploadData, metadata: nil, completion: { (metaData, errorrr) in
                if let error = errorrr {
                    self.errorMessagePlaceHolder.text = error.localizedDescription
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                storageRef.downloadURL(completion: { (urllllll, erririoorrr) in
                    guard let downloadURL = urllllll?.absoluteString else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    
                    ref.child("users").child(uid).child("companyName").setValue(companyName)
                    ref.child("users").child(uid).child("companyFormattedAddress").setValue(companyAddress)
                    ref.child("users").child(uid).child("companyAddressLongitude").setValue(calo)
                    ref.child("users").child(uid).child("companyAddressLatitude").setValue(cala)
                    ref.child("users").child(uid).child("accountNumerRegNr").setValue(accountNumerRegNr)
                    ref.child("users").child(uid).child("accountNumerKontoNr").setValue(accountNumerKontoNr)
                    ref.child("users").child(uid).child("companyLogoImageUrl").setValue(downloadURL)
                    ref.child("users").child(uid).child("companyLogoImageName").setValue("\(imageName).jpeg")
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.handleCreateMarker(shopuuid: uid, companyName: companyName, companyFormattedAddress: companyAddress, companyAddressLongitude: calo, companyAddressLatitude: cala, companyLogoImageUrl: downloadURL)
                    self.handleNextSkipAction()
                })
                
            })
        }
    }
    
    func handleCreateMarker(shopuuid: String, companyName: String, companyFormattedAddress: String, companyAddressLongitude: String, companyAddressLatitude: String, companyLogoImageUrl: String){
        
        let newDate = DateInRegion()
        let strDate = newDate.toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        let ref = Database.database().reference().child("barberMarkers").child(shopuuid)
        let values = ["companyID": shopuuid, "companyName": companyName,"companyLogoImageUrl": companyLogoImageUrl, "companyFormattedAddress": companyFormattedAddress, "companyAddressLongitude": companyAddressLongitude, "companyAddressLatitude": companyAddressLatitude,"dateCreated": strDate, "timezone": userTimezone, "calendar":userCalender, "local":userLocal]
        ref.updateChildValues(values) { (errorrorrrr, refsd) in
            if let error = errorrorrrr {
                print(error.localizedDescription)
                return
            }
            print("Marker created")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            selectImageView.contentMode = .scaleAspectFit
            selectImageView.image = selectedImage
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func moveToNextView(){
        let cmainView = CustomTabBarController()
        self.present(cmainView, animated: true, completion: nil)
    }
    
    func handleNextSkipAction(){
        let openingHoursview = OpeningHoursViewController()
        self.selectImageView.image = UIImage(named: "add_picture")
        let navController = UINavigationController(rootViewController: openingHoursview)
        present(navController, animated: true, completion: nil)
    }
    
    @objc func handleShowGoogleAutocomple(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func charMarLimit(){
        if let character = self.regNrTextField.text {
            if character.count  == 4 {
                self.kontoNrTextField.becomeFirstResponder()
            }
        }
    }

}

extension ChoosePhotoViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //print("Place name: \(place.name)")
        //print("Place address: \(place.formattedAddress)")
        //print("Place attributions: \(place.attributions)")
        if let mainAddress = place.formattedAddress {
            self.addressSearchLabel.textColor = UIColor.black
            self.addressSearchLabel.text = mainAddress
            self.companyAddressLong = place.coordinate.longitude.description
            self.companyAddressLat = place.coordinate.latitude.description
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
