//
//  BookingDetailHistoryViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 11/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate
import MGStarRatingView

class BookingDetailHistoryViewController: UIViewController, StarRatingDelegate {
    
    
    var orderdetailcustomer: AppointmentsCustomer?
    var bookingviewcontroler: BookingHistoryViewController?
    var serviceBooked = [Service]()
    var bookingIDD: String?
    var shopAddressLongitude: String?
    var shopAddressLatitude: String?
    var indexPathSelected: Int?
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var navBar: UINavigationBar = UINavigationBar()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white //(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    //body view objects
    let headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("summaryHeaderNumberOfCustomer", comment: "SUMMARY")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customOrderDetailCustomerCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let costTotalContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let totalTextHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalQuantityPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let costTotalSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    let addressHeaderTextPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("addressLabelTextOrderDetailView", comment: "Address")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var addressDataHoldPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowGoogleMaps)))
        return fnhp
    }()
    
    let contactHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.text = NSLocalizedString("contactLabelTextOrderDetailView", comment: "Contact")
        fnhp.textColor =  UIColor(r: 118, g: 187, b: 220)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let phoneLogothumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.image = UIImage(named: "Call-icon")
        return tniv
    }()
    
    let phoneNumberPlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "HelveticaNeue-Mediu", size: 15)
        fnhp.contentHorizontalAlignment = .left
        fnhp.setTitleColor(UIColor.white, for: .normal)
        fnhp.backgroundColor = UIColor.clear
        fnhp.addTarget(self, action: #selector(handleMakeCall), for: .touchUpInside)
        return fnhp
    }()
    
    let openingHoursHeaderTextPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("openingHoursAvailLabelTextOrderDetailView", comment: "Opening hours")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let openingHoursDataHoldPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    
    let bookingIDHeaderTextPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("bookingIDAvailLabelTextOrderDetailView", comment: "Order ID")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookingIDDataHoldPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .left
        return fnhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        navigationItem.title = NSLocalizedString("bookingNavigationTitleTextOrderDetail", comment: "Booking")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBaction))
        setNavBarToTheView()
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.type = .half
        starView.tag = 0
        starView.delegate = self
        view.addSubview(scrollView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        getTheSpecificBooking()
        getBarberShopWorkHours()
    }
    
    func getTheSpecificBooking(){
        if let appointCompData = self.orderdetailcustomer?.bookingUniqueID, let shopIDAX = self.orderdetailcustomer?.barberShopUniqueID {
            let firebaseReferenceAppointment = Database.database().reference()
            firebaseReferenceAppointment.child("bookings").child(shopIDAX).child(appointCompData).observeSingleEvent(of: .value, with: { (snapshotototCompApp) in
                if let dictionaryComp = snapshotototCompApp.value as? [String: AnyObject], HandleDataRequest.handleBookingsNode(firebaseData: dictionaryComp) == true {
                    let bookingSingleData = Bookings()
                    bookingSingleData.setValuesForKeys(dictionaryComp)
                    if let barberID = bookingSingleData.bookedBarberID, let serviceID = bookingSingleData.bookedServiceID, let totalTime = bookingSingleData.ConfirmedTotalTime, let totalQuantity = bookingSingleData.bookedServiceQuantity, let payIDDS = bookingSingleData.paymentID {
                        self.getBarberDetails(barberIDA: barberID, serviceIDA: serviceID)
                        self.totalQuantityPlaceHolder.text = totalQuantity
                        self.totalTimePlaceHolder.text = totalTime + "min"
                        self.bookingIDDataHoldPlaceHolder.text = appointCompData
                        
                        if payIDDS != "" && payIDDS != "nil" && shopIDAX != "" {
                            let firebaseReferenceCheckPayment = Database.database().reference()
                            firebaseReferenceCheckPayment.child("payments").child(shopIDAX).child(payIDDS).observeSingleEvent(of: .value, with: { (snapshotDataCheckCurrency) in
                                if let dictionaryCurrencyCheck = snapshotDataCheckCurrency.value as? [String: AnyObject], HandleDataRequest.handlePaymentsNode(firebaseData: dictionaryCurrencyCheck) == true {
                                    let paymentsCurrencySingle = Payments()
                                    paymentsCurrencySingle.setValuesForKeys(dictionaryCurrencyCheck)
                                    
                                    if let currentAmount = paymentsCurrencySingle.priceTotal {
                                        
                                        let firebaseReferenceCheckCurrency = Database.database().reference()
                                        firebaseReferenceCheckCurrency.child("users").child(shopIDAX).observeSingleEvent(of: .value, with: { (snapshotDataCheckCurrency) in
                                            if let dictionaryCurrencyCheck = snapshotDataCheckCurrency.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryCurrencyCheck) == true {
                                                let usersCurrencySingle = BarberShop()
                                                usersCurrencySingle.setValuesForKeys(dictionaryCurrencyCheck)
                                                
                                                if let currencySymbol = usersCurrencySingle.currencyCode {
                                                    self.totalPricePlaceHolder.text = currentAmount + currencySymbol
                                                }
                                            }
                                        })
                                        
                                    }
                                }
                            })
                        }
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getBarberDetails(barberIDA: String, serviceIDA: String){
        let firebaseReferenceBarberDetails = Database.database().reference()
        firebaseReferenceBarberDetails.child("users").child(barberIDA).observeSingleEvent(of: .value, with: { (snapshotBarberShopIDData) in
            if let dictionaryUsers = snapshotBarberShopIDData.value as? [String: AnyObject], HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryUsers) {
                let barberUserData = Barber()
                barberUserData.setValuesForKeys(dictionaryUsers)
                if let barberName = barberUserData.firstName, let barberShopUniqueID = barberUserData.barberShopID {
                    self.barberShopAddressPlaceHolder.text = barberName
                    self.getBarberShopName(shopIDX: barberShopUniqueID)
                    self.getServiceDetail(shopID: barberShopUniqueID, servName: serviceIDA)
                }
            }
        }, withCancel: nil)
    }
    
    func getBarberShopName(shopIDX: String){
        let firebaseReferenceBarberShop = Database.database().reference()
        firebaseReferenceBarberShop.child("users").child(shopIDX).observeSingleEvent(of: .value, with: { (snapshotBarberShopIDData) in
            if let dictionaryUsers = snapshotBarberShopIDData.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryUsers) == true {
                let barberShopUserData = BarberShop()
                barberShopUserData.setValuesForKeys(dictionaryUsers)
                if let barberShopName = barberShopUserData.companyName, let barberShopLogoImageURL = barberShopUserData.companyLogoImageUrl, let shopAddress = barberShopUserData.companyFormattedAddress, let phoneNumber = barberShopUserData.mobileNumber, let longiti = barberShopUserData.companyAddressLongitude, let latitu = barberShopUserData.companyAddressLatitude, let rating = barberShopUserData.rating {
                    self.barberShopNamePlaceHolder.text = barberShopName
                    self.barberShopLogoImageView.loadImagesUsingCacheWithUrlString(urlString: barberShopLogoImageURL)
                    self.addressDataHoldPlaceHolder.text = shopAddress
                    self.shopAddressLatitude = latitu
                    self.shopAddressLongitude = longiti
                    self.phoneNumberPlaceHolder.setTitle(phoneNumber, for: .normal)
                    if let ratingInt = Int(rating) {
                        self.starView.configure(self.attribute, current: CGFloat(integerLiteral: ratingInt), max: 5)
                    } else {
                        self.starView.configure(self.attribute, current: CGFloat(integerLiteral: 0), max: 5)
                    }
                }
            }
        }, withCancel: nil)
    }
    
    func getServiceDetail(shopID: String, servName: String){
        let firebaseRedReference = Database.database().reference()
        firebaseRedReference.child("service").child(shopID).child(servName).observeSingleEvent(of: .value, with: { (snapshososostt) in
            if let dictionary = snapshososostt.value as? [String: AnyObject], HandleDataRequest.handleServiceNode(firebaseData: dictionary) == true {
                let serDic = Service()
                serDic.setValuesForKeys(dictionary)
                self.serviceBooked.append(serDic)
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }, withCancel: nil)
        
    }
    
    func getBarberShopWorkHours(){
        let curentDate = DateInRegion()
        let clendarName = DateByUserDeviceInitializer.calenderNow
        let tZoneNow = DateByUserDeviceInitializer.tzone
        let modLocal = "en_US"
        let translatorRegion = DateByUserDeviceInitializer.getRegion(TZoneName: tZoneNow, calenName: clendarName, LocName: modLocal)
        let newDate = curentDate.convertTo(region: translatorRegion)
        let selectedDay = newDate.weekdayName(.default)
        
        if let shopIDAS = self.orderdetailcustomer?.barberShopUniqueID {
            let firebaseWorkHourRef = Database.database().reference()
            firebaseWorkHourRef.child("workhours").child(shopIDAS).child(selectedDay).observeSingleEvent(of: .value, with: { (snapshotWorkHour) in
                if let dictionaryWorkHour = snapshotWorkHour.value as? [String: AnyObject], HandleDataRequest.handleWorkhoursNode(firebaseData: dictionaryWorkHour) == true {
                    let chosenDayWorkhour = Workhours()
                    chosenDayWorkhour.setValuesForKeys(dictionaryWorkHour)
                    
                    if let openTimeHour = chosenDayWorkhour.openingTimeHour, let openTimeMinute = chosenDayWorkhour.openingTimeMinute, let openTimeSecond = chosenDayWorkhour.openingTimeSecond, let closingTimeHour = chosenDayWorkhour.closingTimeHour, let closingTimeMinute = chosenDayWorkhour.closingTimeMinute, let closingTimeSecond = chosenDayWorkhour.closingTimeSecond, let minutesBeforeClosing = chosenDayWorkhour.minutesBeforeClosing{
                        
                        guard let openHour = Int(openTimeHour), let openMinute = Int(openTimeMinute), let openSecond = Int(openTimeSecond), let closeHour = Int(closingTimeHour), let closeMinute = Int(closingTimeMinute), let closeSecond = Int(closingTimeSecond), let minutesBefore = Int(minutesBeforeClosing) else {
                            return
                        }
                        
                        
                        if let startTimeDate = curentDate.dateBySet(hour: openHour, min: openMinute, secs: openSecond), let closeTimeDate = curentDate.dateBySet(hour: closeHour, min: closeMinute, secs: closeSecond) {
                            let newClosingTime = closeTimeDate - minutesBefore.minutes
                            
                            self.openingHoursDataHoldPlaceHolder.text = "\(startTimeDate.toString(.time(.short))) - \(newClosingTime.toString(.time(.short)))"
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
            }, withCancel: nil)
        }
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
    @objc func handleBaction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleDismisView(){
        if let itemPoint = self.indexPathSelected, let specialObject = self.bookingviewcontroler {
            let indexPathHolder = IndexPath(row: itemPoint, section: 0)
            specialObject.collectionView.performBatchUpdates({
                specialObject.collectionView.deleteItems(at: [indexPathHolder])
                specialObject.appointmentsCustomer.remove(at: itemPoint)
            }, completion: { (finished) in
                specialObject.collectionView.reloadItems(at: (specialObject.collectionView.indexPathsForVisibleItems))
                specialObject.getBookingDataAsAppointments()
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    @objc func handleMakeCall(sender: UIButton){
        if let mobNUm = sender.titleLabel?.text {
            if let url = URL(string: "tel://\(mobNUm)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @objc func handleShowGoogleMaps(){
        if let long = self.shopAddressLongitude, let latit = self.shopAddressLatitude {
            guard let longFloat = Float(long), let latFloat = Float(latit) else {
                return
            }
            
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(latFloat),\(longFloat)&directionsmode=driving"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopLogoImageView)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(starView)
        
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopLogoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopLogoImageView.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        starView.topAnchor.constraint(equalTo: barberShopAddressPlaceHolder.bottomAnchor, constant: 5).isActive = true
        starView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
    }
    
    func setupViewObjectContriants(){
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 160).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 400)
        
        scrollView.addSubview(headerViewDescriptionPlaceHolder)
        scrollView.addSubview(collectView)
        scrollView.addSubview(costTotalContanerView)
        scrollView.addSubview(costTotalSeperatorView)
        scrollView.addSubview(addressHeaderTextPlaceHolder)
        scrollView.addSubview(addressDataHoldPlaceHolder)
        scrollView.addSubview(contactHeaderPlaceHolder)
        scrollView.addSubview(phoneLogothumbnailImageView)
        scrollView.addSubview(phoneNumberPlaceHolder)
        scrollView.addSubview(openingHoursHeaderTextPlaceHolder)
        scrollView.addSubview(bookingIDHeaderTextPlaceHolder)
        scrollView.addSubview(bookingIDDataHoldPlaceHolder)
        scrollView.addSubview(openingHoursDataHoldPlaceHolder) 
        
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 5).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        collectView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        costTotalContanerView.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 10).isActive = true
        costTotalContanerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        costTotalContanerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        costTotalContanerView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        costTotalContanerView.addSubview(totalTextHeaderPlaceHolder)
        costTotalContanerView.addSubview(totalQuantityPlaceHolder)
        costTotalContanerView.addSubview(totalTimePlaceHolder)
        costTotalContanerView.addSubview(totalPricePlaceHolder)
        
        totalTextHeaderPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTextHeaderPlaceHolder.leftAnchor.constraint(equalTo: costTotalContanerView.leftAnchor).isActive = true
        totalTextHeaderPlaceHolder.widthAnchor.constraint(equalToConstant: 70).isActive = true
        totalTextHeaderPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalPricePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalPricePlaceHolder.rightAnchor.constraint(equalTo: costTotalContanerView.rightAnchor).isActive = true
        totalPricePlaceHolder.widthAnchor.constraint(equalToConstant: 120).isActive = true
        totalPricePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalTimePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTimePlaceHolder.rightAnchor.constraint(equalTo: totalPricePlaceHolder.leftAnchor).isActive = true
        totalTimePlaceHolder.widthAnchor.constraint(equalToConstant: 90).isActive = true
        totalTimePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalQuantityPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalQuantityPlaceHolder.rightAnchor.constraint(equalTo: totalTimePlaceHolder.leftAnchor).isActive = true
        totalQuantityPlaceHolder.widthAnchor.constraint(equalToConstant: 75).isActive = true
        totalQuantityPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        costTotalSeperatorView.topAnchor.constraint(equalTo: costTotalContanerView.bottomAnchor).isActive = true
        costTotalSeperatorView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        costTotalSeperatorView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        costTotalSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        addressHeaderTextPlaceHolder.topAnchor.constraint(equalTo: costTotalSeperatorView.bottomAnchor, constant: 10).isActive = true
        addressHeaderTextPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        addressHeaderTextPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        addressHeaderTextPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        addressDataHoldPlaceHolder.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 12).isActive = true
        addressDataHoldPlaceHolder.topAnchor.constraint(equalTo: addressHeaderTextPlaceHolder.bottomAnchor).isActive = true
        addressDataHoldPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.5, constant: 30).isActive = true
        addressDataHoldPlaceHolder.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        contactHeaderPlaceHolder.topAnchor.constraint(equalTo: addressDataHoldPlaceHolder.bottomAnchor, constant: 10).isActive = true
        contactHeaderPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contactHeaderPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        contactHeaderPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        phoneLogothumbnailImageView.topAnchor.constraint(equalTo: contactHeaderPlaceHolder.bottomAnchor).isActive = true
        phoneLogothumbnailImageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 12).isActive = true
        phoneLogothumbnailImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        phoneLogothumbnailImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        phoneNumberPlaceHolder.topAnchor.constraint(equalTo: contactHeaderPlaceHolder.bottomAnchor).isActive = true
        phoneNumberPlaceHolder.leftAnchor.constraint(equalTo: phoneLogothumbnailImageView.rightAnchor, constant: 10).isActive = true
        phoneNumberPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -75).isActive = true
        phoneNumberPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        openingHoursHeaderTextPlaceHolder.topAnchor.constraint(equalTo: phoneNumberPlaceHolder.bottomAnchor, constant: 10).isActive = true
        openingHoursHeaderTextPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        openingHoursHeaderTextPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        openingHoursHeaderTextPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        openingHoursDataHoldPlaceHolder.topAnchor.constraint(equalTo: openingHoursHeaderTextPlaceHolder.bottomAnchor, constant: 10).isActive = true
        openingHoursDataHoldPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        openingHoursDataHoldPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        openingHoursDataHoldPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        bookingIDHeaderTextPlaceHolder.topAnchor.constraint(equalTo: openingHoursDataHoldPlaceHolder.bottomAnchor, constant: 10).isActive = true
        bookingIDHeaderTextPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bookingIDHeaderTextPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        bookingIDHeaderTextPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        bookingIDDataHoldPlaceHolder.topAnchor.constraint(equalTo: bookingIDHeaderTextPlaceHolder.bottomAnchor, constant: 10).isActive = true
        bookingIDDataHoldPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bookingIDDataHoldPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        bookingIDDataHoldPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
}
