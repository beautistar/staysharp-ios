//
//  BarberProfileEditorViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import GooglePlaces
import SwiftDate

class BarberProfileEditorViewController: UIViewController, UIImagePickerControllerDelegate, UITextFieldDelegate, UINavigationControllerDelegate {
    //Barber editor section
    var userRoleFromProfile: String?
    var companyAddressLong: String?
    var companyAddressLat: String?
    
    let picker = UIImagePickerController()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var passwordValid = false
    
    let imageAndFirstLastContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let firstNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let firstNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let firstNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let lastNameHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let lastNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let lastNameSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    let firstNameLastNameContainerView: UIView = {
        let fnlncview = UIView()
        fnlncview.translatesAutoresizingMaskIntoConstraints = false
        return fnlncview
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    
    let mobileNumberHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("mobileNumberTextProfileEditorView", comment: "Mobile Number")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let mobileNumberTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("mobileNumberTextProfileEditorView", comment: "Mobile Number")
        em.keyboardType = .phonePad
        em.addTarget(self, action: #selector(mobileNumbertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(mobileNumbertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let mobileNumberSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    //BarberShop profile Data
    let companyNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let companyNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChangeBarberShop), for: .editingChanged)
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let companyNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let companyEmailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("barberShopEmailTextBarberProfileEditorView", comment: "BarberShop Email")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let companyEmailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("barberShopEmailTextBarberProfileEditorView", comment: "BarberShop Email")
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(textFieldDidChangeBarberShop), for: .editingChanged)
        em.addTarget(self, action: #selector(companyEmailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(companyEmailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmailBarberShop), for: .editingChanged)
        return em
    }()
    
    let companyEmailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return esv
    }()
    
    lazy var addressSearchLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("barberShopAddressTextBarberProfileEditorView", comment: "Address")
        ht.textColor = UIColor.gray
        ht.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ht.isUserInteractionEnabled = true
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        ht.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowGoogleAutocomple)))
        return ht
    }()
    
    let addressSearchSeperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black
        return view
    }()
    
    let companyMobileNumberHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberShopMobileNumberTextBarberProfileEditorView", comment: "BarberShop Mobile Number")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let companyMobileNumberTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = NSLocalizedString("barberShopMobileNumberTextBarberProfileEditorView", comment: "BarberShop Mobile Number")
        em.keyboardType = .phonePad
        em.addTarget(self, action: #selector(companyMobileNumbertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChangeBarberShop), for: .editingChanged)
        em.addTarget(self, action: #selector(companyMobileNumbertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let companyMobileNumberSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return fnsv
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("navigationTitleTextCustomerProfileView", comment: "Profile")
        picker.delegate = self
        view.backgroundColor = UIColor.white
        self.companyMobileNumberTextField.delegate = self
        self.mobileNumberTextField.delegate = self
        
        if let role = self.userRoleFromProfile {
            switch role {
            case "barberShop":
                navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("barButtonTitleText", comment: "Save"), style: .done, target: self, action: #selector(checkAuthenticationType))
                navigationItem.rightBarButtonItem?.isEnabled = false
                view.addSubview(imageAndFirstLastContainerView)
                view.addSubview(addressSearchLabel)
                view.addSubview(addressSearchSeperatorView)
                view.addSubview(companyMobileNumberHiddenPlaceHolder)
                view.addSubview(companyMobileNumberTextField)
                view.addSubview(companyMobileNumberSeperatorView)
                view.addSubview(errorMessagePlaceHolder)
                setupViewContriantsBarberShop()
                getUserDataBarberShop()
            case "barberStaff":
                navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("barButtonTitleText", comment: "Save"), style: .done, target: self, action: #selector(checkAuthenticationType))
                navigationItem.rightBarButtonItem?.isEnabled = false
                view.addSubview(imageAndFirstLastContainerView)
                view.addSubview(emailHiddenPlaceHolder)
                view.addSubview(emailTextField)
                view.addSubview(emailSeperatorView)
                view.addSubview(mobileNumberHiddenPlaceHolder)
                view.addSubview(mobileNumberTextField)
                view.addSubview(mobileNumberSeperatorView)
                view.addSubview(errorMessagePlaceHolder)
                setupViewConstriants()
                getUserData()
            default:
                print("Sky high")
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    func getUserData(){
        DispatchQueue.global(qos: .background).async {
            if let userUUID = Auth.auth().currentUser?.uid {
                let firebaseReferenceUserDataEdit = Database.database().reference()
                firebaseReferenceUserDataEdit.child("users").child(userUUID).observeSingleEvent(of: .value, with: { (snapshotUserDataEdit) in
                    if let dictionaryUserDataEdit = snapshotUserDataEdit.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryUserDataEdit) == true {
                        let userDataEdit = Barber()
                        userDataEdit.setValuesForKeys(dictionaryUserDataEdit)
                        
                        if let firstName = userDataEdit.firstName, let lastName = userDataEdit.lastName, let email = userDataEdit.email, let mobNumber = userDataEdit.mobileNumber, let imgURL = userDataEdit.profileImageUrl, let imageName = userDataEdit.profileImageName {
                            DispatchQueue.main.async {
                                self.firstNameTextField.text = firstName
                                self.lastNameTextField.text = lastName
                                self.emailTextField.text = email
                                self.mobileNumberTextField.text = mobNumber
                                self.selectImageView.loadImagesUsingCacheWithUrlString(urlString: imgURL)
                                UserDefaults.standard.set(imageName, forKey: "profileImageFileNameIncludingJPEGEnding")
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func getUserDataBarberShop(){
        DispatchQueue.global(qos: .background).async {
            if let userUUID = Auth.auth().currentUser?.uid {
                let firebaseReferenceUserDataEdit = Database.database().reference()
                firebaseReferenceUserDataEdit.child("users").child(userUUID).observeSingleEvent(of: .value, with: { (snapshotUserDataEdit) in
                    if let dictionaryUserDataEdit = snapshotUserDataEdit.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryUserDataEdit) == true {
                        let userDataEdit = BarberShop()
                        userDataEdit.setValuesForKeys(dictionaryUserDataEdit)
                        
                        if let cmpName = userDataEdit.companyName, let addres = userDataEdit.companyFormattedAddress, let email = userDataEdit.email, let mobNumber = userDataEdit.mobileNumber, let imgURL = userDataEdit.companyLogoImageUrl, let imageName = userDataEdit.companyLogoImageName, let locLong = userDataEdit.companyAddressLongitude, let locLatit = userDataEdit.companyAddressLatitude  {
                            
                            DispatchQueue.main.async {
                                self.companyNameTextField.text = cmpName
                                self.addressSearchLabel.text = addres
                                self.companyEmailTextField.text = email
                                self.companyMobileNumberTextField.text = mobNumber
                                self.selectImageView.loadImagesUsingCacheWithUrlString(urlString: imgURL)
                                UserDefaults.standard.set(imageName, forKey: "profileImageFileNameIncludingJPEGEnding")
                                UserDefaults.standard.set(locLong, forKey: "companyAddressFormattedForLongitude")
                                UserDefaults.standard.set(locLatit, forKey: "companyAddressFormattedForLatitude")
                            }
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func setupViewConstriants(){
        imageAndFirstLastContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        imageAndFirstLastContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageAndFirstLastContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        imageAndFirstLastContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        imageAndFirstLastContainerView.addSubview(selectImageView)
        imageAndFirstLastContainerView.addSubview(firstNameLastNameContainerView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: imageAndFirstLastContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: imageAndFirstLastContainerView.widthAnchor, multiplier: 0.25).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        firstNameLastNameContainerView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        firstNameLastNameContainerView.rightAnchor.constraint(equalTo: imageAndFirstLastContainerView.rightAnchor).isActive = true
        firstNameLastNameContainerView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.addSubview(firstNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(firstNameTextField)
        firstNameLastNameContainerView.addSubview(firstNameSeperatorView)
        firstNameLastNameContainerView.addSubview(lastNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(lastNameTextField)
        firstNameLastNameContainerView.addSubview(lastNameSeperatorView)
        
        firstNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.topAnchor).isActive = true
        firstNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        firstNameTextField.topAnchor.constraint(equalTo: firstNameHiddenPlaceHolder.bottomAnchor).isActive = true
        firstNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        firstNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        firstNameSeperatorView.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        firstNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        lastNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        lastNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        lastNameTextField.topAnchor.constraint(equalTo: lastNameHiddenPlaceHolder.bottomAnchor).isActive = true
        lastNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        lastNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        lastNameSeperatorView.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        lastNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: emailTextField.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        mobileNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        mobileNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        mobileNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        mobileNumberTextField.topAnchor.constraint(equalTo: mobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
        mobileNumberTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mobileNumberTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        mobileNumberTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        mobileNumberSeperatorView.topAnchor.constraint(equalTo: mobileNumberTextField.bottomAnchor).isActive = true
        mobileNumberSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mobileNumberSeperatorView.widthAnchor.constraint(equalTo: mobileNumberTextField.widthAnchor).isActive = true
        mobileNumberSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: mobileNumberSeperatorView.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupViewContriantsBarberShop(){
        imageAndFirstLastContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        imageAndFirstLastContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageAndFirstLastContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        imageAndFirstLastContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        imageAndFirstLastContainerView.addSubview(selectImageView)
        imageAndFirstLastContainerView.addSubview(firstNameLastNameContainerView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: imageAndFirstLastContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: imageAndFirstLastContainerView.widthAnchor, multiplier: 0.25).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        firstNameLastNameContainerView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        firstNameLastNameContainerView.rightAnchor.constraint(equalTo: imageAndFirstLastContainerView.rightAnchor).isActive = true
        firstNameLastNameContainerView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.addSubview(companyNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(companyNameTextField)
        firstNameLastNameContainerView.addSubview(companyNameSeperatorView)
        firstNameLastNameContainerView.addSubview(companyEmailHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(companyEmailTextField)
        firstNameLastNameContainerView.addSubview(companyEmailSeperatorView)
        
        companyNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.topAnchor).isActive = true
        companyNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        companyNameTextField.topAnchor.constraint(equalTo: companyNameHiddenPlaceHolder.bottomAnchor).isActive = true
        companyNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        companyNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        companyNameSeperatorView.topAnchor.constraint(equalTo: companyNameTextField.bottomAnchor).isActive = true
        companyNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        companyEmailHiddenPlaceHolder.topAnchor.constraint(equalTo: companyNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        companyEmailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyEmailHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyEmailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        companyEmailTextField.topAnchor.constraint(equalTo: companyEmailHiddenPlaceHolder.bottomAnchor).isActive = true
        companyEmailTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyEmailTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyEmailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        companyEmailSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        companyEmailSeperatorView.topAnchor.constraint(equalTo: companyEmailTextField.bottomAnchor).isActive = true
        companyEmailSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyEmailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        //hello
        addressSearchLabel.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.bottomAnchor, constant: 5).isActive = true
        addressSearchLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        addressSearchLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        addressSearchLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        addressSearchSeperatorView.topAnchor.constraint(equalTo: addressSearchLabel.bottomAnchor).isActive = true
        addressSearchSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        addressSearchSeperatorView.widthAnchor.constraint(equalTo: addressSearchLabel.widthAnchor).isActive = true
        addressSearchSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        companyMobileNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: addressSearchSeperatorView.bottomAnchor, constant: 5).isActive = true
        companyMobileNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        companyMobileNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        companyMobileNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        companyMobileNumberTextField.topAnchor.constraint(equalTo: companyMobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
        companyMobileNumberTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        companyMobileNumberTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        companyMobileNumberTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        companyMobileNumberSeperatorView.topAnchor.constraint(equalTo: companyMobileNumberTextField.bottomAnchor).isActive = true
        companyMobileNumberSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        companyMobileNumberSeperatorView.widthAnchor.constraint(equalTo: companyMobileNumberTextField.widthAnchor).isActive = true
        companyMobileNumberSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: companyMobileNumberSeperatorView.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func handleShowGoogleAutocomple(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(){
        if firstNameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == "" || mobileNumberTextField.text == ""{
            //Disable button
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            navigationItem.rightBarButtonItem?.isEnabled = false
            
        } else {
            //Enable button
            self.isValidEmail()
            self.firstNameHiddenPlaceHolder.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
            if emailValid == true {
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
    }
    
    @objc func textFieldDidChangeBarberShop(){
        if companyNameTextField.text == "" || companyEmailTextField.text == "" || addressSearchLabel.text == "" || companyMobileNumberTextField.text == ""{
            //Disable button
            self.companyNameHiddenPlaceHolder.text = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
            navigationItem.rightBarButtonItem?.isEnabled = false
            
        } else {
            //Enable button
            self.isValidEmailBarberShop()
            self.companyNameHiddenPlaceHolder.text = NSLocalizedString("shopNameTextBarberProfileEditorView", comment: "BarberShop Name")
            if emailValid == true {
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
    }
    
    @objc func firstNametextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = false
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func lastNametextFieldDidChange(){
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func mobileNumbertextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = false
    }
    
    //BarberShop
    
    @objc func companyNametextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.companyEmailSeperatorView.backgroundColor = UIColor.black
        self.addressSearchSeperatorView.backgroundColor = UIColor.black
        self.companyMobileNumberSeperatorView.backgroundColor = UIColor.black
        self.companyNameHiddenPlaceHolder.isHidden = false
        self.companyEmailHiddenPlaceHolder.isHidden = true
        self.companyMobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func companyEmailtextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor.black
        self.companyEmailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.addressSearchSeperatorView.backgroundColor = UIColor.black
        self.companyMobileNumberSeperatorView.backgroundColor = UIColor.black
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.companyEmailHiddenPlaceHolder.isHidden = false
        self.companyMobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func companyMobileNumbertextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor.black
        self.companyEmailSeperatorView.backgroundColor = UIColor.black
        self.addressSearchSeperatorView.backgroundColor = UIColor.black
        self.companyMobileNumberSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.companyEmailHiddenPlaceHolder.isHidden = true
        self.companyMobileNumberHiddenPlaceHolder.isHidden = false
    }
    
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = "Email"
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = "Email is not properly formatted"
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    @objc func isValidEmailBarberShop() {
        let testStr = companyEmailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.companyEmailHiddenPlaceHolder.text = "Email"
            print("email is ok", self.emailValid, emailValid)
        }else{
            self.emailValid = false
            self.companyEmailHiddenPlaceHolder.text = "Email is not properly formatted"
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        print("Hello therer baby")
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: "Take photo", style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: "Choose photo", style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        print("Hello")
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            print("Image selected ########")
            self.selectImageView.contentMode = .scaleAspectFit
            self.selectImageView.image = selectedImage
            navigationItem.rightBarButtonItem?.isEnabled = true
        }else {
            print("image not selected")
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = "Sorry, this device has no camera"
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @objc func checkAuthenticationType(){
        if let provider = Auth.auth().currentUser?.providerData{
            for access in provider {
                let accessProvider = access.providerID
                print(accessProvider)
                switch accessProvider {
                case "password":
                    self.handleEditProfileData()
                case "facebook.com":
                    self.handleFacebookEditProfileData()
                default:
                    print("Sky high")
                }
            }
        }
    }
    
    func handleFacebookEditProfileData(){
        let user = Auth.auth().currentUser
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        
        user?.reauthenticateAndRetrieveData(with: credential, completion: { (AuthUserData, errorororor) in
            if let error = errorororor {
                print(error.localizedDescription)
                self.errorMessagePlaceHolder.text = error.localizedDescription
            } else {
                // User re-authenticated.
                //self.handleSaveChanges()
                self.handleReUploadingData()
                
            }
        })
        
    }
    
    func handleEditProfileData(){
        let alert = UIAlertController(title: NSLocalizedString("reauthenticateUserProfileEdit", comment: "Re-authenticate user"), message: NSLocalizedString("reauthenticateUserMessageProfileEdit", comment: "Please Insert old email & password."), preferredStyle: .alert)
        
        // Login button
        let loginAction = UIAlertAction(title: NSLocalizedString("reauthenticateUserOKButtonProfileEdit", comment: "Ok"), style: .default, handler: { (action) -> Void in
            // Get TextFields text
            
            let usernameTxt = alert.textFields![0]
            let passwordTxt = alert.textFields![1]
            
            guard let email = usernameTxt.text, let currentPassword = passwordTxt.text else {
                self.errorMessagePlaceHolder.text = NSLocalizedString("reauthenticateUserErrorMessageEnterValueProfileEdit", comment: "Did not enter value")
                return
            }
            let user = Auth.auth().currentUser
            let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
            
            // Prompt the user to re-provide their sign-in credentials
            
            if let userAuthData = Auth.auth().currentUser {
                let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
                userAuthData.reauthenticateAndRetrieveData(with: credential, completion: { (AuthDataHold, errooroorr) in
                    if let error = errooroorr {
                        print(error.localizedDescription)
                    } else {
                        // User re-authenticated.
                        //self.handleSaveChanges()
                        if let role = self.userRoleFromProfile {
                            switch role {
                            case "barberShop":
                                self.handleReUploadingDataBarberShop()
                            case "barberStaff":
                                self.handleReUploadingData()
                            default:
                                print("Sky high")
                            }
                        }
                    }
                })
            }
            
        })
        
        let cancel = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel, handler: { (action) -> Void in })
        
        // Add 1 textField (for username)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardType = .emailAddress
            textField.autocorrectionType = .default
            textField.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
            textField.textColor = UIColor.black
        }
        
        // Add 2nd textField (for password)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardType = .default
            textField.placeholder = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
            textField.isSecureTextEntry = true
            textField.textColor = UIColor.black
        }
        
        // Add action buttons and present the Alert
        alert.addAction(loginAction)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
    }
    
    func handleReUploadingData(){
        if let imageName = UserDefaults.standard.object(forKey: "profileImageFileNameIncludingJPEGEnding") as? String {
            let storageRef = Storage.storage().reference().child("profileImages")
            
            let desertRef = storageRef.child(imageName)
            print(imageName, "image name")
            desertRef.delete { error in
                if error != nil {
                    if imageName == "no_image"{
                        self.handleSaveChanges()
                    }else{
                        self.handleSaveChanges()
                    }
                } else {
                    self.handleSaveChanges()
                }
            }
            
        }
    }
    
    func handleReUploadingDataBarberShop(){
        if let imageName = UserDefaults.standard.object(forKey: "profileImageFileNameIncludingJPEGEnding") as? String {
            let storageRef = Storage.storage().reference().child("companyLogo")
            
            let desertRef = storageRef.child(imageName)
            print(imageName, "image name")
            desertRef.delete { error in
                if error != nil {
                    if imageName == "no_image"{
                        self.handleSaveChangesBarberShop()
                    }else{
                        self.handleSaveChangesBarberShop()
                    }
                } else {
                    self.handleSaveChangesBarberShop()
                }
            }
            
        }
    }
    
    
    func handleSaveChanges(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let imageName = NSUUID().uuidString
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference().child("profileImages")
        let currentUser = Auth.auth().currentUser
        
        if let profileImage = self.selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1), let firstName = self.firstNameTextField.text, let lastName = self.lastNameTextField.text, let mNumber = self.mobileNumberTextField.text, let userEmail =  self.emailTextField.text {
            
            storageRef.child("\(imageName).jpeg").putData(uploadData, metadata: nil, completion: { (stMetaData, erroroorrrrooorrrr) in
                if let error = erroroorrrrooorrrr {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.errorMessagePlaceHolder.text = error.localizedDescription
                    return
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                currentUser?.updateEmail(to: userEmail) { (errorrr) in
                    if let error = errorrr {
                        self.errorMessagePlaceHolder.text = error.localizedDescription
                        return
                    } else {
                        
                        storageRef.downloadURL(completion: { (urlldll, erororororororr) in
                            
                            guard let downloadURL = urlldll?.absoluteString else {
                                // Uh-oh, an error occurred!
                                return
                            }
                            
                            ref.child("users").child(uid).child("profileImageUrl").setValue(downloadURL)
                            ref.child("users").child(uid).child("firstName").setValue(firstName)
                            ref.child("users").child(uid).child("lastName").setValue(lastName)
                            ref.child("users").child(uid).child("mobileNumber").setValue(mNumber)
                            ref.child("users").child(uid).child("email").setValue(userEmail)
                            ref.child("users").child(uid).child("profileImageName").setValue("\(imageName).jpeg")
                            self.navigationItem.rightBarButtonItem?.isEnabled = false
                        })
                        
                    }
                    
                }
                
            })
        }
    }
    
    func handleSaveChangesBarberShop(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let imageName = NSUUID().uuidString
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference().child("companyLogo")
        let currentUser = Auth.auth().currentUser
        
        //UserDefaults.standard.set(place.coordinate.longitude.description, forKey: "companyAddressFormattedForLongitude")
        //UserDefaults.standard.set(place.coordinate.latitude.description, forKey: "companyAddressFormattedForLatitude")
        
        if let profileImage = self.selectImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1), let cmpName = self.companyNameTextField.text, let addres = self.addressSearchLabel.text, let mNumber = self.companyMobileNumberTextField.text, let userEmail =  self.companyEmailTextField.text, let addLong = UserDefaults.standard.object(forKey: "companyAddressFormattedForLongitude") as? String, let addLat = UserDefaults.standard.object(forKey: "companyAddressFormattedForLatitude") as? String {
            
            storageRef.child("\(imageName).jpeg").putData(uploadData, metadata: nil, completion: { (stMetaData, erroroorrrrooorrrr) in
                if let error = erroroorrrrooorrrr {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.errorMessagePlaceHolder.text = error.localizedDescription
                    return
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                currentUser?.updateEmail(to: userEmail) { (errorrr) in
                    if let error = errorrr {
                        self.errorMessagePlaceHolder.text = error.localizedDescription
                        return
                    } else {
                        print("persisted data")
                        
                        storageRef.downloadURL(completion: { (urlllll, errororororooror) in
                            guard let downloadURL = urlllll?.absoluteString else {
                                // Uh-oh, an error occurred!
                                return
                            }
                            
                            ref.child("users").child(uid).child("companyLogoImageUrl").setValue(downloadURL)
                            ref.child("users").child(uid).child("companyName").setValue(cmpName)
                            ref.child("users").child(uid).child("companyFormattedAddress").setValue(addres)
                            ref.child("users").child(uid).child("companyAddressLongitude").setValue(addLong)
                            ref.child("users").child(uid).child("companyAddressLatitude").setValue(addLat)
                            ref.child("users").child(uid).child("mobileNumber").setValue(mNumber)
                            ref.child("users").child(uid).child("email").setValue(userEmail)
                            ref.child("users").child(uid).child("companyLogoImageName").setValue("\(imageName).jpeg")
                            self.updateMarkerValueWithChange(compName: cmpName, logoURl: downloadURL, formAdd: addres, latiti: addLat, longiti: addLong, shopID: uid)
                            self.navigationItem.rightBarButtonItem?.isEnabled = false
                        })
                    }
                    
                }
                
            })
        } else {
            print("one empty data field")
        }
    }
    
    func updateMarkerValueWithChange(compName: String, logoURl: String, formAdd: String, latiti: String, longiti: String, shopID: String){
        let firebaseRefUpdateMarker = Database.database().reference()
        firebaseRefUpdateMarker.child("barberMarkers").child(shopID).child("companyAddressLatitude").setValue(latiti)
        firebaseRefUpdateMarker.child("barberMarkers").child(shopID).child("companyAddressLongitude").setValue(longiti)
        firebaseRefUpdateMarker.child("barberMarkers").child(shopID).child("companyFormattedAddress").setValue(formAdd)
        firebaseRefUpdateMarker.child("barberMarkers").child(shopID).child("companyLogoImageUrl").setValue(logoURl)
        firebaseRefUpdateMarker.child("barberMarkers").child(shopID).child("companyName").setValue(compName)
    }

}



extension BarberProfileEditorViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //print("Place name: \(place.name)")
        //print("Place address: \(place.formattedAddress)")
        //print("Place attributions: \(place.attributions)")
        if let mainAddress = place.formattedAddress {
            self.addressSearchLabel.textColor = UIColor.black
            self.addressSearchLabel.text = mainAddress
            UserDefaults.standard.set(place.coordinate.longitude.description, forKey: "companyAddressFormattedForLongitude")
            UserDefaults.standard.set(place.coordinate.latitude.description, forKey: "companyAddressFormattedForLatitude")
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
