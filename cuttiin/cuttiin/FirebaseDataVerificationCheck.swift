//
//  FirebaseDataVerificationCheck.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 26/08/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

class FirebaseDataVerificationCheck {
    static func checkDateData(firebaseData: [String], localObjectKeys: [String]) -> Bool {
        if firebaseData.containsSameElements(as: localObjectKeys) {
            return true
        } else {
            return false
        }
    }
}
