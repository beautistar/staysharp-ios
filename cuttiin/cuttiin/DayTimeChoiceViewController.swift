//
//  DayTimeChoiceViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/20/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import SwiftDate

//store a list of opening and closing times
class DayTimeChoiceViewController: UIViewController, UITextFieldDelegate {
    var verifyTimeDifference = false
    var specificDayData: String?
    var daysOfTheWeek = ["Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    var selectedDaysOfTheWeek = [String]()
    var selectedOpeningTimeForTheDay: String?
    var selectedOpeningTimeHour: String?
    var selectedOpeningTimeMinute: String?
    var selectedOpeningTimeSecond: String?
    var selectedClosingTimeForTheDay: String?
    var selectedClosingTimeHour: String?
    var selectedClosingTimeMinute: String?
    var selectedClosingTimeSecond: String?
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    let scrollViewInnerContaine: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        return tpcview
    }()
    
    
    
    lazy var openCloseSegmentedControl: UISegmentedControl = {
        let ocsegmentcontrol = UISegmentedControl(items: [NSLocalizedString("openSegmentedButtonTextDayTimeView", comment: "OPEN"), NSLocalizedString("closedSegmentedButtonTextDayTimeView", comment: "CLOSED")])
        ocsegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        ocsegmentcontrol.tintColor = UIColor(r: 118, g: 187, b: 220)
        ocsegmentcontrol.selectedSegmentIndex = 0
        ocsegmentcontrol.addTarget(self, action: #selector(handleLoginRegisterChange), for: .valueChanged)
        return ocsegmentcontrol
    }()
    
    let timePickerContainerView: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        return tpcview
    }()
    
    let openingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("openAtlabelTextDayTimeView", comment: "OPENS AT")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let closingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("closedAtlabelTextDayTimeView", comment: "CLOSES AT")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var openingTimePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .time
        //
        opentime.setValue(UIColor.white, forKey: "textColor")
        opentime.setValue(false, forKeyPath: "highlightsToday")
        //
        let currentDate = Date()
        opentime.date = currentDate
        opentime.addTarget(self, action: #selector(handleOpenTimePickerChange), for: .valueChanged)
        return opentime
    }()
    
    let openingAndClosingTimeSeperatorView: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        tpcview.backgroundColor = UIColor.white
        return tpcview
    }()
    
    lazy var closingTimePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .time
        //
        opentime.setValue(UIColor.white, forKey: "textColor")
        opentime.setValue(false, forKeyPath: "highlightsToday")
        //
        let currentDate = Date()
        opentime.date = currentDate
        opentime.addTarget(self, action: #selector(handleClosingTimePickerChange), for: .valueChanged)
        return opentime
    }()
    
    let nearClosingTimeBokingContainerView: UIView = {
        let nctbcview = UIView()
        nctbcview.translatesAutoresizingMaskIntoConstraints = false
        nctbcview.isUserInteractionEnabled = true
        return nctbcview
    }()
    
    let nearClosingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("nearClosingTimeTextDayTimeView", comment: "Accept bookings near closing time!")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.numberOfLines = 0
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var acceptBookingSwitch: UISwitch = {
       let acbswitch = UISwitch()
        acbswitch.translatesAutoresizingMaskIntoConstraints = false
        acbswitch.layer.masksToBounds = true
        acbswitch.setOn(false, animated: true)
        acbswitch.addTarget(self, action: #selector(handleAcceptSwitchAction), for: .valueChanged)
        return acbswitch
    }()
    
    let nearClosingTimeTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.white
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.textAlignment = .center
        em.keyboardType = .numberPad
        em.layer.cornerRadius = 10
        em.layer.masksToBounds = true
        em.layer.borderWidth = 2
        em.layer.borderColor = UIColor.white.cgColor
        em.isUserInteractionEnabled = false
        em.addTarget(self, action: #selector(nearClosingTimetextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(nearClosingTimetextFieldDidChange), for: .editingChanged)
        //em.addTarget(self, action: #selector(getTimeDifference), for: .editingChanged)
        return em
    }()
    
    let nearClosingTimeHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("minutesBeforeTextDayTimeView", comment: "Minutes before")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .center
        fnhp.isHidden = true
        return fnhp
    }()
    
    let applyToTextLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("applyToLabelTextDayTimeView", comment: "APPLY TO")
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let applyToSeperatorView: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        tpcview.backgroundColor = UIColor.white
        return tpcview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.white
        cv.allowsMultipleSelection = true
        cv.register(customCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("selectOpeningHourLabelTextDayTimeView", comment: "Select opening hours!")
        ht.font = UIFont(name: "HelveticaNeue-Bold", size: 13)
        ht.textColor = UIColor.white
        ht.textAlignment = .center
        return ht
    }()
    
    let descriptionBody: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("describtionBodyLabelTextDayTimeSelectView", comment: "During these selected days and hours customers will be able to book an appointment")
        ht.numberOfLines = 0
        ht.font = UIFont(name: "HelveticaNeue", size: 10)
        ht.textColor = UIColor.white
        ht.textAlignment = .center
        return ht
    }()
    
    lazy var saveButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("saveButtonUploadPictureView", comment: "SAVE"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor.black.cgColor
        st.addTarget(self, action: #selector(handleSaveTime), for: .touchUpInside)
        st.isEnabled = false
        return st
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        view.addSubview(scrollView)
        collectionView.dataSource = self
        collectionView.delegate = self 
        view.addSubview(saveButton)
        nearClosingTimeTextField.delegate = self
        setupViewObjectsContriants()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        nearClosingTimeTextField.resignFirstResponder()
    }
    
    func setupViewObjectsContriants() {
        
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 600) //528
        
        scrollView.addSubview(scrollViewInnerContaine)
        
        scrollViewInnerContaine.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        scrollViewInnerContaine.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        scrollViewInnerContaine.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        scrollViewInnerContaine.heightAnchor.constraint(equalToConstant: scrollView.contentSize.height).isActive = true
        
        
        scrollViewInnerContaine.addSubview(openCloseSegmentedControl)
        scrollViewInnerContaine.addSubview(timePickerContainerView)
        //scrollViewInnerContaine.addSubview(openingTimePlaceHolder)
        //scrollViewInnerContaine.addSubview(openingTimePicker)
        scrollViewInnerContaine.addSubview(nearClosingTimeBokingContainerView)
        scrollViewInnerContaine.addSubview(nearClosingTimeHiddenPlaceHolder)
        scrollViewInnerContaine.addSubview(applyToTextLabel)
        scrollViewInnerContaine.addSubview(applyToSeperatorView)
        scrollViewInnerContaine.addSubview(collectView)
        scrollViewInnerContaine.addSubview(descriptionTitle)
        scrollViewInnerContaine.addSubview(descriptionBody)
        scrollViewInnerContaine.addSubview(saveButton)
        
        
        
        
        openCloseSegmentedControl.topAnchor.constraint(equalTo: scrollViewInnerContaine.topAnchor).isActive = true
        openCloseSegmentedControl.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        openCloseSegmentedControl.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        openCloseSegmentedControl.heightAnchor.constraint(equalToConstant: 50)
        
        timePickerContainerView.topAnchor.constraint(equalTo: openCloseSegmentedControl.bottomAnchor, constant: 10).isActive = true
        timePickerContainerView.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        timePickerContainerView.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -12).isActive = true
        timePickerContainerView.heightAnchor.constraint(equalToConstant: 170).isActive = true
        
        timePickerContainerView.addSubview(openingTimePlaceHolder)
        timePickerContainerView.addSubview(closingTimePlaceHolder)
        timePickerContainerView.addSubview(openingTimePicker)
        timePickerContainerView.addSubview(openingAndClosingTimeSeperatorView)
        timePickerContainerView.addSubview(closingTimePicker)
        
        openingTimePlaceHolder.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor).isActive = true
        openingTimePlaceHolder.leftAnchor.constraint(equalTo: timePickerContainerView.leftAnchor).isActive = true
        openingTimePlaceHolder.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5).isActive = true
        openingTimePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        closingTimePlaceHolder.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor).isActive = true
        closingTimePlaceHolder.leftAnchor.constraint(equalTo: openingTimePlaceHolder.rightAnchor).isActive = true
        closingTimePlaceHolder.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5).isActive = true
        closingTimePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        openingTimePicker.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor, constant: 15).isActive = true
        openingTimePicker.leftAnchor.constraint(equalTo: timePickerContainerView.leftAnchor).isActive = true
        openingTimePicker.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5, constant: -10).isActive = true
        openingTimePicker.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        openingAndClosingTimeSeperatorView.topAnchor.constraint(equalTo: openingTimePlaceHolder.topAnchor, constant: 90).isActive = true
        openingAndClosingTimeSeperatorView.leftAnchor.constraint(equalTo: openingTimePicker.rightAnchor).isActive = true
        openingAndClosingTimeSeperatorView.rightAnchor.constraint(equalTo: closingTimePicker.leftAnchor).isActive = true
        openingAndClosingTimeSeperatorView.widthAnchor.constraint(equalToConstant: 10) .isActive = true
        openingAndClosingTimeSeperatorView.heightAnchor.constraint(equalToConstant: 3).isActive = true
        
        closingTimePicker.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor, constant: 15).isActive = true
        closingTimePicker.rightAnchor.constraint(equalTo: timePickerContainerView.rightAnchor).isActive = true
        closingTimePicker.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5, constant: -10).isActive = true
        closingTimePicker.heightAnchor.constraint(equalToConstant: 150).isActive = true
        //
        nearClosingTimeBokingContainerView.topAnchor.constraint(equalTo: timePickerContainerView.bottomAnchor).isActive = true
        nearClosingTimeBokingContainerView.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        nearClosingTimeBokingContainerView.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        nearClosingTimeBokingContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        nearClosingTimeBokingContainerView.addSubview(nearClosingTimePlaceHolder)
        nearClosingTimeBokingContainerView.addSubview(acceptBookingSwitch)
        nearClosingTimeBokingContainerView.addSubview(nearClosingTimeTextField)
        
        nearClosingTimePlaceHolder.topAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.topAnchor).isActive = true
        nearClosingTimePlaceHolder.leftAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.leftAnchor).isActive = true
        nearClosingTimePlaceHolder.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nearClosingTimePlaceHolder.heightAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.heightAnchor).isActive = true
        
        acceptBookingSwitch.topAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.topAnchor, constant: 10).isActive = true
        acceptBookingSwitch.leftAnchor.constraint(equalTo: nearClosingTimePlaceHolder.rightAnchor, constant: 20).isActive = true
        acceptBookingSwitch.widthAnchor.constraint(equalToConstant: 50).isActive = true
        acceptBookingSwitch.heightAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.heightAnchor).isActive = true
        
        nearClosingTimeTextField.topAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.topAnchor).isActive = true
        nearClosingTimeTextField.leftAnchor.constraint(equalTo: acceptBookingSwitch.rightAnchor, constant: 20).isActive = true
        nearClosingTimeTextField.rightAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.rightAnchor).isActive = true
        nearClosingTimeTextField.heightAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.heightAnchor).isActive = true
        
        nearClosingTimeHiddenPlaceHolder.topAnchor.constraint(equalTo: nearClosingTimeBokingContainerView.bottomAnchor).isActive = true
        nearClosingTimeHiddenPlaceHolder.rightAnchor.constraint(equalTo: scrollViewInnerContaine.rightAnchor, constant: -48).isActive = true
        nearClosingTimeHiddenPlaceHolder.widthAnchor.constraint(equalTo: nearClosingTimeTextField.widthAnchor).isActive = true
        nearClosingTimeHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15)
        
        applyToTextLabel.topAnchor.constraint(equalTo: nearClosingTimeHiddenPlaceHolder.bottomAnchor, constant: 10).isActive = true
        applyToTextLabel.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        applyToTextLabel.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        applyToTextLabel.heightAnchor.constraint(equalToConstant: 25)
        
        applyToSeperatorView.leftAnchor.constraint(equalTo: scrollViewInnerContaine.leftAnchor, constant: 24).isActive = true
        applyToSeperatorView.topAnchor.constraint(equalTo: applyToTextLabel.bottomAnchor).isActive = true
        applyToSeperatorView.widthAnchor.constraint(equalTo: applyToTextLabel.widthAnchor).isActive = true
        applyToSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        collectView.topAnchor.constraint(equalTo: applyToSeperatorView.bottomAnchor).isActive = true
        collectView.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, multiplier: 1, constant: -48).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        descriptionTitle.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 10).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        descriptionBody.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        descriptionBody.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        descriptionBody.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        descriptionBody.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        saveButton.topAnchor.constraint(equalTo: descriptionBody.bottomAnchor, constant: 10).isActive = true
        saveButton.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        saveButton.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        saveButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    @objc func handleSaveTime(){
        let newDate = DateInRegion()
        let strDate = newDate.toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        
        let newUserLocal = "en"
        let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: newUserLocal)
        let newUserDate = DateInRegion().convertTo(region: englishDateRegion)
        let newStrDate = newUserDate.toString(.extended)
        
        let localeTimezoneVerify = VerifyLocales.checkForUnavailableLocale()
        
        let correctDateToUse = (localeTimezoneVerify) ? strDate : newStrDate
        let correctLocaleToUSe = (localeTimezoneVerify) ? userLocal : newUserLocal
        
        //sucessfully logged in //let openTime = self.selectedOpeningTimeForTheDay, let closeTime = self.selectedClosingTimeForTheDay,
        let ref = Database.database().reference()
        if self.openCloseSegmentedControl.selectedSegmentIndex == 0 {
            if let uid = Auth.auth().currentUser?.uid, let minutesBeforeClosing = self.nearClosingTimeTextField.text, let selectedChosenDay = self.specificDayData, let openTimeHour = self.selectedOpeningTimeHour, let openTimeMin = self.selectedOpeningTimeMinute, let openTimeSecond = self.selectedOpeningTimeSecond, let closeTimeHour = self.selectedClosingTimeHour, let closeTimeMinute = self.selectedClosingTimeMinute, let closeTimeSecond = self.selectedClosingTimeSecond {
                if self.selectedDaysOfTheWeek.count > 0 {
                    self.selectedDaysOfTheWeek.append(selectedChosenDay)
                    for daySelec in selectedDaysOfTheWeek {
                        let userReference = ref.child("workhours").child(uid).child(daySelec)
                        let value = ["openingTimeHour": openTimeHour,"openingTimeMinute":openTimeMin, "openingTimeSecond": openTimeSecond, "closingTimeHour": closeTimeHour,"closingTimeMinute": closeTimeMinute,"closingTimeSecond": closeTimeSecond, "minutesBeforeClosing": minutesBeforeClosing,"dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                        userReference.updateChildValues(value, withCompletionBlock: { (errorrrr, ref) in
                            if let error = errorrrr {
                                print(error.localizedDescription)
                            }else {
                                print("Succesfully persisted to database")
                                self.handleDismissAndChangeButtonTitle()
                            }
                        })
                        
                    }
                    
                    
                }else {
                    let userReference = ref.child("workhours").child(uid).child(selectedChosenDay)
                    let value = ["openingTimeHour": openTimeHour,"openingTimeMinute":openTimeMin, "openingTimeSecond": openTimeSecond, "closingTimeHour": closeTimeHour,"closingTimeMinute": closeTimeMinute,"closingTimeSecond": closeTimeSecond, "minutesBeforeClosing": minutesBeforeClosing,"dateCreated": correctDateToUse, "timezone": userTimezone, "calendar":userCalender, "local":correctLocaleToUSe]
                    userReference.updateChildValues(value, withCompletionBlock: { (errorrrr, ref) in
                        if let error = errorrrr {
                            print(error.localizedDescription)
                        }else {
                            print("Succesfully persisted to database")
                            self.handleDismissAndChangeButtonTitle()
                        }
                    })
                    
                }
            } else {
                print("Error occured")
            }
            
        }
    }
    
    func handleDismissAndChangeButtonTitle(){
        if let chosen = self.specificDayData {
            self.selectedDaysOfTheWeek.append(chosen)
            UserDefaults.standard.set(self.selectedDaysOfTheWeek, forKey: "theOpeningAndClosingTimeWasSet")
            UserDefaults.standard.set(self.selectedOpeningTimeForTheDay, forKey: "theOpeningTimeWasSet")
            UserDefaults.standard.set(self.selectedClosingTimeForTheDay, forKey: "theClosingTimeWasSet")
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func handleAcceptSwitchAction(sender: UISwitch){
        if sender.isOn {
            self.nearClosingTimeTextField.isUserInteractionEnabled = true
            self.nearClosingTimeTextField.becomeFirstResponder()
            self.nearClosingTimeHiddenPlaceHolder.text = "Minutes before"
            
        }else {
            self.nearClosingTimeTextField.isUserInteractionEnabled = false
            self.nearClosingTimeTextField.resignFirstResponder()
            self.nearClosingTimeHiddenPlaceHolder.isHidden = true
            self.nearClosingTimeHiddenPlaceHolder.text = "Minutes before"
        }
    }
    
    @objc func nearClosingTimetextFieldDidChange(){
        print("Called function")
        self.nearClosingTimeHiddenPlaceHolder.isHidden = false
        guard let minut = self.nearClosingTimeTextField.text else {
            return
            }
        
        if let minutess = Int(minut) {
            print("Converted")
            if minutess >= 1 && minutess  <= 60 && self.verifyTimeDifference == true {
                saveButton.isEnabled = true
                saveButton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
                saveButton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
                saveButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
                self.nearClosingTimeHiddenPlaceHolder.text = "Minutes before"
            }else {
                saveButton.isEnabled = false
                saveButton.setTitleColor(UIColor.black, for: .normal)
                saveButton.layer.borderColor = UIColor.black.cgColor
                saveButton.backgroundColor = UIColor.clear
                self.nearClosingTimeHiddenPlaceHolder.text = "Minutes must be greater than 10 less than 60"
            }
        }else {
            saveButton.isEnabled = false
            saveButton.setTitleColor(UIColor.black, for: .normal)
            saveButton.layer.borderColor = UIColor.black.cgColor
            saveButton.backgroundColor = UIColor.clear
            self.nearClosingTimeHiddenPlaceHolder.text = "Minutes must be greater than 10 less than 60"
        }
    }
    
    @objc func handleOpenTimePickerChange(sender: UIDatePicker){
        print("current time")
        self.closingTimePicker.minimumDate = sender.date
        self.getTimeDifference()
    }
    
    @objc func handleClosingTimePickerChange(sender: UIDatePicker){
        print("current time")
        self.openingTimePicker.maximumDate = sender.date
        self.getTimeDifference()
    }
    
    func getTimeDifference(){
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "HH:mm"
        
        
        let calendar = NSCalendar.current
        let openhour = calendar.component(.hour, from: self.openingTimePicker.date as Date)
        let closehour = calendar.component(.hour, from: self.closingTimePicker.date as Date)
        
        print(openhour, closehour, "@#$%A^")
        
        if closehour > openhour {
            print("it worked well")
            self.nearClosingTimeHiddenPlaceHolder.isHidden = false
            self.verifyTimeDifference = true
            let selectedOpeningDate: String = dateFormatter.string(from: self.openingTimePicker.date)
            let selectedClosingDate: String = dateFormatter.string(from: self.closingTimePicker.date)
            
            //get hour minutes and seconds
            let calendarHold = NSCalendar.current
            let selecOpenHour = calendarHold.component(.hour, from: self.openingTimePicker.date as Date)
            let selecOpenMinute = calendarHold.component(.minute, from: self.openingTimePicker.date as Date)
            let selecOpenSecond = calendarHold.component(.second, from: self.openingTimePicker.date as Date)
            
            let selecCloseHour = calendarHold.component(.hour, from: self.closingTimePicker.date as Date)
            let selecCloseMinute = calendarHold.component(.minute, from: self.closingTimePicker.date as Date)
            let selecCloseSecond = calendarHold.component(.second, from: self.closingTimePicker.date as Date)
            
            self.selectedOpeningTimeForTheDay = selectedOpeningDate
            self.selectedOpeningTimeHour = String(selecOpenHour)
            self.selectedOpeningTimeMinute = String(selecOpenMinute)
            self.selectedOpeningTimeSecond = String(selecOpenSecond)
            
            self.selectedClosingTimeForTheDay = selectedClosingDate
            self.selectedClosingTimeHour = String(selecCloseHour)
            self.selectedClosingTimeMinute = String(selecCloseMinute)
            self.selectedClosingTimeSecond = String(selecCloseSecond)
        }else {
            print("it failed roughly")
            self.verifyTimeDifference = false
            
        }
        
    }
    
    @objc func handleLoginRegisterChange(){
        if openCloseSegmentedControl.selectedSegmentIndex == 1 {
            self.openingTimePicker.isUserInteractionEnabled = false
            self.closingTimePicker.isUserInteractionEnabled = false
            self.acceptBookingSwitch.isUserInteractionEnabled = false
            self.collectionView.isUserInteractionEnabled = false
            saveButton.isEnabled = true
            saveButton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
            saveButton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
            saveButton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
            
            self.saveButton.removeTarget(nil, action: nil, for: .allEvents)
            saveButton.addTarget(self, action: #selector(deleteSelectedWorkHourOnSpecificDay), for: .touchUpInside)
            
            self.nearClosingTimeHiddenPlaceHolder.text = "Minutes before"
            self.nearClosingTimeHiddenPlaceHolder.isHidden = true
            
        }else {
            self.openingTimePicker.isUserInteractionEnabled = true
            self.closingTimePicker.isUserInteractionEnabled = true
            self.acceptBookingSwitch.isUserInteractionEnabled = true
            self.collectionView.isUserInteractionEnabled = true
            saveButton.isEnabled = false
            saveButton.setTitleColor(UIColor.black, for: .normal)
            self.saveButton.removeTarget(nil, action: nil, for: .allEvents)
            self.saveButton.addTarget(self, action: #selector(handleSaveTime), for: .touchUpInside)
            saveButton.layer.borderColor = UIColor.black.cgColor
            saveButton.backgroundColor = UIColor.clear
            self.nearClosingTimeHiddenPlaceHolder.text = "Minutes before"
            self.nearClosingTimeHiddenPlaceHolder.isHidden = true
        }
    }
    
    @objc func deleteSelectedWorkHourOnSpecificDay(){
        let ref = Database.database().reference()
        if let uid = Auth.auth().currentUser?.uid, let selectedChosenDay = self.specificDayData {
            let userReference = ref.child("workhours").child(uid).child(selectedChosenDay)
            userReference.removeValue(completionBlock: { (errorrrr, ref) in
                if let error = errorrrr{
                    print(error.localizedDescription)
                    return
                }
                
                UserDefaults.standard.set(selectedChosenDay, forKey: "workHoursDeleted")
                
                print("User successfully deleted")
                self.navigationController?.popViewController(animated: true)
            })
        }
    }

}

class customCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.white
        tniv.contentMode = .scaleAspectFit
        tniv.image = UIImage(named: "check_box_inactive")
        return tniv
    }()
    
    let dayOfTheWeek: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.text = "Hello"
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.black
        return sv
    }()
    
    func setupViews(){
        addSubview(dayOfTheWeek)
        addSubview(thumbnailImageView)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.white
        addContraintsWithFormat(format: "H:|-16-[v0]-16-[v1(20)]-16-|", views: dayOfTheWeek, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: dayOfTheWeek,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
