//
//  CalendarViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import SwiftDate

class CalendarViewController: UIViewController {
    var paymentsArrayHold = [Payments]()
    var customDateHolderNoSum = [CustomDateAmountHolder]()
    var customddatesholder = [CustomDateAmountHolder]()
    var customYearHolder = [CustomYearHolder]()
    var amountPaidIntegerHolder = [Int]()
    var firstLoad = true
    var signedUserRole: String?
    var summedUpBookingArrayHolder = [String]()
    var currencyCode: String?
    
    let upperCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let upperCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customCalendarViewControllerCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdUP")
        return cv
    }()
    
    let upperCollectionViewSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return fnsv
    }()
    
    let lowerCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let lowerCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customCalendarViewControllerLowerCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdDW")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.selectedIndex = 1
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("navigationTitleForCalenderView", comment: "Calendar")
        view.addSubview(upperCollectView)
        upperCollectionView.dataSource = self
        upperCollectionView.delegate = self
        view.addSubview(upperCollectionViewSeperatorView)
        view.addSubview(lowerCollectView)
        lowerCollectionView.dataSource = self
        lowerCollectionView.delegate = self
        setupViewObjectContraints()
        getCorrectRoleData()
        self.firstLoad = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !self.firstLoad {
            self.getCorrectRoleData()
        }
        self.firstLoad = false
    }
    
    func getCorrectRoleData(){
        DispatchQueue.global(qos: .background).async {
            if let uid = Auth.auth().currentUser?.uid {
                let firebaseReferenceCurrentUser = Database.database().reference()
                firebaseReferenceCurrentUser.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshotUserData) in
                    if let dictionaryUser = snapshotUserData.value as? [String: AnyObject] {
                        
                        let BarberShopCOnfirm = HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryUser)
                        let barberConfirm = HandleDataRequest.handleUserBarberNode(firebaseData: dictionaryUser)
                        
                        if BarberShopCOnfirm {
                            let chosenUser = BarberShop()
                            chosenUser.setValuesForKeys(dictionaryUser)
                            if let role = chosenUser.role, let userCurrency = chosenUser.currencyCode {
                                if role == "barberShop" {
                                    self.getThisBarberShopBookingPayments(shopIDD: uid)
                                    self.signedUserRole = "barberShop"
                                    self.currencyCode = userCurrency
                                }
                            }
                        } else if barberConfirm {
                            let barberChosenUser = Barber()
                            barberChosenUser.setValuesForKeys(dictionaryUser)
                            
                            if let barberShopID = barberChosenUser.barberShopID, let roleNow = barberChosenUser.role, roleNow == "barberStaff" {
                                
                                let firebaseRefrenceCurrentUser = Database.database().reference()
                                firebaseRefrenceCurrentUser.child("users").child(barberShopID).observeSingleEvent(of: .value, with: { (snapshotCurrentUser) in
                                    if let dictionaryCurrentUSer = snapshotCurrentUser.value as? [String: AnyObject], HandleDataRequest.handleUserBarberShop(firebaseData: dictionaryCurrentUSer) == true {
                                        let userChosen = BarberShop()
                                        userChosen.setValuesForKeys(dictionaryCurrentUSer)
                                        if let currencyCode = userChosen.currencyCode {
                                            self.currencyCode = currencyCode
                                            self.getThisBarberPayments(shoPIDD: barberShopID, barberUniqueID: uid)
                                            self.signedUserRole = "barberStaff"
                                        }
                                    }
                                })
                                
                            }
                            
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func getThisBarberShopBookingPayments(shopIDD: String){
        DispatchQueue.global(qos: .background).async {
            //self.customddatesholder.removeAll()
            //self.customYearHolder.removeAll()
            //self.customDateHolderNoSum.removeAll()
            
            let firebaseReferenceBookingPayment = Database.database().reference()
            firebaseReferenceBookingPayment.child("bookings").child(shopIDD).observeSingleEvent(of: .value, with: { (snapshootDataBookingPayment) in
                if let dictionaryBookingPayment = snapshootDataBookingPayment.value as? [String: AnyObject] {
                    for bookingPaymentItem in dictionaryBookingPayment {
                        if let bookingItem = bookingPaymentItem.value as? [String: AnyObject] {
                            
                            let bookingConfirm = HandleDataRequest.handleBookingsNode(firebaseData: bookingItem)
                            
                            if bookingConfirm {
                                
                                let singleBooking = Bookings()
                                singleBooking.setValuesForKeys(bookingItem)
                                
                                if let payIDDAX = singleBooking.paymentID, let bookNotifyID = singleBooking.bookingID {
                                    let firebaseReferencePaymentVerification = Database.database().reference()
                                    
                                    if shopIDD != "" && payIDDAX != "" && payIDDAX != "nil" {
                                        firebaseReferencePaymentVerification.child("payments").child(shopIDD).child(payIDDAX).observeSingleEvent(of: .value, with: { (snapshopPaymentVerification) in
                                            if let dictionayRefund = snapshopPaymentVerification.value as? [String: AnyObject] {
                                                
                                                let paymentConfirm = HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund)
                                                
                                                if paymentConfirm {
                                                    print(payIDDAX, "Yikes")
                                                    let paymentHold = Payments()
                                                    paymentHold.setValuesForKeys(dictionayRefund)
                                                    
                                                    if let payConfirm = paymentHold.payment_aq_status_msg, let captureConfirm = paymentHold.capture_aq_status_msg, let checkRefund = paymentHold.refund_aq_status_msg  {
                                                        if payConfirm == "Approved" && captureConfirm == "Approved" && checkRefund == "Approved" {
                                                            if let amountPaid = singleBooking.ConfirmedTotalPrice, let startTime = singleBooking.bookingStartTime, let bookingCalendar = singleBooking.calendar, let bookingTimeZOne = singleBooking.timezone, let bookingLocal = singleBooking.local, let refundAMountString = paymentHold.amountRefunded  {
                                                                
                                                                
                                                                let bookedTimeRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bookingTimeZOne, calenName: bookingCalendar, LocName: bookingLocal)
                                                                //current user region
                                                                let currentCalendar = DateByUserDeviceInitializer.calenderNow
                                                                let currentTimeZone = DateByUserDeviceInitializer.tzone
                                                                let currentLocal = DateByUserDeviceInitializer.localCode
                                                                let currentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: currentTimeZone, calenName: currentCalendar, LocName: currentLocal)
                                                                
                                                                let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal)
                                                                let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal, dateData: startTime)
                                                                let verifyBookingDataCurrent = VerifyDateDetails.checkDateData(timeZone: currentTimeZone, calendar: currentCalendar, locale: currentLocal)
                                                                
                                                                if verifyBookingData == true && verifyBookingStartDate == true && verifyBookingDataCurrent == true {
                                                                    if let bookingStartDate = startTime.toDate(style: .extended, region: bookedTimeRegion), let amountPaidInt = Double(amountPaid), let amountRefundDouble = Double(refundAMountString) {
                                                                        let bookingStartTimePresentRegion = bookingStartDate.convertTo(region: currentRegion)
                                                                        
                                                                        let monthName = bookingStartTimePresentRegion.monthName(.default)
                                                                        let customdateamount = CustomDateAmountHolder()
                                                                        customdateamount.monthName = monthName
                                                                        customdateamount.datePaid = bookingStartTimePresentRegion
                                                                        customdateamount.amount = amountPaidInt - amountRefundDouble
                                                                        customdateamount.bookingUUID = bookNotifyID
                                                                        
                                                                        if let uuiidBook = customdateamount.bookingUUID {
                                                                            if let _ = self.customDateHolderNoSum.first(where: { $0.bookingUUID ==  uuiidBook }) {
                                                                                print("runaway dragon slayer")
                                                                            } else {
                                                                                self.customDateHolderNoSum.append(customdateamount)
                                                                                self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                            }
                                                                        }
                                                                        //self.customDateHolderNoSum.append(customdateamount)
                                                                        //self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                        
                                                                        
                                                                    }
                                                                }
                                                                
                                                            }
                                                        } else if payConfirm == "Approved" && captureConfirm == "Approved" {
                                                            if let amountPaid = singleBooking.ConfirmedTotalPrice, let startTime = singleBooking.bookingStartTime, let bookingCalendar = singleBooking.calendar, let bookingTimeZOne = singleBooking.timezone, let bookingLocal = singleBooking.local  {
                                                                
                                                                let bookedTimeRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bookingTimeZOne, calenName: bookingCalendar, LocName: bookingLocal)
                                                                //current user region
                                                                let currentCalendar = DateByUserDeviceInitializer.calenderNow
                                                                let currentTimeZone = DateByUserDeviceInitializer.tzone
                                                                let currentLocal = DateByUserDeviceInitializer.localCode
                                                                let currentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: currentTimeZone, calenName: currentCalendar, LocName: currentLocal)
                                                                
                                                                
                                                                let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal)
                                                                let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal, dateData: startTime)
                                                                let verifyBookingDataCurrent = VerifyDateDetails.checkDateData(timeZone: currentTimeZone, calendar: currentCalendar, locale: currentLocal)
                                                                
                                                                if verifyBookingData == true && verifyBookingStartDate == true && verifyBookingDataCurrent == true {
                                                                    if let bookingStartDate = startTime.toDate(style: .extended, region: bookedTimeRegion), let amountPaidInt = Double(amountPaid) {
                                                                        let bookingStartTimePresentRegion = bookingStartDate.convertTo(region: currentRegion)
                                                                        
                                                                        let monthName = bookingStartTimePresentRegion.monthName(.default)
                                                                        let customdateamount = CustomDateAmountHolder()
                                                                        customdateamount.monthName = monthName
                                                                        customdateamount.datePaid = bookingStartTimePresentRegion
                                                                        customdateamount.amount = amountPaidInt
                                                                        customdateamount.bookingUUID = bookNotifyID
                                                                        
                                                                        if let uuiidBook = customdateamount.bookingUUID {
                                                                            if let _ = self.customDateHolderNoSum.first(where: { $0.bookingUUID ==  uuiidBook }) {
                                                                                print("runaway dragon slayer without refund")
                                                                            } else {
                                                                                self.customDateHolderNoSum.append(customdateamount)
                                                                                self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                            }
                                                                        }
                                                                        
                                                                        //self.customDateHolderNoSum.append(customdateamount)
                                                                        //self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                        
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                }
                                                
                                            }//here
                                        }, withCancel: nil)
                                    }
                                }
                            }
                        }//here
                    }
                }
            }, withCancel: nil)
        }
    }
    
    
    
    func getThisBarberPayments(shoPIDD: String, barberUniqueID: String){
        DispatchQueue.global(qos: .background).async {
            //self.customddatesholder.removeAll()
            //self.customYearHolder.removeAll()
            //self.customDateHolderNoSum.removeAll()
            
            let firebaseReferenceBookingPaymentBarbers = Database.database().reference()
            firebaseReferenceBookingPaymentBarbers.child("bookings").child(shoPIDD).observeSingleEvent(of: .value, with: { (snapshootDataBookingPayment) in
                if let dictionaryBookingPayment = snapshootDataBookingPayment.value as? [String: AnyObject] {
                    for bookingPaymentItem in dictionaryBookingPayment {
                        if let bookingItem = bookingPaymentItem.value as? [String: AnyObject] {
                            
                            let bookingConfirm = HandleDataRequest.handleBookingsNode(firebaseData: bookingItem)
                            
                            if bookingConfirm {
                                let singleBooking = Bookings()
                                singleBooking.setValuesForKeys(bookingItem)
                                
                                
                                if let payIDDAX = singleBooking.paymentID, let barberIdentifier = singleBooking.bookedBarberID, let bookNotifyID = singleBooking.bookingID {
                                    
                                    if barberIdentifier == barberUniqueID {
                                        
                                        if shoPIDD != "" && payIDDAX != "" && payIDDAX != "nil" {
                                            let firebaseReferencePaymentVerificationBarbers = Database.database().reference()
                                            firebaseReferencePaymentVerificationBarbers.child("payments").child(shoPIDD).child(payIDDAX).observeSingleEvent(of: .value, with: { (snapshopPaymentVerification) in
                                                if let dictionayRefund = snapshopPaymentVerification.value as? [String: AnyObject] {
                                                    
                                                    let payconfirm = HandleDataRequest.handlePaymentsNode(firebaseData: dictionayRefund)
                                                    
                                                    if payconfirm {
                                                        let paymentHold = Payments()
                                                        paymentHold.setValuesForKeys(dictionayRefund)
                                                        
                                                        if let payConfirm = paymentHold.payment_aq_status_msg, let captureConfirm = paymentHold.capture_aq_status_msg, let checkRefund = paymentHold.refund_aq_status_msg  {
                                                            if payConfirm == "Approved" && captureConfirm == "Approved" && checkRefund == "Approved"  {
                                                                if let amountPaid = singleBooking.ConfirmedTotalPrice, let startTime = singleBooking.bookingStartTime, let bookingCalendar = singleBooking.calendar, let bookingTimeZOne = singleBooking.timezone, let bookingLocal = singleBooking.local, let barberIDX = singleBooking.bookedBarberID , let userUUID = Auth.auth().currentUser?.uid, let refundAMountString = paymentHold.amountRefunded  {
                                                                    
                                                                    if barberIDX == userUUID {
                                                                        let bookedTimeRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bookingTimeZOne, calenName: bookingCalendar, LocName: bookingLocal)
                                                                        //current user region
                                                                        let currentCalendar = DateByUserDeviceInitializer.calenderNow
                                                                        let currentTimeZone = DateByUserDeviceInitializer.tzone
                                                                        let currentLocal = DateByUserDeviceInitializer.localCode
                                                                        let currentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: currentTimeZone, calenName: currentCalendar, LocName: currentLocal)
                                                                        
                                                                        let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal)
                                                                        let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal, dateData: startTime)
                                                                        let verifyBookingDataCurrent = VerifyDateDetails.checkDateData(timeZone: currentTimeZone, calendar: currentCalendar, locale: currentLocal)
                                                                        
                                                                        if verifyBookingData == true && verifyBookingStartDate == true && verifyBookingDataCurrent == true {
                                                                            if let bookingStartDate = startTime.toDate(style: .extended, region: bookedTimeRegion), let amountPaidInt = Double(amountPaid), let amountRefundDouble = Double(refundAMountString)  {
                                                                                let bookingStartTimePresentRegion = bookingStartDate.convertTo(region: currentRegion)
                                                                                
                                                                                let monthName = bookingStartTimePresentRegion.monthName(.default)
                                                                                let customdateamount = CustomDateAmountHolder()
                                                                                customdateamount.monthName = monthName
                                                                                customdateamount.datePaid = bookingStartTimePresentRegion
                                                                                customdateamount.amount = amountPaidInt - amountRefundDouble
                                                                                print(amountPaidInt, amountRefundDouble, "Hell there")
                                                                                customdateamount.bookingUUID = bookNotifyID
                                                                                
                                                                                if let uuiidBook = customdateamount.bookingUUID {
                                                                                    if let _ = self.customDateHolderNoSum.first(where: { $0.bookingUUID ==  uuiidBook }) {
                                                                                        print("runaway dragon slayer without refund")
                                                                                    } else {
                                                                                        self.customDateHolderNoSum.append(customdateamount)
                                                                                        self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                                    }
                                                                                }
                                                                                
                                                                                //self.customDateHolderNoSum.append(customdateamount)
                                                                                //self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                            } else if payConfirm == "Approved" && captureConfirm == "Approved" {
                                                                // here
                                                                if let amountPaid = singleBooking.ConfirmedTotalPrice, let startTime = singleBooking.bookingStartTime, let bookingCalendar = singleBooking.calendar, let bookingTimeZOne = singleBooking.timezone, let bookingLocal = singleBooking.local, let barberIDX = singleBooking.bookedBarberID , let userUUID = Auth.auth().currentUser?.uid {
                                                                    
                                                                    if barberIDX == userUUID {
                                                                        let bookedTimeRegion = DateByUserDeviceInitializer.getRegion(TZoneName: bookingTimeZOne, calenName: bookingCalendar, LocName: bookingLocal)
                                                                        //current user region
                                                                        let currentCalendar = DateByUserDeviceInitializer.calenderNow
                                                                        let currentTimeZone = DateByUserDeviceInitializer.tzone
                                                                        let currentLocal = DateByUserDeviceInitializer.localCode
                                                                        let currentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: currentTimeZone, calenName: currentCalendar, LocName: currentLocal)
                                                                        
                                                                        
                                                                        let verifyBookingData = VerifyDateDetails.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal)
                                                                        let verifyBookingStartDate = VerifyDateAssociation.checkDateData(timeZone: bookingTimeZOne, calendar: bookingCalendar, locale: bookingLocal, dateData: startTime)
                                                                        let verifyBookingDataCurrent = VerifyDateDetails.checkDateData(timeZone: currentTimeZone, calendar: currentCalendar, locale: currentLocal)
                                                                        
                                                                        if verifyBookingData == true && verifyBookingStartDate == true && verifyBookingDataCurrent == true {
                                                                            if let bookingStartDate = startTime.toDate(style: .extended, region: bookedTimeRegion), let amountPaidInt = Double(amountPaid) {
                                                                                let bookingStartTimePresentRegion = bookingStartDate.convertTo(region: currentRegion)
                                                                                
                                                                                let monthName = bookingStartTimePresentRegion.monthName(.default)
                                                                                let customdateamount = CustomDateAmountHolder()
                                                                                customdateamount.monthName = monthName
                                                                                customdateamount.datePaid = bookingStartTimePresentRegion
                                                                                customdateamount.amount = amountPaidInt
                                                                                customdateamount.bookingUUID = bookNotifyID
                                                                                
                                                                                if let uuiidBook = customdateamount.bookingUUID {
                                                                                    if let _ = self.customDateHolderNoSum.first(where: { $0.bookingUUID ==  uuiidBook }) {
                                                                                        print("runaway dragon slayer without refund")
                                                                                    } else {
                                                                                        self.customDateHolderNoSum.append(customdateamount)
                                                                                        self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                                    }
                                                                                }
                                                                                
                                                                                //self.customDateHolderNoSum.append(customdateamount)
                                                                                //self.getYearsForPayments(dateAmountYear: customdateamount)
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }, withCancel: nil)
                                        }
                                    }
                                }
                            }
                            
                        } //here
                    }
                }
            }, withCancel: nil)
        }
    }
    
    func getYearsForPayments(dateAmountYear: CustomDateAmountHolder){
        if self.customYearHolder.isEmpty {
            if let yearINT = dateAmountYear.datePaid?.year {
                let yearString = String(describing: yearINT)
                let yearDate = CustomYearHolder()
                yearDate.yearIntValue = yearINT
                yearDate.yearStringValue = yearString
                if let yearValue = yearDate.yearIntValue {
                    if let _ = self.customYearHolder.first(where: { $0.yearIntValue ==  yearValue }) {
                        print("year append already exist")
                    } else {
                        self.customYearHolder.append(yearDate)
                    }
                }
                //self.customYearHolder.append(yearDate)
                
                self.customYearHolder.sort(by: { (appstx, appsty) -> Bool in
                    if let mannerx = appstx.yearIntValue,let mannery = appsty.yearIntValue {
                        return mannerx > mannery
                    } else {
                        return false
                    }
                })
                
                DispatchQueue.main.async {
                    self.upperCollectionView.reloadData()
                }
                
                if let firstItem = self.customYearHolder.first {
                    if let firstItemInt = firstItem.yearIntValue {
                        if let firstYearInitial = UserDefaults.standard.object(forKey: "initialYearValueToCalculateAccount") as? Int {
                            UserDefaults.standard.set(firstYearInitial, forKey: "initialYearValueToCalculateAccount")
                            self.dateAmountSummation(yearChoice: firstYearInitial, dateBooked: dateAmountYear)
                        } else {
                            UserDefaults.standard.set(firstItemInt, forKey: "initialYearValueToCalculateAccount")
                            self.dateAmountSummation(yearChoice: firstItemInt, dateBooked: dateAmountYear)
                        }
                    }
                }
            }
        } else {
            if let yearNowInt = dateAmountYear.datePaid?.year {
                let yearNowString = String(describing: yearNowInt)
                if let firstNegative = self.customYearHolder.first(where: { $0.yearStringValue ==  yearNowString }) { //self.customYearHolder.contains(yearNowString)
                    if let firstItemInt = firstNegative.yearIntValue, let firstYearInitial = UserDefaults.standard.object(forKey: "initialYearValueToCalculateAccount") as? Int {
                        if firstItemInt == firstYearInitial {
                            self.dateAmountSummation(yearChoice: firstItemInt, dateBooked: dateAmountYear)
                            self.customYearHolder.sort(by: { (appstx, appsty) -> Bool in
                                if let mannerx = appstx.yearIntValue,let mannery = appsty.yearIntValue {
                                    return mannerx > mannery
                                } else {
                                    return false
                                }
                            })
                            
                            DispatchQueue.main.async {
                                self.upperCollectionView.reloadData()
                            }
                        }
                    }
                } else {
                    
                    let yearDateXX = CustomYearHolder()
                    yearDateXX.yearIntValue = yearNowInt
                    yearDateXX.yearStringValue = yearNowString
                    //self.customYearHolder.append(yearDateXX)
                    
                    if let yearValue = yearDateXX.yearIntValue {
                        if let _ = self.customYearHolder.first(where: { $0.yearIntValue ==  yearValue }) {
                            print("year append already exist")
                        } else {
                            self.customYearHolder.append(yearDateXX)
                        }
                    }
                    
                    self.customYearHolder.sort(by: { (appstx, appsty) -> Bool in
                        if let mannerx = appstx.yearIntValue,let mannery = appsty.yearIntValue {
                            return mannerx > mannery
                        } else {
                            return false
                        }
                    })
                    
                    DispatchQueue.main.async {
                        self.upperCollectionView.reloadData()
                    }
//                    if let firstItemInt = Int(yearNowString){
//                        self.dateAmountSummation(yearChoice: firstItemInt, dateBooked: dateAmountYear)
//                    }
                }
            }
        }
    }
    
    func dateAmountSummation(yearChoice: Int, dateBooked: CustomDateAmountHolder){
        if let month = dateBooked.monthName, let amount = dateBooked.amount, let yearIncoming = dateBooked.datePaid?.year, let boodidee = dateBooked.bookingUUID {
            if yearIncoming == yearChoice {
                if customddatesholder.isEmpty {
                    if let _ = self.customddatesholder.first(where: { $0.monthName ==  month }) {
                        print("year append already exist")
                    } else {
                        self.customddatesholder.append(dateBooked)
                    }
                    
                    if let _ = self.summedUpBookingArrayHolder.first(where: { $0 ==  boodidee }) {
                        print("year append already exist")
                    } else {
                        self.summedUpBookingArrayHolder.append(boodidee)
                    }
                    
                    //sort data
                    self.customddatesholder.sort(by: { (appstx, appsty) -> Bool in
                        if let mannerx = appstx.datePaid,let mannery = appsty.datePaid {
                            return mannerx.isAfterDate(mannery, granularity: .month) // < mannery
                        } else {
                            return false
                        }
                    })
                    
                    DispatchQueue.main.async {
                        self.lowerCollectionView.reloadData()
                    }
                } else {
                    if let firstNegative = self.customddatesholder.first(where: { $0.monthName ==  month }) {
                        
                        if self.summedUpBookingArrayHolder.first(where: { $0 ==  boodidee }) == nil {
                            firstNegative.amount = firstNegative.amount.map { $0 + amount }
                            
                            if let _ = self.summedUpBookingArrayHolder.first(where: { $0 ==  boodidee }) {
                                print("year append already exist")
                            } else {
                                self.summedUpBookingArrayHolder.append(boodidee)
                            }
                            
                            //sort data
                            self.customddatesholder.sort(by: { (appstx, appsty) -> Bool in
                                if let mannerx = appstx.datePaid,let mannery = appsty.datePaid {
                                    return mannerx.isAfterDate(mannery, granularity: .month) // < mannery
                                } else {
                                    return false
                                }
                            })
                            
                            DispatchQueue.main.async {
                                self.lowerCollectionView.reloadData()
                            }
                        }
                        
                    } else {
                        if let _ = self.customddatesholder.first(where: { $0.monthName ==  month }) {
                            print("yeardate append already exist")
                        } else {
                            self.customddatesholder.append(dateBooked)
                        }
                        
                        if let _ = self.summedUpBookingArrayHolder.first(where: { $0 ==  boodidee }) {
                            print("yeardate append already exist")
                        } else {
                            self.summedUpBookingArrayHolder.append(boodidee)
                        }
                        
                        //sort data
                        self.customddatesholder.sort(by: { (appstx, appsty) -> Bool in
                            if let mannerx = appstx.datePaid,let mannery = appsty.datePaid {
                                return mannerx.isAfterDate(mannery, granularity: .month) // < mannery
                            } else {
                                return false
                            }
                        })
                        
                        DispatchQueue.main.async {
                            self.lowerCollectionView.reloadData()
                        }
                        
                    }
                }
            }
        }
    }
    
    func handleUpperCollectionSelction(yearChoice: Int){
        UserDefaults.standard.set(yearChoice, forKey: "initialYearValueToCalculateAccount")
        self.customddatesholder.removeAll()
        self.customDateHolderNoSum.removeAll()
        self.summedUpBookingArrayHolder.removeAll()
        DispatchQueue.main.async {
            self.lowerCollectionView.reloadData()
        }
        self.getCorrectRoleData()
    }
    
    func handleLowerCollectionViewSelection(monthSelected: String){
        if let userRole = self.signedUserRole, let curr =  self.currencyCode {
            let monthView = MonthlyPaymentViewController()
            monthView.monthChosen = monthSelected
            monthView.userRole = userRole
            monthView.currentUserCurency = curr
            navigationController?.pushViewController(monthView, animated: true)
        }
    }
    
    func setupViewObjectContraints(){
        upperCollectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        upperCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        upperCollectView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        upperCollectView.addSubview(upperCollectionView)
        
        upperCollectionView.topAnchor.constraint(equalTo: upperCollectView.topAnchor).isActive = true
        upperCollectionView.centerXAnchor.constraint(equalTo: upperCollectView.centerXAnchor).isActive = true
        upperCollectionView.widthAnchor.constraint(equalTo: upperCollectView.widthAnchor).isActive = true
        upperCollectionView.heightAnchor.constraint(equalTo: upperCollectView.heightAnchor).isActive = true
        
        
        upperCollectionViewSeperatorView.topAnchor.constraint(equalTo: upperCollectView.bottomAnchor).isActive = true
        upperCollectionViewSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperCollectionViewSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        upperCollectionViewSeperatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        lowerCollectView.topAnchor.constraint(equalTo: upperCollectionViewSeperatorView.bottomAnchor).isActive = true
        lowerCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lowerCollectView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        lowerCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        lowerCollectView.addSubview(lowerCollectionView)
        
        lowerCollectionView.topAnchor.constraint(equalTo: lowerCollectView.topAnchor).isActive = true
        lowerCollectionView.centerXAnchor.constraint(equalTo: lowerCollectView.centerXAnchor).isActive = true
        lowerCollectionView.widthAnchor.constraint(equalTo: lowerCollectView.widthAnchor).isActive = true
        lowerCollectionView.heightAnchor.constraint(equalTo: lowerCollectView.heightAnchor).isActive = true
        
    }

}

class customCalendarViewControllerCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let openStartTimeHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.lightGray //UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    func setupViews(){
        addSubview(openStartTimeHolder)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|[v0]|", views: openStartTimeHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-|", views: openStartTimeHolder)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customCalendarViewControllerLowerCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let monthOfTheYear: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "BebasNeue", size: 30)
        ehp.textColor = UIColor(r: 11, g: 49, b: 68)
        ehp.textAlignment = .left
        return ehp
    }()
    
    let totalEarningsForTheMonth: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        ehp.textColor = UIColor(r: 11, g: 49, b: 68)
        ehp.textAlignment = .right
        return ehp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    func setupViews(){
        addSubview(monthOfTheYear)
        addSubview(totalEarningsForTheMonth)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|-16-[v0(125)]-5-[v1(150)]-16-|", views: monthOfTheYear,totalEarningsForTheMonth)
        addContraintsWithFormat(format: "V:|-16-[v0]-16-[v1(1)]|", views: monthOfTheYear,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0]-16-[v1(0.5)]|", views: totalEarningsForTheMonth,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

