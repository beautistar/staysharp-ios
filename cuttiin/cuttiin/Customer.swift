//
//  Customer.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 18/09/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Customer: NSObject {
    
    var calendar: String?
    var cardDateCalendar: String?
    var cardDateCreated: String?
    var cardDateLocale: String?
    var cardDateTimeZone: String?
    var cardHasBeenDeleted: String?
    var cardID: String?
    var dateAccountCreated: String?
    var email: String?
    var firstName: String?
    var lastName: String?
    var local: String?
    var mobileNumber: String?
    var profileImageName: String?
    var profileImageUrl: String?
    var role: String?
    var timezone: String?
    var uniqueID: String?
    var isCompleted: String?
    
}
