//
//  Barber.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 19/09/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

@objcMembers class Barber: NSObject {
    
    var barberShopID: String?
    var calendar: String?
    var dateAccountCreated: String?
    var email: String?
    var emailSent: String?
    var firstName: String?
    var isDeleted: String?
    var lastName: String?
    var local: String?
    var mobileNumber: String?
    var profileImageName: String?
    var profileImageUrl: String?
    var rating: String?
    var role: String?
    var timezone: String?
    var uniqueID: String?
    var profileImage: UIImage?
    var ratingValue: CGFloat?
    
}
