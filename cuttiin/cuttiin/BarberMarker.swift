//
//  BarberMarker.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/28/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class BarberMarker: NSObject {
    var companyID: String?
    var companyName: String?
    var companyLogoImageUrl: String?
    
    
    var companyLogoImage: UIImage?
    
    var companyFormattedAddress: String?
    var companyAddressLongitude: String?
    var companyAddressLatitude: String?
    
    
    var distanceFromCurrentUser: Double?
    
    
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
    
    
    var ratingValue: CGFloat?
}
