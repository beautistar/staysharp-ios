//
//  IntermediaryAvailableTimes.swift
//  cuttiin
//
//  Created by Umoru Joseph on 11/19/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

@objcMembers class IntermediaryAvailableTimes: NSObject {
    var bookingUniqueIDFirst: String?
    var bookingEndTimeFirst: DateInRegion?
}
